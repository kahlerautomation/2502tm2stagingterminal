﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KahlerAutomation;

namespace Tm2StagingTerminal 
{
    public partial class Message : Form {
        public Message(string title, string message) {
            InitializeComponent();
            Appearances.ScaleFonts(this, new ArrayList());
            Text = title;
            lblMessage.Text = message;
        }

        private void btnOk_Click(object sender, EventArgs e) {
            try {
                Close();
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        static public void ShowMessage(string title, string message) {
            Message f = new Message(title, message);
            f.ShowDialog();
        }
    }
}
