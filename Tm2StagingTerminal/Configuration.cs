﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;
using KahlerAutomation.KaTm2Database;
using KahlerAutomation.KaTm2LoadFramework;
using System.Drawing.Printing;
using KaCommonObjects;
using System.Data.OleDb;

namespace Tm2StagingTerminal {
    public partial class Configuration : Form {
        public Configuration() {
            InitializeComponent();
            Appearances.ScaleFonts(this, new ArrayList());
        }

        #region  general option indices
        private int ALLOW_DRIVERS_TO_ADD_DESTINATIONS_INDEX;
        private int USE_ALPHANUMERIC_KEYBOARD_FOR_DRIVERS_INDEX;
        private int USE_ALPHANUMERIC_KEYBOARD_FOR_TRANSPORTS_INDEX;
        private int USE_ALPHANUMERIC_KEYBOARD_FOR_CARRIERS_INDEX;
        private int USE_ALPHANUMERIC_KEYBOARD_FOR_LOAD_NUMBERS_INDEX;
        private int SHOW_OK_BUTTON_FOR_SELECT_FROM_LIST_INDEX;
        private int INCLUDE_RELEASE_NUMBER_IN_ORDER_NUMBER_LOOKUP_INDEX;
        private int SHOW_ORDER_DETAIL_SCREEN_INDEX;
        private int INCLUDE_PURCHASE_ORDER_NUMBER_IN_ORDER_NUMBER_LOOKUP_INDEX;
        private int ORDER_NUMBER_MUST_MATCH_EXACT_ORDER_NUMBER_LOOKUP_INDEX;
        private int SHOW_DONT_CARE_IN_DESTINATION_LIST_INDEX;
        private int SHOW_FINAL_CONFIRMATION_SCREEN_INDEX;
        private int AUTOMATICALLY_ADD_COMPARTMENT_FOR_TRANSPORT;

        // Inbound specific
        private int INBOUND_ALLOW_MULTIPLE_STAGED_ORDERS_INDEX;
        private int INBOUND_PROMPT_FOR_DRIVER_NUMBER_INDEX;
        private int INBOUND_VALIDATE_DRIVER_NUMBER_INDEX;
        private int INBOUND_PROMPT_FOR_TRANSPORT_NUMBER_INDEX;
        private int INBOUND_VALIDATE_TRANSPORT_NUMBER_INDEX;
        private int INBOUND_PROMPT_FOR_CARRIER_NUMBER_INDEX;
        private int INBOUND_VALIDATE_CARRIER_NUMBER_INDEX;
        private int INBOUND_SHOW_ORDER_LIST_INDEX;
        private int ENFORE_ORDER_LOCKING_INDEX;

        // Outbound specific
        private int OUTBOUND_PROMPT_FOR_DRIVER_NUMBER_INDEX;
        private int OUTBOUND_VALIDATE_DRIVER_NUMBER_INDEX;
        private int OUTBOUND_PROMPT_FOR_TRANSPORT_NUMBER_INDEX;
        private int OUTBOUND_VALIDATE_TRANSPORT_NUMBER_INDEX;
        private int OUTBOUND_PROMPT_FOR_CARRIER_NUMBER_INDEX;
        private int OUTBOUND_VALIDATE_CARRIER_NUMBER_INDEX;
        private int OUTBOUND_SHOW_ORDER_LIST_INDEX;
        private int MOVE_DRIVER_TO_HISTORY_INDEX;
        private int MOVE_TRANSPORT_TO_HISTORY_INDEX;
        private int EMAIL_TICKETS_INDEX;
        private int DO_NOT_CREATE_TICKET_FOR_LOAD_WITH_NOTHING_DISPENSED;
        #endregion

        #region Events
        private void Configuration_Load(object sender, EventArgs e) {
            try {
                Version v = Assembly.GetExecutingAssembly().GetName().Version;
                lblTm2StagingTerminalversion.Text += String.Format("{0:0}.{1:0}.{2:0}", v.Major, v.Minor, v.Build);
                if (v.Revision > 0) lblTm2StagingTerminalversion.Text += String.Format(" X{0:0}", v.Revision);
                v = Assembly.GetAssembly(typeof(KaOrder)).GetName().Version;
                lblTm2DatabaseVersion.Text += String.Format(" {0:0}.{1:0}.{2:0} D{3:0}", v.Major, v.Minor, v.Build, v.Revision);

                PopulateUsers();

                tbxNumericScalar.Text = Database.Settings.NumericKeyboardScalar.ToString();
                tbxAlphaNumericScalar.Text = Database.Settings.AlphaNumericKeyboardScalar.ToString();

                nudMaxOnScreenTime.Value = Database.Settings.MaxOnScreenTime;
                nudDefaultMaxWeightForNewTransports.Value = (decimal)Database.Settings.DefaultMaxWeightForNewTransports;
                PopulateUnits();

                PopulateOptions();
                PopulateOwners();
                PopulatePanelDropDownLists();
                PopulatePrinters();
                PopulateBaysList();
                chkBayDirectionEnabled.Checked = Database.Settings.BayDirectionEnabled;

                tbxInboundBaggedProductTicketWebAddress.Text = Database.Settings.BaggedProductPickTicketAddress;
                tbxInboundBulkProductTicketWebAddress.Text = Database.Settings.WebPickTicketAddress;
                tbxReceivingProductPickTicketWebAddress.Text = Database.Settings.WebPoPickTicketAddress;
                tbxInboundTruckInspectionWebAddress.Text = Database.Settings.TruckInspectionTicketAddress;

                // Order complete wording
                tbxOrderCompleteInboundOrderBayNotAssignedNoTickets.Text = Database.Settings.OrderCompleteInboundOrderBayNotAssignedNoTickets;
                tbxOrderCompleteInboundOrderBayNotAssignedWithTickets.Text = Database.Settings.OrderCompleteInboundOrderBayNotAssignedHasTickets;
                tbxOrderCompleteInboundOrderBayAssignedNoTickets.Text = Database.Settings.OrderCompleteInboundOrderBayAssignedNoTickets;
                tbxOrderCompleteInboundOrderBayAssignedWithTickets.Text = Database.Settings.OrderCompleteInboundOrderBayAssignedHasTickets;
                tbxOrderCompleteInboundReceivingBayNotAssignedNoTickets.Text = Database.Settings.OrderCompleteInboundReceivingBayNotAssignedNoTickets;
                tbxOrderCompleteInboundReceivingBayNotAssignedHasTickets.Text = Database.Settings.OrderCompleteInboundReceivingBayNotAssignedHasTickets;
                tbxOrderCompleteInboundReceivingBayAssignedNoTickets.Text = Database.Settings.OrderCompleteInboundReceivingBayAssignedNoTickets;
                tbxOrderCompleteInboundReceivingBayAssignedHasTickets.Text = Database.Settings.OrderCompleteInboundReceivingBayAssignedHasTickets;
                tbxOrderCompleteOutboundNoTickets.Text = Database.Settings.OrderCompleteOutboundNoTickets;
                tbxOrderCompleteOutboundHasTickets.Text = Database.Settings.OrderCompleteOutboundHasTickets;

            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        private void CheckListBoxOptionsItemCheck(object sender, ItemCheckEventArgs e) {
            try {
                if (e.NewValue == CheckState.Checked) { // resolve incompatible options

                }
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e) {
            try {
                if (ValidateSettings() && SaveSettings()) {
                    this.DialogResult = DialogResult.OK;
                    Close();
                }
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        private void btnExit_Click(object sender, EventArgs e) {
            //Clear any order locks if anything was locked
            this.DialogResult = DialogResult.OK;
            Application.Exit();
        }

        private void Configuration_HelpButtonClicked(object sender, CancelEventArgs e) {
            string bookmark = "configuration";
            Manual manual = new Manual(bookmark);
            manual.Show();
            e.Cancel = true;
        }
        #endregion

        private bool ValidateSettings() {
            bool retval = true;
            TabPage errorTab = tpGeneral;
            double valueParsed = 0;
            if (!Double.TryParse(tbxNumericScalar.Text.Trim(), out valueParsed)) {
                MessageBox.Show("Numeric Keyboard Scalar must be a valid number.");
                retval = false;
            } else if (valueParsed < 50) {
                MessageBox.Show("Numeric Keyboard Scalar value may not be less than 50%.");
                retval = false;
            } else if (!Double.TryParse(tbxAlphaNumericScalar.Text.Trim(), out valueParsed)) {
                MessageBox.Show("Alpha Numeric Keyboard Scalar must be a valid number.");
                retval = false;
            } else if (valueParsed < 50) {
                MessageBox.Show("Alpha Numeric Keyboard Scalar value may not be less than 50%.");
                retval = false;
            } else if (((KaUser)cmbUser.SelectedItem).Id == Guid.Empty) {
                lblUserError.Visible = true;
                MessageBox.Show("The user must be defined.");
                retval = false;
            } else if (cmbPanel.SelectedIndex < 1) {
                MessageBox.Show("Panel for the scale must be assigned.");
                retval = false;
            } else if (cmbDefaultMassUnit.SelectedIndex < 1) {
                MessageBox.Show("Default mass unit must be defined.");
                retval = false;
            } else if (cmbDefaultVolumeUnit.SelectedIndex < 1) {
                MessageBox.Show("Default volume unit must be defined.");
                retval = false;
            }

            if (!retval) tabControl.SelectedTab = errorTab;

            return retval;
        }

        private bool SaveSettings() {
            OleDbConnection connection = Tm2Database.Connection;
            try {
                Database.UserId = ((KaUser)cmbUser.SelectedItem).Id;

                Database.Settings.NumericKeyboardScalar = double.Parse(tbxNumericScalar.Text);
                Database.Settings.AlphaNumericKeyboardScalar = double.Parse(tbxAlphaNumericScalar.Text);

                Database.Settings.MaxOnScreenTime = Convert.ToInt32(nudMaxOnScreenTime.Value);
                Database.Settings.DefaultMaxWeightForNewTransports = (double)nudDefaultMaxWeightForNewTransports.Value;
                LfDatabase.DefaultMassUnitId = ((KaUnit)cmbDefaultMassUnit.SelectedItem).Id;
                LfDatabase.DefaultVolumeUnitId = ((KaUnit)cmbDefaultVolumeUnit.SelectedItem).Id;

                //Options
                Database.Settings.AllowDriversToAddDestinations = clbOptions.CheckedIndices.IndexOf(ALLOW_DRIVERS_TO_ADD_DESTINATIONS_INDEX) != -1;
                Database.Settings.ShowDontCareInDestinationList = clbOptions.CheckedIndices.IndexOf(SHOW_DONT_CARE_IN_DESTINATION_LIST_INDEX) != -1;
                Database.Settings.ShowOrderDetailScreen = clbOptions.CheckedIndices.IndexOf(SHOW_ORDER_DETAIL_SCREEN_INDEX) != -1;
                Database.Settings.ShowFinalConfirmationScreen = clbOptions.CheckedIndices.IndexOf(SHOW_FINAL_CONFIRMATION_SCREEN_INDEX) != -1;
                Database.Settings.ShowOkButtonForSelectFromList = clbOptions.CheckedIndices.IndexOf(SHOW_OK_BUTTON_FOR_SELECT_FROM_LIST_INDEX) != -1;
                Database.Settings.UseAlaphaNumKeyboardForCarriers = clbOptions.CheckedIndices.IndexOf(USE_ALPHANUMERIC_KEYBOARD_FOR_CARRIERS_INDEX) != -1;
                Database.Settings.UseAlaphaNumKeyboardForDrivers = clbOptions.CheckedIndices.IndexOf(USE_ALPHANUMERIC_KEYBOARD_FOR_DRIVERS_INDEX) != -1;
                Database.Settings.UseAlaphaNumKeyboardForLoadNumbers = clbOptions.CheckedIndices.IndexOf(USE_ALPHANUMERIC_KEYBOARD_FOR_LOAD_NUMBERS_INDEX) != -1;
                Database.Settings.UseAlaphaNumKeyboardForTransports = clbOptions.CheckedIndices.IndexOf(USE_ALPHANUMERIC_KEYBOARD_FOR_TRANSPORTS_INDEX) != -1;
                Database.Settings.IncludeOrderReleaseNumberInLookupForValidOrders = clbOptions.CheckedIndices.IndexOf(INCLUDE_RELEASE_NUMBER_IN_ORDER_NUMBER_LOOKUP_INDEX) != -1;
                Database.Settings.IncludeOrderPurchaseOrderNumberInLookupForValidOrders = clbOptions.CheckedIndices.IndexOf(INCLUDE_PURCHASE_ORDER_NUMBER_IN_ORDER_NUMBER_LOOKUP_INDEX) != -1;
                Database.Settings.IncludeOrderNumbersThatHaveDashesInTheNumberInLookupForValidOrders = clbOptions.CheckedIndices.IndexOf(ORDER_NUMBER_MUST_MATCH_EXACT_ORDER_NUMBER_LOOKUP_INDEX) != -1;
                Database.Settings.AutomaticallyAddCompartmentForTransport = clbOptions.CheckedIndices.IndexOf(AUTOMATICALLY_ADD_COMPARTMENT_FOR_TRANSPORT) != -1;

                //Inbound specific
                Database.Settings.Inbound.Enabled = cbxInboundOptionsEnabled.Checked;
                Database.Settings.EnforceOrderLocking = clbInboundOptions.CheckedIndices.IndexOf(ENFORE_ORDER_LOCKING_INDEX) != -1;
                Database.Settings.Inbound.AllowOrderListLookup = clbInboundOptions.CheckedIndices.IndexOf(INBOUND_SHOW_ORDER_LIST_INDEX) != -1;
                Database.Settings.Inbound.AllowMultipleStagedOrders = clbInboundOptions.CheckedIndices.IndexOf(INBOUND_ALLOW_MULTIPLE_STAGED_ORDERS_INDEX) != -1;
                Database.Settings.Inbound.PromptForCarrierNumber = clbInboundOptions.CheckedIndices.IndexOf(INBOUND_PROMPT_FOR_CARRIER_NUMBER_INDEX) != -1;
                Database.Settings.Inbound.PromptForDriverNumber = clbInboundOptions.CheckedIndices.IndexOf(INBOUND_PROMPT_FOR_DRIVER_NUMBER_INDEX) != -1;
                Database.Settings.Inbound.PromptForTransportNumber = clbInboundOptions.CheckedIndices.IndexOf(INBOUND_PROMPT_FOR_TRANSPORT_NUMBER_INDEX) != -1;
                Database.Settings.Inbound.ValidateCarrierNumber = clbInboundOptions.CheckedIndices.IndexOf(INBOUND_VALIDATE_CARRIER_NUMBER_INDEX) != -1;
                Database.Settings.Inbound.ValidateDriverNumber = clbInboundOptions.CheckedIndices.IndexOf(INBOUND_VALIDATE_DRIVER_NUMBER_INDEX) != -1;
                Database.Settings.Inbound.ValidateTransportNumber = clbInboundOptions.CheckedIndices.IndexOf(INBOUND_VALIDATE_TRANSPORT_NUMBER_INDEX) != -1;

                //Outbound specific
                Database.Settings.Outbound.Enabled = cbxOutboundOptionsEnabled.Checked;
                Database.Settings.MoveDriversToHistory = clbOutboundOptions.CheckedIndices.IndexOf(MOVE_DRIVER_TO_HISTORY_INDEX) != -1;
                Database.Settings.MoveTransportsToHistory = clbOutboundOptions.CheckedIndices.IndexOf(MOVE_TRANSPORT_TO_HISTORY_INDEX) != -1;
                Database.Settings.Outbound.AllowOrderListLookup = clbOutboundOptions.CheckedIndices.IndexOf(OUTBOUND_SHOW_ORDER_LIST_INDEX) != -1;
                Database.Settings.Outbound.PromptForCarrierNumber = clbOutboundOptions.CheckedIndices.IndexOf(OUTBOUND_PROMPT_FOR_CARRIER_NUMBER_INDEX) != -1;
                Database.Settings.Outbound.PromptForDriverNumber = clbOutboundOptions.CheckedIndices.IndexOf(OUTBOUND_PROMPT_FOR_DRIVER_NUMBER_INDEX) != -1;
                Database.Settings.Outbound.PromptForTransportNumber = clbOutboundOptions.CheckedIndices.IndexOf(OUTBOUND_PROMPT_FOR_TRANSPORT_NUMBER_INDEX) != -1;
                Database.Settings.Inbound.ValidateCarrierNumber = clbOutboundOptions.CheckedIndices.IndexOf(OUTBOUND_VALIDATE_CARRIER_NUMBER_INDEX) != -1;
                Database.Settings.Inbound.ValidateDriverNumber = clbOutboundOptions.CheckedIndices.IndexOf(OUTBOUND_VALIDATE_DRIVER_NUMBER_INDEX) != -1;
                Database.Settings.Inbound.ValidateTransportNumber = clbOutboundOptions.CheckedIndices.IndexOf(OUTBOUND_VALIDATE_TRANSPORT_NUMBER_INDEX) != -1;
                Database.Settings.EmailTickets = clbOutboundOptions.CheckedIndices.IndexOf(EMAIL_TICKETS_INDEX) != -1;
                Database.Settings.DoNotCreateTicketForLoadWithNothingDispensed = clbOutboundOptions.CheckedIndices.IndexOf(DO_NOT_CREATE_TICKET_FOR_LOAD_WITH_NOTHING_DISPENSED) != -1;

                // Owner settings
                Database.RequireRestrictedUseOwners = new List<Guid>();
                foreach (NewCheckboxListItem item in cblOwnersRequiringRupForm.CheckedItems) {
                    Guid ownerId = (Guid)item.Tag;
                    if (!Database.RequireRestrictedUseOwners.Contains(ownerId)) Database.RequireRestrictedUseOwners.Add(ownerId);
                }
                Database.Settings.PromptForShipToDestinationOwners = new List<Guid>();
                foreach (NewCheckboxListItem item in cblPromptForShipToDestinationOwners.CheckedItems) {
                    Guid ownerId = (Guid)item.Tag;
                    if (!Database.Settings.PromptForShipToDestinationOwners.Contains(ownerId)) Database.Settings.PromptForShipToDestinationOwners.Add(ownerId);
                }
                Database.Settings.OwnersIgnoreMaplewoodShipTo = new List<Guid>();
                foreach (NewCheckboxListItem item in cblOwnersThatShouldNotDisplayMaplewoodAsShipTo.CheckedItems) {
                    Guid ownerId = (Guid)item.Tag;
                    if (!Database.Settings.OwnersIgnoreMaplewoodShipTo.Contains(ownerId)) Database.Settings.OwnersIgnoreMaplewoodShipTo.Add(ownerId);
                }

                // Scale comms 
                Database.Settings.Scale.PanelId = ((KaPanel)cmbPanel.SelectedItem).Id;
                Database.Settings.Scale.CommType = (ScaleComm.ScaleCommType)cmbScaleCommType.SelectedItem;
                Database.Settings.Scale.ScaleDataStreamType = (ScaleComm.ScaleDataStreamType)cmbScaleDataStreamType.SelectedItem;

                // Printers
                Database.Settings.InboundBaggedProductPrinters = new List<PrinterConfiguration>(GetPrinterConfigurations(lvInboundBaggedProductPrinters));
                Database.Settings.InboundBulkProductPrinters = new List<PrinterConfiguration>(GetPrinterConfigurations(lvInboundBulkProductPrinters));
                Database.Settings.InboundTruckInspectionPrinters = new List<PrinterConfiguration>(GetPrinterConfigurations(lvInboundTruckInspectionPrinters));
                Database.Settings.OutboundTicketPrinters = new List<PrinterConfiguration>(GetPrinterConfigurations(lvOutboundTicketPrinters));
                Database.Settings.ReceivingProductPickTicketPrinters = new List<PrinterConfiguration>(GetPrinterConfigurations(lvReceivingProductPickTicketPrinters));
                Database.Settings.ReceivingProductTicketPrinters = new List<PrinterConfiguration>(GetPrinterConfigurations(lvReceivingProductTicketPrinters));

                // Pick ticket addresses
                Database.Settings.BaggedProductPickTicketAddress = tbxInboundBaggedProductTicketWebAddress.Text;
                Database.Settings.WebPickTicketAddress = tbxInboundBulkProductTicketWebAddress.Text;
                Database.Settings.WebPoPickTicketAddress = tbxReceivingProductPickTicketWebAddress.Text;
                Database.Settings.TruckInspectionTicketAddress = tbxInboundTruckInspectionWebAddress.Text;
                Database.Settings.BayDirectionEnabled = chkBayDirectionEnabled.Checked;

                KaSetting.WriteSetting(connection, string.Format(Database.SN_SETTINGS, Database.ComputerName, Database.APP_ID), Tm2Database.ToXml(Database.Settings, typeof(TerminalSettings)));
                KaSetting.WriteSetting(connection, Database.GetSettingName(Database.SN_REQUIRE_RESTRICTED_USE_OWNERS), Tm2Database.ToXml(Database.RequireRestrictedUseOwners, typeof(List<Guid>)));

                // Bay directions
                if (Database.Settings.BayDirectionEnabled) {
                    foreach (TerminalSettings.BaySetting bay in Database.Settings.Bays) {
                        bay.RestoreEmptyAvailabilityListsFromDb();
                        KaSetting.WriteSetting(connection, string.Format(Database.SN_BAY_ITEM_AVAILABILITY, Database.ComputerName, Database.APP_ID, bay.BayId, KaBulkProduct.TABLE_NAME), Tm2Database.ToXml(RemoveNotSetBayItemAvailibilityRecords(bay.BulkIngredientAvailCollection), typeof(List<BayItemAvailibility>)));
                        KaSetting.WriteSetting(connection, string.Format(Database.SN_BAY_ITEM_AVAILABILITY, Database.ComputerName, Database.APP_ID, bay.BayId, KaCarrier.TABLE_NAME), Tm2Database.ToXml(RemoveNotSetBayItemAvailibilityRecords(bay.CarrierAvailCollection), typeof(List<BayItemAvailibility>)));
                        KaSetting.WriteSetting(connection, string.Format(Database.SN_BAY_ITEM_AVAILABILITY, Database.ComputerName, Database.APP_ID, bay.BayId, KaCustomerAccount.TABLE_NAME), Tm2Database.ToXml(RemoveNotSetBayItemAvailibilityRecords(bay.CustomerAvailCollection), typeof(List<BayItemAvailibility>)));
                        KaSetting.WriteSetting(connection, string.Format(Database.SN_BAY_ITEM_AVAILABILITY, Database.ComputerName, Database.APP_ID, bay.BayId, KaDriver.TABLE_NAME), Tm2Database.ToXml(RemoveNotSetBayItemAvailibilityRecords(bay.DriverAvailCollection), typeof(List<BayItemAvailibility>)));
                        KaSetting.WriteSetting(connection, string.Format(Database.SN_BAY_ITEM_AVAILABILITY, Database.ComputerName, Database.APP_ID, bay.BayId, KaOwner.TABLE_NAME), Tm2Database.ToXml(RemoveNotSetBayItemAvailibilityRecords(bay.OwnerAvailCollection), typeof(List<BayItemAvailibility>)));
                        KaSetting.WriteSetting(connection, string.Format(Database.SN_BAY_ITEM_AVAILABILITY, Database.ComputerName, Database.APP_ID, bay.BayId, KaProduct.TABLE_NAME), Tm2Database.ToXml(RemoveNotSetBayItemAvailibilityRecords(bay.ProductAvailCollection), typeof(List<BayItemAvailibility>)));
                        KaSetting.WriteSetting(connection, string.Format(Database.SN_BAY_ITEM_AVAILABILITY, Database.ComputerName, Database.APP_ID, bay.BayId, KaTransport.TABLE_NAME), Tm2Database.ToXml(RemoveNotSetBayItemAvailibilityRecords(bay.TransportAvailCollection), typeof(List<BayItemAvailibility>)));
                    }
                }

                // Order complete wording
                Database.Settings.OrderCompleteInboundOrderBayNotAssignedNoTickets = tbxOrderCompleteInboundOrderBayNotAssignedNoTickets.Text;
                Database.Settings.OrderCompleteInboundOrderBayNotAssignedHasTickets = tbxOrderCompleteInboundOrderBayNotAssignedWithTickets.Text;
                Database.Settings.OrderCompleteInboundOrderBayAssignedNoTickets = tbxOrderCompleteInboundOrderBayAssignedNoTickets.Text;
                Database.Settings.OrderCompleteInboundOrderBayAssignedHasTickets = tbxOrderCompleteInboundOrderBayAssignedWithTickets.Text;
                Database.Settings.OrderCompleteInboundReceivingBayNotAssignedNoTickets = tbxOrderCompleteInboundReceivingBayNotAssignedNoTickets.Text;
                Database.Settings.OrderCompleteInboundReceivingBayNotAssignedHasTickets = tbxOrderCompleteInboundReceivingBayNotAssignedHasTickets.Text;
                Database.Settings.OrderCompleteInboundReceivingBayAssignedNoTickets = tbxOrderCompleteInboundReceivingBayAssignedNoTickets.Text;
                Database.Settings.OrderCompleteInboundReceivingBayAssignedHasTickets = tbxOrderCompleteInboundReceivingBayAssignedHasTickets.Text;
                Database.Settings.OrderCompleteOutboundNoTickets = tbxOrderCompleteOutboundNoTickets.Text;
                Database.Settings.OrderCompleteOutboundHasTickets = tbxOrderCompleteOutboundHasTickets.Text;
                return true;
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
                return false;
            }
        }

        private List<BayItemAvailibility> RemoveNotSetBayItemAvailibilityRecords(Dictionary<Guid, BayItemAvailibility> currentList) {
            List<BayItemAvailibility> list = new List<BayItemAvailibility>();
            foreach (Guid recordId in currentList.Keys) {
                BayItemAvailibility item = currentList[recordId];
                if (item.Availability != BayItemAvailibility.ItemAvailable.NotSet)
                    list.Add(item);
            }
            return list;
        }

        private List<PrinterConfiguration> GetPrinterConfigurations(ListView printerListView) {
            List<PrinterConfiguration> printerlist = new List<PrinterConfiguration>();
            foreach (ListViewItem item in printerListView.Items) {
                PrinterConfiguration p = new PrinterConfiguration();
                p.PrinterName = item.Text;
                p.Copies = int.Parse(item.SubItems[1].Text);
                if (p.Copies > 0) printerlist.Add(p);
            }
            return printerlist;
        }

        private void PopulateOptions() {
            clbOptions.Items.Clear();
            AddOptionToCheckedListBox(ref clbOptions, "Allow drivers to add destinations", Database.Settings.AllowDriversToAddDestinations, ref ALLOW_DRIVERS_TO_ADD_DESTINATIONS_INDEX);
            AddOptionToCheckedListBox(ref clbOptions, "Automatically add compartments for transport", Database.Settings.AutomaticallyAddCompartmentForTransport, ref AUTOMATICALLY_ADD_COMPARTMENT_FOR_TRANSPORT);
            AddOptionToCheckedListBox(ref clbOptions, "Include Order Release Number in lookup for valid orders", Database.Settings.IncludeOrderReleaseNumberInLookupForValidOrders, ref INCLUDE_RELEASE_NUMBER_IN_ORDER_NUMBER_LOOKUP_INDEX);
            AddOptionToCheckedListBox(ref clbOptions, "Include Order Purchase Order Number in lookup for valid orders", Database.Settings.IncludeOrderPurchaseOrderNumberInLookupForValidOrders, ref INCLUDE_PURCHASE_ORDER_NUMBER_IN_ORDER_NUMBER_LOOKUP_INDEX);
            AddOptionToCheckedListBox(ref clbOptions, "Include Order Numbers that have dashes in the number in lookup for valid orders", Database.Settings.IncludeOrderNumbersThatHaveDashesInTheNumberInLookupForValidOrders, ref ORDER_NUMBER_MUST_MATCH_EXACT_ORDER_NUMBER_LOOKUP_INDEX);
            AddOptionToCheckedListBox(ref clbOptions, "Show don't care for select destination list", Database.Settings.ShowDontCareInDestinationList, ref SHOW_DONT_CARE_IN_DESTINATION_LIST_INDEX);
            AddOptionToCheckedListBox(ref clbOptions, "Show order detail screen", Database.Settings.ShowOrderDetailScreen, ref SHOW_ORDER_DETAIL_SCREEN_INDEX);
            AddOptionToCheckedListBox(ref clbOptions, "Show final confirmation screen", Database.Settings.ShowFinalConfirmationScreen, ref SHOW_FINAL_CONFIRMATION_SCREEN_INDEX);
            AddOptionToCheckedListBox(ref clbOptions, "Show OK Button For Select From List", Database.Settings.ShowOkButtonForSelectFromList, ref SHOW_OK_BUTTON_FOR_SELECT_FROM_LIST_INDEX);
            AddOptionToCheckedListBox(ref clbOptions, "Use alphanumeric keyboard for carriers", Database.Settings.UseAlaphaNumKeyboardForCarriers, ref USE_ALPHANUMERIC_KEYBOARD_FOR_CARRIERS_INDEX);
            AddOptionToCheckedListBox(ref clbOptions, "Use alphanumeric keyboard for drivers", Database.Settings.UseAlaphaNumKeyboardForDrivers, ref USE_ALPHANUMERIC_KEYBOARD_FOR_DRIVERS_INDEX);
            AddOptionToCheckedListBox(ref clbOptions, "Use alphanumeric keyboard for load numbers", Database.Settings.UseAlaphaNumKeyboardForLoadNumbers, ref USE_ALPHANUMERIC_KEYBOARD_FOR_LOAD_NUMBERS_INDEX);
            AddOptionToCheckedListBox(ref clbOptions, "Use alphanumeric keyboard for transports", Database.Settings.UseAlaphaNumKeyboardForTransports, ref USE_ALPHANUMERIC_KEYBOARD_FOR_TRANSPORTS_INDEX);

            //Inbound specific
            cbxInboundOptionsEnabled.Checked = Database.Settings.Inbound.Enabled;
            AddOptionToCheckedListBox(ref clbInboundOptions, "Enforce order locking", Database.Settings.EnforceOrderLocking, ref ENFORE_ORDER_LOCKING_INDEX);
            AddOptionToCheckedListBox(ref clbInboundOptions, "Show order list", Database.Settings.Inbound.AllowOrderListLookup, ref INBOUND_SHOW_ORDER_LIST_INDEX);
            AddOptionToCheckedListBox(ref clbInboundOptions, "Allow order to be staged multiple times", Database.Settings.Inbound.AllowMultipleStagedOrders, ref INBOUND_ALLOW_MULTIPLE_STAGED_ORDERS_INDEX);
            AddOptionToCheckedListBox(ref clbInboundOptions, "Prompt for carrier number", Database.Settings.Inbound.PromptForCarrierNumber, ref INBOUND_PROMPT_FOR_CARRIER_NUMBER_INDEX);
            AddOptionToCheckedListBox(ref clbInboundOptions, "Prompt for driver number", Database.Settings.Inbound.PromptForDriverNumber, ref INBOUND_PROMPT_FOR_DRIVER_NUMBER_INDEX);
            AddOptionToCheckedListBox(ref clbInboundOptions, "Prompt for transport number", Database.Settings.Inbound.PromptForTransportNumber, ref INBOUND_PROMPT_FOR_TRANSPORT_NUMBER_INDEX);
            AddOptionToCheckedListBox(ref clbInboundOptions, "Validate carrier number", Database.Settings.Inbound.ValidateCarrierNumber, ref INBOUND_VALIDATE_CARRIER_NUMBER_INDEX);
            AddOptionToCheckedListBox(ref clbInboundOptions, "Validate driver number", Database.Settings.Inbound.ValidateDriverNumber, ref INBOUND_VALIDATE_DRIVER_NUMBER_INDEX);
            AddOptionToCheckedListBox(ref clbInboundOptions, "Validate transport number", Database.Settings.Inbound.ValidateTransportNumber, ref INBOUND_VALIDATE_TRANSPORT_NUMBER_INDEX);

            //Outbound specific
            cbxOutboundOptionsEnabled.Checked = Database.Settings.Outbound.Enabled;
            AddOptionToCheckedListBox(ref clbOutboundOptions, "Do not create ticket for load with nothing dispensed", Database.Settings.DoNotCreateTicketForLoadWithNothingDispensed, ref DO_NOT_CREATE_TICKET_FOR_LOAD_WITH_NOTHING_DISPENSED);
            AddOptionToCheckedListBox(ref clbOutboundOptions, "Email Tickets", Database.Settings.EmailTickets, ref EMAIL_TICKETS_INDEX);
            AddOptionToCheckedListBox(ref clbOutboundOptions, "Move driver in facility records to history upon order completion", Database.Settings.MoveDriversToHistory, ref MOVE_DRIVER_TO_HISTORY_INDEX);
            AddOptionToCheckedListBox(ref clbOutboundOptions, "Move transport in facility records to history upon order completion", Database.Settings.MoveTransportsToHistory, ref MOVE_TRANSPORT_TO_HISTORY_INDEX);
            AddOptionToCheckedListBox(ref clbOutboundOptions, "Show order list", Database.Settings.Outbound.AllowOrderListLookup, ref OUTBOUND_SHOW_ORDER_LIST_INDEX);
            AddOptionToCheckedListBox(ref clbOutboundOptions, "Prompt for carrier number", Database.Settings.Outbound.PromptForCarrierNumber, ref OUTBOUND_PROMPT_FOR_CARRIER_NUMBER_INDEX);
            AddOptionToCheckedListBox(ref clbOutboundOptions, "Prompt for driver number", Database.Settings.Outbound.PromptForDriverNumber, ref OUTBOUND_PROMPT_FOR_DRIVER_NUMBER_INDEX);
            AddOptionToCheckedListBox(ref clbOutboundOptions, "Prompt for transport number", Database.Settings.Outbound.PromptForTransportNumber, ref OUTBOUND_PROMPT_FOR_TRANSPORT_NUMBER_INDEX);
            AddOptionToCheckedListBox(ref clbOutboundOptions, "Validate carrier number", Database.Settings.Inbound.ValidateCarrierNumber, ref OUTBOUND_VALIDATE_CARRIER_NUMBER_INDEX);
            AddOptionToCheckedListBox(ref clbOutboundOptions, "Validate driver number", Database.Settings.Inbound.ValidateDriverNumber, ref OUTBOUND_VALIDATE_DRIVER_NUMBER_INDEX);
            AddOptionToCheckedListBox(ref clbOutboundOptions, "Validate transport number", Database.Settings.Inbound.ValidateTransportNumber, ref OUTBOUND_VALIDATE_TRANSPORT_NUMBER_INDEX);
        }

        private void PopulateOwners() {
            cblOwnersRequiringRupForm.Items.Clear();
            cblPromptForShipToDestinationOwners.Items.Clear();
            cblOwnersThatShouldNotDisplayMaplewoodAsShipTo.Items.Clear();
            List<Guid> requireRestrictedUseOwners = Database.RequireRestrictedUseOwners;
            foreach (KaOwner owner in KaOwner.GetAll(Tm2Database.Connection, "deleted = 0", "name")) {
                cblOwnersRequiringRupForm.Items.Add(new NewCheckboxListItem() { Text = owner.Name, Tag = owner.Id }, requireRestrictedUseOwners.Contains(owner.Id));
                cblPromptForShipToDestinationOwners.Items.Add(new NewCheckboxListItem() { Text = owner.Name, Tag = owner.Id }, Database.Settings.PromptForShipToDestinationOwners.Contains(owner.Id));
                cblOwnersThatShouldNotDisplayMaplewoodAsShipTo.Items.Add(new NewCheckboxListItem() { Text = owner.Name, Tag = owner.Id }, Database.Settings.OwnersIgnoreMaplewoodShipTo.Contains(owner.Id));
            }
        }

        #region Populate printers
        private void PopulatePrinters() {
            lvInboundBaggedProductPrinters.Items.Clear();
            lvInboundBulkProductPrinters.Items.Clear();
            lvInboundTruckInspectionPrinters.Items.Clear();
            lvOutboundTicketPrinters.Items.Clear();
            lvReceivingProductPickTicketPrinters.Items.Clear();
            lvReceivingProductTicketPrinters.Items.Clear();
            foreach (string printer in PrinterSettings.InstalledPrinters) {
                AddPrinterToList(printer, lvInboundBaggedProductPrinters, Database.Settings.InboundBaggedProductPrinters);
                AddPrinterToList(printer, lvInboundBulkProductPrinters, Database.Settings.InboundBulkProductPrinters);
                AddPrinterToList(printer, lvInboundTruckInspectionPrinters, Database.Settings.InboundTruckInspectionPrinters);
                AddPrinterToList(printer, lvOutboundTicketPrinters, Database.Settings.OutboundTicketPrinters);
                AddPrinterToList(printer, lvReceivingProductPickTicketPrinters, Database.Settings.ReceivingProductPickTicketPrinters);
                AddPrinterToList(printer, lvReceivingProductTicketPrinters, Database.Settings.ReceivingProductTicketPrinters);
            }
        }

        private void AddPrinterToList(string printer, ListView printerList, List<PrinterConfiguration> settingsList) {
            ListViewItem item = new ListViewItem(new string[] { printer, "0" });
            foreach (PrinterConfiguration r in settingsList) {
                if (r.PrinterName == printer) {
                    item.SubItems[1].Text = r.Copies.ToString();
                    break;
                }
            }
            printerList.Items.Add(item);
        }
        #endregion

        private void PopulatePanelDropDownLists() {
            KaPanel blank = new KaPanel();
            blank.Name = "Select panel";
            cmbPanel.Items.Clear();
            cmbPanel.Items.Add(blank);
            cmbPanel.SelectedIndex = 0;
            foreach (KaPanel panel in KaPanel.GetAll(Tm2Database.Connection, $"deleted=0 AND (connection_type<>2 OR id={Database.Q(Database.Settings.Scale.PanelId)})", "name ASC")) {
                cmbPanel.Items.Add(panel);
                if (panel.Id == Database.Settings.Scale.PanelId) cmbPanel.SelectedIndex = cmbPanel.Items.Count - 1;
            }
            cmbScaleCommType.Items.Clear();
            cmbScaleCommType.Items.Add(ScaleComm.ScaleCommType.Ka2000);
            cmbScaleCommType.Items.Add(ScaleComm.ScaleCommType.TCPIP);
            cmbScaleCommType.Items.Add(ScaleComm.ScaleCommType.WebService);
            cmbScaleDataStreamType.Items.Clear();
            cmbScaleDataStreamType.Items.Add(ScaleComm.ScaleDataStreamType.MettlerToledo);
            cmbScaleDataStreamType.Items.Add(ScaleComm.ScaleDataStreamType.RLWS_Condec);

            try {
                cmbScaleCommType.SelectedItem = Database.Settings.Scale.CommType;
            } catch (ArgumentOutOfRangeException) {
                cmbScaleCommType.SelectedItem = ScaleComm.ScaleCommType.Ka2000;
            }
            try {
                cmbScaleDataStreamType.SelectedItem = Database.Settings.Scale.ScaleDataStreamType;
            } catch (ArgumentOutOfRangeException) {
                cmbScaleDataStreamType.SelectedItem = ScaleComm.ScaleDataStreamType.RLWS_Condec;
            }
            cmbPanel_SelectedIndexChanged(cmbPanel, new EventArgs());
        }

        private void PopulateUnits() {
            cmbDefaultMassUnit.Items.Clear();
            cmbDefaultVolumeUnit.Items.Clear();

            foreach (KaUnit unit in KaUnit.GetAll(Tm2Database.Connection, "deleted=0", "abbreviation ASC")) {
                if (KaUnit.IsWeight(unit.BaseUnit)) {
                    cmbDefaultMassUnit.Items.Add(unit);
                    if (unit.Id.Equals(LfDatabase.DefaultMassUnitId)) cmbDefaultMassUnit.SelectedIndex = cmbDefaultMassUnit.Items.Count - 1;
                } else if (unit.BaseUnit != KaUnit.Unit.Seconds && unit.BaseUnit != KaUnit.Unit.Seconds) {
                    cmbDefaultVolumeUnit.Items.Add(unit);
                    if (unit.Id.Equals(LfDatabase.DefaultVolumeUnitId)) cmbDefaultVolumeUnit.SelectedIndex = cmbDefaultVolumeUnit.Items.Count - 1;
                }
            }
        }

        private void PopulateUsers() {
            bool userSet = false;
            cmbUser.Items.Clear();
            KaUser selectUser = new KaUser();
            selectUser.Name = "Select a user";
            selectUser.Id = Guid.Empty;
            cmbUser.Items.Add(selectUser);
            foreach (KaUser user in KaUser.GetAll(Tm2Database.Connection, "deleted=0", "name asc")) {
                cmbUser.Items.Add(user);
                if (user.Id == Database.UserId) {
                    cmbUser.SelectedIndex = cmbUser.Items.Count - 1;
                    userSet = true;
                }
            }
            if (!userSet) {
                cmbUser.SelectedIndex = 0;
            }
            cmbUser_SelectedIndexChanged(cmbUser, new EventArgs());
        }

        private void AddOptionToCheckedListBox(ref KahlerAutomation.CheckedListBox listBox, string item, bool isChecked, ref int indexNumber) {
            listBox.Items.Add(item, isChecked);
            indexNumber = listBox.Items.Count - 1;
        }

        private void cmbUser_SelectedIndexChanged(object sender, EventArgs e) {
            lblUserError.Visible = !(cmbUser.SelectedIndex > 0);
        }

        private bool _selectingPrinter = false;

        private void lvPrinters_SelectedIndexChanged(object sender, EventArgs e) {
            ListView printerList = (ListView)sender;
            try {
                _selectingPrinter = true;
                NumericUpDown copies = null;
                switch (printerList.Name) {
                    case "lvInboundBaggedProductPrinters":
                        copies = nudInboundBaggedProductCopies;
                        break;
                    case "lvInboundBulkProductPrinters":
                        copies = nudInboundBulkProductCopies;
                        break;
                    case "lvInboundTruckInspectionPrinters":
                        copies = nudInboundTruckInspectionCopies;
                        break;
                    case "lvOutboundTicketPrinters":
                        copies = nudOutboundTicketCopies;
                        break;
                    case "lvReceivingProductPickTicketPrinters":
                        copies = nudReceivingProductPickTicketCopies;
                        break;
                    case "lvReceivingProductTicketPrinters":
                        copies = nudReceivingProductTicketCopies;
                        break;
                }
                if (copies != null) {
                    if (printerList.SelectedItems.Count > 0) {
                        copies.Enabled = true;
                        copies.Value = decimal.Parse(printerList.SelectedItems[0].SubItems[1].Text);
                    } else {
                        copies.Enabled = false;
                        copies.Value = 0;
                    }
                }
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            } finally {
                _selectingPrinter = false;
            }
        }

        private void nudPrinterCopies_ValueChanged(object sender, EventArgs e) {
            if (!_selectingPrinter) {
                NumericUpDown copies = (NumericUpDown)sender;

                ListView printerList = null;
                switch (copies.Name) {
                    case "nudInboundBaggedProductCopies":
                        printerList = lvInboundBaggedProductPrinters;
                        break;
                    case "nudInboundBulkProductCopies":
                        printerList = lvInboundBulkProductPrinters;
                        break;
                    case "nudInboundTruckInspectionCopies":
                        printerList = lvInboundTruckInspectionPrinters;
                        break;
                    case "nudOutboundTicketCopies":
                        printerList = lvOutboundTicketPrinters;
                        break;
                    case "nudReceivingProductPickTicketCopies":
                        printerList = lvReceivingProductPickTicketPrinters;
                        break;
                    case "nudReceivingProductTicketCopies":
                        printerList = lvReceivingProductTicketPrinters;
                        break;
                }

                try {
                    if (printerList != null && !_selectingPrinter && printerList.SelectedItems.Count > 0)
                        printerList.SelectedItems[0].SubItems[1].Text = copies.Value.ToString();
                } catch (Exception ex) {
                    StUnhandledException.ShowException(ex);
                }
            }
        }

        private void cmbPanel_SelectedIndexChanged(object sender, EventArgs e) {
            bool showCommType = false;
            bool showDataStreamType = false;

            if (cmbPanel.SelectedIndex > 0) {
                KaPanel panel = (KaPanel)cmbPanel.SelectedItem;
                if (panel.ConnectionType == KaPanel.PanelConnectionType.Ethernet) {
                    showCommType = true;
                    //} else if (  panel.ConnectionType == KaPanel.PanelConnectionType.ModbusTcp) {
                    //    lblScaleCommType.Visible = true;
                    //    cmbScaleCommType.Visible = true;
                } else if (panel.ConnectionType == KaPanel.PanelConnectionType.SerialPort) {
                    showDataStreamType = true;
                }
            }

            lblScaleCommType.Visible = showCommType;
            cmbScaleCommType.Visible = showCommType;
            lblScaleDataStreamType.Visible = showDataStreamType;
            cmbScaleDataStreamType.Visible = showDataStreamType;

        }

        #region   Thread-Safe Delegate Subs  
        public delegate void SetPropertyValueCallback(System.Windows.Forms.Control obj, string propertyName, object value);
        public void SetPropertyValue(System.Windows.Forms.Control obj, string propertyName, object value) {
            if (this.InvokeRequired) {
                SetPropertyValueCallback d = new SetPropertyValueCallback(SetPropertyValue);
                this.BeginInvoke(d, new object[] { obj, propertyName, value });
            } else {
                obj.GetType().GetProperty(propertyName).SetValue(obj, value, null);
                try {
                    obj.Refresh();
                } catch (Exception) {
                }
            }
        }
        #endregion

        public class NewCheckboxListItem {
            public string Text;
            public object Tag;
            public override string ToString() {
                return this.Text;
            }
        }

        #region Bay directions
        private void chkBayEnabled_CheckedChanged(object sender, EventArgs e) {
            GetCurrentBaySelected().Enabled = chkBayEnabled.Checked;
            SetPropertyValue(tbcBayItemAvailability, "Enabled", chkBayEnabled.Checked);
        }

        private void PopulateBaysList() {
            bool currentBayFound = false;
            Guid currentBayId = Guid.Empty;
            if (cmbBay.Items.Count > 1 && cmbBay.SelectedIndex >= 1) {
                currentBayId = (Guid)((ComboBoxItem)cmbBay.SelectedItem).Value;
            }

            cmbBay.Items.Clear();
            cmbBay.Items.Add(new ComboBoxItem("Select bay", null));

            bool noBaysPreviouslyDefined = (Database.Settings.Bays.Count == 0);
            ArrayList bayList = KaBay.GetAll(Tm2Database.Connection, "deleted=0", "name");
            foreach (KaBay bayInfo in bayList) {
                bool bayFound = false;
                foreach (TerminalSettings.BaySetting clientBay in Database.Settings.Bays)
                    if (bayInfo.Id.Equals(clientBay.BayInfo.Id)) {
                        cmbBay.Items.Add(new ComboBoxItem(clientBay.BayInfo.Name, clientBay.BayInfo.Id));
                        bayFound = true;
                        break;
                    }
                if (!bayFound) {
                    TerminalSettings.BaySetting newClientBay = new TerminalSettings.BaySetting();
                    newClientBay.BayInfo = bayInfo;
                    newClientBay.Enabled = noBaysPreviouslyDefined;
                    newClientBay.PopulateAvailabilityLists();
                    cmbBay.Items.Add(new ComboBoxItem(newClientBay.BayInfo.Name, bayInfo.Id));
                    Database.Settings.Bays.Add(newClientBay);
                }
                if (currentBayId.Equals(bayInfo.Id)) {
                    currentBayFound = true;
                    cmbBay.SelectedIndex = cmbBay.Items.Count - 1;
                }

            }

            if (!currentBayFound) cmbBay.SelectedIndex = 0;
            cmbBay_SelectedIndexChanged(cmbBay, new EventArgs());
        }

        private void cmbBay_SelectedIndexChanged(Object sender, System.EventArgs e) {
            this.SuspendLayout();
            TerminalSettings.BaySetting bayInfo = GetCurrentBaySelected();
            if (bayInfo != null) {
                chkBayEnabled.Checked = bayInfo.Enabled;

                if (bayInfo.AllowAllBulkIngredients) {
                    rbBayDirectionsBulkIngredientAllowAll.Checked = true;
                    rbBayDirectionsBulkIngredientUseList.Checked = false;
                    rbBayDirectionsBulkIngredientUsePanelBulkProducts.Checked = false;
                } else if (bayInfo.BulkIngredientsUseAvailList) {
                    rbBayDirectionsBulkIngredientAllowAll.Checked = false;
                    rbBayDirectionsBulkIngredientUseList.Checked = true;
                    rbBayDirectionsBulkIngredientUsePanelBulkProducts.Checked = false;
                } else {
                    rbBayDirectionsBulkIngredientAllowAll.Checked = false;
                    rbBayDirectionsBulkIngredientUseList.Checked = false;
                    rbBayDirectionsBulkIngredientUsePanelBulkProducts.Checked = true;
                }
                chkBayAllowAllItems_CheckedChanged(rbBayDirectionsBulkIngredientAllowAll, new EventArgs());
                chkBayAllowAllProducts.Checked = bayInfo.AllowAllProducts;
                chkBayAllowAllItems_CheckedChanged(chkBayAllowAllProducts, new EventArgs());
                chkBayAllowAllCustomers.Checked = bayInfo.AllowAllCustomers;
                chkBayAllowAllItems_CheckedChanged(chkBayAllowAllCustomers, new EventArgs());
                chkBayAllowAllOwners.Checked = bayInfo.AllowAllOwners;
                chkBayAllowAllItems_CheckedChanged(chkBayAllowAllOwners, new EventArgs());
                chkBayAllowAllDrivers.Checked = bayInfo.AllowAllDrivers;
                chkBayAllowAllItems_CheckedChanged(chkBayAllowAllDrivers, new EventArgs());
                chkBayAllowAllTransports.Checked = bayInfo.AllowAllTransports;
                chkBayAllowAllItems_CheckedChanged(chkBayAllowAllTransports, new EventArgs());
                chkBayAllowAllCarriers.Checked = bayInfo.AllowAllCarriers;
                chkBayAllowAllItems_CheckedChanged(chkBayAllowAllCarriers, new EventArgs());
                chkBayAvailableForLoadoutOrders.Checked = bayInfo.AvailableForLoadoutOrders;
                chkBayAvailableForLoadoutOrders_CheckedChanged(chkBayAvailableForLoadoutOrders, new EventArgs());
                chkBayAvailableForReceivingOrders.Checked = bayInfo.AvailableForReceivingOrders;

                FillBayAllowed();
                SetPropertyValue(tbcBayItemAvailability, "Enabled", true);
                SetPropertyValue(chkBayEnabled, "Enabled", true);
                SetPropertyValue(chkBayAvailableForLoadoutOrders, "Enabled", true);
                SetPropertyValue(chkBayAvailableForReceivingOrders, "Enabled", true);
            } else {
                cmbBay.Text = "";
                SetPropertyValue(tbcBayItemAvailability, "Enabled", false);
                SetPropertyValue(chkBayEnabled, "Enabled", false);
                SetPropertyValue(chkBayAvailableForLoadoutOrders, "Enabled", false);
                SetPropertyValue(chkBayAvailableForReceivingOrders, "Enabled", false);

                FillBayAllowed();
            }
            this.ResumeLayout(true);
        }

        private void chkBayAllowAllItems_CheckedChanged(Object sender, System.EventArgs e) {
            System.Windows.Forms.Control control = (Control)sender;

            TerminalSettings.BaySetting bayInfo = GetCurrentBaySelected();

            if (control.Name == "rbBayDirectionsBulkIngredientAllowAll" || control.Name == "rbBayDirectionsBulkIngredientUseList" || control.Name == "rbBayDirectionsBulkIngredientUsePanelBulkProducts") {
                bool allowAllChecked = rbBayDirectionsBulkIngredientAllowAll.Checked;
                bool useList = rbBayDirectionsBulkIngredientUseList.Checked;
                bayInfo.AllowAllBulkIngredients = allowAllChecked;
                bayInfo.BulkIngredientsUseAvailList = useList;
                SetPropertyValue(pnlBayBulkIngredientsAvailable, "Enabled", useList);
                SetPropertyValue(cmdBayBulkIngredientsCheckAll, "Enabled", useList);
                SetPropertyValue(cmdBayBulkIngredientsUncheckAll, "Enabled", useList);
            } else {
                bool allowAllChecked = false;
                if (control.GetType() == typeof(CheckBox))
                    allowAllChecked = ((CheckBox)control).Checked;
                else if (control.GetType() == typeof(RadioButton))
                    allowAllChecked = ((RadioButton)control).Checked;

                switch (control.Name) {
                    case "chkBayAllowAllProducts":
                        bayInfo.AllowAllProducts = allowAllChecked;
                        SetPropertyValue(pnlBayProductsAvailable, "Enabled", !allowAllChecked);
                        SetPropertyValue(cmdBayProductsCheckAll, "Enabled", !allowAllChecked);
                        SetPropertyValue(cmdBayProductsUncheckAll, "Enabled", !allowAllChecked);
                        break;
                    case "chkBayAllowAllOwners":
                        bayInfo.AllowAllOwners = allowAllChecked;
                        SetPropertyValue(pnlBayOwnersAvailable, "Enabled", !allowAllChecked);
                        SetPropertyValue(cmdBayOwnerCheckAll, "Enabled", !allowAllChecked);
                        SetPropertyValue(cmdBayOwnerUncheckAll, "Enabled", !allowAllChecked);
                        break;
                    case "chkBayAllowAllCustomers":
                        bayInfo.AllowAllCustomers = allowAllChecked;
                        SetPropertyValue(pnlBayCustomersAvailable, "Enabled", !allowAllChecked);
                        SetPropertyValue(cmdBayCustomerCheckAll, "Enabled", !allowAllChecked);
                        SetPropertyValue(cmdBayCustomerUncheckAll, "Enabled", !allowAllChecked);
                        break;
                    case "chkBayAllowAllCarriers":
                        bayInfo.AllowAllCarriers = allowAllChecked;
                        SetPropertyValue(pnlBayCarriersAvailable, "Enabled", !allowAllChecked);
                        SetPropertyValue(cmdBayCarrierCheckAll, "Enabled", !allowAllChecked);
                        SetPropertyValue(cmdBayCarrierUncheckAll, "Enabled", !allowAllChecked);
                        break;
                    case "chkBayAllowAllTransports":
                        bayInfo.AllowAllTransports = allowAllChecked;
                        SetPropertyValue(pnlBayTransportsAvailable, "Enabled", !allowAllChecked);
                        SetPropertyValue(cmdBayTransportCheckAll, "Enabled", !allowAllChecked);
                        SetPropertyValue(cmdBayTransportUncheckAll, "Enabled", !allowAllChecked);
                        break;
                    case "chkBayAllowAllDrivers":
                        bayInfo.AllowAllDrivers = allowAllChecked;
                        SetPropertyValue(pnlBayDriversAvailable, "Enabled", !allowAllChecked);
                        SetPropertyValue(cmdBayDriverCheckAll, "Enabled", !allowAllChecked);
                        SetPropertyValue(cmdBayDriverUncheckAll, "Enabled", !allowAllChecked);
                        break;
                }
            }
        }

        private void BayItemAvailable_CheckedChanged(Object sender, System.EventArgs e) {
            ItemAvailabiltyCheckBox itemAvailObject = (ItemAvailabiltyCheckBox)sender;
            TerminalSettings.BaySetting bayInfo = GetCurrentBaySelected();

            string collectionSourceTag = itemAvailObject.Parent.Parent.Text.ToLower();
            Dictionary<Guid, BayItemAvailibility> collectionSource = new Dictionary<Guid, BayItemAvailibility>();
            switch (collectionSourceTag) {
                case "bulk ingredients":
                    collectionSource = bayInfo.BulkIngredientAvailCollection;
                    break;
                case "products":
                    collectionSource = bayInfo.ProductAvailCollection;
                    break;
                case "customers":
                    collectionSource = bayInfo.CustomerAvailCollection;
                    break;
                case "owners":
                    collectionSource = bayInfo.OwnerAvailCollection;
                    break;
                case "carriers":
                    collectionSource = bayInfo.CarrierAvailCollection;
                    break;
                case "Transports":
                    collectionSource = bayInfo.TransportAvailCollection;
                    break;
                case "drivers":
                    collectionSource = bayInfo.DriverAvailCollection;
                    break;
            }
            if (collectionSource != null) {
                if (collectionSource.ContainsKey(itemAvailObject.Value.RecordId)) {
                    BayItemAvailibility item = collectionSource[itemAvailObject.Value.RecordId];
                    if (itemAvailObject.CheckState == CheckState.Checked) {
                        item.Availability = BayItemAvailibility.ItemAvailable.Yes;
                    } else if (itemAvailObject.CheckState != CheckState.Indeterminate) {
                        item.Availability = BayItemAvailibility.ItemAvailable.No;
                    } else {
                        item.Availability = BayItemAvailibility.ItemAvailable.NotSet;
                    }
                }
            }
        }

        private void cmdBayItemsCheckAll_Click(object sender, EventArgs e) {
            TerminalSettings.BaySetting bayInfo = GetCurrentBaySelected();

            FlowLayoutPanel collectionSource = GetBaydirectionParentCollectionSource((Control)sender);
            if (collectionSource != null) {
                foreach (ItemAvailabiltyCheckBox itemAvailObject in collectionSource.Controls) {
                    itemAvailObject.CheckState = CheckState.Checked;
                    itemAvailObject.Checked = true;
                }
            }
        }

        private FlowLayoutPanel GetBaydirectionParentCollectionSource(Control sender) {
            FlowLayoutPanel collectionSource = null;
            try {
                string collectionSourceTag = sender.Text.ToLower();
                switch (collectionSourceTag) {
                    case "bulk ingredients":
                        collectionSource = pnlBayBulkIngredientsAvailable;
                        break;
                    case "products":
                        collectionSource = pnlBayProductsAvailable;
                        break;
                    case "customers":
                        collectionSource = pnlBayCustomersAvailable;
                        break;
                    case "owners":
                        collectionSource = pnlBayOwnersAvailable;
                        break;
                    case "carriers":
                        collectionSource = pnlBayCarriersAvailable;
                        break;
                    case "transports":
                        collectionSource = pnlBayTransportsAvailable;
                        break;
                    case "drivers":
                        collectionSource = pnlBayDriversAvailable;
                        break;
                }
            } catch (Exception) { /* suppress */ }
            if (collectionSource != null || sender.Parent == null)
                return collectionSource;
            else
                return GetBaydirectionParentCollectionSource(sender.Parent);
        }
        private void cmdBayItemsUncheckAll_Click(object sender, EventArgs e) {
            TerminalSettings.BaySetting bayInfo = GetCurrentBaySelected();

            FlowLayoutPanel collectionSource = GetBaydirectionParentCollectionSource((Control)sender);

            if (collectionSource != null) {
                foreach (ItemAvailabiltyCheckBox itemAvailObject in collectionSource.Controls) {
                    itemAvailObject.CheckState = CheckState.Unchecked;
                    itemAvailObject.Checked = false;
                }
            }
        }

        private void FillBayAllowed() {
            System.Data.OleDb.OleDbConnection connection = Tm2Database.Connection;
            TerminalSettings.BaySetting bayInfo = GetCurrentBaySelected();
            StProgressBar progressBar = new StProgressBar() {
                ProgressMarqueeAnimationSpeed = 20,
                ProgressStyle = ProgressBarStyle.Marquee
            };
            try {
                progressBar.Show();
                progressBar.BringToFront();
                Cursor = Cursors.WaitCursor;
                if (bayInfo != null) {
                    progressBar.ProgressBarLabel = "Populating bulk products...";
                    FillBayPanelAllowed(pnlBayBulkIngredientsAvailable, bayInfo.BulkIngredientAvailCollection);
                    progressBar.ProgressBarLabel = "Populating products...";
                    FillBayPanelAllowed(pnlBayProductsAvailable, bayInfo.ProductAvailCollection);
                    progressBar.ProgressBarLabel = "Populating customers and suppliers...";
                    FillBayPanelAllowed(pnlBayCustomersAvailable, bayInfo.CustomerAvailCollection);
                    progressBar.ProgressBarLabel = "Populating owners...";
                    FillBayPanelAllowed(pnlBayOwnersAvailable, bayInfo.OwnerAvailCollection);
                    progressBar.ProgressBarLabel = "Populating carriers...";
                    FillBayPanelAllowed(pnlBayCarriersAvailable, bayInfo.CarrierAvailCollection);
                    progressBar.ProgressBarLabel = "Populating transports...";
                    FillBayPanelAllowed(pnlBayTransportsAvailable, bayInfo.TransportAvailCollection);
                    progressBar.ProgressBarLabel = "Populating drivers...";
                    FillBayPanelAllowed(pnlBayDriversAvailable, bayInfo.DriverAvailCollection);
                } else {
                    progressBar.ProgressBarLabel = "Populating bulk products...";
                    FillBayPanelAllowed(pnlBayBulkIngredientsAvailable, null);
                    progressBar.ProgressBarLabel = "Populating products...";
                    FillBayPanelAllowed(pnlBayProductsAvailable, null);
                    progressBar.ProgressBarLabel = "Populating customers and suppliers...";
                    FillBayPanelAllowed(pnlBayCustomersAvailable, null);
                    progressBar.ProgressBarLabel = "Populating owners...";
                    FillBayPanelAllowed(pnlBayOwnersAvailable, null);
                    progressBar.ProgressBarLabel = "Populating carriers...";
                    FillBayPanelAllowed(pnlBayCarriersAvailable, new Dictionary<Guid, BayItemAvailibility>());
                    progressBar.ProgressBarLabel = "Populating transports...";
                    FillBayPanelAllowed(pnlBayTransportsAvailable, new Dictionary<Guid, BayItemAvailibility>());
                    progressBar.ProgressBarLabel = "Populating drivers...";
                    FillBayPanelAllowed(pnlBayDriversAvailable, null);
                }
            } finally {
                progressBar.Close();
                Cursor = DefaultCursor;
            }
        }

        private void FillBayPanelAllowed(FlowLayoutPanel panelToFill, Dictionary<Guid, BayItemAvailibility> itemDictionary) {
            panelToFill.SuspendLayout();
            panelToFill.Controls.Clear();
            List<BayItemAvailibility> itemCollection = new List<BayItemAvailibility>();
            if (itemDictionary != null) {
                foreach (Guid recordId in itemDictionary.Keys) {
                    itemCollection.Add(itemDictionary[recordId]);
                }
            }
            itemCollection.Sort((x, y) => x.Name.CompareTo(y.Name));

            foreach (BayItemAvailibility itemAvailable in itemCollection) {
                ItemAvailabiltyCheckBox newCheckBox = new ItemAvailabiltyCheckBox(itemAvailable.Name, itemAvailable);
                newCheckBox.ThreeState = false;
                newCheckBox.AutoSize = true;
                if (itemAvailable.Availability == BayItemAvailibility.ItemAvailable.Yes) {
                    newCheckBox.CheckState = CheckState.Checked;
                } else if (itemAvailable.Availability == BayItemAvailibility.ItemAvailable.NotSet) {
                    newCheckBox.CheckState = CheckState.Indeterminate;
                } else {
                    newCheckBox.CheckState = CheckState.Unchecked;
                }
                newCheckBox.CheckedChanged += BayItemAvailable_CheckedChanged;

                panelToFill.Controls.Add(newCheckBox);
                Application.DoEvents();
            }
            panelToFill.ResumeLayout();
        }

        private void chkBayAvailableForLoadoutOrders_CheckedChanged(object sender, System.EventArgs e) {
            GetCurrentBaySelected().AvailableForLoadoutOrders = chkBayAvailableForLoadoutOrders.Checked;
        }

        private void chkBayAvailableForReceivingOrders_CheckedChanged(object sender, System.EventArgs e) {
            GetCurrentBaySelected().AvailableForReceivingOrders = chkBayAvailableForLoadoutOrders.Checked;
        }

        private TerminalSettings.BaySetting GetCurrentBaySelected() {
            Guid baySelectedId;
            if (cmbBay.SelectedIndex > 0)
                baySelectedId = (Guid)((ComboBoxItem)cmbBay.SelectedItem).Value;
            else
                return null;

            foreach (TerminalSettings.BaySetting panelBaySetting in Database.Settings.Bays) {
                if (panelBaySetting.BayId.Equals(baySelectedId)) {
                    return panelBaySetting;
                }
            }
            return null;
        }
        #endregion
        private void Configuration_FormClosing(object sender, FormClosingEventArgs e) {
            if (this.DialogResult != DialogResult.OK) {
                LfDatabase.InitializeSettings();
                Database.InitializeSettings();
            }
        }

        private void cbxInboundOptionsEnabled_CheckedChanged(object sender, EventArgs e) {
            SetPropertyValue(clbInboundOptions, "Enabled", cbxInboundOptionsEnabled.Checked);
        }

        private void cbxOutboundOptionsEnabled_CheckedChanged(object sender, EventArgs e) {
            SetPropertyValue(clbOutboundOptions, "Enabled", cbxOutboundOptionsEnabled.Checked);
        }
    }
}