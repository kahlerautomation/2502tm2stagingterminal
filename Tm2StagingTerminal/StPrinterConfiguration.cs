﻿using System;

namespace Tm2StagingTerminal {
    [Serializable()]
    public class PrinterConfiguration {
        public string PrinterName { get; set; }
        public int Copies { get; set; }
    }
}
