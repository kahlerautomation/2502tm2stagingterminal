﻿namespace Tm2StagingTerminal {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.pbConfig = new System.Windows.Forms.PictureBox();
            this.picTopBar = new System.Windows.Forms.PictureBox();
            this.tmrPrintTicket = new System.Windows.Forms.Timer(this.components);
            this.tmrTransition = new System.Windows.Forms.Timer(this.components);
            this.tmrMaxScreenTime = new System.Windows.Forms.Timer(this.components);
            this.tmrUpdateScale = new System.Windows.Forms.Timer(this.components);
            this.messageView1 = new Tm2StagingTerminal.MessageView();
            this.receivingDetails1 = new Tm2StagingTerminal.ReceivingDetails();
            this.orderDetails1 = new Tm2StagingTerminal.OrderDetails();
            this.confirm1 = new Tm2StagingTerminal.Confirm();
            this.complete1 = new Tm2StagingTerminal.Complete();
            this.restrictedUseProductHauled1 = new Tm2StagingTerminal.RestrictedUseProductHauled();
            this.weighTruck1 = new Tm2StagingTerminal.WeighTruck();
            ((System.ComponentModel.ISupportInitialize)(this.pbConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTopBar)).BeginInit();
            this.SuspendLayout();
            // 
            // pbConfig
            // 
            this.pbConfig.Image = global::Tm2StagingTerminal.Properties.Resources.config_blue;
            this.pbConfig.Location = new System.Drawing.Point(964, -2);
            this.pbConfig.Name = "pbConfig";
            this.pbConfig.Size = new System.Drawing.Size(41, 50);
            this.pbConfig.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbConfig.TabIndex = 24;
            this.pbConfig.TabStop = false;
            this.pbConfig.WaitOnLoad = true;
            this.pbConfig.Click += new System.EventHandler(this.pbConfig_Click);
            // 
            // picTopBar
            // 
            this.picTopBar.BackgroundImage = global::Tm2StagingTerminal.Properties.Resources.blue_bar;
            this.picTopBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picTopBar.Location = new System.Drawing.Point(0, -2);
            this.picTopBar.Name = "picTopBar";
            this.picTopBar.Size = new System.Drawing.Size(1005, 50);
            this.picTopBar.TabIndex = 25;
            this.picTopBar.TabStop = false;
            // 
            // tmrPrintTicket
            // 
            this.tmrPrintTicket.Enabled = true;
            this.tmrPrintTicket.Interval = 1000;
            this.tmrPrintTicket.Tick += new System.EventHandler(this.tmrPrintTickets_Tick);
            // 
            // tmrTransition
            // 
            this.tmrTransition.Interval = 1;
            this.tmrTransition.Tick += new System.EventHandler(this.tmrTransition_Tick);
            // 
            // tmrMaxScreenTime
            // 
            this.tmrMaxScreenTime.Interval = 500;
            this.tmrMaxScreenTime.Tick += new System.EventHandler(this.tmrMaxScreenTime_Tick);
            // 
            // tmrUpdateScale
            // 
            this.tmrUpdateScale.Interval = 10000;
            this.tmrUpdateScale.Tick += new System.EventHandler(this.tmrUpdateScale_Tick);
            // 
            // messageView1
            // 
            this.messageView1.BackColor = System.Drawing.Color.Blue;
            this.messageView1.CallingControl = null;
            this.messageView1.Location = new System.Drawing.Point(665, 44);
            this.messageView1.Message = "";
            this.messageView1.Name = "messageView1";
            this.messageView1.Size = new System.Drawing.Size(340, 239);
            this.messageView1.TabIndex = 14;
            this.messageView1.Visible = false;
            this.messageView1.OkClicked += new System.EventHandler(this.messageView_OkClicked);
            this.messageView1.OtherClicked += new System.EventHandler(this.messageView_OtherClicked);
            // 
            // receivingDetails1
            // 
            this.receivingDetails1.Location = new System.Drawing.Point(0, 289);
            this.receivingDetails1.Name = "receivingDetails1";
            this.receivingDetails1.Size = new System.Drawing.Size(663, 700);
            this.receivingDetails1.TabIndex = 32;
            this.receivingDetails1.Title = "Receiving details";
            this.receivingDetails1.Visible = false;
            // 
            // orderDetails1
            // 
            this.orderDetails1.Location = new System.Drawing.Point(0, 245);
            this.orderDetails1.Name = "orderDetails1";
            this.orderDetails1.Size = new System.Drawing.Size(663, 700);
            this.orderDetails1.TabIndex = 31;
            this.orderDetails1.Title = "Order details";
            this.orderDetails1.Visible = false;
            // 
            // confirm1
            // 
            this.confirm1.Location = new System.Drawing.Point(0, 198);
            this.confirm1.Name = "confirm1";
            this.confirm1.Size = new System.Drawing.Size(663, 700);
            this.confirm1.TabIndex = 30;
            this.confirm1.Title = "Confirm";
            this.confirm1.Visible = false;
            this.confirm1.BackClicked += new System.EventHandler(this.confirm1_BackClicked);
            this.confirm1.OkClicked += new System.EventHandler(this.confirm1_OkClicked);
            this.confirm1.StartOverClicked += new System.EventHandler(this.StartOverClicked);
            // 
            // complete1
            // 
            this.complete1.Location = new System.Drawing.Point(0, 153);
            this.complete1.Name = "complete1";
            this.complete1.Size = new System.Drawing.Size(663, 700);
            this.complete1.TabIndex = 29;
            this.complete1.Title = "Finished";
            this.complete1.Visible = false;
            this.complete1.BackClicked += new System.EventHandler(this.complete1_BackClicked);
            this.complete1.OkClicked += new System.EventHandler(this.complete1_OkClicked);
            this.complete1.StartOverClicked += new System.EventHandler(this.StartOverClicked);
            // 
            // restrictedUseProductHauled1
            // 
            this.restrictedUseProductHauled1.Location = new System.Drawing.Point(0, 106);
            this.restrictedUseProductHauled1.Name = "restrictedUseProductHauled1";
            this.restrictedUseProductHauled1.Size = new System.Drawing.Size(663, 400);
            this.restrictedUseProductHauled1.TabIndex = 28;
            this.restrictedUseProductHauled1.Title = "Restricted Use Protein Products";
            this.restrictedUseProductHauled1.Visible = false;
            // 
            // weighTruck1
            // 
            this.weighTruck1.BackColor = System.Drawing.Color.White;
            this.weighTruck1.Location = new System.Drawing.Point(0, 54);
            this.weighTruck1.Name = "weighTruck1";
            this.weighTruck1.Size = new System.Drawing.Size(663, 411);
            this.weighTruck1.TabIndex = 26;
            this.weighTruck1.Title = "Record Empty Weight";
            this.weighTruck1.Unit = KahlerAutomation.KaTm2Database.KaUnit.Unit.Pounds;
            this.weighTruck1.Visible = false;
            this.weighTruck1.Weight = 0D;
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.messageView1);
            this.Controls.Add(this.pbConfig);
            this.Controls.Add(this.picTopBar);
            this.Controls.Add(this.receivingDetails1);
            this.Controls.Add(this.orderDetails1);
            this.Controls.Add(this.confirm1);
            this.Controls.Add(this.complete1);
            this.Controls.Add(this.restrictedUseProductHauled1);
            this.Controls.Add(this.weighTruck1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Origination: Staging Terminal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pbConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTopBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.PictureBox pbConfig;
        private System.Windows.Forms.PictureBox picTopBar;
        private System.Windows.Forms.Timer tmrPrintTicket;
        private System.Windows.Forms.Timer tmrTransition;
        private System.Windows.Forms.Timer tmrMaxScreenTime;
        private MessageView messageView1;
        private WeighTruck weighTruck1;
        internal System.Windows.Forms.Timer tmrUpdateScale;
        private RestrictedUseProductHauled restrictedUseProductHauled1;
        private Complete complete1;
        private Confirm confirm1;
        private OrderDetails orderDetails1;
        private ReceivingDetails receivingDetails1;
    }
}

