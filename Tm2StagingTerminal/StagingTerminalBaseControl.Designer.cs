﻿namespace Tm2StagingTerminal {
    partial class StagingTerminalBaseControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.buttonBar = new System.Windows.Forms.Panel();
            this.btnStartOver = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.titleBar = new KahlerAutomation.TitleBar();
            this.buttonBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonBar
            // 
            this.buttonBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonBar.Controls.Add(this.btnStartOver);
            this.buttonBar.Controls.Add(this.btnOk);
            this.buttonBar.Controls.Add(this.btnBack);
            this.buttonBar.Location = new System.Drawing.Point(0, 50);
            this.buttonBar.Name = "buttonBar";
            this.buttonBar.Size = new System.Drawing.Size(1020, 50);
            this.buttonBar.TabIndex = 7;
            // 
            // btnStartOver
            // 
            this.btnStartOver.AutoSize = true;
            this.btnStartOver.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnStartOver.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnStartOver.Location = new System.Drawing.Point(3, 3);
            this.btnStartOver.Name = "btnStartOver";
            this.btnStartOver.Size = new System.Drawing.Size(150, 44);
            this.btnStartOver.TabIndex = 0;
            this.btnStartOver.Text = "Start Over";
            this.btnStartOver.UseVisualStyleBackColor = true;
            this.btnStartOver.Click += new System.EventHandler(this.btnStartOver_Click);
            // 
            // btnOk
            // 
            this.btnOk.AutoSize = true;
            this.btnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnOk.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnOk.Location = new System.Drawing.Point(867, 3);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(150, 44);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnBack
            // 
            this.btnBack.AutoSize = true;
            this.btnBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnBack.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnBack.Location = new System.Drawing.Point(159, 3);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(150, 44);
            this.btnBack.TabIndex = 1;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // titleBar
            // 
            this.titleBar.Location = new System.Drawing.Point(0, 0);
            this.titleBar.MaximumSize = new System.Drawing.Size(65536, 50);
            this.titleBar.MinWidthOfTitleBar = 500D;
            this.titleBar.Name = "titleBar";
            this.titleBar.Size = new System.Drawing.Size(1020, 50);
            this.titleBar.TabIndex = 6;
            this.titleBar.Title = "Title";
            // 
            // StagingTerminalBaseControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.buttonBar);
            this.Controls.Add(this.titleBar);
            this.Name = "StagingTerminalBaseControl";
            this.Size = new System.Drawing.Size(1020, 700);
            this.Resize += new System.EventHandler(this.StagingTerminalBaseControl_Resize);
            this.buttonBar.ResumeLayout(false);
            this.buttonBar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel buttonBar;
        internal System.Windows.Forms.Button btnStartOver;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button btnBack;
        internal KahlerAutomation.TitleBar titleBar;
    }
}
