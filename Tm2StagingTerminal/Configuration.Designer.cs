﻿namespace Tm2StagingTerminal {
    partial class Configuration {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Configuration));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tpGeneral = new System.Windows.Forms.TabPage();
            this.cbxOutboundOptionsEnabled = new System.Windows.Forms.CheckBox();
            this.cbxInboundOptionsEnabled = new System.Windows.Forms.CheckBox();
            this.clbOutboundOptions = new KahlerAutomation.CheckedListBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.clbInboundOptions = new KahlerAutomation.CheckedListBox();
            this.nudDefaultMaxWeightForNewTransports = new System.Windows.Forms.NumericUpDown();
            this.lblDefaultMaxWeightForNewTransports = new System.Windows.Forms.Label();
            this.gpbScale = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblPanel = new System.Windows.Forms.Label();
            this.cmbPanel = new System.Windows.Forms.ComboBox();
            this.lblScaleCommType = new System.Windows.Forms.Label();
            this.cmbScaleCommType = new System.Windows.Forms.ComboBox();
            this.lblScaleDataStreamType = new System.Windows.Forms.Label();
            this.cmbScaleDataStreamType = new System.Windows.Forms.ComboBox();
            this.nudMaxOnScreenTime = new System.Windows.Forms.NumericUpDown();
            this.lblMaxOnScreenTime = new System.Windows.Forms.Label();
            this.cmbUser = new System.Windows.Forms.ComboBox();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblUserError = new System.Windows.Forms.Label();
            this.tbxAlphaNumericScalar = new System.Windows.Forms.TextBox();
            this.lblAlphanumericKeyboardScalar = new System.Windows.Forms.Label();
            this.tbxNumericScalar = new System.Windows.Forms.TextBox();
            this.lblNumericKeyboardScalar = new System.Windows.Forms.Label();
            this.lblOptions = new System.Windows.Forms.Label();
            this.clbOptions = new KahlerAutomation.CheckedListBox();
            this.cmbDefaultVolumeUnit = new System.Windows.Forms.ComboBox();
            this.lblDefaultVolumeUnit = new System.Windows.Forms.Label();
            this.cmbDefaultMassUnit = new System.Windows.Forms.ComboBox();
            this.lblDefaultMassUnit = new System.Windows.Forms.Label();
            this.tpTicketPrinters = new System.Windows.Forms.TabPage();
            this.gbpReceivingProductTicketPrinters = new System.Windows.Forms.GroupBox();
            this.lvReceivingProductTicketPrinters = new System.Windows.Forms.ListView();
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblReceivingProductTicketCopies = new System.Windows.Forms.Label();
            this.nudReceivingProductTicketCopies = new System.Windows.Forms.NumericUpDown();
            this.gpbOutboundTicketPrinters = new System.Windows.Forms.GroupBox();
            this.lvOutboundTicketPrinters = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblOutboundTicketCopies = new System.Windows.Forms.Label();
            this.nudOutboundTicketCopies = new System.Windows.Forms.NumericUpDown();
            this.tpPickTickets = new System.Windows.Forms.TabPage();
            this.gpbInboundTruckInspection = new System.Windows.Forms.GroupBox();
            this.tbxInboundTruckInspectionWebAddress = new System.Windows.Forms.TextBox();
            this.lblInboundTruckInspectionWebAddress = new System.Windows.Forms.Label();
            this.lvInboundTruckInspectionPrinters = new System.Windows.Forms.ListView();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblInboundTruckInspectionCopies = new System.Windows.Forms.Label();
            this.nudInboundTruckInspectionCopies = new System.Windows.Forms.NumericUpDown();
            this.gpbReceivingProductPickTicketPrinters = new System.Windows.Forms.GroupBox();
            this.tbxReceivingProductPickTicketWebAddress = new System.Windows.Forms.TextBox();
            this.lblReceivingProductPickTicketWebAddress = new System.Windows.Forms.Label();
            this.lvReceivingProductPickTicketPrinters = new System.Windows.Forms.ListView();
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblReceivingProductPickTicketCopies = new System.Windows.Forms.Label();
            this.nudReceivingProductPickTicketCopies = new System.Windows.Forms.NumericUpDown();
            this.gpbInboundBaggedProductPrinters = new System.Windows.Forms.GroupBox();
            this.tbxInboundBaggedProductTicketWebAddress = new System.Windows.Forms.TextBox();
            this.lblInboundBaggedProductTicketWebAddress = new System.Windows.Forms.Label();
            this.lvInboundBaggedProductPrinters = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblInboundBaggedProductCopies = new System.Windows.Forms.Label();
            this.nudInboundBaggedProductCopies = new System.Windows.Forms.NumericUpDown();
            this.gpbInboundBulkProductPrinters = new System.Windows.Forms.GroupBox();
            this.tbxInboundBulkProductTicketWebAddress = new System.Windows.Forms.TextBox();
            this.lblInboundBulkProductTicketWebAddress = new System.Windows.Forms.Label();
            this.lvInboundBulkProductPrinters = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblInboundBulkProductCopies = new System.Windows.Forms.Label();
            this.nudInboundBulkProductCopies = new System.Windows.Forms.NumericUpDown();
            this.tpOwners = new System.Windows.Forms.TabPage();
            this.gpbOwnersThatShouldNotDisplayMaplewoodAsShipTo = new System.Windows.Forms.GroupBox();
            this.cblOwnersThatShouldNotDisplayMaplewoodAsShipTo = new KahlerAutomation.CheckedListBox();
            this.gpbPromptForShipToDestinationOwners = new System.Windows.Forms.GroupBox();
            this.cblPromptForShipToDestinationOwners = new KahlerAutomation.CheckedListBox();
            this.gpbOwnersRequiringRupForm = new System.Windows.Forms.GroupBox();
            this.cblOwnersRequiringRupForm = new KahlerAutomation.CheckedListBox();
            this.tpBayDirections = new System.Windows.Forms.TabPage();
            this.gpbBaySettings = new System.Windows.Forms.GroupBox();
            this.chkBayAvailableForReceivingOrders = new System.Windows.Forms.CheckBox();
            this.chkBayAvailableForLoadoutOrders = new System.Windows.Forms.CheckBox();
            this.tbcBayItemAvailability = new System.Windows.Forms.TabControl();
            this.tbpBulkIngredients = new System.Windows.Forms.TabPage();
            this.pnlBayDirectionsBulkIngredientOptions = new System.Windows.Forms.Panel();
            this.rbBayDirectionsBulkIngredientUsePanelBulkProducts = new System.Windows.Forms.RadioButton();
            this.rbBayDirectionsBulkIngredientUseList = new System.Windows.Forms.RadioButton();
            this.rbBayDirectionsBulkIngredientAllowAll = new System.Windows.Forms.RadioButton();
            this.pnlBayBulkIngredientsCheckAll = new System.Windows.Forms.Panel();
            this.cmdBayBulkIngredientsCheckAll = new System.Windows.Forms.Button();
            this.cmdBayBulkIngredientsUncheckAll = new System.Windows.Forms.Button();
            this.pnlBayBulkIngredientsAvailable = new System.Windows.Forms.FlowLayoutPanel();
            this.tbpProducts = new System.Windows.Forms.TabPage();
            this.pnlBayProductsUncheckAll = new System.Windows.Forms.Panel();
            this.cmdBayProductsCheckAll = new System.Windows.Forms.Button();
            this.cmdBayProductsUncheckAll = new System.Windows.Forms.Button();
            this.chkBayAllowAllProducts = new System.Windows.Forms.CheckBox();
            this.pnlBayProductsAvailable = new System.Windows.Forms.FlowLayoutPanel();
            this.tbpCustomers = new System.Windows.Forms.TabPage();
            this.pnlBayCustomerCheckAll = new System.Windows.Forms.Panel();
            this.cmdBayCustomerCheckAll = new System.Windows.Forms.Button();
            this.cmdBayCustomerUncheckAll = new System.Windows.Forms.Button();
            this.chkBayAllowAllCustomers = new System.Windows.Forms.CheckBox();
            this.pnlBayCustomersAvailable = new System.Windows.Forms.FlowLayoutPanel();
            this.tbpOwners = new System.Windows.Forms.TabPage();
            this.pnlBayOwnerCheckAll = new System.Windows.Forms.Panel();
            this.cmdBayOwnerCheckAll = new System.Windows.Forms.Button();
            this.cmdBayOwnerUncheckAll = new System.Windows.Forms.Button();
            this.chkBayAllowAllOwners = new System.Windows.Forms.CheckBox();
            this.pnlBayOwnersAvailable = new System.Windows.Forms.FlowLayoutPanel();
            this.tbpDrivers = new System.Windows.Forms.TabPage();
            this.pnlBayDriverCheckAll = new System.Windows.Forms.Panel();
            this.cmdBayDriverCheckAll = new System.Windows.Forms.Button();
            this.cmdBayDriverUncheckAll = new System.Windows.Forms.Button();
            this.chkBayAllowAllDrivers = new System.Windows.Forms.CheckBox();
            this.pnlBayDriversAvailable = new System.Windows.Forms.FlowLayoutPanel();
            this.tbpCarriers = new System.Windows.Forms.TabPage();
            this.pnlBayCarriersCheckAll = new System.Windows.Forms.Panel();
            this.cmdBayCarrierCheckAll = new System.Windows.Forms.Button();
            this.cmdBayCarrierUncheckAll = new System.Windows.Forms.Button();
            this.chkBayAllowAllCarriers = new System.Windows.Forms.CheckBox();
            this.pnlBayCarriersAvailable = new System.Windows.Forms.FlowLayoutPanel();
            this.tbpTransports = new System.Windows.Forms.TabPage();
            this.pnlBayTransportCheckAll = new System.Windows.Forms.Panel();
            this.cmdBayTransportCheckAll = new System.Windows.Forms.Button();
            this.cmdBayTransportUncheckAll = new System.Windows.Forms.Button();
            this.chkBayAllowAllTransports = new System.Windows.Forms.CheckBox();
            this.pnlBayTransportsAvailable = new System.Windows.Forms.FlowLayoutPanel();
            this.chkBayEnabled = new System.Windows.Forms.CheckBox();
            this.cmbBay = new System.Windows.Forms.ComboBox();
            this.lblBay = new System.Windows.Forms.Label();
            this.chkBayDirectionEnabled = new System.Windows.Forms.CheckBox();
            this.tpCompleteScreen = new System.Windows.Forms.TabPage();
            this.gpbOrderCompleteInboundOrder = new System.Windows.Forms.GroupBox();
            this.gpbOrderCompleteInboundOrderBayNotAssigned = new System.Windows.Forms.GroupBox();
            this.tbxOrderCompleteInboundOrderBayNotAssignedNoTickets = new System.Windows.Forms.TextBox();
            this.lblOrderCompleteInboundOrderBayNotAssignedNoTickets = new System.Windows.Forms.Label();
            this.tbxOrderCompleteInboundOrderBayNotAssignedWithTickets = new System.Windows.Forms.TextBox();
            this.lblOrderCompleteInboundOrderBayNotAssignedWithTickets = new System.Windows.Forms.Label();
            this.gpbOrderCompleteInboundOrderBayAssigned = new System.Windows.Forms.GroupBox();
            this.tbxOrderCompleteInboundOrderBayAssignedNoTickets = new System.Windows.Forms.TextBox();
            this.lblOrderCompleteInboundOrderBayAssignedNoTickets = new System.Windows.Forms.Label();
            this.tbxOrderCompleteInboundOrderBayAssignedWithTickets = new System.Windows.Forms.TextBox();
            this.lblOrderCompleteInboundOrderBayAssignedWithTickets = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.lblTm2DatabaseVersion = new System.Windows.Forms.Label();
            this.lblTm2StagingTerminalversion = new System.Windows.Forms.Label();
            this.gpbOrderCompleteInboundReceiving = new System.Windows.Forms.GroupBox();
            this.gpbOrderCompleteInboundReceivingBayNotAssigned = new System.Windows.Forms.GroupBox();
            this.tbxOrderCompleteInboundReceivingBayNotAssignedNoTickets = new System.Windows.Forms.TextBox();
            this.lblOrderCompleteInboundReceivingBayNotAssignedNoTickets = new System.Windows.Forms.Label();
            this.tbxOrderCompleteInboundReceivingBayNotAssignedHasTickets = new System.Windows.Forms.TextBox();
            this.lblOrderCompleteInboundReceivingBayNotAssignedHasTickets = new System.Windows.Forms.Label();
            this.gpbOrderCompleteInboundReceivingBayAssigned = new System.Windows.Forms.GroupBox();
            this.tbxOrderCompleteInboundReceivingBayAssignedNoTickets = new System.Windows.Forms.TextBox();
            this.lblOrderCompleteInboundReceivingBayAssignedNoTickets = new System.Windows.Forms.Label();
            this.tbxOrderCompleteInboundReceivingBayAssignedHasTickets = new System.Windows.Forms.TextBox();
            this.lblOrderCompleteInboundReceivingBayAssignedHasTickets = new System.Windows.Forms.Label();
            this.gpbOrderCompleteOutbound = new System.Windows.Forms.GroupBox();
            this.tbxOrderCompleteOutboundNoTickets = new System.Windows.Forms.TextBox();
            this.lblOrderCompleteOutboundNoTickets = new System.Windows.Forms.Label();
            this.tbxOrderCompleteOutboundHasTickets = new System.Windows.Forms.TextBox();
            this.lblOrderCompleteOutboundHasTickets = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.tpGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDefaultMaxWeightForNewTransports)).BeginInit();
            this.gpbScale.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxOnScreenTime)).BeginInit();
            this.tpTicketPrinters.SuspendLayout();
            this.gbpReceivingProductTicketPrinters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudReceivingProductTicketCopies)).BeginInit();
            this.gpbOutboundTicketPrinters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudOutboundTicketCopies)).BeginInit();
            this.tpPickTickets.SuspendLayout();
            this.gpbInboundTruckInspection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudInboundTruckInspectionCopies)).BeginInit();
            this.gpbReceivingProductPickTicketPrinters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudReceivingProductPickTicketCopies)).BeginInit();
            this.gpbInboundBaggedProductPrinters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudInboundBaggedProductCopies)).BeginInit();
            this.gpbInboundBulkProductPrinters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudInboundBulkProductCopies)).BeginInit();
            this.tpOwners.SuspendLayout();
            this.gpbOwnersThatShouldNotDisplayMaplewoodAsShipTo.SuspendLayout();
            this.gpbPromptForShipToDestinationOwners.SuspendLayout();
            this.gpbOwnersRequiringRupForm.SuspendLayout();
            this.tpBayDirections.SuspendLayout();
            this.gpbBaySettings.SuspendLayout();
            this.tbcBayItemAvailability.SuspendLayout();
            this.tbpBulkIngredients.SuspendLayout();
            this.pnlBayDirectionsBulkIngredientOptions.SuspendLayout();
            this.pnlBayBulkIngredientsCheckAll.SuspendLayout();
            this.tbpProducts.SuspendLayout();
            this.pnlBayProductsUncheckAll.SuspendLayout();
            this.tbpCustomers.SuspendLayout();
            this.pnlBayCustomerCheckAll.SuspendLayout();
            this.tbpOwners.SuspendLayout();
            this.pnlBayOwnerCheckAll.SuspendLayout();
            this.tbpDrivers.SuspendLayout();
            this.pnlBayDriverCheckAll.SuspendLayout();
            this.tbpCarriers.SuspendLayout();
            this.pnlBayCarriersCheckAll.SuspendLayout();
            this.tbpTransports.SuspendLayout();
            this.pnlBayTransportCheckAll.SuspendLayout();
            this.tpCompleteScreen.SuspendLayout();
            this.gpbOrderCompleteInboundOrder.SuspendLayout();
            this.gpbOrderCompleteInboundOrderBayNotAssigned.SuspendLayout();
            this.gpbOrderCompleteInboundOrderBayAssigned.SuspendLayout();
            this.gpbOrderCompleteInboundReceiving.SuspendLayout();
            this.gpbOrderCompleteInboundReceivingBayNotAssigned.SuspendLayout();
            this.gpbOrderCompleteInboundReceivingBayAssigned.SuspendLayout();
            this.gpbOrderCompleteOutbound.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tpGeneral);
            this.tabControl.Controls.Add(this.tpTicketPrinters);
            this.tabControl.Controls.Add(this.tpPickTickets);
            this.tabControl.Controls.Add(this.tpOwners);
            this.tabControl.Controls.Add(this.tpBayDirections);
            this.tabControl.Controls.Add(this.tpCompleteScreen);
            this.tabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1008, 580);
            this.tabControl.TabIndex = 1;
            // 
            // tpGeneral
            // 
            this.tpGeneral.Controls.Add(this.cbxOutboundOptionsEnabled);
            this.tpGeneral.Controls.Add(this.cbxInboundOptionsEnabled);
            this.tpGeneral.Controls.Add(this.clbOutboundOptions);
            this.tpGeneral.Controls.Add(this.clbInboundOptions);
            this.tpGeneral.Controls.Add(this.nudDefaultMaxWeightForNewTransports);
            this.tpGeneral.Controls.Add(this.lblDefaultMaxWeightForNewTransports);
            this.tpGeneral.Controls.Add(this.gpbScale);
            this.tpGeneral.Controls.Add(this.nudMaxOnScreenTime);
            this.tpGeneral.Controls.Add(this.lblMaxOnScreenTime);
            this.tpGeneral.Controls.Add(this.cmbUser);
            this.tpGeneral.Controls.Add(this.lblUser);
            this.tpGeneral.Controls.Add(this.lblUserError);
            this.tpGeneral.Controls.Add(this.tbxAlphaNumericScalar);
            this.tpGeneral.Controls.Add(this.lblAlphanumericKeyboardScalar);
            this.tpGeneral.Controls.Add(this.tbxNumericScalar);
            this.tpGeneral.Controls.Add(this.lblNumericKeyboardScalar);
            this.tpGeneral.Controls.Add(this.lblOptions);
            this.tpGeneral.Controls.Add(this.clbOptions);
            this.tpGeneral.Controls.Add(this.cmbDefaultVolumeUnit);
            this.tpGeneral.Controls.Add(this.lblDefaultVolumeUnit);
            this.tpGeneral.Controls.Add(this.cmbDefaultMassUnit);
            this.tpGeneral.Controls.Add(this.lblDefaultMassUnit);
            this.tpGeneral.Location = new System.Drawing.Point(4, 33);
            this.tpGeneral.Name = "tpGeneral";
            this.tpGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.tpGeneral.Size = new System.Drawing.Size(1000, 543);
            this.tpGeneral.TabIndex = 0;
            this.tpGeneral.Text = "General";
            this.tpGeneral.UseVisualStyleBackColor = true;
            // 
            // cbxOutboundOptionsEnabled
            // 
            this.cbxOutboundOptionsEnabled.AutoSize = true;
            this.cbxOutboundOptionsEnabled.Location = new System.Drawing.Point(538, 343);
            this.cbxOutboundOptionsEnabled.Name = "cbxOutboundOptionsEnabled";
            this.cbxOutboundOptionsEnabled.Size = new System.Drawing.Size(188, 28);
            this.cbxOutboundOptionsEnabled.TabIndex = 20;
            this.cbxOutboundOptionsEnabled.Text = "Outbound enabled";
            this.cbxOutboundOptionsEnabled.UseVisualStyleBackColor = true;
            this.cbxOutboundOptionsEnabled.CheckedChanged += new System.EventHandler(this.cbxOutboundOptionsEnabled_CheckedChanged);
            // 
            // cbxInboundOptionsEnabled
            // 
            this.cbxInboundOptionsEnabled.AutoSize = true;
            this.cbxInboundOptionsEnabled.Location = new System.Drawing.Point(538, 171);
            this.cbxInboundOptionsEnabled.Name = "cbxInboundOptionsEnabled";
            this.cbxInboundOptionsEnabled.Size = new System.Drawing.Size(173, 28);
            this.cbxInboundOptionsEnabled.TabIndex = 18;
            this.cbxInboundOptionsEnabled.Text = "Inbound enabled";
            this.cbxInboundOptionsEnabled.UseVisualStyleBackColor = true;
            this.cbxInboundOptionsEnabled.CheckedChanged += new System.EventHandler(this.cbxInboundOptionsEnabled_CheckedChanged);
            // 
            // clbOutboundOptions
            // 
            this.clbOutboundOptions.CheckOnClick = true;
            this.clbOutboundOptions.Enabled = false;
            this.clbOutboundOptions.FormattingEnabled = true;
            this.clbOutboundOptions.HorizontalScrollbar = true;
            this.clbOutboundOptions.Location = new System.Drawing.Point(538, 377);
            this.clbOutboundOptions.Name = "clbOutboundOptions";
            this.clbOutboundOptions.Size = new System.Drawing.Size(462, 134);
            this.clbOutboundOptions.TabIndex = 21;
            this.clbOutboundOptions.ToolTip = this.toolTip1;
            this.clbOutboundOptions.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.CheckListBoxOptionsItemCheck);
            // 
            // clbInboundOptions
            // 
            this.clbInboundOptions.CheckOnClick = true;
            this.clbInboundOptions.Enabled = false;
            this.clbInboundOptions.FormattingEnabled = true;
            this.clbInboundOptions.HorizontalScrollbar = true;
            this.clbInboundOptions.Location = new System.Drawing.Point(538, 205);
            this.clbInboundOptions.Name = "clbInboundOptions";
            this.clbInboundOptions.Size = new System.Drawing.Size(462, 134);
            this.clbInboundOptions.TabIndex = 19;
            this.clbInboundOptions.ToolTip = this.toolTip1;
            this.clbInboundOptions.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.CheckListBoxOptionsItemCheck);
            // 
            // nudDefaultMaxWeightForNewTransports
            // 
            this.nudDefaultMaxWeightForNewTransports.Location = new System.Drawing.Point(371, 269);
            this.nudDefaultMaxWeightForNewTransports.Maximum = new decimal(new int[] {
            120000,
            0,
            0,
            0});
            this.nudDefaultMaxWeightForNewTransports.Name = "nudDefaultMaxWeightForNewTransports";
            this.nudDefaultMaxWeightForNewTransports.Size = new System.Drawing.Size(76, 29);
            this.nudDefaultMaxWeightForNewTransports.TabIndex = 14;
            this.nudDefaultMaxWeightForNewTransports.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDefaultMaxWeightForNewTransports
            // 
            this.lblDefaultMaxWeightForNewTransports.AutoSize = true;
            this.lblDefaultMaxWeightForNewTransports.Location = new System.Drawing.Point(6, 269);
            this.lblDefaultMaxWeightForNewTransports.Name = "lblDefaultMaxWeightForNewTransports";
            this.lblDefaultMaxWeightForNewTransports.Size = new System.Drawing.Size(321, 24);
            this.lblDefaultMaxWeightForNewTransports.TabIndex = 13;
            this.lblDefaultMaxWeightForNewTransports.Text = "Default max weight for new transports";
            // 
            // gpbScale
            // 
            this.gpbScale.Controls.Add(this.flowLayoutPanel1);
            this.gpbScale.Location = new System.Drawing.Point(10, 304);
            this.gpbScale.Name = "gpbScale";
            this.gpbScale.Size = new System.Drawing.Size(448, 149);
            this.gpbScale.TabIndex = 15;
            this.gpbScale.TabStop = false;
            this.gpbScale.Text = "Scale";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.lblPanel);
            this.flowLayoutPanel1.Controls.Add(this.cmbPanel);
            this.flowLayoutPanel1.Controls.Add(this.lblScaleCommType);
            this.flowLayoutPanel1.Controls.Add(this.cmbScaleCommType);
            this.flowLayoutPanel1.Controls.Add(this.lblScaleDataStreamType);
            this.flowLayoutPanel1.Controls.Add(this.cmbScaleDataStreamType);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(6, 28);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(442, 121);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // lblPanel
            // 
            this.lblPanel.Location = new System.Drawing.Point(3, 3);
            this.lblPanel.Name = "lblPanel";
            this.lblPanel.Size = new System.Drawing.Size(211, 32);
            this.lblPanel.TabIndex = 0;
            this.lblPanel.Text = "Panel";
            this.lblPanel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // cmbPanel
            // 
            this.cmbPanel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPanel.FormattingEnabled = true;
            this.cmbPanel.Location = new System.Drawing.Point(220, 6);
            this.cmbPanel.Name = "cmbPanel";
            this.cmbPanel.Size = new System.Drawing.Size(216, 32);
            this.cmbPanel.TabIndex = 1;
            this.cmbPanel.SelectedIndexChanged += new System.EventHandler(this.cmbPanel_SelectedIndexChanged);
            // 
            // lblScaleCommType
            // 
            this.lblScaleCommType.Location = new System.Drawing.Point(3, 41);
            this.lblScaleCommType.Name = "lblScaleCommType";
            this.lblScaleCommType.Size = new System.Drawing.Size(211, 32);
            this.lblScaleCommType.TabIndex = 2;
            this.lblScaleCommType.Text = "Communication type";
            this.lblScaleCommType.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // cmbScaleCommType
            // 
            this.cmbScaleCommType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbScaleCommType.FormattingEnabled = true;
            this.cmbScaleCommType.Location = new System.Drawing.Point(220, 44);
            this.cmbScaleCommType.Name = "cmbScaleCommType";
            this.cmbScaleCommType.Size = new System.Drawing.Size(216, 32);
            this.cmbScaleCommType.TabIndex = 3;
            // 
            // lblScaleDataStreamType
            // 
            this.lblScaleDataStreamType.Location = new System.Drawing.Point(3, 79);
            this.lblScaleDataStreamType.Name = "lblScaleDataStreamType";
            this.lblScaleDataStreamType.Size = new System.Drawing.Size(211, 32);
            this.lblScaleDataStreamType.TabIndex = 4;
            this.lblScaleDataStreamType.Text = "Scale format";
            this.lblScaleDataStreamType.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // cmbScaleDataStreamType
            // 
            this.cmbScaleDataStreamType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbScaleDataStreamType.FormattingEnabled = true;
            this.cmbScaleDataStreamType.Location = new System.Drawing.Point(220, 82);
            this.cmbScaleDataStreamType.Name = "cmbScaleDataStreamType";
            this.cmbScaleDataStreamType.Size = new System.Drawing.Size(216, 32);
            this.cmbScaleDataStreamType.TabIndex = 5;
            // 
            // nudMaxOnScreenTime
            // 
            this.nudMaxOnScreenTime.Location = new System.Drawing.Point(371, 204);
            this.nudMaxOnScreenTime.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudMaxOnScreenTime.Name = "nudMaxOnScreenTime";
            this.nudMaxOnScreenTime.Size = new System.Drawing.Size(76, 29);
            this.nudMaxOnScreenTime.TabIndex = 12;
            this.nudMaxOnScreenTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblMaxOnScreenTime
            // 
            this.lblMaxOnScreenTime.AutoSize = true;
            this.lblMaxOnScreenTime.Location = new System.Drawing.Point(6, 204);
            this.lblMaxOnScreenTime.Name = "lblMaxOnScreenTime";
            this.lblMaxOnScreenTime.Size = new System.Drawing.Size(365, 48);
            this.lblMaxOnScreenTime.TabIndex = 11;
            this.lblMaxOnScreenTime.Text = "Maximum time for on screen display (sec)\r\n                                       " +
    "             0 to disable";
            // 
            // cmbUser
            // 
            this.cmbUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUser.FormattingEnabled = true;
            this.cmbUser.Location = new System.Drawing.Point(6, 32);
            this.cmbUser.Name = "cmbUser";
            this.cmbUser.Size = new System.Drawing.Size(526, 32);
            this.cmbUser.TabIndex = 2;
            this.cmbUser.SelectedIndexChanged += new System.EventHandler(this.cmbUser_SelectedIndexChanged);
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(6, 5);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(111, 24);
            this.lblUser.TabIndex = 0;
            this.lblUser.Text = "Run as user";
            // 
            // lblUserError
            // 
            this.lblUserError.AutoSize = true;
            this.lblUserError.ForeColor = System.Drawing.Color.Red;
            this.lblUserError.Location = new System.Drawing.Point(186, 5);
            this.lblUserError.Name = "lblUserError";
            this.lblUserError.Size = new System.Drawing.Size(196, 24);
            this.lblUserError.TabIndex = 1;
            this.lblUserError.Text = "Select a user to run as";
            this.lblUserError.Visible = false;
            // 
            // tbxAlphaNumericScalar
            // 
            this.tbxAlphaNumericScalar.Location = new System.Drawing.Point(398, 169);
            this.tbxAlphaNumericScalar.Name = "tbxAlphaNumericScalar";
            this.tbxAlphaNumericScalar.Size = new System.Drawing.Size(49, 29);
            this.tbxAlphaNumericScalar.TabIndex = 10;
            this.tbxAlphaNumericScalar.Text = "100";
            // 
            // lblAlphanumericKeyboardScalar
            // 
            this.lblAlphanumericKeyboardScalar.AutoSize = true;
            this.lblAlphanumericKeyboardScalar.Location = new System.Drawing.Point(6, 174);
            this.lblAlphanumericKeyboardScalar.Name = "lblAlphanumericKeyboardScalar";
            this.lblAlphanumericKeyboardScalar.Size = new System.Drawing.Size(392, 24);
            this.lblAlphanumericKeyboardScalar.TabIndex = 9;
            this.lblAlphanumericKeyboardScalar.Text = "Alpha numeric keyboard scalar (default 100%)";
            // 
            // tbxNumericScalar
            // 
            this.tbxNumericScalar.Location = new System.Drawing.Point(398, 134);
            this.tbxNumericScalar.Name = "tbxNumericScalar";
            this.tbxNumericScalar.Size = new System.Drawing.Size(49, 29);
            this.tbxNumericScalar.TabIndex = 8;
            this.tbxNumericScalar.Text = "100";
            // 
            // lblNumericKeyboardScalar
            // 
            this.lblNumericKeyboardScalar.AutoSize = true;
            this.lblNumericKeyboardScalar.Location = new System.Drawing.Point(6, 137);
            this.lblNumericKeyboardScalar.Name = "lblNumericKeyboardScalar";
            this.lblNumericKeyboardScalar.Size = new System.Drawing.Size(341, 24);
            this.lblNumericKeyboardScalar.TabIndex = 7;
            this.lblNumericKeyboardScalar.Text = "Numeric keyboard scalar (default 100%)";
            // 
            // lblOptions
            // 
            this.lblOptions.AutoSize = true;
            this.lblOptions.Location = new System.Drawing.Point(538, 5);
            this.lblOptions.Name = "lblOptions";
            this.lblOptions.Size = new System.Drawing.Size(75, 24);
            this.lblOptions.TabIndex = 16;
            this.lblOptions.Text = "Options";
            // 
            // clbOptions
            // 
            this.clbOptions.CheckOnClick = true;
            this.clbOptions.FormattingEnabled = true;
            this.clbOptions.HorizontalScrollbar = true;
            this.clbOptions.Location = new System.Drawing.Point(538, 32);
            this.clbOptions.Name = "clbOptions";
            this.clbOptions.Size = new System.Drawing.Size(462, 134);
            this.clbOptions.TabIndex = 17;
            this.clbOptions.ToolTip = this.toolTip1;
            this.clbOptions.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.CheckListBoxOptionsItemCheck);
            // 
            // cmbDefaultVolumeUnit
            // 
            this.cmbDefaultVolumeUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultVolumeUnit.FormattingEnabled = true;
            this.cmbDefaultVolumeUnit.Location = new System.Drawing.Point(231, 102);
            this.cmbDefaultVolumeUnit.Name = "cmbDefaultVolumeUnit";
            this.cmbDefaultVolumeUnit.Size = new System.Drawing.Size(216, 32);
            this.cmbDefaultVolumeUnit.TabIndex = 6;
            // 
            // lblDefaultVolumeUnit
            // 
            this.lblDefaultVolumeUnit.AutoSize = true;
            this.lblDefaultVolumeUnit.Location = new System.Drawing.Point(228, 75);
            this.lblDefaultVolumeUnit.Name = "lblDefaultVolumeUnit";
            this.lblDefaultVolumeUnit.Size = new System.Drawing.Size(169, 24);
            this.lblDefaultVolumeUnit.TabIndex = 5;
            this.lblDefaultVolumeUnit.Text = "Default volume unit";
            // 
            // cmbDefaultMassUnit
            // 
            this.cmbDefaultMassUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultMassUnit.FormattingEnabled = true;
            this.cmbDefaultMassUnit.Location = new System.Drawing.Point(6, 102);
            this.cmbDefaultMassUnit.Name = "cmbDefaultMassUnit";
            this.cmbDefaultMassUnit.Size = new System.Drawing.Size(216, 32);
            this.cmbDefaultMassUnit.TabIndex = 4;
            // 
            // lblDefaultMassUnit
            // 
            this.lblDefaultMassUnit.AutoSize = true;
            this.lblDefaultMassUnit.Location = new System.Drawing.Point(6, 75);
            this.lblDefaultMassUnit.Name = "lblDefaultMassUnit";
            this.lblDefaultMassUnit.Size = new System.Drawing.Size(151, 24);
            this.lblDefaultMassUnit.TabIndex = 3;
            this.lblDefaultMassUnit.Text = "Default mass unit";
            // 
            // tpTicketPrinters
            // 
            this.tpTicketPrinters.Controls.Add(this.gbpReceivingProductTicketPrinters);
            this.tpTicketPrinters.Controls.Add(this.gpbOutboundTicketPrinters);
            this.tpTicketPrinters.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpTicketPrinters.Location = new System.Drawing.Point(4, 33);
            this.tpTicketPrinters.Name = "tpTicketPrinters";
            this.tpTicketPrinters.Size = new System.Drawing.Size(1000, 543);
            this.tpTicketPrinters.TabIndex = 1;
            this.tpTicketPrinters.Text = "Ticket printers";
            this.tpTicketPrinters.UseVisualStyleBackColor = true;
            // 
            // gbpReceivingProductTicketPrinters
            // 
            this.gbpReceivingProductTicketPrinters.Controls.Add(this.lvReceivingProductTicketPrinters);
            this.gbpReceivingProductTicketPrinters.Controls.Add(this.lblReceivingProductTicketCopies);
            this.gbpReceivingProductTicketPrinters.Controls.Add(this.nudReceivingProductTicketCopies);
            this.gbpReceivingProductTicketPrinters.Location = new System.Drawing.Point(476, 3);
            this.gbpReceivingProductTicketPrinters.Name = "gbpReceivingProductTicketPrinters";
            this.gbpReceivingProductTicketPrinters.Size = new System.Drawing.Size(467, 215);
            this.gbpReceivingProductTicketPrinters.TabIndex = 5;
            this.gbpReceivingProductTicketPrinters.TabStop = false;
            this.gbpReceivingProductTicketPrinters.Text = "Receiving Product Ticket";
            // 
            // lvReceivingProductTicketPrinters
            // 
            this.lvReceivingProductTicketPrinters.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9,
            this.columnHeader10});
            this.lvReceivingProductTicketPrinters.FullRowSelect = true;
            this.lvReceivingProductTicketPrinters.GridLines = true;
            this.lvReceivingProductTicketPrinters.HideSelection = false;
            this.lvReceivingProductTicketPrinters.Location = new System.Drawing.Point(6, 28);
            this.lvReceivingProductTicketPrinters.MultiSelect = false;
            this.lvReceivingProductTicketPrinters.Name = "lvReceivingProductTicketPrinters";
            this.lvReceivingProductTicketPrinters.Size = new System.Drawing.Size(452, 151);
            this.lvReceivingProductTicketPrinters.TabIndex = 0;
            this.lvReceivingProductTicketPrinters.UseCompatibleStateImageBehavior = false;
            this.lvReceivingProductTicketPrinters.View = System.Windows.Forms.View.Details;
            this.lvReceivingProductTicketPrinters.SelectedIndexChanged += new System.EventHandler(this.lvPrinters_SelectedIndexChanged);
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Printer";
            this.columnHeader9.Width = 303;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Copies";
            this.columnHeader10.Width = 110;
            // 
            // lblReceivingProductTicketCopies
            // 
            this.lblReceivingProductTicketCopies.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblReceivingProductTicketCopies.AutoSize = true;
            this.lblReceivingProductTicketCopies.Location = new System.Drawing.Point(246, 187);
            this.lblReceivingProductTicketCopies.Name = "lblReceivingProductTicketCopies";
            this.lblReceivingProductTicketCopies.Size = new System.Drawing.Size(55, 18);
            this.lblReceivingProductTicketCopies.TabIndex = 1;
            this.lblReceivingProductTicketCopies.Text = "Copies";
            // 
            // nudReceivingProductTicketCopies
            // 
            this.nudReceivingProductTicketCopies.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.nudReceivingProductTicketCopies.Enabled = false;
            this.nudReceivingProductTicketCopies.Location = new System.Drawing.Point(321, 185);
            this.nudReceivingProductTicketCopies.Name = "nudReceivingProductTicketCopies";
            this.nudReceivingProductTicketCopies.Size = new System.Drawing.Size(137, 24);
            this.nudReceivingProductTicketCopies.TabIndex = 2;
            this.nudReceivingProductTicketCopies.ValueChanged += new System.EventHandler(this.nudPrinterCopies_ValueChanged);
            // 
            // gpbOutboundTicketPrinters
            // 
            this.gpbOutboundTicketPrinters.Controls.Add(this.lvOutboundTicketPrinters);
            this.gpbOutboundTicketPrinters.Controls.Add(this.lblOutboundTicketCopies);
            this.gpbOutboundTicketPrinters.Controls.Add(this.nudOutboundTicketCopies);
            this.gpbOutboundTicketPrinters.Location = new System.Drawing.Point(3, 3);
            this.gpbOutboundTicketPrinters.Name = "gpbOutboundTicketPrinters";
            this.gpbOutboundTicketPrinters.Size = new System.Drawing.Size(467, 215);
            this.gpbOutboundTicketPrinters.TabIndex = 4;
            this.gpbOutboundTicketPrinters.TabStop = false;
            this.gpbOutboundTicketPrinters.Text = "Outbound Ticket";
            // 
            // lvOutboundTicketPrinters
            // 
            this.lvOutboundTicketPrinters.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader8});
            this.lvOutboundTicketPrinters.FullRowSelect = true;
            this.lvOutboundTicketPrinters.GridLines = true;
            this.lvOutboundTicketPrinters.HideSelection = false;
            this.lvOutboundTicketPrinters.Location = new System.Drawing.Point(6, 28);
            this.lvOutboundTicketPrinters.MultiSelect = false;
            this.lvOutboundTicketPrinters.Name = "lvOutboundTicketPrinters";
            this.lvOutboundTicketPrinters.Size = new System.Drawing.Size(452, 151);
            this.lvOutboundTicketPrinters.TabIndex = 0;
            this.lvOutboundTicketPrinters.UseCompatibleStateImageBehavior = false;
            this.lvOutboundTicketPrinters.View = System.Windows.Forms.View.Details;
            this.lvOutboundTicketPrinters.SelectedIndexChanged += new System.EventHandler(this.lvPrinters_SelectedIndexChanged);
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Printer";
            this.columnHeader7.Width = 303;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Copies";
            this.columnHeader8.Width = 110;
            // 
            // lblOutboundTicketCopies
            // 
            this.lblOutboundTicketCopies.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOutboundTicketCopies.AutoSize = true;
            this.lblOutboundTicketCopies.Location = new System.Drawing.Point(249, 187);
            this.lblOutboundTicketCopies.Name = "lblOutboundTicketCopies";
            this.lblOutboundTicketCopies.Size = new System.Drawing.Size(55, 18);
            this.lblOutboundTicketCopies.TabIndex = 1;
            this.lblOutboundTicketCopies.Text = "Copies";
            // 
            // nudOutboundTicketCopies
            // 
            this.nudOutboundTicketCopies.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.nudOutboundTicketCopies.Enabled = false;
            this.nudOutboundTicketCopies.Location = new System.Drawing.Point(324, 185);
            this.nudOutboundTicketCopies.Name = "nudOutboundTicketCopies";
            this.nudOutboundTicketCopies.Size = new System.Drawing.Size(137, 24);
            this.nudOutboundTicketCopies.TabIndex = 2;
            this.nudOutboundTicketCopies.ValueChanged += new System.EventHandler(this.nudPrinterCopies_ValueChanged);
            // 
            // tpPickTickets
            // 
            this.tpPickTickets.Controls.Add(this.gpbInboundTruckInspection);
            this.tpPickTickets.Controls.Add(this.gpbReceivingProductPickTicketPrinters);
            this.tpPickTickets.Controls.Add(this.gpbInboundBaggedProductPrinters);
            this.tpPickTickets.Controls.Add(this.gpbInboundBulkProductPrinters);
            this.tpPickTickets.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.tpPickTickets.Location = new System.Drawing.Point(4, 33);
            this.tpPickTickets.Name = "tpPickTickets";
            this.tpPickTickets.Size = new System.Drawing.Size(1000, 543);
            this.tpPickTickets.TabIndex = 3;
            this.tpPickTickets.Text = "Pick tickets";
            this.tpPickTickets.UseVisualStyleBackColor = true;
            // 
            // gpbInboundTruckInspection
            // 
            this.gpbInboundTruckInspection.Controls.Add(this.tbxInboundTruckInspectionWebAddress);
            this.gpbInboundTruckInspection.Controls.Add(this.lblInboundTruckInspectionWebAddress);
            this.gpbInboundTruckInspection.Controls.Add(this.lvInboundTruckInspectionPrinters);
            this.gpbInboundTruckInspection.Controls.Add(this.lblInboundTruckInspectionCopies);
            this.gpbInboundTruckInspection.Controls.Add(this.nudInboundTruckInspectionCopies);
            this.gpbInboundTruckInspection.Location = new System.Drawing.Point(482, 3);
            this.gpbInboundTruckInspection.Name = "gpbInboundTruckInspection";
            this.gpbInboundTruckInspection.Size = new System.Drawing.Size(467, 215);
            this.gpbInboundTruckInspection.TabIndex = 1;
            this.gpbInboundTruckInspection.TabStop = false;
            this.gpbInboundTruckInspection.Text = "Inbound Truck Inspection";
            // 
            // tbxInboundTruckInspectionWebAddress
            // 
            this.tbxInboundTruckInspectionWebAddress.Location = new System.Drawing.Point(74, 160);
            this.tbxInboundTruckInspectionWebAddress.Multiline = true;
            this.tbxInboundTruckInspectionWebAddress.Name = "tbxInboundTruckInspectionWebAddress";
            this.tbxInboundTruckInspectionWebAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbxInboundTruckInspectionWebAddress.Size = new System.Drawing.Size(387, 49);
            this.tbxInboundTruckInspectionWebAddress.TabIndex = 4;
            // 
            // lblInboundTruckInspectionWebAddress
            // 
            this.lblInboundTruckInspectionWebAddress.AutoSize = true;
            this.lblInboundTruckInspectionWebAddress.Location = new System.Drawing.Point(6, 163);
            this.lblInboundTruckInspectionWebAddress.Name = "lblInboundTruckInspectionWebAddress";
            this.lblInboundTruckInspectionWebAddress.Size = new System.Drawing.Size(62, 18);
            this.lblInboundTruckInspectionWebAddress.TabIndex = 3;
            this.lblInboundTruckInspectionWebAddress.Text = "Address";
            // 
            // lvInboundTruckInspectionPrinters
            // 
            this.lvInboundTruckInspectionPrinters.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader5});
            this.lvInboundTruckInspectionPrinters.FullRowSelect = true;
            this.lvInboundTruckInspectionPrinters.GridLines = true;
            this.lvInboundTruckInspectionPrinters.HideSelection = false;
            this.lvInboundTruckInspectionPrinters.Location = new System.Drawing.Point(6, 28);
            this.lvInboundTruckInspectionPrinters.MultiSelect = false;
            this.lvInboundTruckInspectionPrinters.Name = "lvInboundTruckInspectionPrinters";
            this.lvInboundTruckInspectionPrinters.Size = new System.Drawing.Size(452, 96);
            this.lvInboundTruckInspectionPrinters.TabIndex = 0;
            this.lvInboundTruckInspectionPrinters.UseCompatibleStateImageBehavior = false;
            this.lvInboundTruckInspectionPrinters.View = System.Windows.Forms.View.Details;
            this.lvInboundTruckInspectionPrinters.SelectedIndexChanged += new System.EventHandler(this.lvPrinters_SelectedIndexChanged);
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Printer";
            this.columnHeader4.Width = 303;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Copies";
            this.columnHeader5.Width = 110;
            // 
            // lblInboundTruckInspectionCopies
            // 
            this.lblInboundTruckInspectionCopies.AutoSize = true;
            this.lblInboundTruckInspectionCopies.Location = new System.Drawing.Point(249, 132);
            this.lblInboundTruckInspectionCopies.Name = "lblInboundTruckInspectionCopies";
            this.lblInboundTruckInspectionCopies.Size = new System.Drawing.Size(55, 18);
            this.lblInboundTruckInspectionCopies.TabIndex = 1;
            this.lblInboundTruckInspectionCopies.Text = "Copies";
            // 
            // nudInboundTruckInspectionCopies
            // 
            this.nudInboundTruckInspectionCopies.Enabled = false;
            this.nudInboundTruckInspectionCopies.Location = new System.Drawing.Point(324, 130);
            this.nudInboundTruckInspectionCopies.Name = "nudInboundTruckInspectionCopies";
            this.nudInboundTruckInspectionCopies.Size = new System.Drawing.Size(137, 24);
            this.nudInboundTruckInspectionCopies.TabIndex = 2;
            this.nudInboundTruckInspectionCopies.ValueChanged += new System.EventHandler(this.nudPrinterCopies_ValueChanged);
            // 
            // gpbReceivingProductPickTicketPrinters
            // 
            this.gpbReceivingProductPickTicketPrinters.Controls.Add(this.tbxReceivingProductPickTicketWebAddress);
            this.gpbReceivingProductPickTicketPrinters.Controls.Add(this.lblReceivingProductPickTicketWebAddress);
            this.gpbReceivingProductPickTicketPrinters.Controls.Add(this.lvReceivingProductPickTicketPrinters);
            this.gpbReceivingProductPickTicketPrinters.Controls.Add(this.lblReceivingProductPickTicketCopies);
            this.gpbReceivingProductPickTicketPrinters.Controls.Add(this.nudReceivingProductPickTicketCopies);
            this.gpbReceivingProductPickTicketPrinters.Location = new System.Drawing.Point(9, 224);
            this.gpbReceivingProductPickTicketPrinters.Name = "gpbReceivingProductPickTicketPrinters";
            this.gpbReceivingProductPickTicketPrinters.Size = new System.Drawing.Size(467, 215);
            this.gpbReceivingProductPickTicketPrinters.TabIndex = 2;
            this.gpbReceivingProductPickTicketPrinters.TabStop = false;
            this.gpbReceivingProductPickTicketPrinters.Text = "Receiving Product Pick Ticket";
            // 
            // tbxReceivingProductPickTicketWebAddress
            // 
            this.tbxReceivingProductPickTicketWebAddress.Location = new System.Drawing.Point(74, 160);
            this.tbxReceivingProductPickTicketWebAddress.Multiline = true;
            this.tbxReceivingProductPickTicketWebAddress.Name = "tbxReceivingProductPickTicketWebAddress";
            this.tbxReceivingProductPickTicketWebAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbxReceivingProductPickTicketWebAddress.Size = new System.Drawing.Size(387, 49);
            this.tbxReceivingProductPickTicketWebAddress.TabIndex = 4;
            // 
            // lblReceivingProductPickTicketWebAddress
            // 
            this.lblReceivingProductPickTicketWebAddress.AutoSize = true;
            this.lblReceivingProductPickTicketWebAddress.Location = new System.Drawing.Point(6, 163);
            this.lblReceivingProductPickTicketWebAddress.Name = "lblReceivingProductPickTicketWebAddress";
            this.lblReceivingProductPickTicketWebAddress.Size = new System.Drawing.Size(62, 18);
            this.lblReceivingProductPickTicketWebAddress.TabIndex = 3;
            this.lblReceivingProductPickTicketWebAddress.Text = "Address";
            // 
            // lvReceivingProductPickTicketPrinters
            // 
            this.lvReceivingProductPickTicketPrinters.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader11,
            this.columnHeader12});
            this.lvReceivingProductPickTicketPrinters.FullRowSelect = true;
            this.lvReceivingProductPickTicketPrinters.GridLines = true;
            this.lvReceivingProductPickTicketPrinters.HideSelection = false;
            this.lvReceivingProductPickTicketPrinters.Location = new System.Drawing.Point(6, 28);
            this.lvReceivingProductPickTicketPrinters.MultiSelect = false;
            this.lvReceivingProductPickTicketPrinters.Name = "lvReceivingProductPickTicketPrinters";
            this.lvReceivingProductPickTicketPrinters.Size = new System.Drawing.Size(452, 96);
            this.lvReceivingProductPickTicketPrinters.TabIndex = 0;
            this.lvReceivingProductPickTicketPrinters.UseCompatibleStateImageBehavior = false;
            this.lvReceivingProductPickTicketPrinters.View = System.Windows.Forms.View.Details;
            this.lvReceivingProductPickTicketPrinters.SelectedIndexChanged += new System.EventHandler(this.lvPrinters_SelectedIndexChanged);
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Printer";
            this.columnHeader11.Width = 303;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Copies";
            this.columnHeader12.Width = 110;
            // 
            // lblReceivingProductPickTicketCopies
            // 
            this.lblReceivingProductPickTicketCopies.AutoSize = true;
            this.lblReceivingProductPickTicketCopies.Location = new System.Drawing.Point(249, 132);
            this.lblReceivingProductPickTicketCopies.Name = "lblReceivingProductPickTicketCopies";
            this.lblReceivingProductPickTicketCopies.Size = new System.Drawing.Size(55, 18);
            this.lblReceivingProductPickTicketCopies.TabIndex = 1;
            this.lblReceivingProductPickTicketCopies.Text = "Copies";
            // 
            // nudReceivingProductPickTicketCopies
            // 
            this.nudReceivingProductPickTicketCopies.Enabled = false;
            this.nudReceivingProductPickTicketCopies.Location = new System.Drawing.Point(324, 130);
            this.nudReceivingProductPickTicketCopies.Name = "nudReceivingProductPickTicketCopies";
            this.nudReceivingProductPickTicketCopies.Size = new System.Drawing.Size(137, 24);
            this.nudReceivingProductPickTicketCopies.TabIndex = 2;
            this.nudReceivingProductPickTicketCopies.ValueChanged += new System.EventHandler(this.nudPrinterCopies_ValueChanged);
            // 
            // gpbInboundBaggedProductPrinters
            // 
            this.gpbInboundBaggedProductPrinters.Controls.Add(this.tbxInboundBaggedProductTicketWebAddress);
            this.gpbInboundBaggedProductPrinters.Controls.Add(this.lblInboundBaggedProductTicketWebAddress);
            this.gpbInboundBaggedProductPrinters.Controls.Add(this.lvInboundBaggedProductPrinters);
            this.gpbInboundBaggedProductPrinters.Controls.Add(this.lblInboundBaggedProductCopies);
            this.gpbInboundBaggedProductPrinters.Controls.Add(this.nudInboundBaggedProductCopies);
            this.gpbInboundBaggedProductPrinters.Location = new System.Drawing.Point(488, 224);
            this.gpbInboundBaggedProductPrinters.Name = "gpbInboundBaggedProductPrinters";
            this.gpbInboundBaggedProductPrinters.Size = new System.Drawing.Size(467, 215);
            this.gpbInboundBaggedProductPrinters.TabIndex = 3;
            this.gpbInboundBaggedProductPrinters.TabStop = false;
            this.gpbInboundBaggedProductPrinters.Text = "Inbound Bagged Product Pick Ticket";
            // 
            // tbxInboundBaggedProductTicketWebAddress
            // 
            this.tbxInboundBaggedProductTicketWebAddress.Location = new System.Drawing.Point(74, 160);
            this.tbxInboundBaggedProductTicketWebAddress.Multiline = true;
            this.tbxInboundBaggedProductTicketWebAddress.Name = "tbxInboundBaggedProductTicketWebAddress";
            this.tbxInboundBaggedProductTicketWebAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbxInboundBaggedProductTicketWebAddress.Size = new System.Drawing.Size(387, 49);
            this.tbxInboundBaggedProductTicketWebAddress.TabIndex = 4;
            // 
            // lblInboundBaggedProductTicketWebAddress
            // 
            this.lblInboundBaggedProductTicketWebAddress.AutoSize = true;
            this.lblInboundBaggedProductTicketWebAddress.Location = new System.Drawing.Point(6, 163);
            this.lblInboundBaggedProductTicketWebAddress.Name = "lblInboundBaggedProductTicketWebAddress";
            this.lblInboundBaggedProductTicketWebAddress.Size = new System.Drawing.Size(62, 18);
            this.lblInboundBaggedProductTicketWebAddress.TabIndex = 3;
            this.lblInboundBaggedProductTicketWebAddress.Text = "Address";
            // 
            // lvInboundBaggedProductPrinters
            // 
            this.lvInboundBaggedProductPrinters.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lvInboundBaggedProductPrinters.FullRowSelect = true;
            this.lvInboundBaggedProductPrinters.GridLines = true;
            this.lvInboundBaggedProductPrinters.HideSelection = false;
            this.lvInboundBaggedProductPrinters.Location = new System.Drawing.Point(6, 28);
            this.lvInboundBaggedProductPrinters.MultiSelect = false;
            this.lvInboundBaggedProductPrinters.Name = "lvInboundBaggedProductPrinters";
            this.lvInboundBaggedProductPrinters.Size = new System.Drawing.Size(452, 96);
            this.lvInboundBaggedProductPrinters.TabIndex = 0;
            this.lvInboundBaggedProductPrinters.UseCompatibleStateImageBehavior = false;
            this.lvInboundBaggedProductPrinters.View = System.Windows.Forms.View.Details;
            this.lvInboundBaggedProductPrinters.SelectedIndexChanged += new System.EventHandler(this.lvPrinters_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Printer";
            this.columnHeader1.Width = 303;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Copies";
            this.columnHeader2.Width = 110;
            // 
            // lblInboundBaggedProductCopies
            // 
            this.lblInboundBaggedProductCopies.AutoSize = true;
            this.lblInboundBaggedProductCopies.Location = new System.Drawing.Point(249, 132);
            this.lblInboundBaggedProductCopies.Name = "lblInboundBaggedProductCopies";
            this.lblInboundBaggedProductCopies.Size = new System.Drawing.Size(55, 18);
            this.lblInboundBaggedProductCopies.TabIndex = 1;
            this.lblInboundBaggedProductCopies.Text = "Copies";
            // 
            // nudInboundBaggedProductCopies
            // 
            this.nudInboundBaggedProductCopies.Enabled = false;
            this.nudInboundBaggedProductCopies.Location = new System.Drawing.Point(324, 130);
            this.nudInboundBaggedProductCopies.Name = "nudInboundBaggedProductCopies";
            this.nudInboundBaggedProductCopies.Size = new System.Drawing.Size(137, 24);
            this.nudInboundBaggedProductCopies.TabIndex = 2;
            this.nudInboundBaggedProductCopies.ValueChanged += new System.EventHandler(this.nudPrinterCopies_ValueChanged);
            // 
            // gpbInboundBulkProductPrinters
            // 
            this.gpbInboundBulkProductPrinters.Controls.Add(this.tbxInboundBulkProductTicketWebAddress);
            this.gpbInboundBulkProductPrinters.Controls.Add(this.lblInboundBulkProductTicketWebAddress);
            this.gpbInboundBulkProductPrinters.Controls.Add(this.lvInboundBulkProductPrinters);
            this.gpbInboundBulkProductPrinters.Controls.Add(this.lblInboundBulkProductCopies);
            this.gpbInboundBulkProductPrinters.Controls.Add(this.nudInboundBulkProductCopies);
            this.gpbInboundBulkProductPrinters.Location = new System.Drawing.Point(9, 3);
            this.gpbInboundBulkProductPrinters.Name = "gpbInboundBulkProductPrinters";
            this.gpbInboundBulkProductPrinters.Size = new System.Drawing.Size(467, 215);
            this.gpbInboundBulkProductPrinters.TabIndex = 0;
            this.gpbInboundBulkProductPrinters.TabStop = false;
            this.gpbInboundBulkProductPrinters.Text = "Inbound Bulk Product Pick Ticket";
            // 
            // tbxInboundBulkProductTicketWebAddress
            // 
            this.tbxInboundBulkProductTicketWebAddress.Location = new System.Drawing.Point(74, 160);
            this.tbxInboundBulkProductTicketWebAddress.Multiline = true;
            this.tbxInboundBulkProductTicketWebAddress.Name = "tbxInboundBulkProductTicketWebAddress";
            this.tbxInboundBulkProductTicketWebAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbxInboundBulkProductTicketWebAddress.Size = new System.Drawing.Size(387, 49);
            this.tbxInboundBulkProductTicketWebAddress.TabIndex = 4;
            // 
            // lblInboundBulkProductTicketWebAddress
            // 
            this.lblInboundBulkProductTicketWebAddress.AutoSize = true;
            this.lblInboundBulkProductTicketWebAddress.Location = new System.Drawing.Point(6, 163);
            this.lblInboundBulkProductTicketWebAddress.Name = "lblInboundBulkProductTicketWebAddress";
            this.lblInboundBulkProductTicketWebAddress.Size = new System.Drawing.Size(62, 18);
            this.lblInboundBulkProductTicketWebAddress.TabIndex = 3;
            this.lblInboundBulkProductTicketWebAddress.Text = "Address";
            // 
            // lvInboundBulkProductPrinters
            // 
            this.lvInboundBulkProductPrinters.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader6});
            this.lvInboundBulkProductPrinters.FullRowSelect = true;
            this.lvInboundBulkProductPrinters.GridLines = true;
            this.lvInboundBulkProductPrinters.HideSelection = false;
            this.lvInboundBulkProductPrinters.Location = new System.Drawing.Point(6, 28);
            this.lvInboundBulkProductPrinters.MultiSelect = false;
            this.lvInboundBulkProductPrinters.Name = "lvInboundBulkProductPrinters";
            this.lvInboundBulkProductPrinters.Size = new System.Drawing.Size(452, 96);
            this.lvInboundBulkProductPrinters.TabIndex = 0;
            this.lvInboundBulkProductPrinters.UseCompatibleStateImageBehavior = false;
            this.lvInboundBulkProductPrinters.View = System.Windows.Forms.View.Details;
            this.lvInboundBulkProductPrinters.SelectedIndexChanged += new System.EventHandler(this.lvPrinters_SelectedIndexChanged);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Printer";
            this.columnHeader3.Width = 303;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Copies";
            this.columnHeader6.Width = 110;
            // 
            // lblInboundBulkProductCopies
            // 
            this.lblInboundBulkProductCopies.AutoSize = true;
            this.lblInboundBulkProductCopies.Location = new System.Drawing.Point(249, 132);
            this.lblInboundBulkProductCopies.Name = "lblInboundBulkProductCopies";
            this.lblInboundBulkProductCopies.Size = new System.Drawing.Size(55, 18);
            this.lblInboundBulkProductCopies.TabIndex = 1;
            this.lblInboundBulkProductCopies.Text = "Copies";
            // 
            // nudInboundBulkProductCopies
            // 
            this.nudInboundBulkProductCopies.Enabled = false;
            this.nudInboundBulkProductCopies.Location = new System.Drawing.Point(324, 130);
            this.nudInboundBulkProductCopies.Name = "nudInboundBulkProductCopies";
            this.nudInboundBulkProductCopies.Size = new System.Drawing.Size(137, 24);
            this.nudInboundBulkProductCopies.TabIndex = 2;
            this.nudInboundBulkProductCopies.ValueChanged += new System.EventHandler(this.nudPrinterCopies_ValueChanged);
            // 
            // tpOwners
            // 
            this.tpOwners.Controls.Add(this.gpbOwnersThatShouldNotDisplayMaplewoodAsShipTo);
            this.tpOwners.Controls.Add(this.gpbPromptForShipToDestinationOwners);
            this.tpOwners.Controls.Add(this.gpbOwnersRequiringRupForm);
            this.tpOwners.Location = new System.Drawing.Point(4, 33);
            this.tpOwners.Name = "tpOwners";
            this.tpOwners.Size = new System.Drawing.Size(1000, 543);
            this.tpOwners.TabIndex = 2;
            this.tpOwners.Text = "Owners";
            this.tpOwners.UseVisualStyleBackColor = true;
            // 
            // gpbOwnersThatShouldNotDisplayMaplewoodAsShipTo
            // 
            this.gpbOwnersThatShouldNotDisplayMaplewoodAsShipTo.Controls.Add(this.cblOwnersThatShouldNotDisplayMaplewoodAsShipTo);
            this.gpbOwnersThatShouldNotDisplayMaplewoodAsShipTo.Location = new System.Drawing.Point(4, 260);
            this.gpbOwnersThatShouldNotDisplayMaplewoodAsShipTo.Name = "gpbOwnersThatShouldNotDisplayMaplewoodAsShipTo";
            this.gpbOwnersThatShouldNotDisplayMaplewoodAsShipTo.Size = new System.Drawing.Size(496, 250);
            this.gpbOwnersThatShouldNotDisplayMaplewoodAsShipTo.TabIndex = 11;
            this.gpbOwnersThatShouldNotDisplayMaplewoodAsShipTo.TabStop = false;
            this.gpbOwnersThatShouldNotDisplayMaplewoodAsShipTo.Text = "Owners that should not display Maplewood as ship to";
            // 
            // cblOwnersThatShouldNotDisplayMaplewoodAsShipTo
            // 
            this.cblOwnersThatShouldNotDisplayMaplewoodAsShipTo.CheckOnClick = true;
            this.cblOwnersThatShouldNotDisplayMaplewoodAsShipTo.FormattingEnabled = true;
            this.cblOwnersThatShouldNotDisplayMaplewoodAsShipTo.HorizontalScrollbar = true;
            this.cblOwnersThatShouldNotDisplayMaplewoodAsShipTo.Location = new System.Drawing.Point(6, 28);
            this.cblOwnersThatShouldNotDisplayMaplewoodAsShipTo.Name = "cblOwnersThatShouldNotDisplayMaplewoodAsShipTo";
            this.cblOwnersThatShouldNotDisplayMaplewoodAsShipTo.Size = new System.Drawing.Size(484, 212);
            this.cblOwnersThatShouldNotDisplayMaplewoodAsShipTo.TabIndex = 9;
            this.cblOwnersThatShouldNotDisplayMaplewoodAsShipTo.ToolTip = this.toolTip1;
            // 
            // gpbPromptForShipToDestinationOwners
            // 
            this.gpbPromptForShipToDestinationOwners.Controls.Add(this.cblPromptForShipToDestinationOwners);
            this.gpbPromptForShipToDestinationOwners.Location = new System.Drawing.Point(501, 4);
            this.gpbPromptForShipToDestinationOwners.Name = "gpbPromptForShipToDestinationOwners";
            this.gpbPromptForShipToDestinationOwners.Size = new System.Drawing.Size(496, 250);
            this.gpbPromptForShipToDestinationOwners.TabIndex = 10;
            this.gpbPromptForShipToDestinationOwners.TabStop = false;
            this.gpbPromptForShipToDestinationOwners.Text = "Owners require prompt for ship to destination";
            // 
            // cblPromptForShipToDestinationOwners
            // 
            this.cblPromptForShipToDestinationOwners.CheckOnClick = true;
            this.cblPromptForShipToDestinationOwners.FormattingEnabled = true;
            this.cblPromptForShipToDestinationOwners.HorizontalScrollbar = true;
            this.cblPromptForShipToDestinationOwners.Location = new System.Drawing.Point(6, 28);
            this.cblPromptForShipToDestinationOwners.Name = "cblPromptForShipToDestinationOwners";
            this.cblPromptForShipToDestinationOwners.Size = new System.Drawing.Size(484, 212);
            this.cblPromptForShipToDestinationOwners.TabIndex = 9;
            this.cblPromptForShipToDestinationOwners.ToolTip = this.toolTip1;
            // 
            // gpbOwnersRequiringRupForm
            // 
            this.gpbOwnersRequiringRupForm.Controls.Add(this.cblOwnersRequiringRupForm);
            this.gpbOwnersRequiringRupForm.Location = new System.Drawing.Point(4, 4);
            this.gpbOwnersRequiringRupForm.Name = "gpbOwnersRequiringRupForm";
            this.gpbOwnersRequiringRupForm.Size = new System.Drawing.Size(496, 250);
            this.gpbOwnersRequiringRupForm.TabIndex = 0;
            this.gpbOwnersRequiringRupForm.TabStop = false;
            this.gpbOwnersRequiringRupForm.Text = "Owners requiring RUP form";
            // 
            // cblOwnersRequiringRupForm
            // 
            this.cblOwnersRequiringRupForm.CheckOnClick = true;
            this.cblOwnersRequiringRupForm.FormattingEnabled = true;
            this.cblOwnersRequiringRupForm.HorizontalScrollbar = true;
            this.cblOwnersRequiringRupForm.Location = new System.Drawing.Point(6, 28);
            this.cblOwnersRequiringRupForm.Name = "cblOwnersRequiringRupForm";
            this.cblOwnersRequiringRupForm.Size = new System.Drawing.Size(484, 212);
            this.cblOwnersRequiringRupForm.TabIndex = 9;
            this.cblOwnersRequiringRupForm.ToolTip = this.toolTip1;
            // 
            // tpBayDirections
            // 
            this.tpBayDirections.Controls.Add(this.gpbBaySettings);
            this.tpBayDirections.Controls.Add(this.chkBayDirectionEnabled);
            this.tpBayDirections.Location = new System.Drawing.Point(4, 33);
            this.tpBayDirections.Name = "tpBayDirections";
            this.tpBayDirections.Size = new System.Drawing.Size(1000, 543);
            this.tpBayDirections.TabIndex = 4;
            this.tpBayDirections.Text = "Bay directions";
            this.tpBayDirections.UseVisualStyleBackColor = true;
            // 
            // gpbBaySettings
            // 
            this.gpbBaySettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpbBaySettings.Controls.Add(this.chkBayAvailableForReceivingOrders);
            this.gpbBaySettings.Controls.Add(this.chkBayAvailableForLoadoutOrders);
            this.gpbBaySettings.Controls.Add(this.tbcBayItemAvailability);
            this.gpbBaySettings.Controls.Add(this.chkBayEnabled);
            this.gpbBaySettings.Controls.Add(this.cmbBay);
            this.gpbBaySettings.Controls.Add(this.lblBay);
            this.gpbBaySettings.Location = new System.Drawing.Point(3, 37);
            this.gpbBaySettings.Name = "gpbBaySettings";
            this.gpbBaySettings.Size = new System.Drawing.Size(992, 500);
            this.gpbBaySettings.TabIndex = 1;
            this.gpbBaySettings.TabStop = false;
            this.gpbBaySettings.Text = "Bay settings";
            // 
            // chkBayAvailableForReceivingOrders
            // 
            this.chkBayAvailableForReceivingOrders.AutoSize = true;
            this.chkBayAvailableForReceivingOrders.Checked = true;
            this.chkBayAvailableForReceivingOrders.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBayAvailableForReceivingOrders.Location = new System.Drawing.Point(703, 30);
            this.chkBayAvailableForReceivingOrders.Name = "chkBayAvailableForReceivingOrders";
            this.chkBayAvailableForReceivingOrders.Size = new System.Drawing.Size(272, 28);
            this.chkBayAvailableForReceivingOrders.TabIndex = 4;
            this.chkBayAvailableForReceivingOrders.Text = "Available for receiving orders";
            this.chkBayAvailableForReceivingOrders.UseVisualStyleBackColor = true;
            this.chkBayAvailableForReceivingOrders.CheckedChanged += new System.EventHandler(this.chkBayAvailableForReceivingOrders_CheckedChanged);
            // 
            // chkBayAvailableForLoadoutOrders
            // 
            this.chkBayAvailableForLoadoutOrders.AutoSize = true;
            this.chkBayAvailableForLoadoutOrders.Checked = true;
            this.chkBayAvailableForLoadoutOrders.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBayAvailableForLoadoutOrders.Location = new System.Drawing.Point(440, 30);
            this.chkBayAvailableForLoadoutOrders.Name = "chkBayAvailableForLoadoutOrders";
            this.chkBayAvailableForLoadoutOrders.Size = new System.Drawing.Size(257, 28);
            this.chkBayAvailableForLoadoutOrders.TabIndex = 3;
            this.chkBayAvailableForLoadoutOrders.Text = "Available for loadout orders";
            this.chkBayAvailableForLoadoutOrders.UseVisualStyleBackColor = true;
            this.chkBayAvailableForLoadoutOrders.CheckedChanged += new System.EventHandler(this.chkBayAvailableForLoadoutOrders_CheckedChanged);
            // 
            // tbcBayItemAvailability
            // 
            this.tbcBayItemAvailability.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbcBayItemAvailability.Controls.Add(this.tbpBulkIngredients);
            this.tbcBayItemAvailability.Controls.Add(this.tbpProducts);
            this.tbcBayItemAvailability.Controls.Add(this.tbpCustomers);
            this.tbcBayItemAvailability.Controls.Add(this.tbpOwners);
            this.tbcBayItemAvailability.Controls.Add(this.tbpDrivers);
            this.tbcBayItemAvailability.Controls.Add(this.tbpCarriers);
            this.tbcBayItemAvailability.Controls.Add(this.tbpTransports);
            this.tbcBayItemAvailability.Location = new System.Drawing.Point(6, 73);
            this.tbcBayItemAvailability.Name = "tbcBayItemAvailability";
            this.tbcBayItemAvailability.SelectedIndex = 0;
            this.tbcBayItemAvailability.Size = new System.Drawing.Size(980, 405);
            this.tbcBayItemAvailability.TabIndex = 5;
            // 
            // tbpBulkIngredients
            // 
            this.tbpBulkIngredients.Controls.Add(this.pnlBayDirectionsBulkIngredientOptions);
            this.tbpBulkIngredients.Controls.Add(this.pnlBayBulkIngredientsCheckAll);
            this.tbpBulkIngredients.Controls.Add(this.pnlBayBulkIngredientsAvailable);
            this.tbpBulkIngredients.Location = new System.Drawing.Point(4, 33);
            this.tbpBulkIngredients.Name = "tbpBulkIngredients";
            this.tbpBulkIngredients.Padding = new System.Windows.Forms.Padding(3);
            this.tbpBulkIngredients.Size = new System.Drawing.Size(972, 368);
            this.tbpBulkIngredients.TabIndex = 0;
            this.tbpBulkIngredients.Tag = "";
            this.tbpBulkIngredients.Text = "Bulk ingredients";
            this.tbpBulkIngredients.UseVisualStyleBackColor = true;
            // 
            // pnlBayDirectionsBulkIngredientOptions
            // 
            this.pnlBayDirectionsBulkIngredientOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlBayDirectionsBulkIngredientOptions.Controls.Add(this.rbBayDirectionsBulkIngredientUsePanelBulkProducts);
            this.pnlBayDirectionsBulkIngredientOptions.Controls.Add(this.rbBayDirectionsBulkIngredientUseList);
            this.pnlBayDirectionsBulkIngredientOptions.Controls.Add(this.rbBayDirectionsBulkIngredientAllowAll);
            this.pnlBayDirectionsBulkIngredientOptions.Location = new System.Drawing.Point(7, 2);
            this.pnlBayDirectionsBulkIngredientOptions.Name = "pnlBayDirectionsBulkIngredientOptions";
            this.pnlBayDirectionsBulkIngredientOptions.Size = new System.Drawing.Size(959, 32);
            this.pnlBayDirectionsBulkIngredientOptions.TabIndex = 0;
            // 
            // rbBayDirectionsBulkIngredientUsePanelBulkProducts
            // 
            this.rbBayDirectionsBulkIngredientUsePanelBulkProducts.AutoSize = true;
            this.rbBayDirectionsBulkIngredientUsePanelBulkProducts.Checked = true;
            this.rbBayDirectionsBulkIngredientUsePanelBulkProducts.Location = new System.Drawing.Point(379, 2);
            this.rbBayDirectionsBulkIngredientUsePanelBulkProducts.Name = "rbBayDirectionsBulkIngredientUsePanelBulkProducts";
            this.rbBayDirectionsBulkIngredientUsePanelBulkProducts.Size = new System.Drawing.Size(477, 28);
            this.rbBayDirectionsBulkIngredientUsePanelBulkProducts.TabIndex = 2;
            this.rbBayDirectionsBulkIngredientUsePanelBulkProducts.TabStop = true;
            this.rbBayDirectionsBulkIngredientUsePanelBulkProducts.Text = "Use bulk products assigned to panels assigned to bay";
            this.rbBayDirectionsBulkIngredientUsePanelBulkProducts.UseVisualStyleBackColor = true;
            this.rbBayDirectionsBulkIngredientUsePanelBulkProducts.CheckedChanged += new System.EventHandler(this.chkBayAllowAllItems_CheckedChanged);
            // 
            // rbBayDirectionsBulkIngredientUseList
            // 
            this.rbBayDirectionsBulkIngredientUseList.AutoSize = true;
            this.rbBayDirectionsBulkIngredientUseList.Location = new System.Drawing.Point(230, 2);
            this.rbBayDirectionsBulkIngredientUseList.Name = "rbBayDirectionsBulkIngredientUseList";
            this.rbBayDirectionsBulkIngredientUseList.Size = new System.Drawing.Size(143, 28);
            this.rbBayDirectionsBulkIngredientUseList.TabIndex = 1;
            this.rbBayDirectionsBulkIngredientUseList.Text = "Use list below";
            this.rbBayDirectionsBulkIngredientUseList.UseVisualStyleBackColor = true;
            this.rbBayDirectionsBulkIngredientUseList.CheckedChanged += new System.EventHandler(this.chkBayAllowAllItems_CheckedChanged);
            // 
            // rbBayDirectionsBulkIngredientAllowAll
            // 
            this.rbBayDirectionsBulkIngredientAllowAll.AutoSize = true;
            this.rbBayDirectionsBulkIngredientAllowAll.Location = new System.Drawing.Point(4, 2);
            this.rbBayDirectionsBulkIngredientAllowAll.Name = "rbBayDirectionsBulkIngredientAllowAll";
            this.rbBayDirectionsBulkIngredientAllowAll.Size = new System.Drawing.Size(220, 28);
            this.rbBayDirectionsBulkIngredientAllowAll.TabIndex = 0;
            this.rbBayDirectionsBulkIngredientAllowAll.Text = "Allow All Bulk Products";
            this.rbBayDirectionsBulkIngredientAllowAll.UseVisualStyleBackColor = true;
            this.rbBayDirectionsBulkIngredientAllowAll.CheckedChanged += new System.EventHandler(this.chkBayAllowAllItems_CheckedChanged);
            // 
            // pnlBayBulkIngredientsCheckAll
            // 
            this.pnlBayBulkIngredientsCheckAll.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pnlBayBulkIngredientsCheckAll.Controls.Add(this.cmdBayBulkIngredientsCheckAll);
            this.pnlBayBulkIngredientsCheckAll.Controls.Add(this.cmdBayBulkIngredientsUncheckAll);
            this.pnlBayBulkIngredientsCheckAll.Location = new System.Drawing.Point(346, 326);
            this.pnlBayBulkIngredientsCheckAll.Margin = new System.Windows.Forms.Padding(0);
            this.pnlBayBulkIngredientsCheckAll.Name = "pnlBayBulkIngredientsCheckAll";
            this.pnlBayBulkIngredientsCheckAll.Size = new System.Drawing.Size(280, 40);
            this.pnlBayBulkIngredientsCheckAll.TabIndex = 2;
            // 
            // cmdBayBulkIngredientsCheckAll
            // 
            this.cmdBayBulkIngredientsCheckAll.Image = ((System.Drawing.Image)(resources.GetObject("cmdBayBulkIngredientsCheckAll.Image")));
            this.cmdBayBulkIngredientsCheckAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdBayBulkIngredientsCheckAll.Location = new System.Drawing.Point(2, 1);
            this.cmdBayBulkIngredientsCheckAll.Name = "cmdBayBulkIngredientsCheckAll";
            this.cmdBayBulkIngredientsCheckAll.Size = new System.Drawing.Size(134, 38);
            this.cmdBayBulkIngredientsCheckAll.TabIndex = 0;
            this.cmdBayBulkIngredientsCheckAll.Text = "Check all";
            this.cmdBayBulkIngredientsCheckAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdBayBulkIngredientsCheckAll.UseVisualStyleBackColor = true;
            this.cmdBayBulkIngredientsCheckAll.Click += new System.EventHandler(this.cmdBayItemsCheckAll_Click);
            // 
            // cmdBayBulkIngredientsUncheckAll
            // 
            this.cmdBayBulkIngredientsUncheckAll.Image = ((System.Drawing.Image)(resources.GetObject("cmdBayBulkIngredientsUncheckAll.Image")));
            this.cmdBayBulkIngredientsUncheckAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdBayBulkIngredientsUncheckAll.Location = new System.Drawing.Point(144, 1);
            this.cmdBayBulkIngredientsUncheckAll.Name = "cmdBayBulkIngredientsUncheckAll";
            this.cmdBayBulkIngredientsUncheckAll.Size = new System.Drawing.Size(134, 38);
            this.cmdBayBulkIngredientsUncheckAll.TabIndex = 1;
            this.cmdBayBulkIngredientsUncheckAll.Text = "Uncheck all";
            this.cmdBayBulkIngredientsUncheckAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdBayBulkIngredientsUncheckAll.UseVisualStyleBackColor = true;
            this.cmdBayBulkIngredientsUncheckAll.Click += new System.EventHandler(this.cmdBayItemsUncheckAll_Click);
            // 
            // pnlBayBulkIngredientsAvailable
            // 
            this.pnlBayBulkIngredientsAvailable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlBayBulkIngredientsAvailable.AutoScroll = true;
            this.pnlBayBulkIngredientsAvailable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlBayBulkIngredientsAvailable.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.pnlBayBulkIngredientsAvailable.Location = new System.Drawing.Point(7, 38);
            this.pnlBayBulkIngredientsAvailable.Name = "pnlBayBulkIngredientsAvailable";
            this.pnlBayBulkIngredientsAvailable.Size = new System.Drawing.Size(959, 278);
            this.pnlBayBulkIngredientsAvailable.TabIndex = 1;
            this.pnlBayBulkIngredientsAvailable.Tag = "";
            // 
            // tbpProducts
            // 
            this.tbpProducts.Controls.Add(this.pnlBayProductsUncheckAll);
            this.tbpProducts.Controls.Add(this.chkBayAllowAllProducts);
            this.tbpProducts.Controls.Add(this.pnlBayProductsAvailable);
            this.tbpProducts.Location = new System.Drawing.Point(4, 22);
            this.tbpProducts.Name = "tbpProducts";
            this.tbpProducts.Padding = new System.Windows.Forms.Padding(3);
            this.tbpProducts.Size = new System.Drawing.Size(972, 379);
            this.tbpProducts.TabIndex = 1;
            this.tbpProducts.Text = "Products";
            this.tbpProducts.UseVisualStyleBackColor = true;
            // 
            // pnlBayProductsUncheckAll
            // 
            this.pnlBayProductsUncheckAll.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pnlBayProductsUncheckAll.Controls.Add(this.cmdBayProductsCheckAll);
            this.pnlBayProductsUncheckAll.Controls.Add(this.cmdBayProductsUncheckAll);
            this.pnlBayProductsUncheckAll.Location = new System.Drawing.Point(346, 337);
            this.pnlBayProductsUncheckAll.Name = "pnlBayProductsUncheckAll";
            this.pnlBayProductsUncheckAll.Size = new System.Drawing.Size(280, 40);
            this.pnlBayProductsUncheckAll.TabIndex = 18;
            // 
            // cmdBayProductsCheckAll
            // 
            this.cmdBayProductsCheckAll.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmdBayProductsCheckAll.Image = ((System.Drawing.Image)(resources.GetObject("cmdBayProductsCheckAll.Image")));
            this.cmdBayProductsCheckAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdBayProductsCheckAll.Location = new System.Drawing.Point(2, 1);
            this.cmdBayProductsCheckAll.Name = "cmdBayProductsCheckAll";
            this.cmdBayProductsCheckAll.Size = new System.Drawing.Size(134, 38);
            this.cmdBayProductsCheckAll.TabIndex = 13;
            this.cmdBayProductsCheckAll.Text = "Check all";
            this.cmdBayProductsCheckAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdBayProductsCheckAll.UseVisualStyleBackColor = true;
            this.cmdBayProductsCheckAll.Click += new System.EventHandler(this.cmdBayItemsCheckAll_Click);
            // 
            // cmdBayProductsUncheckAll
            // 
            this.cmdBayProductsUncheckAll.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmdBayProductsUncheckAll.Image = ((System.Drawing.Image)(resources.GetObject("cmdBayProductsUncheckAll.Image")));
            this.cmdBayProductsUncheckAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdBayProductsUncheckAll.Location = new System.Drawing.Point(144, 1);
            this.cmdBayProductsUncheckAll.Name = "cmdBayProductsUncheckAll";
            this.cmdBayProductsUncheckAll.Size = new System.Drawing.Size(134, 38);
            this.cmdBayProductsUncheckAll.TabIndex = 14;
            this.cmdBayProductsUncheckAll.Text = "Uncheck all";
            this.cmdBayProductsUncheckAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdBayProductsUncheckAll.UseVisualStyleBackColor = true;
            this.cmdBayProductsUncheckAll.Click += new System.EventHandler(this.cmdBayItemsUncheckAll_Click);
            // 
            // chkBayAllowAllProducts
            // 
            this.chkBayAllowAllProducts.AutoSize = true;
            this.chkBayAllowAllProducts.Checked = true;
            this.chkBayAllowAllProducts.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBayAllowAllProducts.Location = new System.Drawing.Point(6, 6);
            this.chkBayAllowAllProducts.Name = "chkBayAllowAllProducts";
            this.chkBayAllowAllProducts.Size = new System.Drawing.Size(176, 28);
            this.chkBayAllowAllProducts.TabIndex = 17;
            this.chkBayAllowAllProducts.Text = "Allow all products";
            this.chkBayAllowAllProducts.UseVisualStyleBackColor = true;
            this.chkBayAllowAllProducts.CheckedChanged += new System.EventHandler(this.chkBayAllowAllItems_CheckedChanged);
            // 
            // pnlBayProductsAvailable
            // 
            this.pnlBayProductsAvailable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlBayProductsAvailable.AutoScroll = true;
            this.pnlBayProductsAvailable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlBayProductsAvailable.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.pnlBayProductsAvailable.Location = new System.Drawing.Point(6, 40);
            this.pnlBayProductsAvailable.Name = "pnlBayProductsAvailable";
            this.pnlBayProductsAvailable.Size = new System.Drawing.Size(959, 289);
            this.pnlBayProductsAvailable.TabIndex = 16;
            this.pnlBayProductsAvailable.Tag = "";
            // 
            // tbpCustomers
            // 
            this.tbpCustomers.Controls.Add(this.pnlBayCustomerCheckAll);
            this.tbpCustomers.Controls.Add(this.chkBayAllowAllCustomers);
            this.tbpCustomers.Controls.Add(this.pnlBayCustomersAvailable);
            this.tbpCustomers.Location = new System.Drawing.Point(4, 22);
            this.tbpCustomers.Name = "tbpCustomers";
            this.tbpCustomers.Size = new System.Drawing.Size(972, 379);
            this.tbpCustomers.TabIndex = 2;
            this.tbpCustomers.Text = "Customers/Suppliers";
            this.tbpCustomers.UseVisualStyleBackColor = true;
            // 
            // pnlBayCustomerCheckAll
            // 
            this.pnlBayCustomerCheckAll.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pnlBayCustomerCheckAll.Controls.Add(this.cmdBayCustomerCheckAll);
            this.pnlBayCustomerCheckAll.Controls.Add(this.cmdBayCustomerUncheckAll);
            this.pnlBayCustomerCheckAll.Location = new System.Drawing.Point(346, 337);
            this.pnlBayCustomerCheckAll.Name = "pnlBayCustomerCheckAll";
            this.pnlBayCustomerCheckAll.Size = new System.Drawing.Size(280, 40);
            this.pnlBayCustomerCheckAll.TabIndex = 18;
            // 
            // cmdBayCustomerCheckAll
            // 
            this.cmdBayCustomerCheckAll.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmdBayCustomerCheckAll.Image = ((System.Drawing.Image)(resources.GetObject("cmdBayCustomerCheckAll.Image")));
            this.cmdBayCustomerCheckAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdBayCustomerCheckAll.Location = new System.Drawing.Point(2, 1);
            this.cmdBayCustomerCheckAll.Name = "cmdBayCustomerCheckAll";
            this.cmdBayCustomerCheckAll.Size = new System.Drawing.Size(134, 38);
            this.cmdBayCustomerCheckAll.TabIndex = 13;
            this.cmdBayCustomerCheckAll.Text = "Check all";
            this.cmdBayCustomerCheckAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdBayCustomerCheckAll.UseVisualStyleBackColor = true;
            this.cmdBayCustomerCheckAll.Click += new System.EventHandler(this.cmdBayItemsCheckAll_Click);
            // 
            // cmdBayCustomerUncheckAll
            // 
            this.cmdBayCustomerUncheckAll.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmdBayCustomerUncheckAll.Image = ((System.Drawing.Image)(resources.GetObject("cmdBayCustomerUncheckAll.Image")));
            this.cmdBayCustomerUncheckAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdBayCustomerUncheckAll.Location = new System.Drawing.Point(144, 1);
            this.cmdBayCustomerUncheckAll.Name = "cmdBayCustomerUncheckAll";
            this.cmdBayCustomerUncheckAll.Size = new System.Drawing.Size(134, 38);
            this.cmdBayCustomerUncheckAll.TabIndex = 14;
            this.cmdBayCustomerUncheckAll.Text = "Uncheck all";
            this.cmdBayCustomerUncheckAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdBayCustomerUncheckAll.UseVisualStyleBackColor = true;
            this.cmdBayCustomerUncheckAll.Click += new System.EventHandler(this.cmdBayItemsUncheckAll_Click);
            // 
            // chkBayAllowAllCustomers
            // 
            this.chkBayAllowAllCustomers.AutoSize = true;
            this.chkBayAllowAllCustomers.Checked = true;
            this.chkBayAllowAllCustomers.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBayAllowAllCustomers.Location = new System.Drawing.Point(6, 6);
            this.chkBayAllowAllCustomers.Name = "chkBayAllowAllCustomers";
            this.chkBayAllowAllCustomers.Size = new System.Drawing.Size(271, 28);
            this.chkBayAllowAllCustomers.TabIndex = 17;
            this.chkBayAllowAllCustomers.Text = "Allow all customers/suppliers";
            this.chkBayAllowAllCustomers.UseVisualStyleBackColor = true;
            this.chkBayAllowAllCustomers.CheckedChanged += new System.EventHandler(this.chkBayAllowAllItems_CheckedChanged);
            // 
            // pnlBayCustomersAvailable
            // 
            this.pnlBayCustomersAvailable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlBayCustomersAvailable.AutoScroll = true;
            this.pnlBayCustomersAvailable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlBayCustomersAvailable.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.pnlBayCustomersAvailable.Location = new System.Drawing.Point(6, 40);
            this.pnlBayCustomersAvailable.Name = "pnlBayCustomersAvailable";
            this.pnlBayCustomersAvailable.Size = new System.Drawing.Size(959, 289);
            this.pnlBayCustomersAvailable.TabIndex = 16;
            this.pnlBayCustomersAvailable.Tag = "";
            // 
            // tbpOwners
            // 
            this.tbpOwners.Controls.Add(this.pnlBayOwnerCheckAll);
            this.tbpOwners.Controls.Add(this.chkBayAllowAllOwners);
            this.tbpOwners.Controls.Add(this.pnlBayOwnersAvailable);
            this.tbpOwners.Location = new System.Drawing.Point(4, 22);
            this.tbpOwners.Name = "tbpOwners";
            this.tbpOwners.Size = new System.Drawing.Size(972, 379);
            this.tbpOwners.TabIndex = 3;
            this.tbpOwners.Text = "Owners";
            this.tbpOwners.UseVisualStyleBackColor = true;
            // 
            // pnlBayOwnerCheckAll
            // 
            this.pnlBayOwnerCheckAll.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pnlBayOwnerCheckAll.Controls.Add(this.cmdBayOwnerCheckAll);
            this.pnlBayOwnerCheckAll.Controls.Add(this.cmdBayOwnerUncheckAll);
            this.pnlBayOwnerCheckAll.Location = new System.Drawing.Point(346, 337);
            this.pnlBayOwnerCheckAll.Name = "pnlBayOwnerCheckAll";
            this.pnlBayOwnerCheckAll.Size = new System.Drawing.Size(280, 40);
            this.pnlBayOwnerCheckAll.TabIndex = 18;
            // 
            // cmdBayOwnerCheckAll
            // 
            this.cmdBayOwnerCheckAll.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmdBayOwnerCheckAll.Image = ((System.Drawing.Image)(resources.GetObject("cmdBayOwnerCheckAll.Image")));
            this.cmdBayOwnerCheckAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdBayOwnerCheckAll.Location = new System.Drawing.Point(2, 1);
            this.cmdBayOwnerCheckAll.Name = "cmdBayOwnerCheckAll";
            this.cmdBayOwnerCheckAll.Size = new System.Drawing.Size(134, 38);
            this.cmdBayOwnerCheckAll.TabIndex = 13;
            this.cmdBayOwnerCheckAll.Text = "Check all";
            this.cmdBayOwnerCheckAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdBayOwnerCheckAll.UseVisualStyleBackColor = true;
            this.cmdBayOwnerCheckAll.Click += new System.EventHandler(this.cmdBayItemsCheckAll_Click);
            // 
            // cmdBayOwnerUncheckAll
            // 
            this.cmdBayOwnerUncheckAll.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmdBayOwnerUncheckAll.Image = ((System.Drawing.Image)(resources.GetObject("cmdBayOwnerUncheckAll.Image")));
            this.cmdBayOwnerUncheckAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdBayOwnerUncheckAll.Location = new System.Drawing.Point(144, 1);
            this.cmdBayOwnerUncheckAll.Name = "cmdBayOwnerUncheckAll";
            this.cmdBayOwnerUncheckAll.Size = new System.Drawing.Size(134, 38);
            this.cmdBayOwnerUncheckAll.TabIndex = 14;
            this.cmdBayOwnerUncheckAll.Text = "Uncheck all";
            this.cmdBayOwnerUncheckAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdBayOwnerUncheckAll.UseVisualStyleBackColor = true;
            this.cmdBayOwnerUncheckAll.Click += new System.EventHandler(this.cmdBayItemsUncheckAll_Click);
            // 
            // chkBayAllowAllOwners
            // 
            this.chkBayAllowAllOwners.AutoSize = true;
            this.chkBayAllowAllOwners.Checked = true;
            this.chkBayAllowAllOwners.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBayAllowAllOwners.Location = new System.Drawing.Point(6, 6);
            this.chkBayAllowAllOwners.Name = "chkBayAllowAllOwners";
            this.chkBayAllowAllOwners.Size = new System.Drawing.Size(165, 28);
            this.chkBayAllowAllOwners.TabIndex = 17;
            this.chkBayAllowAllOwners.Text = "Allow all owners";
            this.chkBayAllowAllOwners.UseVisualStyleBackColor = true;
            this.chkBayAllowAllOwners.CheckedChanged += new System.EventHandler(this.chkBayAllowAllItems_CheckedChanged);
            // 
            // pnlBayOwnersAvailable
            // 
            this.pnlBayOwnersAvailable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlBayOwnersAvailable.AutoScroll = true;
            this.pnlBayOwnersAvailable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlBayOwnersAvailable.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.pnlBayOwnersAvailable.Location = new System.Drawing.Point(6, 40);
            this.pnlBayOwnersAvailable.Name = "pnlBayOwnersAvailable";
            this.pnlBayOwnersAvailable.Size = new System.Drawing.Size(959, 289);
            this.pnlBayOwnersAvailable.TabIndex = 16;
            this.pnlBayOwnersAvailable.Tag = "";
            // 
            // tbpDrivers
            // 
            this.tbpDrivers.Controls.Add(this.pnlBayDriverCheckAll);
            this.tbpDrivers.Controls.Add(this.chkBayAllowAllDrivers);
            this.tbpDrivers.Controls.Add(this.pnlBayDriversAvailable);
            this.tbpDrivers.Location = new System.Drawing.Point(4, 22);
            this.tbpDrivers.Name = "tbpDrivers";
            this.tbpDrivers.Size = new System.Drawing.Size(972, 379);
            this.tbpDrivers.TabIndex = 4;
            this.tbpDrivers.Text = "Drivers";
            this.tbpDrivers.UseVisualStyleBackColor = true;
            // 
            // pnlBayDriverCheckAll
            // 
            this.pnlBayDriverCheckAll.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pnlBayDriverCheckAll.Controls.Add(this.cmdBayDriverCheckAll);
            this.pnlBayDriverCheckAll.Controls.Add(this.cmdBayDriverUncheckAll);
            this.pnlBayDriverCheckAll.Location = new System.Drawing.Point(346, 337);
            this.pnlBayDriverCheckAll.Name = "pnlBayDriverCheckAll";
            this.pnlBayDriverCheckAll.Size = new System.Drawing.Size(280, 40);
            this.pnlBayDriverCheckAll.TabIndex = 18;
            // 
            // cmdBayDriverCheckAll
            // 
            this.cmdBayDriverCheckAll.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmdBayDriverCheckAll.Image = ((System.Drawing.Image)(resources.GetObject("cmdBayDriverCheckAll.Image")));
            this.cmdBayDriverCheckAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdBayDriverCheckAll.Location = new System.Drawing.Point(2, 1);
            this.cmdBayDriverCheckAll.Name = "cmdBayDriverCheckAll";
            this.cmdBayDriverCheckAll.Size = new System.Drawing.Size(134, 38);
            this.cmdBayDriverCheckAll.TabIndex = 13;
            this.cmdBayDriverCheckAll.Text = "Check all";
            this.cmdBayDriverCheckAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdBayDriverCheckAll.UseVisualStyleBackColor = true;
            this.cmdBayDriverCheckAll.Click += new System.EventHandler(this.cmdBayItemsCheckAll_Click);
            // 
            // cmdBayDriverUncheckAll
            // 
            this.cmdBayDriverUncheckAll.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmdBayDriverUncheckAll.Image = ((System.Drawing.Image)(resources.GetObject("cmdBayDriverUncheckAll.Image")));
            this.cmdBayDriverUncheckAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdBayDriverUncheckAll.Location = new System.Drawing.Point(144, 1);
            this.cmdBayDriverUncheckAll.Name = "cmdBayDriverUncheckAll";
            this.cmdBayDriverUncheckAll.Size = new System.Drawing.Size(134, 38);
            this.cmdBayDriverUncheckAll.TabIndex = 14;
            this.cmdBayDriverUncheckAll.Text = "Uncheck all";
            this.cmdBayDriverUncheckAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdBayDriverUncheckAll.UseVisualStyleBackColor = true;
            this.cmdBayDriverUncheckAll.Click += new System.EventHandler(this.cmdBayItemsUncheckAll_Click);
            // 
            // chkBayAllowAllDrivers
            // 
            this.chkBayAllowAllDrivers.AutoSize = true;
            this.chkBayAllowAllDrivers.Checked = true;
            this.chkBayAllowAllDrivers.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBayAllowAllDrivers.Location = new System.Drawing.Point(6, 6);
            this.chkBayAllowAllDrivers.Name = "chkBayAllowAllDrivers";
            this.chkBayAllowAllDrivers.Size = new System.Drawing.Size(159, 28);
            this.chkBayAllowAllDrivers.TabIndex = 17;
            this.chkBayAllowAllDrivers.Text = "Allow all drivers";
            this.chkBayAllowAllDrivers.UseVisualStyleBackColor = true;
            this.chkBayAllowAllDrivers.CheckedChanged += new System.EventHandler(this.chkBayAllowAllItems_CheckedChanged);
            // 
            // pnlBayDriversAvailable
            // 
            this.pnlBayDriversAvailable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlBayDriversAvailable.AutoScroll = true;
            this.pnlBayDriversAvailable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlBayDriversAvailable.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.pnlBayDriversAvailable.Location = new System.Drawing.Point(6, 40);
            this.pnlBayDriversAvailable.Name = "pnlBayDriversAvailable";
            this.pnlBayDriversAvailable.Size = new System.Drawing.Size(959, 289);
            this.pnlBayDriversAvailable.TabIndex = 16;
            // 
            // tbpCarriers
            // 
            this.tbpCarriers.Controls.Add(this.pnlBayCarriersCheckAll);
            this.tbpCarriers.Controls.Add(this.chkBayAllowAllCarriers);
            this.tbpCarriers.Controls.Add(this.pnlBayCarriersAvailable);
            this.tbpCarriers.Location = new System.Drawing.Point(4, 22);
            this.tbpCarriers.Name = "tbpCarriers";
            this.tbpCarriers.Size = new System.Drawing.Size(972, 379);
            this.tbpCarriers.TabIndex = 6;
            this.tbpCarriers.Text = "Carriers";
            this.tbpCarriers.UseVisualStyleBackColor = true;
            // 
            // pnlBayCarriersCheckAll
            // 
            this.pnlBayCarriersCheckAll.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pnlBayCarriersCheckAll.Controls.Add(this.cmdBayCarrierCheckAll);
            this.pnlBayCarriersCheckAll.Controls.Add(this.cmdBayCarrierUncheckAll);
            this.pnlBayCarriersCheckAll.Location = new System.Drawing.Point(346, 337);
            this.pnlBayCarriersCheckAll.Name = "pnlBayCarriersCheckAll";
            this.pnlBayCarriersCheckAll.Size = new System.Drawing.Size(280, 40);
            this.pnlBayCarriersCheckAll.TabIndex = 18;
            // 
            // cmdBayCarrierCheckAll
            // 
            this.cmdBayCarrierCheckAll.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmdBayCarrierCheckAll.Image = ((System.Drawing.Image)(resources.GetObject("cmdBayCarrierCheckAll.Image")));
            this.cmdBayCarrierCheckAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdBayCarrierCheckAll.Location = new System.Drawing.Point(2, 1);
            this.cmdBayCarrierCheckAll.Name = "cmdBayCarrierCheckAll";
            this.cmdBayCarrierCheckAll.Size = new System.Drawing.Size(134, 38);
            this.cmdBayCarrierCheckAll.TabIndex = 13;
            this.cmdBayCarrierCheckAll.Text = "Check all";
            this.cmdBayCarrierCheckAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdBayCarrierCheckAll.UseVisualStyleBackColor = true;
            this.cmdBayCarrierCheckAll.Click += new System.EventHandler(this.cmdBayItemsCheckAll_Click);
            // 
            // cmdBayCarrierUncheckAll
            // 
            this.cmdBayCarrierUncheckAll.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmdBayCarrierUncheckAll.Image = ((System.Drawing.Image)(resources.GetObject("cmdBayCarrierUncheckAll.Image")));
            this.cmdBayCarrierUncheckAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdBayCarrierUncheckAll.Location = new System.Drawing.Point(144, 1);
            this.cmdBayCarrierUncheckAll.Name = "cmdBayCarrierUncheckAll";
            this.cmdBayCarrierUncheckAll.Size = new System.Drawing.Size(134, 38);
            this.cmdBayCarrierUncheckAll.TabIndex = 14;
            this.cmdBayCarrierUncheckAll.Text = "Uncheck all";
            this.cmdBayCarrierUncheckAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdBayCarrierUncheckAll.UseVisualStyleBackColor = true;
            this.cmdBayCarrierUncheckAll.Click += new System.EventHandler(this.cmdBayItemsUncheckAll_Click);
            // 
            // chkBayAllowAllCarriers
            // 
            this.chkBayAllowAllCarriers.AutoSize = true;
            this.chkBayAllowAllCarriers.Checked = true;
            this.chkBayAllowAllCarriers.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBayAllowAllCarriers.Location = new System.Drawing.Point(6, 6);
            this.chkBayAllowAllCarriers.Name = "chkBayAllowAllCarriers";
            this.chkBayAllowAllCarriers.Size = new System.Drawing.Size(165, 28);
            this.chkBayAllowAllCarriers.TabIndex = 17;
            this.chkBayAllowAllCarriers.Text = "Allow all carriers";
            this.chkBayAllowAllCarriers.UseVisualStyleBackColor = true;
            this.chkBayAllowAllCarriers.CheckedChanged += new System.EventHandler(this.chkBayAllowAllItems_CheckedChanged);
            // 
            // pnlBayCarriersAvailable
            // 
            this.pnlBayCarriersAvailable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlBayCarriersAvailable.AutoScroll = true;
            this.pnlBayCarriersAvailable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlBayCarriersAvailable.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.pnlBayCarriersAvailable.Location = new System.Drawing.Point(6, 40);
            this.pnlBayCarriersAvailable.Name = "pnlBayCarriersAvailable";
            this.pnlBayCarriersAvailable.Size = new System.Drawing.Size(959, 289);
            this.pnlBayCarriersAvailable.TabIndex = 16;
            // 
            // tbpTransports
            // 
            this.tbpTransports.Controls.Add(this.pnlBayTransportCheckAll);
            this.tbpTransports.Controls.Add(this.chkBayAllowAllTransports);
            this.tbpTransports.Controls.Add(this.pnlBayTransportsAvailable);
            this.tbpTransports.Location = new System.Drawing.Point(4, 22);
            this.tbpTransports.Name = "tbpTransports";
            this.tbpTransports.Size = new System.Drawing.Size(972, 379);
            this.tbpTransports.TabIndex = 5;
            this.tbpTransports.Text = "Transports";
            this.tbpTransports.UseVisualStyleBackColor = true;
            // 
            // pnlBayTransportCheckAll
            // 
            this.pnlBayTransportCheckAll.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pnlBayTransportCheckAll.Controls.Add(this.cmdBayTransportCheckAll);
            this.pnlBayTransportCheckAll.Controls.Add(this.cmdBayTransportUncheckAll);
            this.pnlBayTransportCheckAll.Location = new System.Drawing.Point(346, 337);
            this.pnlBayTransportCheckAll.Name = "pnlBayTransportCheckAll";
            this.pnlBayTransportCheckAll.Size = new System.Drawing.Size(280, 40);
            this.pnlBayTransportCheckAll.TabIndex = 18;
            // 
            // cmdBayTransportCheckAll
            // 
            this.cmdBayTransportCheckAll.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmdBayTransportCheckAll.Image = ((System.Drawing.Image)(resources.GetObject("cmdBayTransportCheckAll.Image")));
            this.cmdBayTransportCheckAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdBayTransportCheckAll.Location = new System.Drawing.Point(2, 1);
            this.cmdBayTransportCheckAll.Name = "cmdBayTransportCheckAll";
            this.cmdBayTransportCheckAll.Size = new System.Drawing.Size(134, 38);
            this.cmdBayTransportCheckAll.TabIndex = 13;
            this.cmdBayTransportCheckAll.Text = "Check all";
            this.cmdBayTransportCheckAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdBayTransportCheckAll.UseVisualStyleBackColor = true;
            this.cmdBayTransportCheckAll.Click += new System.EventHandler(this.cmdBayItemsCheckAll_Click);
            // 
            // cmdBayTransportUncheckAll
            // 
            this.cmdBayTransportUncheckAll.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmdBayTransportUncheckAll.Image = ((System.Drawing.Image)(resources.GetObject("cmdBayTransportUncheckAll.Image")));
            this.cmdBayTransportUncheckAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdBayTransportUncheckAll.Location = new System.Drawing.Point(144, 1);
            this.cmdBayTransportUncheckAll.Name = "cmdBayTransportUncheckAll";
            this.cmdBayTransportUncheckAll.Size = new System.Drawing.Size(134, 38);
            this.cmdBayTransportUncheckAll.TabIndex = 14;
            this.cmdBayTransportUncheckAll.Text = "Uncheck all";
            this.cmdBayTransportUncheckAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdBayTransportUncheckAll.UseVisualStyleBackColor = true;
            this.cmdBayTransportUncheckAll.Click += new System.EventHandler(this.cmdBayItemsUncheckAll_Click);
            // 
            // chkBayAllowAllTransports
            // 
            this.chkBayAllowAllTransports.AutoSize = true;
            this.chkBayAllowAllTransports.Checked = true;
            this.chkBayAllowAllTransports.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBayAllowAllTransports.Location = new System.Drawing.Point(6, 6);
            this.chkBayAllowAllTransports.Name = "chkBayAllowAllTransports";
            this.chkBayAllowAllTransports.Size = new System.Drawing.Size(184, 28);
            this.chkBayAllowAllTransports.TabIndex = 17;
            this.chkBayAllowAllTransports.Text = "Allow all transports";
            this.chkBayAllowAllTransports.UseVisualStyleBackColor = true;
            this.chkBayAllowAllTransports.CheckedChanged += new System.EventHandler(this.chkBayAllowAllItems_CheckedChanged);
            // 
            // pnlBayTransportsAvailable
            // 
            this.pnlBayTransportsAvailable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlBayTransportsAvailable.AutoScroll = true;
            this.pnlBayTransportsAvailable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlBayTransportsAvailable.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.pnlBayTransportsAvailable.Location = new System.Drawing.Point(6, 40);
            this.pnlBayTransportsAvailable.Name = "pnlBayTransportsAvailable";
            this.pnlBayTransportsAvailable.Size = new System.Drawing.Size(959, 289);
            this.pnlBayTransportsAvailable.TabIndex = 16;
            // 
            // chkBayEnabled
            // 
            this.chkBayEnabled.AutoSize = true;
            this.chkBayEnabled.Checked = true;
            this.chkBayEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBayEnabled.Location = new System.Drawing.Point(300, 30);
            this.chkBayEnabled.Name = "chkBayEnabled";
            this.chkBayEnabled.Size = new System.Drawing.Size(134, 28);
            this.chkBayEnabled.TabIndex = 2;
            this.chkBayEnabled.Text = "Bay enabled";
            this.chkBayEnabled.UseVisualStyleBackColor = true;
            this.chkBayEnabled.CheckedChanged += new System.EventHandler(this.chkBayEnabled_CheckedChanged);
            // 
            // cmbBay
            // 
            this.cmbBay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBay.FormattingEnabled = true;
            this.cmbBay.Location = new System.Drawing.Point(56, 28);
            this.cmbBay.Name = "cmbBay";
            this.cmbBay.Size = new System.Drawing.Size(238, 32);
            this.cmbBay.TabIndex = 1;
            this.cmbBay.SelectedIndexChanged += new System.EventHandler(this.cmbBay_SelectedIndexChanged);
            // 
            // lblBay
            // 
            this.lblBay.AutoSize = true;
            this.lblBay.Location = new System.Drawing.Point(9, 31);
            this.lblBay.Name = "lblBay";
            this.lblBay.Size = new System.Drawing.Size(41, 24);
            this.lblBay.TabIndex = 0;
            this.lblBay.Text = "Bay";
            this.lblBay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkBayDirectionEnabled
            // 
            this.chkBayDirectionEnabled.AutoSize = true;
            this.chkBayDirectionEnabled.Checked = true;
            this.chkBayDirectionEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBayDirectionEnabled.Location = new System.Drawing.Point(9, 3);
            this.chkBayDirectionEnabled.Name = "chkBayDirectionEnabled";
            this.chkBayDirectionEnabled.Size = new System.Drawing.Size(211, 28);
            this.chkBayDirectionEnabled.TabIndex = 0;
            this.chkBayDirectionEnabled.Text = "Bay direction enabled";
            this.chkBayDirectionEnabled.UseVisualStyleBackColor = true;
            // 
            // tpCompleteScreen
            // 
            this.tpCompleteScreen.Controls.Add(this.label1);
            this.tpCompleteScreen.Controls.Add(this.gpbOrderCompleteOutbound);
            this.tpCompleteScreen.Controls.Add(this.gpbOrderCompleteInboundReceiving);
            this.tpCompleteScreen.Controls.Add(this.gpbOrderCompleteInboundOrder);
            this.tpCompleteScreen.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpCompleteScreen.Location = new System.Drawing.Point(4, 33);
            this.tpCompleteScreen.Name = "tpCompleteScreen";
            this.tpCompleteScreen.Size = new System.Drawing.Size(1000, 543);
            this.tpCompleteScreen.TabIndex = 5;
            this.tpCompleteScreen.Text = "Complete screen";
            this.tpCompleteScreen.UseVisualStyleBackColor = true;
            // 
            // gpbOrderCompleteInboundOrder
            // 
            this.gpbOrderCompleteInboundOrder.Controls.Add(this.gpbOrderCompleteInboundOrderBayNotAssigned);
            this.gpbOrderCompleteInboundOrder.Controls.Add(this.gpbOrderCompleteInboundOrderBayAssigned);
            this.gpbOrderCompleteInboundOrder.Location = new System.Drawing.Point(3, 3);
            this.gpbOrderCompleteInboundOrder.Name = "gpbOrderCompleteInboundOrder";
            this.gpbOrderCompleteInboundOrder.Size = new System.Drawing.Size(987, 207);
            this.gpbOrderCompleteInboundOrder.TabIndex = 0;
            this.gpbOrderCompleteInboundOrder.TabStop = false;
            this.gpbOrderCompleteInboundOrder.Text = "Inbound order";
            // 
            // gpbOrderCompleteInboundOrderBayNotAssigned
            // 
            this.gpbOrderCompleteInboundOrderBayNotAssigned.Controls.Add(this.tbxOrderCompleteInboundOrderBayNotAssignedNoTickets);
            this.gpbOrderCompleteInboundOrderBayNotAssigned.Controls.Add(this.lblOrderCompleteInboundOrderBayNotAssignedNoTickets);
            this.gpbOrderCompleteInboundOrderBayNotAssigned.Controls.Add(this.tbxOrderCompleteInboundOrderBayNotAssignedWithTickets);
            this.gpbOrderCompleteInboundOrderBayNotAssigned.Controls.Add(this.lblOrderCompleteInboundOrderBayNotAssignedWithTickets);
            this.gpbOrderCompleteInboundOrderBayNotAssigned.Location = new System.Drawing.Point(6, 116);
            this.gpbOrderCompleteInboundOrderBayNotAssigned.Name = "gpbOrderCompleteInboundOrderBayNotAssigned";
            this.gpbOrderCompleteInboundOrderBayNotAssigned.Size = new System.Drawing.Size(975, 87);
            this.gpbOrderCompleteInboundOrderBayNotAssigned.TabIndex = 1;
            this.gpbOrderCompleteInboundOrderBayNotAssigned.TabStop = false;
            this.gpbOrderCompleteInboundOrderBayNotAssigned.Text = "Bay not assigned";
            // 
            // tbxOrderCompleteInboundOrderBayNotAssignedNoTickets
            // 
            this.tbxOrderCompleteInboundOrderBayNotAssignedNoTickets.Location = new System.Drawing.Point(144, 53);
            this.tbxOrderCompleteInboundOrderBayNotAssignedNoTickets.Name = "tbxOrderCompleteInboundOrderBayNotAssignedNoTickets";
            this.tbxOrderCompleteInboundOrderBayNotAssignedNoTickets.Size = new System.Drawing.Size(825, 24);
            this.tbxOrderCompleteInboundOrderBayNotAssignedNoTickets.TabIndex = 3;
            // 
            // lblOrderCompleteInboundOrderBayNotAssignedNoTickets
            // 
            this.lblOrderCompleteInboundOrderBayNotAssignedNoTickets.AutoSize = true;
            this.lblOrderCompleteInboundOrderBayNotAssignedNoTickets.Location = new System.Drawing.Point(7, 56);
            this.lblOrderCompleteInboundOrderBayNotAssignedNoTickets.Name = "lblOrderCompleteInboundOrderBayNotAssignedNoTickets";
            this.lblOrderCompleteInboundOrderBayNotAssignedNoTickets.Size = new System.Drawing.Size(124, 18);
            this.lblOrderCompleteInboundOrderBayNotAssignedNoTickets.TabIndex = 2;
            this.lblOrderCompleteInboundOrderBayNotAssignedNoTickets.Text = "No tickets to print";
            // 
            // tbxOrderCompleteInboundOrderBayNotAssignedWithTickets
            // 
            this.tbxOrderCompleteInboundOrderBayNotAssignedWithTickets.Location = new System.Drawing.Point(144, 23);
            this.tbxOrderCompleteInboundOrderBayNotAssignedWithTickets.Name = "tbxOrderCompleteInboundOrderBayNotAssignedWithTickets";
            this.tbxOrderCompleteInboundOrderBayNotAssignedWithTickets.Size = new System.Drawing.Size(825, 24);
            this.tbxOrderCompleteInboundOrderBayNotAssignedWithTickets.TabIndex = 1;
            // 
            // lblOrderCompleteInboundOrderBayNotAssignedWithTickets
            // 
            this.lblOrderCompleteInboundOrderBayNotAssignedWithTickets.AutoSize = true;
            this.lblOrderCompleteInboundOrderBayNotAssignedWithTickets.Location = new System.Drawing.Point(7, 26);
            this.lblOrderCompleteInboundOrderBayNotAssignedWithTickets.Name = "lblOrderCompleteInboundOrderBayNotAssignedWithTickets";
            this.lblOrderCompleteInboundOrderBayNotAssignedWithTickets.Size = new System.Drawing.Size(131, 18);
            this.lblOrderCompleteInboundOrderBayNotAssignedWithTickets.TabIndex = 0;
            this.lblOrderCompleteInboundOrderBayNotAssignedWithTickets.Text = "Has tickets to print";
            // 
            // gpbOrderCompleteInboundOrderBayAssigned
            // 
            this.gpbOrderCompleteInboundOrderBayAssigned.Controls.Add(this.tbxOrderCompleteInboundOrderBayAssignedNoTickets);
            this.gpbOrderCompleteInboundOrderBayAssigned.Controls.Add(this.lblOrderCompleteInboundOrderBayAssignedNoTickets);
            this.gpbOrderCompleteInboundOrderBayAssigned.Controls.Add(this.tbxOrderCompleteInboundOrderBayAssignedWithTickets);
            this.gpbOrderCompleteInboundOrderBayAssigned.Controls.Add(this.lblOrderCompleteInboundOrderBayAssignedWithTickets);
            this.gpbOrderCompleteInboundOrderBayAssigned.Location = new System.Drawing.Point(6, 23);
            this.gpbOrderCompleteInboundOrderBayAssigned.Name = "gpbOrderCompleteInboundOrderBayAssigned";
            this.gpbOrderCompleteInboundOrderBayAssigned.Size = new System.Drawing.Size(975, 87);
            this.gpbOrderCompleteInboundOrderBayAssigned.TabIndex = 0;
            this.gpbOrderCompleteInboundOrderBayAssigned.TabStop = false;
            this.gpbOrderCompleteInboundOrderBayAssigned.Text = "Bay assigned";
            // 
            // tbxOrderCompleteInboundOrderBayAssignedNoTickets
            // 
            this.tbxOrderCompleteInboundOrderBayAssignedNoTickets.Location = new System.Drawing.Point(144, 53);
            this.tbxOrderCompleteInboundOrderBayAssignedNoTickets.Name = "tbxOrderCompleteInboundOrderBayAssignedNoTickets";
            this.tbxOrderCompleteInboundOrderBayAssignedNoTickets.Size = new System.Drawing.Size(825, 24);
            this.tbxOrderCompleteInboundOrderBayAssignedNoTickets.TabIndex = 3;
            // 
            // lblOrderCompleteInboundOrderBayAssignedNoTickets
            // 
            this.lblOrderCompleteInboundOrderBayAssignedNoTickets.AutoSize = true;
            this.lblOrderCompleteInboundOrderBayAssignedNoTickets.Location = new System.Drawing.Point(7, 56);
            this.lblOrderCompleteInboundOrderBayAssignedNoTickets.Name = "lblOrderCompleteInboundOrderBayAssignedNoTickets";
            this.lblOrderCompleteInboundOrderBayAssignedNoTickets.Size = new System.Drawing.Size(124, 18);
            this.lblOrderCompleteInboundOrderBayAssignedNoTickets.TabIndex = 2;
            this.lblOrderCompleteInboundOrderBayAssignedNoTickets.Text = "No tickets to print";
            // 
            // tbxOrderCompleteInboundOrderBayAssignedWithTickets
            // 
            this.tbxOrderCompleteInboundOrderBayAssignedWithTickets.Location = new System.Drawing.Point(144, 23);
            this.tbxOrderCompleteInboundOrderBayAssignedWithTickets.Name = "tbxOrderCompleteInboundOrderBayAssignedWithTickets";
            this.tbxOrderCompleteInboundOrderBayAssignedWithTickets.Size = new System.Drawing.Size(825, 24);
            this.tbxOrderCompleteInboundOrderBayAssignedWithTickets.TabIndex = 1;
            // 
            // lblOrderCompleteInboundOrderBayAssignedWithTickets
            // 
            this.lblOrderCompleteInboundOrderBayAssignedWithTickets.AutoSize = true;
            this.lblOrderCompleteInboundOrderBayAssignedWithTickets.Location = new System.Drawing.Point(7, 26);
            this.lblOrderCompleteInboundOrderBayAssignedWithTickets.Name = "lblOrderCompleteInboundOrderBayAssignedWithTickets";
            this.lblOrderCompleteInboundOrderBayAssignedWithTickets.Size = new System.Drawing.Size(131, 18);
            this.lblOrderCompleteInboundOrderBayAssignedWithTickets.TabIndex = 0;
            this.lblOrderCompleteInboundOrderBayAssignedWithTickets.Text = "Has tickets to print";
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(883, 598);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(133, 50);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(15, 598);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(160, 50);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "Exit Application";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblTm2DatabaseVersion
            // 
            this.lblTm2DatabaseVersion.AutoSize = true;
            this.lblTm2DatabaseVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTm2DatabaseVersion.Location = new System.Drawing.Point(536, 598);
            this.lblTm2DatabaseVersion.Name = "lblTm2DatabaseVersion";
            this.lblTm2DatabaseVersion.Size = new System.Drawing.Size(109, 18);
            this.lblTm2DatabaseVersion.TabIndex = 4;
            this.lblTm2DatabaseVersion.Text = "TM2 Database:";
            // 
            // lblTm2StagingTerminalversion
            // 
            this.lblTm2StagingTerminalversion.AutoSize = true;
            this.lblTm2StagingTerminalversion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTm2StagingTerminalversion.Location = new System.Drawing.Point(181, 598);
            this.lblTm2StagingTerminalversion.Name = "lblTm2StagingTerminalversion";
            this.lblTm2StagingTerminalversion.Size = new System.Drawing.Size(129, 18);
            this.lblTm2StagingTerminalversion.TabIndex = 3;
            this.lblTm2StagingTerminalversion.Text = "Staging Terminal v";
            // 
            // gpbOrderCompleteInboundReceiving
            // 
            this.gpbOrderCompleteInboundReceiving.Controls.Add(this.gpbOrderCompleteInboundReceivingBayNotAssigned);
            this.gpbOrderCompleteInboundReceiving.Controls.Add(this.gpbOrderCompleteInboundReceivingBayAssigned);
            this.gpbOrderCompleteInboundReceiving.Location = new System.Drawing.Point(3, 216);
            this.gpbOrderCompleteInboundReceiving.Name = "gpbOrderCompleteInboundReceiving";
            this.gpbOrderCompleteInboundReceiving.Size = new System.Drawing.Size(987, 207);
            this.gpbOrderCompleteInboundReceiving.TabIndex = 1;
            this.gpbOrderCompleteInboundReceiving.TabStop = false;
            this.gpbOrderCompleteInboundReceiving.Text = "Inbound receiving order";
            // 
            // gpbOrderCompleteInboundReceivingBayNotAssigned
            // 
            this.gpbOrderCompleteInboundReceivingBayNotAssigned.Controls.Add(this.tbxOrderCompleteInboundReceivingBayNotAssignedNoTickets);
            this.gpbOrderCompleteInboundReceivingBayNotAssigned.Controls.Add(this.lblOrderCompleteInboundReceivingBayNotAssignedNoTickets);
            this.gpbOrderCompleteInboundReceivingBayNotAssigned.Controls.Add(this.tbxOrderCompleteInboundReceivingBayNotAssignedHasTickets);
            this.gpbOrderCompleteInboundReceivingBayNotAssigned.Controls.Add(this.lblOrderCompleteInboundReceivingBayNotAssignedHasTickets);
            this.gpbOrderCompleteInboundReceivingBayNotAssigned.Location = new System.Drawing.Point(6, 116);
            this.gpbOrderCompleteInboundReceivingBayNotAssigned.Name = "gpbOrderCompleteInboundReceivingBayNotAssigned";
            this.gpbOrderCompleteInboundReceivingBayNotAssigned.Size = new System.Drawing.Size(975, 87);
            this.gpbOrderCompleteInboundReceivingBayNotAssigned.TabIndex = 1;
            this.gpbOrderCompleteInboundReceivingBayNotAssigned.TabStop = false;
            this.gpbOrderCompleteInboundReceivingBayNotAssigned.Text = "Bay not assigned";
            // 
            // tbxOrderCompleteInboundReceivingBayNotAssignedNoTickets
            // 
            this.tbxOrderCompleteInboundReceivingBayNotAssignedNoTickets.Location = new System.Drawing.Point(144, 53);
            this.tbxOrderCompleteInboundReceivingBayNotAssignedNoTickets.Name = "tbxOrderCompleteInboundReceivingBayNotAssignedNoTickets";
            this.tbxOrderCompleteInboundReceivingBayNotAssignedNoTickets.Size = new System.Drawing.Size(825, 24);
            this.tbxOrderCompleteInboundReceivingBayNotAssignedNoTickets.TabIndex = 3;
            // 
            // lblOrderCompleteInboundReceivingBayNotAssignedNoTickets
            // 
            this.lblOrderCompleteInboundReceivingBayNotAssignedNoTickets.AutoSize = true;
            this.lblOrderCompleteInboundReceivingBayNotAssignedNoTickets.Location = new System.Drawing.Point(7, 56);
            this.lblOrderCompleteInboundReceivingBayNotAssignedNoTickets.Name = "lblOrderCompleteInboundReceivingBayNotAssignedNoTickets";
            this.lblOrderCompleteInboundReceivingBayNotAssignedNoTickets.Size = new System.Drawing.Size(124, 18);
            this.lblOrderCompleteInboundReceivingBayNotAssignedNoTickets.TabIndex = 2;
            this.lblOrderCompleteInboundReceivingBayNotAssignedNoTickets.Text = "No tickets to print";
            // 
            // tbxOrderCompleteInboundReceivingBayNotAssignedHasTickets
            // 
            this.tbxOrderCompleteInboundReceivingBayNotAssignedHasTickets.Location = new System.Drawing.Point(144, 23);
            this.tbxOrderCompleteInboundReceivingBayNotAssignedHasTickets.Name = "tbxOrderCompleteInboundReceivingBayNotAssignedHasTickets";
            this.tbxOrderCompleteInboundReceivingBayNotAssignedHasTickets.Size = new System.Drawing.Size(825, 24);
            this.tbxOrderCompleteInboundReceivingBayNotAssignedHasTickets.TabIndex = 1;
            // 
            // lblOrderCompleteInboundReceivingBayNotAssignedHasTickets
            // 
            this.lblOrderCompleteInboundReceivingBayNotAssignedHasTickets.AutoSize = true;
            this.lblOrderCompleteInboundReceivingBayNotAssignedHasTickets.Location = new System.Drawing.Point(7, 26);
            this.lblOrderCompleteInboundReceivingBayNotAssignedHasTickets.Name = "lblOrderCompleteInboundReceivingBayNotAssignedHasTickets";
            this.lblOrderCompleteInboundReceivingBayNotAssignedHasTickets.Size = new System.Drawing.Size(131, 18);
            this.lblOrderCompleteInboundReceivingBayNotAssignedHasTickets.TabIndex = 0;
            this.lblOrderCompleteInboundReceivingBayNotAssignedHasTickets.Text = "Has tickets to print";
            // 
            // gpbOrderCompleteInboundReceivingBayAssigned
            // 
            this.gpbOrderCompleteInboundReceivingBayAssigned.Controls.Add(this.tbxOrderCompleteInboundReceivingBayAssignedNoTickets);
            this.gpbOrderCompleteInboundReceivingBayAssigned.Controls.Add(this.lblOrderCompleteInboundReceivingBayAssignedNoTickets);
            this.gpbOrderCompleteInboundReceivingBayAssigned.Controls.Add(this.tbxOrderCompleteInboundReceivingBayAssignedHasTickets);
            this.gpbOrderCompleteInboundReceivingBayAssigned.Controls.Add(this.lblOrderCompleteInboundReceivingBayAssignedHasTickets);
            this.gpbOrderCompleteInboundReceivingBayAssigned.Location = new System.Drawing.Point(6, 23);
            this.gpbOrderCompleteInboundReceivingBayAssigned.Name = "gpbOrderCompleteInboundReceivingBayAssigned";
            this.gpbOrderCompleteInboundReceivingBayAssigned.Size = new System.Drawing.Size(975, 87);
            this.gpbOrderCompleteInboundReceivingBayAssigned.TabIndex = 0;
            this.gpbOrderCompleteInboundReceivingBayAssigned.TabStop = false;
            this.gpbOrderCompleteInboundReceivingBayAssigned.Text = "Bay assigned";
            // 
            // tbxOrderCompleteInboundReceivingBayAssignedNoTickets
            // 
            this.tbxOrderCompleteInboundReceivingBayAssignedNoTickets.Location = new System.Drawing.Point(144, 53);
            this.tbxOrderCompleteInboundReceivingBayAssignedNoTickets.Name = "tbxOrderCompleteInboundReceivingBayAssignedNoTickets";
            this.tbxOrderCompleteInboundReceivingBayAssignedNoTickets.Size = new System.Drawing.Size(825, 24);
            this.tbxOrderCompleteInboundReceivingBayAssignedNoTickets.TabIndex = 3;
            // 
            // lblOrderCompleteInboundReceivingBayAssignedNoTickets
            // 
            this.lblOrderCompleteInboundReceivingBayAssignedNoTickets.AutoSize = true;
            this.lblOrderCompleteInboundReceivingBayAssignedNoTickets.Location = new System.Drawing.Point(7, 56);
            this.lblOrderCompleteInboundReceivingBayAssignedNoTickets.Name = "lblOrderCompleteInboundReceivingBayAssignedNoTickets";
            this.lblOrderCompleteInboundReceivingBayAssignedNoTickets.Size = new System.Drawing.Size(124, 18);
            this.lblOrderCompleteInboundReceivingBayAssignedNoTickets.TabIndex = 2;
            this.lblOrderCompleteInboundReceivingBayAssignedNoTickets.Text = "No tickets to print";
            // 
            // tbxOrderCompleteInboundReceivingBayAssignedHasTickets
            // 
            this.tbxOrderCompleteInboundReceivingBayAssignedHasTickets.Location = new System.Drawing.Point(144, 23);
            this.tbxOrderCompleteInboundReceivingBayAssignedHasTickets.Name = "tbxOrderCompleteInboundReceivingBayAssignedHasTickets";
            this.tbxOrderCompleteInboundReceivingBayAssignedHasTickets.Size = new System.Drawing.Size(825, 24);
            this.tbxOrderCompleteInboundReceivingBayAssignedHasTickets.TabIndex = 1;
            // 
            // lblOrderCompleteInboundReceivingBayAssignedHasTickets
            // 
            this.lblOrderCompleteInboundReceivingBayAssignedHasTickets.AutoSize = true;
            this.lblOrderCompleteInboundReceivingBayAssignedHasTickets.Location = new System.Drawing.Point(7, 26);
            this.lblOrderCompleteInboundReceivingBayAssignedHasTickets.Name = "lblOrderCompleteInboundReceivingBayAssignedHasTickets";
            this.lblOrderCompleteInboundReceivingBayAssignedHasTickets.Size = new System.Drawing.Size(131, 18);
            this.lblOrderCompleteInboundReceivingBayAssignedHasTickets.TabIndex = 0;
            this.lblOrderCompleteInboundReceivingBayAssignedHasTickets.Text = "Has tickets to print";
            // 
            // gpbOrderCompleteOutbound
            // 
            this.gpbOrderCompleteOutbound.Controls.Add(this.tbxOrderCompleteOutboundNoTickets);
            this.gpbOrderCompleteOutbound.Controls.Add(this.lblOrderCompleteOutboundNoTickets);
            this.gpbOrderCompleteOutbound.Controls.Add(this.tbxOrderCompleteOutboundHasTickets);
            this.gpbOrderCompleteOutbound.Controls.Add(this.lblOrderCompleteOutboundHasTickets);
            this.gpbOrderCompleteOutbound.Location = new System.Drawing.Point(3, 429);
            this.gpbOrderCompleteOutbound.Name = "gpbOrderCompleteOutbound";
            this.gpbOrderCompleteOutbound.Size = new System.Drawing.Size(987, 87);
            this.gpbOrderCompleteOutbound.TabIndex = 2;
            this.gpbOrderCompleteOutbound.TabStop = false;
            this.gpbOrderCompleteOutbound.Text = "Outbound";
            // 
            // tbxOrderCompleteOutboundNoTickets
            // 
            this.tbxOrderCompleteOutboundNoTickets.Location = new System.Drawing.Point(144, 53);
            this.tbxOrderCompleteOutboundNoTickets.Name = "tbxOrderCompleteOutboundNoTickets";
            this.tbxOrderCompleteOutboundNoTickets.Size = new System.Drawing.Size(825, 24);
            this.tbxOrderCompleteOutboundNoTickets.TabIndex = 3;
            // 
            // lblOrderCompleteOutboundNoTickets
            // 
            this.lblOrderCompleteOutboundNoTickets.AutoSize = true;
            this.lblOrderCompleteOutboundNoTickets.Location = new System.Drawing.Point(7, 56);
            this.lblOrderCompleteOutboundNoTickets.Name = "lblOrderCompleteOutboundNoTickets";
            this.lblOrderCompleteOutboundNoTickets.Size = new System.Drawing.Size(124, 18);
            this.lblOrderCompleteOutboundNoTickets.TabIndex = 2;
            this.lblOrderCompleteOutboundNoTickets.Text = "No tickets to print";
            // 
            // tbxOrderCompleteOutboundHasTickets
            // 
            this.tbxOrderCompleteOutboundHasTickets.Location = new System.Drawing.Point(144, 23);
            this.tbxOrderCompleteOutboundHasTickets.Name = "tbxOrderCompleteOutboundHasTickets";
            this.tbxOrderCompleteOutboundHasTickets.Size = new System.Drawing.Size(825, 24);
            this.tbxOrderCompleteOutboundHasTickets.TabIndex = 1;
            // 
            // lblOrderCompleteOutboundHasTickets
            // 
            this.lblOrderCompleteOutboundHasTickets.AutoSize = true;
            this.lblOrderCompleteOutboundHasTickets.Location = new System.Drawing.Point(7, 26);
            this.lblOrderCompleteOutboundHasTickets.Name = "lblOrderCompleteOutboundHasTickets";
            this.lblOrderCompleteOutboundHasTickets.Size = new System.Drawing.Size(131, 18);
            this.lblOrderCompleteOutboundHasTickets.TabIndex = 0;
            this.lblOrderCompleteOutboundHasTickets.Text = "Has tickets to print";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(6, 519);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(661, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "<bay> will be replaced with the name of the bay. <br> will be replaced with a lin" +
    "e break.";
            // 
            // Configuration
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1018, 657);
            this.Controls.Add(this.lblTm2StagingTerminalversion);
            this.Controls.Add(this.lblTm2DatabaseVersion);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Configuration";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuration";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Configuration_HelpButtonClicked);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Configuration_FormClosing);
            this.Load += new System.EventHandler(this.Configuration_Load);
            this.tabControl.ResumeLayout(false);
            this.tpGeneral.ResumeLayout(false);
            this.tpGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDefaultMaxWeightForNewTransports)).EndInit();
            this.gpbScale.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxOnScreenTime)).EndInit();
            this.tpTicketPrinters.ResumeLayout(false);
            this.gbpReceivingProductTicketPrinters.ResumeLayout(false);
            this.gbpReceivingProductTicketPrinters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudReceivingProductTicketCopies)).EndInit();
            this.gpbOutboundTicketPrinters.ResumeLayout(false);
            this.gpbOutboundTicketPrinters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudOutboundTicketCopies)).EndInit();
            this.tpPickTickets.ResumeLayout(false);
            this.gpbInboundTruckInspection.ResumeLayout(false);
            this.gpbInboundTruckInspection.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudInboundTruckInspectionCopies)).EndInit();
            this.gpbReceivingProductPickTicketPrinters.ResumeLayout(false);
            this.gpbReceivingProductPickTicketPrinters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudReceivingProductPickTicketCopies)).EndInit();
            this.gpbInboundBaggedProductPrinters.ResumeLayout(false);
            this.gpbInboundBaggedProductPrinters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudInboundBaggedProductCopies)).EndInit();
            this.gpbInboundBulkProductPrinters.ResumeLayout(false);
            this.gpbInboundBulkProductPrinters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudInboundBulkProductCopies)).EndInit();
            this.tpOwners.ResumeLayout(false);
            this.gpbOwnersThatShouldNotDisplayMaplewoodAsShipTo.ResumeLayout(false);
            this.gpbPromptForShipToDestinationOwners.ResumeLayout(false);
            this.gpbOwnersRequiringRupForm.ResumeLayout(false);
            this.tpBayDirections.ResumeLayout(false);
            this.tpBayDirections.PerformLayout();
            this.gpbBaySettings.ResumeLayout(false);
            this.gpbBaySettings.PerformLayout();
            this.tbcBayItemAvailability.ResumeLayout(false);
            this.tbpBulkIngredients.ResumeLayout(false);
            this.pnlBayDirectionsBulkIngredientOptions.ResumeLayout(false);
            this.pnlBayDirectionsBulkIngredientOptions.PerformLayout();
            this.pnlBayBulkIngredientsCheckAll.ResumeLayout(false);
            this.tbpProducts.ResumeLayout(false);
            this.tbpProducts.PerformLayout();
            this.pnlBayProductsUncheckAll.ResumeLayout(false);
            this.tbpCustomers.ResumeLayout(false);
            this.tbpCustomers.PerformLayout();
            this.pnlBayCustomerCheckAll.ResumeLayout(false);
            this.tbpOwners.ResumeLayout(false);
            this.tbpOwners.PerformLayout();
            this.pnlBayOwnerCheckAll.ResumeLayout(false);
            this.tbpDrivers.ResumeLayout(false);
            this.tbpDrivers.PerformLayout();
            this.pnlBayDriverCheckAll.ResumeLayout(false);
            this.tbpCarriers.ResumeLayout(false);
            this.tbpCarriers.PerformLayout();
            this.pnlBayCarriersCheckAll.ResumeLayout(false);
            this.tbpTransports.ResumeLayout(false);
            this.tbpTransports.PerformLayout();
            this.pnlBayTransportCheckAll.ResumeLayout(false);
            this.tpCompleteScreen.ResumeLayout(false);
            this.tpCompleteScreen.PerformLayout();
            this.gpbOrderCompleteInboundOrder.ResumeLayout(false);
            this.gpbOrderCompleteInboundOrderBayNotAssigned.ResumeLayout(false);
            this.gpbOrderCompleteInboundOrderBayNotAssigned.PerformLayout();
            this.gpbOrderCompleteInboundOrderBayAssigned.ResumeLayout(false);
            this.gpbOrderCompleteInboundOrderBayAssigned.PerformLayout();
            this.gpbOrderCompleteInboundReceiving.ResumeLayout(false);
            this.gpbOrderCompleteInboundReceivingBayNotAssigned.ResumeLayout(false);
            this.gpbOrderCompleteInboundReceivingBayNotAssigned.PerformLayout();
            this.gpbOrderCompleteInboundReceivingBayAssigned.ResumeLayout(false);
            this.gpbOrderCompleteInboundReceivingBayAssigned.PerformLayout();
            this.gpbOrderCompleteOutbound.ResumeLayout(false);
            this.gpbOrderCompleteOutbound.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tpGeneral;
        private System.Windows.Forms.Label lblOptions;
        private KahlerAutomation.CheckedListBox clbOptions;
        private System.Windows.Forms.ComboBox cmbDefaultVolumeUnit;
        private System.Windows.Forms.Label lblDefaultVolumeUnit;
        private System.Windows.Forms.ComboBox cmbDefaultMassUnit;
        private System.Windows.Forms.Label lblDefaultMassUnit;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TextBox tbxNumericScalar;
        private System.Windows.Forms.Label lblNumericKeyboardScalar;
        private System.Windows.Forms.TextBox tbxAlphaNumericScalar;
        private System.Windows.Forms.Label lblAlphanumericKeyboardScalar;
        private System.Windows.Forms.Label lblTm2DatabaseVersion;
        private System.Windows.Forms.Label lblTm2StagingTerminalversion;
        private System.Windows.Forms.NumericUpDown nudMaxOnScreenTime;
        private System.Windows.Forms.Label lblMaxOnScreenTime;
        private System.Windows.Forms.ComboBox cmbUser;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblUserError;
        private System.Windows.Forms.TabPage tpTicketPrinters;
        private System.Windows.Forms.GroupBox gbpReceivingProductTicketPrinters;
        private System.Windows.Forms.ListView lvReceivingProductTicketPrinters;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.Label lblReceivingProductTicketCopies;
        private System.Windows.Forms.NumericUpDown nudReceivingProductTicketCopies;
        private System.Windows.Forms.GroupBox gpbReceivingProductPickTicketPrinters;
        private System.Windows.Forms.ListView lvReceivingProductPickTicketPrinters;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.Label lblReceivingProductPickTicketCopies;
        private System.Windows.Forms.NumericUpDown nudReceivingProductPickTicketCopies;
        private System.Windows.Forms.GroupBox gpbOutboundTicketPrinters;
        private System.Windows.Forms.ListView lvOutboundTicketPrinters;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Label lblOutboundTicketCopies;
        private System.Windows.Forms.NumericUpDown nudOutboundTicketCopies;
        private System.Windows.Forms.GroupBox gpbInboundBulkProductPrinters;
        private System.Windows.Forms.ListView lvInboundBulkProductPrinters;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Label lblInboundBulkProductCopies;
        private System.Windows.Forms.NumericUpDown nudInboundBulkProductCopies;
        private System.Windows.Forms.GroupBox gpbInboundBaggedProductPrinters;
        private System.Windows.Forms.ListView lvInboundBaggedProductPrinters;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label lblInboundBaggedProductCopies;
        private System.Windows.Forms.NumericUpDown nudInboundBaggedProductCopies;
        private System.Windows.Forms.GroupBox gpbInboundTruckInspection;
        private System.Windows.Forms.ListView lvInboundTruckInspectionPrinters;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Label lblInboundTruckInspectionCopies;
        private System.Windows.Forms.NumericUpDown nudInboundTruckInspectionCopies;
        private System.Windows.Forms.GroupBox gpbScale;
        private System.Windows.Forms.ComboBox cmbPanel;
        private System.Windows.Forms.Label lblPanel;
        private System.Windows.Forms.ComboBox cmbScaleDataStreamType;
        private System.Windows.Forms.Label lblScaleDataStreamType;
        private System.Windows.Forms.ComboBox cmbScaleCommType;
        private System.Windows.Forms.Label lblScaleCommType;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TabPage tpOwners;
        private System.Windows.Forms.GroupBox gpbOwnersRequiringRupForm;
        private KahlerAutomation.CheckedListBox cblOwnersRequiringRupForm;
        private System.Windows.Forms.GroupBox gpbPromptForShipToDestinationOwners;
        private KahlerAutomation.CheckedListBox cblPromptForShipToDestinationOwners;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.NumericUpDown nudDefaultMaxWeightForNewTransports;
        private System.Windows.Forms.Label lblDefaultMaxWeightForNewTransports;
        private System.Windows.Forms.TabPage tpPickTickets;
        private System.Windows.Forms.TextBox tbxInboundTruckInspectionWebAddress;
        private System.Windows.Forms.Label lblInboundTruckInspectionWebAddress;
        private System.Windows.Forms.TextBox tbxReceivingProductPickTicketWebAddress;
        private System.Windows.Forms.Label lblReceivingProductPickTicketWebAddress;
        private System.Windows.Forms.TextBox tbxInboundBaggedProductTicketWebAddress;
        private System.Windows.Forms.Label lblInboundBaggedProductTicketWebAddress;
        private System.Windows.Forms.TextBox tbxInboundBulkProductTicketWebAddress;
        private System.Windows.Forms.Label lblInboundBulkProductTicketWebAddress;
        private System.Windows.Forms.TabPage tpBayDirections;
        internal System.Windows.Forms.GroupBox gpbBaySettings;
        internal System.Windows.Forms.CheckBox chkBayAvailableForReceivingOrders;
        internal System.Windows.Forms.CheckBox chkBayAvailableForLoadoutOrders;
        internal System.Windows.Forms.TabControl tbcBayItemAvailability;
        internal System.Windows.Forms.TabPage tbpBulkIngredients;
        internal System.Windows.Forms.Panel pnlBayDirectionsBulkIngredientOptions;
        internal System.Windows.Forms.RadioButton rbBayDirectionsBulkIngredientUsePanelBulkProducts;
        internal System.Windows.Forms.RadioButton rbBayDirectionsBulkIngredientUseList;
        internal System.Windows.Forms.RadioButton rbBayDirectionsBulkIngredientAllowAll;
        internal System.Windows.Forms.Panel pnlBayBulkIngredientsCheckAll;
        internal System.Windows.Forms.Button cmdBayBulkIngredientsCheckAll;
        internal System.Windows.Forms.Button cmdBayBulkIngredientsUncheckAll;
        internal System.Windows.Forms.FlowLayoutPanel pnlBayBulkIngredientsAvailable;
        internal System.Windows.Forms.TabPage tbpProducts;
        internal System.Windows.Forms.Panel pnlBayProductsUncheckAll;
        internal System.Windows.Forms.Button cmdBayProductsCheckAll;
        internal System.Windows.Forms.Button cmdBayProductsUncheckAll;
        internal System.Windows.Forms.CheckBox chkBayAllowAllProducts;
        internal System.Windows.Forms.FlowLayoutPanel pnlBayProductsAvailable;
        internal System.Windows.Forms.TabPage tbpCustomers;
        internal System.Windows.Forms.Panel pnlBayCustomerCheckAll;
        internal System.Windows.Forms.Button cmdBayCustomerCheckAll;
        internal System.Windows.Forms.Button cmdBayCustomerUncheckAll;
        internal System.Windows.Forms.CheckBox chkBayAllowAllCustomers;
        internal System.Windows.Forms.FlowLayoutPanel pnlBayCustomersAvailable;
        internal System.Windows.Forms.TabPage tbpOwners;
        internal System.Windows.Forms.Panel pnlBayOwnerCheckAll;
        internal System.Windows.Forms.Button cmdBayOwnerCheckAll;
        internal System.Windows.Forms.Button cmdBayOwnerUncheckAll;
        internal System.Windows.Forms.CheckBox chkBayAllowAllOwners;
        internal System.Windows.Forms.FlowLayoutPanel pnlBayOwnersAvailable;
        internal System.Windows.Forms.TabPage tbpDrivers;
        internal System.Windows.Forms.Panel pnlBayDriverCheckAll;
        internal System.Windows.Forms.Button cmdBayDriverCheckAll;
        internal System.Windows.Forms.Button cmdBayDriverUncheckAll;
        internal System.Windows.Forms.CheckBox chkBayAllowAllDrivers;
        internal System.Windows.Forms.FlowLayoutPanel pnlBayDriversAvailable;
        internal System.Windows.Forms.TabPage tbpCarriers;
        internal System.Windows.Forms.Panel pnlBayCarriersCheckAll;
        internal System.Windows.Forms.Button cmdBayCarrierCheckAll;
        internal System.Windows.Forms.Button cmdBayCarrierUncheckAll;
        internal System.Windows.Forms.CheckBox chkBayAllowAllCarriers;
        internal System.Windows.Forms.FlowLayoutPanel pnlBayCarriersAvailable;
        internal System.Windows.Forms.TabPage tbpTransports;
        internal System.Windows.Forms.Panel pnlBayTransportCheckAll;
        internal System.Windows.Forms.Button cmdBayTransportCheckAll;
        internal System.Windows.Forms.Button cmdBayTransportUncheckAll;
        internal System.Windows.Forms.CheckBox chkBayAllowAllTransports;
        internal System.Windows.Forms.FlowLayoutPanel pnlBayTransportsAvailable;
        internal System.Windows.Forms.CheckBox chkBayEnabled;
        internal System.Windows.Forms.ComboBox cmbBay;
        private System.Windows.Forms.Label lblBay;
        internal System.Windows.Forms.CheckBox chkBayDirectionEnabled;
        private System.Windows.Forms.GroupBox gpbOwnersThatShouldNotDisplayMaplewoodAsShipTo;
        private KahlerAutomation.CheckedListBox cblOwnersThatShouldNotDisplayMaplewoodAsShipTo;
        private System.Windows.Forms.CheckBox cbxOutboundOptionsEnabled;
        private System.Windows.Forms.CheckBox cbxInboundOptionsEnabled;
        private KahlerAutomation.CheckedListBox clbOutboundOptions;
        private KahlerAutomation.CheckedListBox clbInboundOptions;
        private System.Windows.Forms.TabPage tpCompleteScreen;
        private System.Windows.Forms.GroupBox gpbOrderCompleteInboundOrder;
        private System.Windows.Forms.GroupBox gpbOrderCompleteInboundOrderBayNotAssigned;
        private System.Windows.Forms.TextBox tbxOrderCompleteInboundOrderBayNotAssignedNoTickets;
        private System.Windows.Forms.Label lblOrderCompleteInboundOrderBayNotAssignedNoTickets;
        private System.Windows.Forms.TextBox tbxOrderCompleteInboundOrderBayNotAssignedWithTickets;
        private System.Windows.Forms.Label lblOrderCompleteInboundOrderBayNotAssignedWithTickets;
        private System.Windows.Forms.GroupBox gpbOrderCompleteInboundOrderBayAssigned;
        private System.Windows.Forms.TextBox tbxOrderCompleteInboundOrderBayAssignedNoTickets;
        private System.Windows.Forms.Label lblOrderCompleteInboundOrderBayAssignedNoTickets;
        private System.Windows.Forms.TextBox tbxOrderCompleteInboundOrderBayAssignedWithTickets;
        private System.Windows.Forms.Label lblOrderCompleteInboundOrderBayAssignedWithTickets;
        private System.Windows.Forms.GroupBox gpbOrderCompleteOutbound;
        private System.Windows.Forms.TextBox tbxOrderCompleteOutboundNoTickets;
        private System.Windows.Forms.Label lblOrderCompleteOutboundNoTickets;
        private System.Windows.Forms.TextBox tbxOrderCompleteOutboundHasTickets;
        private System.Windows.Forms.Label lblOrderCompleteOutboundHasTickets;
        private System.Windows.Forms.GroupBox gpbOrderCompleteInboundReceiving;
        private System.Windows.Forms.GroupBox gpbOrderCompleteInboundReceivingBayNotAssigned;
        private System.Windows.Forms.TextBox tbxOrderCompleteInboundReceivingBayNotAssignedNoTickets;
        private System.Windows.Forms.Label lblOrderCompleteInboundReceivingBayNotAssignedNoTickets;
        private System.Windows.Forms.TextBox tbxOrderCompleteInboundReceivingBayNotAssignedHasTickets;
        private System.Windows.Forms.Label lblOrderCompleteInboundReceivingBayNotAssignedHasTickets;
        private System.Windows.Forms.GroupBox gpbOrderCompleteInboundReceivingBayAssigned;
        private System.Windows.Forms.TextBox tbxOrderCompleteInboundReceivingBayAssignedNoTickets;
        private System.Windows.Forms.Label lblOrderCompleteInboundReceivingBayAssignedNoTickets;
        private System.Windows.Forms.TextBox tbxOrderCompleteInboundReceivingBayAssignedHasTickets;
        private System.Windows.Forms.Label lblOrderCompleteInboundReceivingBayAssignedHasTickets;
        private System.Windows.Forms.Label label1;
    }
}