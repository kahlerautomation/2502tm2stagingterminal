﻿using System;
using System.Data;
using System.Windows.Forms;
using KahlerAutomation.KaTm2Database;
using KahlerAutomation.KaTm2LoadFramework;
using System.Collections.Generic;

namespace Tm2StagingTerminal {
    public partial class Confirm : StagingTerminalBaseControl {
        private DataTable _details = new DataTable();

        public Confirm() {
            InitializeComponent();
        }

        public void LoadDetails() {
            SetUpDataSet();
            bool showDisclaimer = !CurrentPanelStatus.IsInbound && CurrentPanelStatus.Order != null;
            if (showDisclaimer)
                dgvConfirmInfo.Top = lblDisclaimer.Bottom;
            else
                dgvConfirmInfo.Top = buttonBar.Bottom;

            lblDisclaimer.Visible = showDisclaimer;
            dgvConfirmInfo.Height = this.Height - dgvConfirmInfo.Top;

            PopulateDetails();
        }

        private void PopulateDetails() {
            dgvConfirmInfo.DataSource = null;
            _details.Clear();
            DataRow newInfoRow;
            KaUnit scaleUnit;
            try {
                scaleUnit = new KaUnit(Tm2Database.Connection, CurrentPanelStatus.ValidatedScaleWeight.UnitId);
            } catch (RecordNotFoundException) {
                scaleUnit = new KaUnit(Tm2Database.Connection, KahlerAutomation.KaTm2LoadFramework.LfDatabase.DefaultMassUnitId);
            }
            string formattedWeight = "{0:" + scaleUnit.UnitPrecision + "} " + scaleUnit.Abbreviation;

            KaQuantity totalLoadWeight = new KaQuantity(0, CurrentPanelStatus.ValidatedScaleWeight.UnitId);
            if (CurrentPanelStatus.IsInbound) {
                if (CurrentPanelStatus.StagedOrder != null)
                    totalLoadWeight.Numeric = CurrentPanelStatus.StagedOrder.GetRequestedQuantity(Tm2Database.Connection, null, CurrentPanelStatus.ValidatedScaleWeight.UnitId);
                else if (CurrentPanelStatus.ReceivingPurchaseOrder != null)
                    totalLoadWeight = new KaQuantity(Math.Max(0, CurrentPanelStatus.ReceivingPurchaseOrder.Purchased - CurrentPanelStatus.ReceivingPurchaseOrder.Delivered), CurrentPanelStatus.ReceivingPurchaseOrder.UnitId);
                else if (CurrentPanelStatus.Order != null)
                    totalLoadWeight.Numeric = Math.Max(0, CurrentPanelStatus.Order.GetRequested(LfDatabase.LocationId, totalLoadWeight.UnitId).Numeric - CurrentPanelStatus.Order.GetDelivered(LfDatabase.LocationId, totalLoadWeight.UnitId).Numeric);
            } else { // outbound
                     //ToDo: calculate total weigh that will be accepted...
                if (CurrentPanelStatus.StagedOrder != null) {
                    if (Database.IsPalletedLoad(CurrentPanelStatus.StagedOrder))
                        totalLoadWeight.Numeric = CurrentPanelStatus.StagedOrder.GetRequestedQuantity(Tm2Database.Connection, null, CurrentPanelStatus.ValidatedScaleWeight.UnitId);
                    else {
                        foreach (KaStagedOrderTransport sot in CurrentPanelStatus.StagedOrder.Transports) {
                            if ((CurrentPanelStatus.Transport != null && sot.TransportId.Equals(CurrentPanelStatus.Transport.Id)) || (CurrentPanelStatus.Transport == null && sot.TransportId.Equals(Guid.Empty)))
                                totalLoadWeight = new KaQuantity(CurrentPanelStatus.ValidatedScaleWeight.Numeric - KaUnit.Convert(Tm2Database.Connection, new KaQuantity(sot.TareWeight, sot.TareUnitId), new KaRatio(0, LfDatabase.DefaultMassUnitId, LfDatabase.DefaultVolumeUnitId), CurrentPanelStatus.ValidatedScaleWeight.UnitId).Numeric, CurrentPanelStatus.ValidatedScaleWeight.UnitId);

                        }
                    }
                }
                if (CurrentPanelStatus.InProgress != null) { // cycle through the weighemnts to determine the amount to display
                    KaInProgress ipo = CurrentPanelStatus.InProgress;
                    foreach (KaInProgressWeighment w in ipo.Weighments) {
                        if (!w.Complete) {
                            if (ipo.ReceivingPurchaseOrderId.Equals(Guid.Empty)) {
                                totalLoadWeight = new KaQuantity(CurrentPanelStatus.ValidatedScaleWeight.Numeric - KaUnit.Convert(Tm2Database.Connection, new KaQuantity(w.Tare, w.UnitId), new KaRatio(w.AverageDensity, w.WeightUnitId, w.VolumeUnitId), CurrentPanelStatus.ValidatedScaleWeight.UnitId).Numeric, CurrentPanelStatus.ValidatedScaleWeight.UnitId);
                            } else {
                                totalLoadWeight = new KaQuantity(KaUnit.Convert(Tm2Database.Connection, new KaQuantity(w.Gross, w.UnitId), new KaRatio(w.AverageDensity, w.WeightUnitId, w.VolumeUnitId), CurrentPanelStatus.ValidatedScaleWeight.UnitId).Numeric - CurrentPanelStatus.ValidatedScaleWeight.Numeric, CurrentPanelStatus.ValidatedScaleWeight.UnitId);
                            }
                            break;
                        }
                    }
                }
            }

            if (CurrentPanelStatus.Order != null) {
                newInfoRow = _details.NewRow();
                newInfoRow["Title"] = "Load:";
                newInfoRow["Info"] = CurrentPanelStatus.Order.Number;
                _details.Rows.Add(newInfoRow);

                if (CurrentPanelStatus.Order.OrderAccounts.Count > 0) {
                    KaCustomerAccount customer = new KaCustomerAccount(Tm2Database.Connection, CurrentPanelStatus.Order.OrderAccounts[0].CustomerAccountId);
                    newInfoRow = _details.NewRow();
                    newInfoRow["Title"] = "Customer Name:";
                    newInfoRow["Info"] = customer.Name;
                    _details.Rows.Add(newInfoRow);

                    newInfoRow = _details.NewRow();
                    newInfoRow["Title"] = "Customer Acct:";
                    newInfoRow["Info"] = customer.AccountNumber;
                    try {
                        newInfoRow["Info"] = new KaCustomerAccountInterfaceSettings(Tm2Database.Connection, CurrentPanelStatus.Order.OrderAccounts[0].AccountInterfaceSettingId).CrossReference;
                    } catch (Exception) { /* suppress */ }
                    if (CurrentPanelStatus.Order.OrderAccounts[0].AccountInterfaceSettingId.Equals(Guid.Empty))
                        _details.Rows.Add(newInfoRow);
                }
                if (CurrentPanelStatus.CustomerAccountLocation != null) {
                    newInfoRow = _details.NewRow();
                    newInfoRow["Title"] = "Ship to:";
                    newInfoRow["Info"] = CurrentPanelStatus.CustomerAccountLocation.Name;
                    _details.Rows.Add(newInfoRow);
                }
            } else if (CurrentPanelStatus.ReceivingPurchaseOrder != null) {
                newInfoRow = _details.NewRow();
                newInfoRow["Title"] = "Load:";
                newInfoRow["Info"] = CurrentPanelStatus.ReceivingPurchaseOrder.Number;
                _details.Rows.Add(newInfoRow);
                try {
                    newInfoRow = _details.NewRow();
                    newInfoRow["Title"] = "Supplier:";
                    newInfoRow["Info"] = new KaSupplierAccount(Tm2Database.Connection, CurrentPanelStatus.ReceivingPurchaseOrder.SupplierAccountId).Name;
                    _details.Rows.Add(newInfoRow);
                } catch (Exception) { /* suppress */ }
            }

            if (CurrentPanelStatus.Transport != null) {
                newInfoRow = _details.NewRow();
                newInfoRow["Title"] = "Truck:";
                newInfoRow["Info"] = CurrentPanelStatus.Transport.Name + (CurrentPanelStatus.Transport.Number.Length > 0 ? " {" + CurrentPanelStatus.Transport.Number + "}" : "") + (CurrentPanelStatus.StagedOrder != null ? $" with { CurrentPanelStatus.StagedOrder.Compartments.Count } compartment{ (CurrentPanelStatus.StagedOrder.Compartments.Count > 1 ? "s" : "")}." : "");
                _details.Rows.Add(newInfoRow);
            }

            if (CurrentPanelStatus.IsInbound) {
                newInfoRow = _details.NewRow();
                if (CurrentPanelStatus.Order != null)
                    newInfoRow["Title"] = "Tare Weight:";
                else if (CurrentPanelStatus.ReceivingPurchaseOrder != null)
                    newInfoRow["Title"] = "Gross Weight:";

                newInfoRow["Info"] = string.Format(formattedWeight, CurrentPanelStatus.ValidatedScaleWeight.Numeric);
                _details.Rows.Add(newInfoRow);
            } else { // outbound
                if (CurrentPanelStatus.Order != null || (CurrentPanelStatus.StagedOrder != null && CurrentPanelStatus.StagedOrder.Orders.Count > 0)) {
                    newInfoRow = _details.NewRow();
                    newInfoRow["Title"] = "Gross Weight:";
                    newInfoRow["Info"] = string.Format(formattedWeight, CurrentPanelStatus.ValidatedScaleWeight.Numeric);
                    _details.Rows.Add(newInfoRow);
                    if (CurrentPanelStatus.StagedOrder.Transports.Count > 0) {
                        try {
                            KaUnit transportTareUnit = new KaUnit(Tm2Database.Connection, CurrentPanelStatus.StagedOrder.Transports[0].TareUnitId);
                            newInfoRow = _details.NewRow();
                            newInfoRow["Title"] = "Tare Weight:";
                            newInfoRow["Info"] = string.Format("{0:" + transportTareUnit.UnitPrecision + "} {1}", CurrentPanelStatus.StagedOrder.Transports[0].TareWeight, transportTareUnit.Abbreviation);
                            _details.Rows.Add(newInfoRow);
                        } catch (RecordNotFoundException) { /* Suppress */ }
                    }
                } else if (CurrentPanelStatus.ReceivingPurchaseOrder != null) {
                    // inbound weight
                    double grossWeight = double.MaxValue;
                    if (CurrentPanelStatus.InProgress != null) {
                        for (int weighmentCounter = CurrentPanelStatus.InProgress.Weighments.Count - 1; weighmentCounter >= 0; weighmentCounter--) {
                            KaInProgressWeighment weighment = CurrentPanelStatus.InProgress.Weighments[weighmentCounter];
                            if ((weighment.TransportId == Guid.Empty || (CurrentPanelStatus.Transport != null && weighment.TransportId == CurrentPanelStatus.Transport.Id)) && weighment.Gross > 0) {
                                grossWeight = weighment.Gross;
                                break;
                            }
                        }
                    }
                    if (grossWeight < double.MaxValue) {
                        newInfoRow = _details.NewRow();
                        newInfoRow["Title"] = "Gross Weight:";
                        newInfoRow["Info"] = string.Format(formattedWeight, grossWeight);
                        _details.Rows.Add(newInfoRow);
                    }

                    newInfoRow = _details.NewRow();
                    newInfoRow["Title"] = "Tare Weight:";
                    newInfoRow["Info"] = string.Format(formattedWeight, CurrentPanelStatus.ValidatedScaleWeight.Numeric);
                    _details.Rows.Add(newInfoRow);
                }

                newInfoRow = _details.NewRow();
                newInfoRow["Title"] = "Net Weight:";
                newInfoRow["Info"] = string.Format(formattedWeight, totalLoadWeight.Numeric);
                _details.Rows.Add(newInfoRow);
            }

            if (CurrentPanelStatus.Driver != null) {
                newInfoRow = _details.NewRow();
                newInfoRow["Title"] = "Driver:";
                newInfoRow["Info"] = CurrentPanelStatus.Driver.Name + (CurrentPanelStatus.Driver.Number.Length > 0 ? " {" + CurrentPanelStatus.Driver.Number + "}" : "");
                _details.Rows.Add(newInfoRow);
            }

            if (CurrentPanelStatus.Carrier != null) {
                newInfoRow = _details.NewRow();
                newInfoRow["Title"] = "Carrier:";
                newInfoRow["Info"] = CurrentPanelStatus.Carrier.Name + (CurrentPanelStatus.Carrier.Number.Length > 0 ? " {" + CurrentPanelStatus.Carrier.Number + "}" : "");
                _details.Rows.Add(newInfoRow);
            }
            dgvConfirmInfo.DataSource = _details;
            dgvConfirmInfo.Columns["Title"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvConfirmInfo.Columns["Info"].FillWeight = 100;
            dgvConfirmInfo.Columns["Info"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvConfirmInfo.Refresh();
        }

        private void SetUpDataSet() {
            _details = new DataTable("Data");
            _details.Columns.Add("Title");
            _details.Columns.Add("Info");
        }

        private void Confirm_Resize(object sender, EventArgs e) {
            lblDisclaimer.Width = this.Width;
            dgvConfirmInfo.Width = this.Width;
        }
    }
}
