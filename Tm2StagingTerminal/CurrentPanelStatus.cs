﻿using System;
using System.Collections.Generic;
using System.Collections;
using KahlerAutomation.KaTm2Database;
using KahlerAutomation.KaTm2LoadFramework;

namespace Tm2StagingTerminal {
    class CurrentPanelStatus {
        static private bool _isInbound = false;
        static public bool IsInbound {
            get { return _isInbound; }
            set { _isInbound = value; }
        }

        static private KaOrder _order = null;
        static public KaOrder Order {
            get { return _order; }
            set { _order = value; }
        }

        static private KaReceivingPurchaseOrder _receivingPurchaseOrder = null;
        static public KaReceivingPurchaseOrder ReceivingPurchaseOrder {
            get { return _receivingPurchaseOrder; }
            set { _receivingPurchaseOrder = value; }
        }

        static private string _refinedOrderNumberFilter = "";
        static public string RefinedOrderNumberFilter {
            get { return _refinedOrderNumberFilter; }
            set { _refinedOrderNumberFilter = value; }
        }

        static private KaDriver _driver = null;
        static public KaDriver Driver {
            get { return _driver; }
            set { _driver = value; }
        }

        static private KaTransport _transport = null;
        static public KaTransport Transport {
            get { return _transport; }
            set { _transport = value; }
        }

        static private KaCarrier _carrier = null;
        static public KaCarrier Carrier {
            get { return _carrier; }
            set { _carrier = value; }
        }

        static private KaCustomerAccountLocation _customerAccountLocation = null;
        static public KaCustomerAccountLocation CustomerAccountLocation {
            get { return _customerAccountLocation; }
            set { _customerAccountLocation = value; }
        }

        static private List<KaCustomerAccountLocation> _customerAccountLocations = null;
        static public List<KaCustomerAccountLocation> CustomerAccountLocations {
            get { return _customerAccountLocations; }
            set { _customerAccountLocations = value; }
        }

        static private string _shipToCity = "";
        static public string ShipToCity {
            get { return _shipToCity; }
            set { _shipToCity = value; }
        }

        static private string _shipToState = "";
        static public string ShipToState {
            get { return _shipToState; }
            set { _shipToState = value; }
        }

        static private string _shipToZip = "";
        static public string ShipToZip {
            get { return _shipToZip; }
            set { _shipToZip = value; }
        }

        static private KaStagedOrder _stagedOrder = null;
        static public KaStagedOrder StagedOrder {
            get { return _stagedOrder; }
            set { _stagedOrder = value; }
        }

        static private List<KaStagedOrder> _stagedOrders = new List<KaStagedOrder>();
        static public List<KaStagedOrder> StagedOrders {
            get { return _stagedOrders; }
            set { _stagedOrders = value; }
        }

        static private KaInProgress _inProgress = null;
        static public KaInProgress InProgress {
            get { return _inProgress; }
            set { _inProgress = value; }
        }

        static private List<KaCustomLoadQuestionFields> _customLoadQuestions = new List<KaCustomLoadQuestionFields>();
        static public List<KaCustomLoadQuestionFields> CustomLoadQuestions {
            get { return _customLoadQuestions; }
            set { _customLoadQuestions = value; }
        }

        static private int _customLoadQuestionsCounter = -1;
        static public int CustomLoadQuestionsCounter {
            get { return _customLoadQuestionsCounter; }
            set { _customLoadQuestionsCounter = value; }
        }

        static private KaQuantity _validatedScaleWeight;
        static public KaQuantity ValidatedScaleWeight {
            get { return _validatedScaleWeight; }
            set { _validatedScaleWeight = value; }
        }

        static private bool _truckCleaned = false;
        static public bool TruckCleaned {
            get { return _truckCleaned; }
            set { _truckCleaned = value; }
        }

        static private bool _truckNotUsedRupp = false;
        static public bool TruckNotUsedRupp {
            get { return _truckNotUsedRupp; }
            set { _truckNotUsedRupp = value; }
        }

        private static string _previousLoadProduct = "";
        public static string PreviousLoadProduct {
            get { return _previousLoadProduct; }
            set { _previousLoadProduct = value; }
        }

        private static Guid _bulkProductId = Guid.Empty;
        public static Guid BulkProductId {
            get { return _bulkProductId; }
            set { _bulkProductId = value; }
        }

        private static Guid _panelId = Guid.Empty;
        public static Guid PanelId {
            get { return _panelId; }
            set { _panelId = value; }
        }

        private static Guid _orderItemId = Guid.Empty;
        public static Guid OrderItemId {
            get { return _orderItemId; }
            set { _orderItemId = value; }
        }

        private static Guid _productId = Guid.Empty;
        public static Guid ProductId {
            get { return _productId; }
            set { _productId = value; }
        }

        private static Guid _stagedOrderInfoCompartmentItemId = Guid.Empty;
        public static Guid StagedOrderInfoCompartmentItemId {
            get { return _stagedOrderInfoCompartmentItemId; }
            set { _stagedOrderInfoCompartmentItemId = value; }
        }

        static private TerminalSettings.BaySetting _bayAssigned = null;
        static public TerminalSettings.BaySetting BayAssigned {
            get { return _bayAssigned; }
            set { _bayAssigned = value; }
        }

        static public void ResetVariables() {
            if (Database.Settings.Inbound.Enabled && !Database.Settings.Outbound.Enabled)
                _isInbound = true;
            else if (!Database.Settings.Inbound.Enabled && Database.Settings.Outbound.Enabled)
                _isInbound = false;
            _driver = null;
            _transport = null;
            _carrier = null;
            _order = null;
            _receivingPurchaseOrder = null;
            _refinedOrderNumberFilter = "";
            _inProgress = null;
            _stagedOrder = null;
            _stagedOrders = new List<KaStagedOrder>();
            _customerAccountLocation = null;
            _customerAccountLocations = null;
            _shipToCity = "";
            _shipToState = "";
            _shipToZip = "";
            _customLoadQuestions = new List<KaCustomLoadQuestionFields>();
            _customLoadQuestionsCounter = -1;
            if (_validatedScaleWeight == null || _validatedScaleWeight.UnitId.Equals(Guid.Empty))
                _validatedScaleWeight = new KaQuantity(0, LfDatabase.DefaultMassUnitId);
            else
                _validatedScaleWeight = new KaQuantity(0, CurrentPanelStatus.ValidatedScaleWeight.UnitId);
            _truckCleaned = false;
            _truckNotUsedRupp = false;
            _previousLoadProduct = "";
            _bulkProductId = Guid.Empty;
            _panelId = Guid.Empty;
            _orderItemId = Guid.Empty;
            _productId = Guid.Empty;
            _stagedOrderInfoCompartmentItemId = Guid.Empty;
            _bayAssigned = null;
        }
    }
}
