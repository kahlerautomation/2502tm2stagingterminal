﻿using System;
using KahlerAutomation.KaTm2Database;
using KahlerAutomation.KaTm2LoadFramework;
using System.Windows.Forms;

namespace Tm2StagingTerminal {
    public partial class OrderDetails : StagingTerminalBaseControl {
        public OrderDetails() {
            InitializeComponent();
        }

        public void OrderDetailsLoaded() {
            lstProducts.Items.Clear();
            try {
                double totalWeight = 0;
                double totalRemainingWeight = 0;
                bool totalValid = true;
                KaOrder order = CurrentPanelStatus.Order;
                int maxAccount = 0;
                int accountCount = 0;
                while (accountCount < order.OrderAccounts.Count) {
                    if (order.OrderAccounts[accountCount].Percentage > order.OrderAccounts[maxAccount].Percentage)
                        maxAccount = accountCount;

                    accountCount += 1;
                }

                if (order.OrderAccounts.Count > 0 && order.OrderAccounts[maxAccount].CustomerAccountId != Guid.Empty) {
                    KaCustomerAccount customer = new KaCustomerAccount(Tm2Database.Connection, order.OrderAccounts[maxAccount].CustomerAccountId);
                    lblCustomer.Text = "Customer: " + customer.Name;
                    lblAccount.Text = "Account: " + customer.AccountNumber;
                } else {
                    lblCustomer.Text = "";
                    lblAccount.Text = "";
                }
                Title = "Order details for " + order.Number;
                int entryCount = 0;

                KaUnit validatedScaleUnit = new KaUnit(Tm2Database.Connection, CurrentPanelStatus.ValidatedScaleWeight.UnitId);
                string formattedWeightString;
                while (entryCount < order.OrderItems.Count) {
                    KaOrderItem orderEntry = order.OrderItems[entryCount];
                    ListViewItem li = new ListViewItem();

                    KaProduct product = new KaProduct(Tm2Database.Connection, orderEntry.ProductId);
                    KaRatio density = product.GetDensity(Tm2Database.Connection, LfDatabase.LocationId, null);
                    KaQuantity converted = KaUnit.Convert(Tm2Database.Connection, new KaQuantity(orderEntry.Request, orderEntry.UnitId), density, validatedScaleUnit.Id);
                    if (converted.Numeric != 0) {
                        totalWeight = totalWeight + converted.Numeric;
                    } else {
                        converted = new KaQuantity(orderEntry.Request, orderEntry.UnitId);
                        totalValid = false;
                        break;
                    }
                    KaQuantity convertedRemaining = KaUnit.Convert(Tm2Database.Connection, new KaQuantity(Math.Max(orderEntry.Request - orderEntry.Delivered, 0), orderEntry.UnitId), density, converted.UnitId);
                    totalRemainingWeight += convertedRemaining.Numeric;
                    KaUnit convertedUnit = new KaUnit(Tm2Database.Connection, converted.UnitId);
                    formattedWeightString = "{0:" + convertedUnit.UnitPrecision + "} " + convertedUnit.Abbreviation;

                    li.Text = product.Name;
                    li.SubItems.Add(string.Format(formattedWeightString, converted.Numeric) + " (" + string.Format(formattedWeightString, convertedRemaining.Numeric) + " Remaining)");
                    lstProducts.Items.Add(li);
                    entryCount += 1;
                }

                if (totalValid) {
                    formattedWeightString = "{0:" + validatedScaleUnit.UnitPrecision + "} " + validatedScaleUnit.Abbreviation;
                    lblTotal.Text = string.Format(formattedWeightString, totalWeight) + " (" + string.Format(formattedWeightString, Math.Max(totalRemainingWeight, 0)) + " Remaining)";
                }
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        private void OrderDetails_Resize(object sender, EventArgs e) {
            lblAccount.Width = this.Width;
            lblCustomer.Width = this.Width;
            lblTotal.Top = this.Height - lblTotal.Height;
            lblTotal.Width = (this.ClientSize.Width - (this.Margin.Horizontal * 2)) / 2;
            lblTotalLabel.Width = lblTotal.Width;
            lblTotalLabel.Top = lblTotal.Top;
            lblTotal.Left = lblTotalLabel.Width + this.Margin.Horizontal;
            lstProducts.Width = this.Width - (2 * lstProducts.Left);
            lstProducts.Height = lblTotal.Top - lstProducts.Top - this.Margin.Vertical;
            chProduct.Width = (lstProducts.Width - 25) / 2;
            chRequest.Width = chProduct.Width;
        }
    }
}
