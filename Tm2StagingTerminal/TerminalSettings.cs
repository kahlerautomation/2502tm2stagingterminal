﻿using System;
using System.Collections;
using System.Collections.Generic;
using KahlerAutomation.KaTm2Database;
using System.Data.OleDb;

namespace Tm2StagingTerminal {
    [Serializable()]
    public class TerminalSettings {
        private Guid _userId = Guid.Empty;
        public Guid UserId {
            get { return _userId; }
            set {
                _userId = value;
            }
        }

        private bool _enforceOrderLocking = true;
        public bool EnforceOrderLocking {
            get { return _enforceOrderLocking; }
            set { _enforceOrderLocking = value; }
        }

        private bool _doNotCreateTicketForLoadWithNothingDispensed = true;
        public bool DoNotCreateTicketForLoadWithNothingDispensed {
            get { return _doNotCreateTicketForLoadWithNothingDispensed; }
            set { _doNotCreateTicketForLoadWithNothingDispensed = value; }
        }

        private bool _automaticallyAddCompartmentsForTransport = true;
        public bool AutomaticallyAddCompartmentForTransport {
            get { return _automaticallyAddCompartmentsForTransport; }
            set { _automaticallyAddCompartmentsForTransport = value; }
        }

        private bool _useAlphaNumKeyboardForDrivers = true;
        public bool UseAlaphaNumKeyboardForDrivers {
            get { return _useAlphaNumKeyboardForDrivers; }
            set {
                _useAlphaNumKeyboardForDrivers = value;
            }
        }

        private bool _useAlphaNumKeyboardForTransports = true;
        public bool UseAlaphaNumKeyboardForTransports {
            get { return _useAlphaNumKeyboardForTransports; }
            set {
                _useAlphaNumKeyboardForTransports = value;
            }
        }

        private bool _useAlphaNumKeyboardForCarriers = true;
        public bool UseAlaphaNumKeyboardForCarriers {
            get { return _useAlphaNumKeyboardForCarriers; }
            set {
                _useAlphaNumKeyboardForCarriers = value;
            }
        }

        private bool _useAlphaNumKeyboardForLoadNumbers = true;
        public bool UseAlaphaNumKeyboardForLoadNumbers {
            get { return _useAlphaNumKeyboardForLoadNumbers; }
            set {
                _useAlphaNumKeyboardForLoadNumbers = value;
            }
        }

        private bool _includeOrderNumbersThatHaveDashesInTheNumberInLookupForValidOrders = false;
        public bool IncludeOrderNumbersThatHaveDashesInTheNumberInLookupForValidOrders {
            get { return _includeOrderNumbersThatHaveDashesInTheNumberInLookupForValidOrders; }
            set { _includeOrderNumbersThatHaveDashesInTheNumberInLookupForValidOrders = value; }
        }

        private bool _includeOrderPurchaseOrderNumberInLookupForValidOrders = false;
        public bool IncludeOrderPurchaseOrderNumberInLookupForValidOrders {
            get { return _includeOrderPurchaseOrderNumberInLookupForValidOrders; }
            set { _includeOrderPurchaseOrderNumberInLookupForValidOrders = value; }
        }

        private bool _includeOrderReleaseNumberInLookupForValidOrders = false;
        public bool IncludeOrderReleaseNumberInLookupForValidOrders {
            get { return _includeOrderReleaseNumberInLookupForValidOrders; }
            set { _includeOrderReleaseNumberInLookupForValidOrders = value; }
        }

        private int _maxOnScreenTime = 0;
        public int MaxOnScreenTime {
            get { return _maxOnScreenTime; }
            set { _maxOnScreenTime = value; }
        }

        private double _numericKeyboardScalar = 100;
        public double NumericKeyboardScalar {
            get { return _numericKeyboardScalar; }
            set {
                _numericKeyboardScalar = value;
            }
        }

        private double _alphaNumericKeyboardScalar = 100;
        public double AlphaNumericKeyboardScalar {
            get { return _alphaNumericKeyboardScalar; }
            set {
                _alphaNumericKeyboardScalar = value;
            }
        }

        private bool _showOkButtonForSelectFromList = true;
        public bool ShowOkButtonForSelectFromList {
            get { return _showOkButtonForSelectFromList; }
            set { _showOkButtonForSelectFromList = value; }
        }

        private bool _emailTickets = true;
        public bool EmailTickets {
            get { return _emailTickets; }
            set { _emailTickets = value; }
        }

        private bool _moveDriversToHistory = true;
        public bool MoveDriversToHistory {
            get { return _moveDriversToHistory; }
            set { _moveDriversToHistory = value; }
        }

        private bool _moveTransportsToHistory = true;
        public bool MoveTransportsToHistory {
            get { return _moveTransportsToHistory; }
            set { _moveTransportsToHistory = value; }
        }

        private bool _allowDriversToAddDestinations = true;
        public bool AllowDriversToAddDestinations {
            get { return _allowDriversToAddDestinations; }
            set { _allowDriversToAddDestinations = value; }
        }

        private bool _showDontCareInDestinationList = false;
        public bool ShowDontCareInDestinationList {
            get { return _showDontCareInDestinationList; }
            set { _showDontCareInDestinationList = value; }
        }

        private bool _showOrderDetailScreen = true;
        public bool ShowOrderDetailScreen {
            get { return _showOrderDetailScreen; }
            set { _showOrderDetailScreen = value; }
        }

        private DirectionSpecificSettings _inbound = new DirectionSpecificSettings();
        public DirectionSpecificSettings Inbound {
            get { return _inbound; }
            set { _inbound = value; }
        }

        private DirectionSpecificSettings _outbound = new DirectionSpecificSettings();
        public DirectionSpecificSettings Outbound {
            get { return _outbound; }
            set { _outbound = value; }
        }

        private List<Guid> _promptForShipToDestinationOwners = new List<Guid>();
        public List<Guid> PromptForShipToDestinationOwners {
            get { return _promptForShipToDestinationOwners; }
            set { _promptForShipToDestinationOwners = value; }
        }

        private bool _showFinalConfirmationScreen = true;
        public bool ShowFinalConfirmationScreen {
            get { return _showFinalConfirmationScreen; }
            set { _showFinalConfirmationScreen = value; }
        }

        private double _defaultMaxWeightForNewTransports = 80000;
        public double DefaultMaxWeightForNewTransports {
            get { return _defaultMaxWeightForNewTransports; }
            set { _defaultMaxWeightForNewTransports = value; }
        }
        private string _webPickTicketAddress = "";
        public string WebPickTicketAddress {
            get { return _webPickTicketAddress; }
            set { _webPickTicketAddress = value; }
        }

        private string _truckInspectionTicketAddress = "";
        public string TruckInspectionTicketAddress {
            get { return _truckInspectionTicketAddress; }
            set { _truckInspectionTicketAddress = value; }
        }

        private string _baggedProductPickTicketAddress = "";
        public string BaggedProductPickTicketAddress {
            get { return _baggedProductPickTicketAddress; }
            set { _baggedProductPickTicketAddress = value; }
        }

        private string _webPoPickTicketAddress = "";
        public string WebPoPickTicketAddress {
            get { return _webPoPickTicketAddress; }
            set { _webPoPickTicketAddress = value; }
        }

        private List<Guid> _ownersIgnoreMaplewoodShipTo = new List<Guid>();
        public List<Guid> OwnersIgnoreMaplewoodShipTo {
            get { return _ownersIgnoreMaplewoodShipTo; }
            set { _ownersIgnoreMaplewoodShipTo = value; }
        }

        private ScaleSettings _scale = new ScaleSettings();
        public ScaleSettings Scale {
            get { return _scale; }
            set { _scale = value; }
        }

        #region Printers
        private List<PrinterConfiguration> _inboundBaggedProductPrinters = new List<PrinterConfiguration>();
        public List<PrinterConfiguration> InboundBaggedProductPrinters {
            get { return _inboundBaggedProductPrinters; }
            set { _inboundBaggedProductPrinters = value; }
        }

        private List<PrinterConfiguration> _inboundBulkProductPrinters = new List<PrinterConfiguration>();
        public List<PrinterConfiguration> InboundBulkProductPrinters {
            get { return _inboundBulkProductPrinters; }
            set { _inboundBulkProductPrinters = value; }
        }

        private List<PrinterConfiguration> _inboundTruckInspectionPrinters = new List<PrinterConfiguration>();
        public List<PrinterConfiguration> InboundTruckInspectionPrinters {
            get { return _inboundTruckInspectionPrinters; }
            set { _inboundTruckInspectionPrinters = value; }
        }

        private List<PrinterConfiguration> _outboundTicketPrinters = new List<PrinterConfiguration>();
        public List<PrinterConfiguration> OutboundTicketPrinters {
            get { return _outboundTicketPrinters; }
            set { _outboundTicketPrinters = value; }
        }

        private List<PrinterConfiguration> _receivingProductPickTicketPrinters = new List<PrinterConfiguration>();
        public List<PrinterConfiguration> ReceivingProductPickTicketPrinters {
            get { return _receivingProductPickTicketPrinters; }
            set { _receivingProductPickTicketPrinters = value; }
        }

        private List<PrinterConfiguration> _receivingProductTicketPrinters = new List<PrinterConfiguration>();
        public List<PrinterConfiguration> ReceivingProductTicketPrinters {
            get { return _receivingProductTicketPrinters; }
            set { _receivingProductTicketPrinters = value; }
        }
        #endregion

        private List<BaySetting> _bays = new List<TerminalSettings.BaySetting>();
        public List<BaySetting> Bays {
            get { return _bays; }
            set { _bays = value; }
        }

        private bool _bayDirectionEnabled = false;
        public bool BayDirectionEnabled {
            get { return _bayDirectionEnabled; }
            set { _bayDirectionEnabled = value; }
        }

        #region 
        private string _orderCompleteInboundOrderBayNotAssignedNoTickets = "Thank you.<br>Please drive ahead.";
        public string OrderCompleteInboundOrderBayNotAssignedNoTickets {
            get { return _orderCompleteInboundOrderBayNotAssignedNoTickets; }
            set { _orderCompleteInboundOrderBayNotAssignedNoTickets = value; }
        }

        private string _orderCompleteInboundOrderBayNotAssignedHasTickets = "Please drive ahead<br>and take a copy of your loading ticket with you.";
        public string OrderCompleteInboundOrderBayNotAssignedHasTickets {
            get { return _orderCompleteInboundOrderBayNotAssignedHasTickets; }
            set { _orderCompleteInboundOrderBayNotAssignedHasTickets = value; }
        }

        private string _orderCompleteInboundOrderBayAssignedNoTickets = "Thank you.<br>Please proceed to <bay>.";
        public string OrderCompleteInboundOrderBayAssignedNoTickets {
            get { return _orderCompleteInboundOrderBayAssignedNoTickets; }
            set { _orderCompleteInboundOrderBayAssignedNoTickets = value; }
        }

        private string _orderCompleteInboundOrderBayAssignedHasTickets = "Please proceed to <bay><br>and take a copy of your loading ticket with you.";
        public string OrderCompleteInboundOrderBayAssignedHasTickets {
            get { return _orderCompleteInboundOrderBayAssignedHasTickets; }
            set { _orderCompleteInboundOrderBayAssignedHasTickets = value; }
        }

        private string _orderCompleteInboundReceivingBayNotAssignedNoTickets = "Thank you.<br>Please drive ahead.";
        public string OrderCompleteInboundReceivingBayNotAssignedNoTickets {
            get { return _orderCompleteInboundReceivingBayNotAssignedNoTickets; }
            set { _orderCompleteInboundReceivingBayNotAssignedNoTickets = value; }
        }

        private string _orderCompleteInboundReceivingBayNotAssignedHasTickets = "Please drive ahead<br>and take a copy of your loading ticket with you.";
        public string OrderCompleteInboundReceivingBayNotAssignedHasTickets {
            get { return _orderCompleteInboundReceivingBayNotAssignedHasTickets; }
            set { _orderCompleteInboundReceivingBayNotAssignedHasTickets = value; }
        }

        private string _orderCompleteInboundReceivingBayAssignedNoTickets = "Thank you.<br>Please proceed to <bay>.";
        public string OrderCompleteInboundReceivingBayAssignedNoTickets {
            get { return _orderCompleteInboundReceivingBayAssignedNoTickets; }
            set { _orderCompleteInboundReceivingBayAssignedNoTickets = value; }
        }

        private string _orderCompleteInboundReceivingBayAssignedHasTickets = "Please proceed to <bay><br>and take a copy of your loading ticket with you.";
        public string OrderCompleteInboundReceivingBayAssignedHasTickets {
            get { return _orderCompleteInboundReceivingBayAssignedHasTickets; }
            set { _orderCompleteInboundReceivingBayAssignedHasTickets = value; }
        }

        private string _orderCompleteOutboundHasTickets = "Please drive ahead<br>and take the 2 printed tickets with you.";
        public string OrderCompleteOutboundHasTickets {
            get { return _orderCompleteOutboundHasTickets; }
            set { _orderCompleteOutboundHasTickets = value; }
        }

        private string _orderCompleteOutboundNoTickets = "Thank you.<br>Please drive ahead.";
        public string OrderCompleteOutboundNoTickets {
            get { return _orderCompleteOutboundNoTickets; }
            set { _orderCompleteOutboundNoTickets = value; }
        }
        #endregion

        [Serializable()]
        public class DirectionSpecificSettings {
            private bool _enabled = true;
            public bool Enabled {
                get { return _enabled; }
                set { _enabled = value; }
            }

            private bool _promptForDriverNumber = true;
            public bool PromptForDriverNumber {
                get { return _promptForDriverNumber; }
                set { _promptForDriverNumber = value; }
            }

            private bool _validateDriverNumber = false;
            public bool ValidateDriverNumber {
                get { return _validateDriverNumber; }
                set { _validateDriverNumber = value; }
            }

            private bool _promptForCarrierNumber = true;
            public bool PromptForCarrierNumber {
                get { return _promptForCarrierNumber; }
                set { _promptForCarrierNumber = value; }
            }

            private bool _validateCarrierNumber = false;
            public bool ValidateCarrierNumber {
                get { return _validateCarrierNumber; }
                set { _validateCarrierNumber = value; }
            }

            private bool _promptForTransportNumber = true;
            public bool PromptForTransportNumber {
                get { return _promptForTransportNumber; }
                set { _promptForTransportNumber = value; }
            }

            private bool _validateTransportNumber = false;
            public bool ValidateTransportNumber {
                get { return _validateTransportNumber; }
                set { _validateTransportNumber = value; }
            }

            private bool _allowOrderListLookup = false;
            public bool AllowOrderListLookup {
                get { return _allowOrderListLookup; }
                set { _allowOrderListLookup = value; }
            }

            private bool _allowMultipleStagedOrders = false;
            public bool AllowMultipleStagedOrders {
                get { return _allowMultipleStagedOrders; }
                set { _allowMultipleStagedOrders = value; }
            }
        }

        [Serializable()]
        public class BaySetting {
            public BaySetting() {
            }

            #region  Properties 
            [NonSerialized()]
            public KahlerAutomation.KaTm2Database.KaBay BayInfo = new KaBay();

            public Guid BayId {
                get { return BayInfo.Id; }
                set { BayInfo = new KahlerAutomation.KaTm2Database.KaBay(Tm2Database.Connection, value); }
            }

            private bool _enabled = true;
            public bool Enabled {
                get { return _enabled; }
                set { _enabled = value; }
            }

            [NonSerialized]
            public DateTime LastUsedTime = DateTime.MinValue;

            private bool _allowAllProducts = true;

            public bool AllowAllProducts {
                get { return _allowAllProducts; }
                set { _allowAllProducts = value; }
            }

            private Dictionary<Guid, BayItemAvailibility> _productAvailCollection = null;
            [System.Xml.Serialization.XmlIgnore]
            public Dictionary<Guid, BayItemAvailibility> ProductAvailCollection {
                get {
                    if (_productAvailCollection == null) {
                        PopulateProductAvailabilityList(Tm2Database.Connection);
                    }
                    return _productAvailCollection;
                }
                set { _productAvailCollection = value; }
            }

            private bool _allowAllBulkIngredients = false; // Default to using the bulk products assigned to a panel
            public bool AllowAllBulkIngredients {
                get {
                    return _allowAllBulkIngredients;
                }
                set { _allowAllBulkIngredients = value; }
            }

            private bool _bulkIngredientsUseAvailList = false; // Default to using the bulk products assigned to a panel
            public bool BulkIngredientsUseAvailList {
                get { return _bulkIngredientsUseAvailList; }
                set { _bulkIngredientsUseAvailList = value; }
            }

            private Dictionary<Guid, BayItemAvailibility> _bulkIngredientAvailCollection = null;
            [System.Xml.Serialization.XmlIgnore]
            public Dictionary<Guid, BayItemAvailibility> BulkIngredientAvailCollection {
                get {
                    if (_bulkIngredientAvailCollection == null) {
                        PopulateBulkIngredientAvailabilityList(Tm2Database.Connection);
                    }
                    return _bulkIngredientAvailCollection;
                }
                set { _bulkIngredientAvailCollection = value; ; }
            }

            private bool _allowAllCustomers = true;
            public bool AllowAllCustomers {
                get { return _allowAllCustomers; }
                set { _allowAllCustomers = value; }
            }

            private Dictionary<Guid, BayItemAvailibility> _customerAvailCollection = null;
            [System.Xml.Serialization.XmlIgnore]
            public Dictionary<Guid, BayItemAvailibility> CustomerAvailCollection {
                get {
                    if (_customerAvailCollection == null) {
                        PopulateCustomerAvailabilityList(Tm2Database.Connection);
                    }
                    return _customerAvailCollection;
                }
                set { _customerAvailCollection = value; }
            }

            private bool _allowAllOwners = true;
            public bool AllowAllOwners {
                get { return _allowAllOwners; }
                set { _allowAllOwners = value; }
            }

            private Dictionary<Guid, BayItemAvailibility> _ownerAvailCollection = null;
            [System.Xml.Serialization.XmlIgnore]
            public Dictionary<Guid, BayItemAvailibility> OwnerAvailCollection {
                get {
                    if (_ownerAvailCollection == null) {
                        PopulateOwnerAvailabilityList(Tm2Database.Connection);
                    }
                    return _ownerAvailCollection;
                }
                set { _ownerAvailCollection = value; }
            }

            private bool _allowAllDrivers = true;
            public bool AllowAllDrivers {
                get { return _allowAllDrivers; }
                set { _allowAllDrivers = value; }
            }

            private Dictionary<Guid, BayItemAvailibility> _driverAvailCollection = null;
            [System.Xml.Serialization.XmlIgnore]
            public Dictionary<Guid, BayItemAvailibility> DriverAvailCollection {
                get {
                    if (_driverAvailCollection == null) {
                        PopulateDriverAvailabilityList(Tm2Database.Connection);
                    }
                    return _driverAvailCollection;
                }
                set { _driverAvailCollection = value; }
            }

            private bool _allowAllTransports = true;
            public bool AllowAllTransports {
                get { return _allowAllTransports; }
                set { _allowAllTransports = value; }
            }

            private Dictionary<Guid, BayItemAvailibility> _transportAvailCollection = null;
            [System.Xml.Serialization.XmlIgnore]
            public Dictionary<Guid, BayItemAvailibility> TransportAvailCollection {
                get {
                    if (_transportAvailCollection == null) {
                        PopulateTransportAvailabilityList(Tm2Database.Connection);
                    }
                    return _transportAvailCollection;
                }
                set { _transportAvailCollection = value; }
            }

            private bool _allowAllCarriers = true;
            public bool AllowAllCarriers {
                get { return _allowAllCarriers; }
                set { _allowAllCarriers = value; }
            }

            private Dictionary<Guid, BayItemAvailibility> _carrierAvailCollection = null;
            [System.Xml.Serialization.XmlIgnore]
            public Dictionary<Guid, BayItemAvailibility> CarrierAvailCollection {
                get {
                    if (_carrierAvailCollection == null) {
                        PopulateCarrierAvailabilityList(Tm2Database.Connection);
                    }
                    return _carrierAvailCollection;
                }
                set { _carrierAvailCollection = value; }
            }

            private bool _availableForLoadoutOrders = true;
            public bool AvailableForLoadoutOrders {
                get { return _availableForLoadoutOrders; }
                set { _availableForLoadoutOrders = value; }
            }

            private bool _availableForReceivingOrders = false;
            public bool AvailableForReceivingOrders {
                get { return _availableForReceivingOrders; }
                set { _availableForReceivingOrders = value; }
            }
            #endregion

            #region Check Bay Availability for Use  
            private bool BayAllowsItem(List<Guid> itemList, Dictionary<Guid, BayItemAvailibility> itemAvailCollection) {
                int itemsNotEmptyGuid = 0;
                foreach (Guid itemToCheckId in itemList) {
                    if (!itemToCheckId.Equals(Guid.Empty)) {
                        // Skip any products, customers, etc. that have an ID of empty
                        itemsNotEmptyGuid += 1;
                        if (itemAvailCollection.ContainsKey(itemToCheckId) && itemAvailCollection[itemToCheckId].Availability != BayItemAvailibility.ItemAvailable.Yes) {
                            return false;
                        }
                    }
                }
                return (itemsNotEmptyGuid > 0);
            }

            public bool BayAllowsBulkIngredients(List<Guid> bulkIngredientsForOrder) {
                if (_enabled) {
                    if (_allowAllBulkIngredients) {
                        return true;
                    } else if (_bulkIngredientsUseAvailList) {
                        return BayAllowsItem(bulkIngredientsForOrder, BulkIngredientAvailCollection);
                    } else {
                        return bulkIngredientsForOrder.Count > 0 && BayInfo.CanBayDischargeBulkProducts(Tm2Database.Connection, bulkIngredientsForOrder);
                    }
                } else {
                    return false;
                }
            }

            public bool BayAllowsProducts(List<Guid> productsForOrder) {
                if (_enabled) {
                    if (_allowAllProducts) {
                        return true;
                    } else {
                        return BayAllowsItem(productsForOrder, _productAvailCollection);
                    }
                } else {
                    return false;
                }
            }

            public bool BayAllowsOwner(List<Guid> ownersForOrder) {
                if (_enabled) {
                    if (_allowAllOwners) {
                        return true;
                    } else {
                        return BayAllowsItem(ownersForOrder, _ownerAvailCollection);
                    }
                } else {
                    return false;
                }
            }

            public bool BayAllowsCustomerOrSupplier(Guid customerIdForOrder) {
                List<Guid> customerList = new List<Guid>();
                customerList.Add(customerIdForOrder);
                return BayAllowsDriver(customerList);
            }

            public bool BayAllowsCustomerOrSupplier(List<Guid> customersForOrder) {
                if (_enabled) {
                    if (_allowAllCustomers) {
                        return true;
                    } else {
                        return BayAllowsItem(customersForOrder, _customerAvailCollection);
                    }
                } else {
                    return false;
                }
            }

            public bool BayAllowsCarrier(Guid carrierIdForOrder) {
                List<Guid> carrierList = new List<System.Guid>();
                carrierList.Add(carrierIdForOrder);
                return BayAllowsDriver(carrierList);
            }

            public bool BayAllowsCarrier(List<Guid> carriersForOrder) {
                bool retVal = false;
                if (_enabled) {
                    if (_allowAllCarriers) {
                        return true;
                    } else {
                        return BayAllowsItem(carriersForOrder, _carrierAvailCollection);
                    }
                }
                return retVal;
            }

            public bool BayAllowsTransport(Guid transportIdForOrder) {
                List<Guid> transportList = new List<Guid>();
                transportList.Add(transportIdForOrder);
                return BayAllowsDriver(transportList);
            }

            public bool BayAllowsTransport(List<Guid> transportsForOrder) {
                bool retVal = false;
                if (_enabled) {
                    if (_allowAllTransports) {
                        return true;
                    } else {
                        return BayAllowsItem(transportsForOrder, _transportAvailCollection);
                    }
                }
                return retVal;
            }

            public bool BayAllowsDriver(Guid driverIdForOrder) {
                List<Guid> driverList = new List<Guid>();
                driverList.Add(driverIdForOrder);
                return BayAllowsDriver(driverList);
            }

            public bool BayAllowsDriver(List<Guid> driversForOrder) {
                bool retVal = false;
                if (_enabled) {
                    if (_allowAllDrivers) {
                        return true;
                    } else {
                        return BayAllowsItem(driversForOrder, _driverAvailCollection);
                    }
                }
                return retVal;
            }
            #endregion

            #region   Populate Availability 
            public void PopulateAvailabilityLists() {
                OleDbConnection connection = Tm2Database.Connection;
                PopulateBulkIngredientAvailabilityList(connection);
                PopulateProductAvailabilityList(connection);
                PopulateCustomerAvailabilityList(connection);
                PopulateOwnerAvailabilityList(connection);
                PopulateDriverAvailabilityList(connection);
                PopulateCarrierAvailabilityList(connection);
                PopulateTransportAvailabilityList(connection);
            }

            private Dictionary<Guid, BayItemAvailibility> RestoreAvailabilityDictionaryFromDb(OleDbConnection connection, string tableName) {
                Dictionary<Guid, BayItemAvailibility> availCollection = new Dictionary<Guid, BayItemAvailibility>();
                try {
                    foreach (BayItemAvailibility item in (List<BayItemAvailibility>)Tm2Database.FromXml(KaSetting.GetSetting(connection,
                        string.Format(Database.SN_BAY_ITEM_AVAILABILITY, Database.ComputerName, Database.APP_ID, BayInfo.Id, tableName), "<ArrayOfBayItemAvailibility />"), typeof(List<BayItemAvailibility>))) {
                        availCollection.Add(item.RecordId, item);
                    }
                } catch (Exception) {
                    return new Dictionary<Guid, BayItemAvailibility>();
                }
                return availCollection;
            }

            private void RestoreBulkIngredientAvailabilityListFromDb(OleDbConnection connection) {
                _bulkIngredientAvailCollection = RestoreAvailabilityDictionaryFromDb(connection, KaBulkProduct.TABLE_NAME);
            }

            private void PopulateBulkIngredientAvailabilityList(OleDbConnection connection) {
                RestoreBulkIngredientAvailabilityListFromDb(connection);
                ArrayList bulkIngredientList = KaBulkProduct.GetAll(connection, "deleted=0", "Name ASC");
                foreach (KaBulkProduct bulkIngredient in bulkIngredientList) {
                    if (!bulkIngredient.IsFunction(connection)) {
                        if (!_bulkIngredientAvailCollection.ContainsKey(bulkIngredient.Id)) {
                            _bulkIngredientAvailCollection.Add(bulkIngredient.Id, new BayItemAvailibility() {
                                RecordId = bulkIngredient.Id,
                                TableName = KaBulkProduct.TABLE_NAME,
                                Availability = BayItemAvailibility.ItemAvailable.NotSet,
                                Name = bulkIngredient.Name
                            });
                        }
                        System.Windows.Forms.Application.DoEvents();
                    }
                }
            }

            private void RestoreProductAvailabilityListFromDb(OleDbConnection connection) {
                _productAvailCollection = RestoreAvailabilityDictionaryFromDb(connection, KaProduct.TABLE_NAME);
            }

            private void PopulateProductAvailabilityList(OleDbConnection connection) {
                RestoreProductAvailabilityListFromDb(connection);
                ArrayList productList = KaProduct.GetAll(connection, "deleted=0", "Name ASC");
                foreach (KaProduct product in productList) {
                    if (!product.IsFunction(connection)) {
                        if (!_productAvailCollection.ContainsKey(product.Id)) {
                            _productAvailCollection.Add(product.Id, new BayItemAvailibility() {
                                RecordId = product.Id,
                                TableName = KaProduct.TABLE_NAME,
                                Availability = BayItemAvailibility.ItemAvailable.NotSet,
                                Name = product.Name
                            });
                        }
                    }
                    System.Windows.Forms.Application.DoEvents();
                }
            }

            private void RestoreCustomerAvailabilityListFromDb(OleDbConnection connection) {
                _customerAvailCollection = RestoreAvailabilityDictionaryFromDb(connection, KaCustomerAccount.TABLE_NAME);
            }

            private void PopulateCustomerAvailabilityList(OleDbConnection connection) {
                RestoreCustomerAvailabilityListFromDb(connection);
                ArrayList customerList = KaCustomerAccount.GetAll(Tm2Database.Connection, "deleted=0", "Name ASC");
                foreach (KaCustomerAccount customer in customerList) {
                    if (!_customerAvailCollection.ContainsKey(customer.Id)) {
                        _customerAvailCollection.Add(customer.Id, new BayItemAvailibility() {
                            RecordId = customer.Id,
                            TableName = KaCustomerAccount.TABLE_NAME,
                            Availability = BayItemAvailibility.ItemAvailable.NotSet,
                            Name = customer.Name
                        });
                    }
                    System.Windows.Forms.Application.DoEvents();
                }
                ArrayList supplierList = KaSupplierAccount.GetAll(Tm2Database.Connection, "deleted=0", "Name ASC");
                foreach (KaSupplierAccount supplier in supplierList) {
                    if (!_customerAvailCollection.ContainsKey(supplier.Id)) {
                        _customerAvailCollection.Add(supplier.Id, new BayItemAvailibility() {
                            RecordId = supplier.Id,
                            TableName = KaSupplierAccount.TABLE_NAME,
                            Availability = BayItemAvailibility.ItemAvailable.NotSet,
                            Name = supplier.Name + " (Supplier)"
                        });
                    } else if (!_customerAvailCollection[supplier.Id].Name.ToLower().EndsWith(" (supplier)"))
                        _customerAvailCollection[supplier.Id].Name += " (Supplier)";
                    System.Windows.Forms.Application.DoEvents();
                }
            }

            private void RestoreOwnerAvailabilityListFromDb(OleDbConnection connection) {
                _ownerAvailCollection = RestoreAvailabilityDictionaryFromDb(connection, KaOwner.TABLE_NAME);
            }

            private void PopulateOwnerAvailabilityList(OleDbConnection connection) {
                RestoreOwnerAvailabilityListFromDb(connection);
                ArrayList ownerList = KaOwner.GetAll(connection, "deleted=0", "Name ASC");
                foreach (KaOwner owner in ownerList) {
                    if (!_ownerAvailCollection.ContainsKey(owner.Id)) {
                        _ownerAvailCollection.Add(owner.Id, new BayItemAvailibility() {
                            RecordId = owner.Id,
                            TableName = KaOwner.TABLE_NAME,
                            Availability = BayItemAvailibility.ItemAvailable.NotSet,
                            Name = owner.Name
                        });
                    }
                    System.Windows.Forms.Application.DoEvents();
                }
            }

            private void RestoreDriverAvailabilityListFromDb(OleDbConnection connection) {
                _driverAvailCollection = RestoreAvailabilityDictionaryFromDb(connection, KaDriver.TABLE_NAME);
            }

            private void PopulateDriverAvailabilityList(OleDbConnection connection) {
                RestoreDriverAvailabilityListFromDb(connection);
                ArrayList driverList = KaDriver.GetAll(connection, "deleted=0", "Name ASC");
                foreach (KaDriver driver in driverList) {
                    if (!_driverAvailCollection.ContainsKey(driver.Id))
                        _driverAvailCollection.Add(driver.Id, new BayItemAvailibility() {
                            RecordId = driver.Id,
                            TableName = KaDriver.TABLE_NAME,
                            Availability = BayItemAvailibility.ItemAvailable.NotSet,
                            Name = driver.Name
                        });
                    System.Windows.Forms.Application.DoEvents();
                }
            }

            private void RestoreCarrierAvailabilityListFromDb(OleDbConnection connection) {
                _carrierAvailCollection = RestoreAvailabilityDictionaryFromDb(connection, KaCarrier.TABLE_NAME);
            }

            private void PopulateCarrierAvailabilityList(OleDbConnection connection) {
                RestoreCarrierAvailabilityListFromDb(connection);
                ArrayList carrierList = KaCarrier.GetAll(connection, "deleted=0", "Name ASC");
                foreach (KaCarrier carrier in carrierList) {
                    if (!_carrierAvailCollection.ContainsKey(carrier.Id))
                        _carrierAvailCollection.Add(carrier.Id, new BayItemAvailibility() {
                            RecordId = carrier.Id,
                            TableName = KaCarrier.TABLE_NAME,
                            Availability = BayItemAvailibility.ItemAvailable.NotSet,
                            Name = carrier.Name
                        });
                    System.Windows.Forms.Application.DoEvents();
                }
            }

            private void RestoreTransportAvailabilityListFromDb(OleDbConnection connection) {
                _transportAvailCollection = RestoreAvailabilityDictionaryFromDb(connection, KaTransport.TABLE_NAME);
            }

            private void PopulateTransportAvailabilityList(OleDbConnection connection) {
                RestoreTransportAvailabilityListFromDb(connection);
                ArrayList transportList = KaTransport.GetAll(connection, "deleted=0", "Name ASC");
                foreach (KaTransport transport in transportList) {
                    if (!_transportAvailCollection.ContainsKey(transport.Id))
                        _transportAvailCollection.Add(transport.Id, new BayItemAvailibility() {
                            RecordId = transport.Id,
                            TableName = KaTransport.TABLE_NAME,
                            Availability = BayItemAvailibility.ItemAvailable.NotSet,
                            Name = transport.Name
                        });
                    System.Windows.Forms.Application.DoEvents();
                }
            }

            public void RestoreEmptyAvailabilityListsFromDb() {
                OleDbConnection connection = Tm2Database.Connection;
                if (_bulkIngredientAvailCollection == null)
                    RestoreBulkIngredientAvailabilityListFromDb(connection);
                if (_carrierAvailCollection == null)
                    RestoreCarrierAvailabilityListFromDb(connection);
                if (_customerAvailCollection == null)
                    RestoreCustomerAvailabilityListFromDb(connection);
                if (_driverAvailCollection == null)
                    RestoreDriverAvailabilityListFromDb(connection);
                if (_ownerAvailCollection == null)
                    RestoreOwnerAvailabilityListFromDb(connection);
                if (_productAvailCollection == null)
                    RestoreProductAvailabilityListFromDb(connection);
                if (_transportAvailCollection == null)
                    RestoreTransportAvailabilityListFromDb(connection);
            }
            #endregion 
        }

        [Serializable()]
        public class ScaleSettings {
            private ScaleComm.ScaleCommType _commType = ScaleComm.ScaleCommType.Ka2000;
            public ScaleComm.ScaleCommType CommType {
                get { return _commType; }
                set { _commType = value; }
            }

            private ScaleComm.ScaleDataStreamType _scaleDataStreamType = ScaleComm.ScaleDataStreamType.RLWS_Condec;
            public ScaleComm.ScaleDataStreamType ScaleDataStreamType {
                get { return _scaleDataStreamType; }
                set { _scaleDataStreamType = value; }
            }

            private Guid _panelId = Guid.Empty;
            public Guid PanelId {
                get { return _panelId; }
                set { _panelId = value; }
            }
        }
    }
}