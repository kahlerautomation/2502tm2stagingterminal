﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KahlerAutomation.KaTm2Database;

namespace Tm2StagingTerminal {
    public partial class WeighTruck : StagingTerminalBaseControl {

        private bool _responding = false;
        private double _weight = 0;
        private KaUnit.Unit _unit = KaUnit.Unit.Pounds;

        public double Weight { get; set; }
        public KaUnit.Unit Unit { get; set; }

        private KaUnit _scaleUnit = null;

        public KaUnit ScaleUnit {
            get {
                if (_scaleUnit == null || _scaleUnit.BaseUnit != _unit)
                    _scaleUnit = KaUnit.GetUnitForBaseUnit(Tm2Database.Connection, _unit);
                return _scaleUnit;
            }
        }

        public WeighTruck() {
            InitializeComponent();
        }

        private void WeighTruck_Resize(object sender, EventArgs e) {
            base.StagingTerminalBaseControl_Resize(sender, e);
            lblCurrentScaleReading.Left = (this.Width - lblCurrentScaleReading.Width) / 2;
            lblCurrentScaleReading.Top = (this.Height - buttonBar.Bottom) / 2 - lblCurrentScaleReading.Height + buttonBar.Bottom;
            lblCurrentScaleReadingValue.Left = (this.Width - lblCurrentScaleReadingValue.Width) / 2;
            lblCurrentScaleReadingValue.Top = lblCurrentScaleReading.Bottom;
        }

        private void WeighTruck_Load(object sender, EventArgs e) {
            if (CurrentPanelStatus.Order != null) {
                if (CurrentPanelStatus.IsInbound)
                    Title = "Record Empty Weight";
                else
                    Title = "Record Filled Weight";

            } else {
                if (CurrentPanelStatus.IsInbound)
                    Title = "Record Full Weight";
                else
                    Title = "Record Empty Weight";
            }
        }

        protected override void btnOk_Click(object sender, EventArgs e) {
            Weight = _weight;
            Unit = _unit;
            base.btnOk_Click(sender, e);
        }

        public void NewWeightReceived(double weight, KaUnit.Unit unit, string mode, string status, bool weightValid) {
            if (!_responding) {
                _responding = true;
                SuspendLayout();
                try {
                    _weight = weight;
                    _unit = unit;

                    // Set the Motion/Hold/Invalid/Valid Flag
                    if (!weightValid) {
                        SetPropertyValue(lblCurrentScaleReadingValue, "Text", "Weight invalid");
                    } else if (mode.Trim().ToUpper() == "G") {
                        string unitName = KaUnit.GetBaseUnitAbbreviation(unit);
                        SetPropertyValue(lblCurrentScaleReadingValue, "Text", weight + " " + unitName + (status.Trim() == "" ? "  " : " M"));
                    } else {
                        SetPropertyValue(lblCurrentScaleReadingValue, "Text", "Please place scale into Gross Mode");
                    }
                    bool _weightValid = MainForm.ScaleObject.ValidReading && status.Trim() == "" && mode.Trim().ToUpper() == "G" && weightValid;
                    SetPropertyValue(btnOk, "Enabled", _weightValid);
                    System.Threading.Thread.Sleep(10);
                } catch (Exception ex) {
                    StUnhandledException.ShowException(ex);
                }
                ResumeLayout(true);
                _responding = false;
            }
        }
    }
}
