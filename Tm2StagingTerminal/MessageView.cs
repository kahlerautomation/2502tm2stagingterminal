﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using KahlerAutomation.KaTm2Database;

namespace Tm2StagingTerminal {
    public partial class MessageView : UserControl {
        public MessageView() {
            InitializeComponent();
            Appearances.ScaleFonts(this, new ArrayList());
        }

        private void MessageView_Resize(object sender, EventArgs e) {
            try {
                btnOk.Left = Math.Max(ClientSize.Width - btnOk.Width - 3, 0);
                btnOther.Left = Math.Max(btnOk.Left - btnOther.Width - 3, 0);
                lblMessage.Width = ClientSize.Width - 6;
                lblMessage.Height = ClientSize.Height - 50;
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        public event EventHandler OkClicked;
        private void btnOk_Click(object sender, EventArgs e) {
            try {
                if (OkClicked != null) {
                    this.Enabled = false;
                    OkClicked(sender, e);
                }
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        public event EventHandler OtherClicked;
        private void btnOther_Click(object sender, EventArgs e) {
            if (OtherClicked != null) {
                this.Enabled = false;
                OtherClicked(sender, e);
            }
        }

        public Button OkButton {
            get { return btnOk; }
        }

        public Button OtherButton {
            get { return btnOther; }
        }

        public void SetColors(Color back, Color fore) {
            BackColor = back;
            lblMessage.ForeColor = fore;
        }

        public string Message {
            get { return lblMessage.Text; }
            set {
                lblMessage.Text = value;
                if (value.Length > 0) {
                    KaEventLog entry = new KaEventLog() {
                        ApplicationIdentifier = Database.APP_ID,
                        ApplicationVersion = Tm2Database.FormatVersion(System.Reflection.Assembly.GetEntryAssembly().GetName().Version, "X"),
                        Category = KaEventLog.Categories.Information,
                        Computer = Database.ComputerName,
                        Description = value
                    };
                    KaEventLog.CreateEventLog(Tm2Database.Connection, entry, 0.25, null, Database.ApplicationIdentifier, Database.ApplicationUsername);
                }
            }
        }

        public void UpdateCulture() {
            ComponentResourceManager resources = new ComponentResourceManager(this.GetType());
            resources.ApplyResources(btnOk, btnOk.Name, Thread.CurrentThread.CurrentUICulture);
        }

        private Control _callingControl = null;
        public Control CallingControl {
            get { return _callingControl; }
            set { _callingControl = value; }
        }
    }
}
