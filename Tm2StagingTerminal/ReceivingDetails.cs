﻿using System;
using KahlerAutomation.KaTm2Database;
using System.Windows.Forms;

namespace Tm2StagingTerminal {
    public partial class ReceivingDetails : StagingTerminalBaseControl {
        public ReceivingDetails() {
            InitializeComponent();
        }

        public void ReceivingDetailsLoaded() {
            lstProducts.Items.Clear();
            try {
                double totalWeight = 0;
                double totalRemainingWeight = 0;
                bool totalValid = true;
                KaReceivingPurchaseOrder recPoInfo = CurrentPanelStatus.ReceivingPurchaseOrder;

                if (recPoInfo.SupplierAccountId != Guid.Empty) {
                    KaSupplierAccount supplierInfo = new KaSupplierAccount(Tm2Database.Connection, recPoInfo.SupplierAccountId);
                    lblSupplier.Text = $"Supplier: {supplierInfo.Name}";
                    string accountNumber = "";
                    try {
                        accountNumber = new KaSupplierAccountInterfaceSettings(Tm2Database.Connection, recPoInfo.SupplierInterfaceSettingId).CrossReference;
                    } catch (RecordNotFoundException) { /* suppress */  }
                    if (accountNumber.Length == 0) accountNumber = supplierInfo.AccountNumber;
                    lblAccountNumber.Text = $"Account Number: { accountNumber}";
                } else {
                    lblSupplier.Text = "";
                    lblAccountNumber.Text = "";
                }
                Title = "Receiving details for " + recPoInfo.Number;
                ListViewItem li = new ListViewItem();

                KaUnit validatedScaleUnit = new KaUnit(Tm2Database.Connection, CurrentPanelStatus.ValidatedScaleWeight.UnitId);
                string formattedWeightString;
                KaBulkProduct product = new KaBulkProduct(Tm2Database.Connection, recPoInfo.BulkProductId);
                totalValid = false;
                KaQuantity converted;
                try {
                    converted = KaUnit.Convert(Tm2Database.Connection, new KaQuantity(recPoInfo.Purchased, recPoInfo.UnitId), new KaRatio(product.Density, product.WeightUnitId, product.VolumeUnitId), validatedScaleUnit.Id);

                    if (converted.Numeric > 0) {
                        totalWeight = totalWeight + converted.Numeric;

                        double convertedRemaining = KaUnit.Convert(Tm2Database.Connection, new KaQuantity(Math.Max(recPoInfo.Purchased - recPoInfo.Delivered, 0), recPoInfo.UnitId), new KaRatio(product.Density, product.WeightUnitId, product.VolumeUnitId), validatedScaleUnit.Id).Numeric;
                        totalRemainingWeight += convertedRemaining;

                        li.Text = product.Name;
                        formattedWeightString = "{0:" + validatedScaleUnit.UnitPrecision + "} " + validatedScaleUnit.Abbreviation;

                        li.SubItems.Add(string.Format(formattedWeightString, converted.Numeric) + " (" + string.Format(formattedWeightString, convertedRemaining) + " Remaining)");
                        lstProducts.Items.Add(li);
                    }
                } catch (UnitConversionException) {
                    converted = new KaQuantity(recPoInfo.Purchased, recPoInfo.UnitId);
                }
                if (totalValid) {
                    formattedWeightString = "{0:" + validatedScaleUnit.UnitPrecision + "} " + validatedScaleUnit.Abbreviation;
                    lblTotal.Text = string.Format(formattedWeightString, totalWeight) + " (" + string.Format(formattedWeightString, Math.Max(totalRemainingWeight, 0)) + " Remaining)";
                }
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        private void ReceivingDetails_Resize(object sender, EventArgs e) {
            lblSupplier.Width = this.Width;
            lblAccountNumber.Width = this.Width;
            lblTotal.Top = this.Height - lblTotal.Height;
            lblTotal.Width = (this.ClientSize.Width - (this.Margin.Horizontal * 2)) / 2;
            lblTotalLabel.Width = lblTotal.Width;
            lblTotalLabel.Top = lblTotal.Top;
            lblTotal.Left = lblTotalLabel.Width + this.Margin.Horizontal;
            lstProducts.Width = this.Width - (2 * lstProducts.Left);
            lstProducts.Height = lblTotal.Top - lstProducts.Top - this.Margin.Vertical;
            chProduct.Width = (lstProducts.Width - 25) / 2;
            chRequest.Width = chProduct.Width;
        }
    }
}
