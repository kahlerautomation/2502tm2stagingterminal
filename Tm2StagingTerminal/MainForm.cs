﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Collections;
using KahlerAutomation.KaTm2Database;
using KahlerAutomation.KaTm2LoadFramework;
using KahlerAutomation;
using System.Threading;
using System.Globalization;
using System.Data.OleDb;
using System.Runtime.InteropServices;

namespace Tm2StagingTerminal {
    public partial class MainForm : Form {
        private int _firstScreenIndex = -1;
        private List<string> _viewChain = new List<string>() { SelectInboundOutboundTag, EnterConfigurationPasswordTag };
        static private Dictionary<KaUnit.Unit, Guid> _baseUnitCrossReference;

        static private ScaleComm _scaleObject;
        static public ScaleComm ScaleObject {
            get { return _scaleObject; }
            set { _scaleObject = value; }
        }

        private DateTime _timeLastScreenShown = DateTime.MinValue;

        #region Control Screen Tags
        private const string SelectInboundOutboundTag = "SelectInboundOutbound";
        private const string EnterDriverNumberTag = "EnterDriverNumber";
        private const string EnterTransportNumberTag = "EnterTransportNumber";
        private const string EnterCarrierNumberTag = "EnterCarrierNumber";
        private const string EnterOrderNumberTag = "EnterOrderNumber";
        private const string SelectOrderTag = "SelectOrder";
        private const string SelectStagedOrderTag = "SelectStagedOrder";
        private const string SelectCustomerAccountLocationTag = "SelectCustomerAccountLocation";
        private const string EnterShipToCityTag = "EnterShipToCity";
        private const string EnterShipToStateTag = "EnterShipToState";
        private const string EnterShipToZipTag = "EnterShipToZipTag";
        private const string EnterConfigurationPasswordTag = "EnterConfigurationPassword";
        private const string ShowOrderDetailsTag = "ShowOrderDetailsTag";
        private const string ShowReceivingOrderDetailsTag = "ShowReceivingOrderDetailsTag";
        private const string ShowFinalConfirmationScreenTag = "ShowFinalConfirmationScreenTag";
        private const string ShowCompleteTag = "ShowCompleteTag";
        private const string SelectPrestageCompleteProductTag = "SelectPrestageCompleteProductTag";
        private const string VerifyCurrentTruckWeightTag = "VerifyCurrentTruckWeightTag";
        private const string RequireRestrictedUseTag = "RequireRestrictedUseTag";
        private const string PreviousLoadProductTag = "PreviousLoadProductTag";
        private const string SaveDataTag = "SaveDataTag";
        private const string SetupStagedOrderTag = "SetupStagedOrderTag";
        #endregion

        public MainForm() {
            InitializeComponent();
            MainForm_Resize(null, null);
            Appearances.ScaleFonts(this, new ArrayList(new string[] { "Label" }));
            CurrentPanelStatus.ResetVariables();
            SetUpViewChain();
            ShowDetermineInboundOutboundScreen(true);
            _firstScreenIndex = GetScreenIndex((string)_currentObjectView.Tag);  //First screen is set at this point, remember what it is so we can come back to it.
            this.weighTruck1.BackClicked += new System.EventHandler(this.weighTruck1_BackClicked);
            this.weighTruck1.OkClicked += new System.EventHandler(this.weighTruck1_OkClicked);
            this.weighTruck1.StartOverClicked += new System.EventHandler(this.StartOverClicked);
            this.weighTruck1.Tag = VerifyCurrentTruckWeightTag;
            this.restrictedUseProductHauled1.BackClicked += new System.EventHandler(this.restrictedUseProductHauled1_BackClicked);
            this.restrictedUseProductHauled1.OkClicked += new System.EventHandler(this.restrictedUseProductHauled1_OkClicked);
            this.restrictedUseProductHauled1.StartOverClicked += new System.EventHandler(this.StartOverClicked);
            this.restrictedUseProductHauled1.Tag = RequireRestrictedUseTag;
            this.orderDetails1.BackClicked += new System.EventHandler(this.orderDetails1_BackClicked);
            this.orderDetails1.OkClicked += new System.EventHandler(this.orderDetails1_OkClicked);
            this.orderDetails1.StartOverClicked += new System.EventHandler(this.StartOverClicked);
            this.orderDetails1.Tag = ShowOrderDetailsTag;
            this.receivingDetails1.BackClicked += new System.EventHandler(this.receivingDetails1_BackClicked);
            this.receivingDetails1.OkClicked += new System.EventHandler(this.receivingDetails1_OkClicked);
            this.receivingDetails1.StartOverClicked += new System.EventHandler(this.StartOverClicked);
            this.receivingDetails1.Tag = ShowReceivingOrderDetailsTag;
            this.confirm1.Tag = ShowFinalConfirmationScreenTag;
            this.complete1.Tag = ShowCompleteTag;
        }

        private void MainForm_Resize(object sender, EventArgs e) {
            try {
                if (_currentObjectView != null) {
                    _currentObjectView.Width = ClientSize.Width;
                    _currentObjectView.Height = ClientSize.Height;
                }
                if (_objectIn != null) {
                    _objectIn.Width = ClientSize.Width;
                    _objectIn.Height = ClientSize.Height;
                }
                if (_objectOut != null) {
                    _objectOut.Width = ClientSize.Width;
                    _objectOut.Height = ClientSize.Height;
                }
                messageView1.Width = ClientSize.Width;
                messageView1.Height = ClientSize.Height;
                restrictedUseProductHauled1.Width = ClientSize.Width;
                restrictedUseProductHauled1.Height = ClientSize.Height;
                picTopBar.Width = ClientSize.Width;
                pbConfig.Left = Math.Max(ClientSize.Width - pbConfig.Width, 0);
                weighTruck1.Width = ClientSize.Width;
                weighTruck1.Height = ClientSize.Height;
                restrictedUseProductHauled1.Width = ClientSize.Width;
                restrictedUseProductHauled1.Height = ClientSize.Height;
                confirm1.Width = ClientSize.Width;
                confirm1.Height = ClientSize.Height;
                complete1.Width = ClientSize.Width;
                complete1.Height = ClientSize.Height;
                orderDetails1.Width = ClientSize.Width;
                orderDetails1.Height = ClientSize.Height;
                receivingDetails1.Width = ClientSize.Width;
                receivingDetails1.Height = ClientSize.Height;
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        private void UpdateCulture(string cultureCode) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(cultureCode);
            messageView1.UpdateCulture();
        }

        private int GetScreenIndex(string tag) {
            int retval = 0;

            if (tag == EnterConfigurationPasswordTag)
                retval = -2;
            else
                retval = _viewChain.IndexOf(tag);

            return retval;
        }

        #region view transitions
        private Control _currentObjectView;
        private Control _previousObjectView;
        private Control _currentView = null;
        private Control _objectOut = null;
        private Control _objectIn = null;
        private bool _transitionLeft = false;
        private double _transitionSpeed = 0;

        private void Transition(Control objectOut, Control objectIn, bool transitionLeft) {
            picTopBar.SendToBack();
            _timeLastScreenShown = DateTime.Now;

            tmrMaxScreenTime.Enabled = false;
            bool notFirstScreen = !IsFirstScreen(objectIn);
            if (objectIn.GetType() == typeof(KahlerAutomation.EnterText)) {
                ((KahlerAutomation.EnterText)objectIn).ShowBackButton = notFirstScreen;
                ((KahlerAutomation.EnterText)objectIn).ShowStartOverButton = notFirstScreen && (string)((KahlerAutomation.EnterText)objectIn).Tag != EnterConfigurationPasswordTag;
            } else if (objectIn.GetType() == typeof(KahlerAutomation.SelectFromList)) {
                ((KahlerAutomation.SelectFromList)objectIn).ShowBackButton = notFirstScreen;
                ((KahlerAutomation.SelectFromList)objectIn).ShowStartOverButton = notFirstScreen;
            } else if (objectIn.GetType() == typeof(KahlerAutomation.YesNo)) {
                ((YesNo)objectIn).ShowBackButton = notFirstScreen;
                ((YesNo)objectIn).ShowStartOverButton = notFirstScreen;
            }
            if (objectIn != null && objectIn.GetType() == typeof(WeighTruck)) {
                tmrUpdateScale.Enabled = true;
                tmrUpdateScale_Tick(tmrUpdateScale, new EventArgs());
            }

            bool enableMaxTimeOnScreen = notFirstScreen && Database.Settings.MaxOnScreenTime > 0;
            if (_currentObjectView != null) {
                if (_currentObjectView.Equals(objectIn)) {
                    try {
                        objectIn.Enabled = true;
                    } catch (Exception) { /* suppress */ }
                    tmrMaxScreenTime.Enabled = enableMaxTimeOnScreen;
                    return;
                }
                _objectOut = _currentObjectView; // set the control leaving the screen
            } else {
                if (objectOut.Equals(objectIn)) {
                    try {
                        objectIn.Enabled = true;
                    } catch (Exception) { /* suppress */ }
                    tmrMaxScreenTime.Enabled = enableMaxTimeOnScreen;
                    return;
                }
                _objectOut = objectOut; // set the control leaving the screen
            }
            _objectOut.Left = 0; _objectOut.Top = 0; _objectOut.Visible = true; // make sure the leaving control is at the top of the form, in view
            _objectIn = objectIn; // set the control entering the screen
            _objectIn.Top = 0; _objectIn.Left = transitionLeft ? ClientSize.Width : -ClientSize.Width; _objectIn.Visible = true; // make sure the entering control is at the top of the form, out of view to the left
            _transitionLeft = transitionLeft; _transitionSpeed = 1.0; // set the transition type and speed
            tmrTransition.Enabled = true;
            _currentObjectView = objectIn;
            _currentObjectView.Width = ClientSize.Width;
            _currentObjectView.Height = ClientSize.Height;

            if (!objectOut.Tag.Equals(EnterConfigurationPasswordTag) && !(objectOut is MessageView)) {
                _previousObjectView = objectOut;
            }
            if (objectOut != null && objectOut.GetType() == typeof(WeighTruck))
                tmrUpdateScale.Enabled = false;
        }

        private void tmrTransition_Tick(object sender, EventArgs e) {
            try {
                _transitionSpeed += 0.3 * _transitionSpeed; // accelerate the transition
                if (_transitionLeft) {
                    _objectOut.Left -= (int)_transitionSpeed;
                    _objectIn.Left -= (int)_transitionSpeed;
                    if (_objectIn.Left <= 0) { tmrTransition.Enabled = false; _objectOut.Visible = false; _objectIn.Left = 0; _currentView = _objectIn; }
                } else { // transition right
                    _objectOut.Left += (int)_transitionSpeed;
                    _objectIn.Left += (int)_transitionSpeed;
                    if (_objectIn.Left >= 0) { tmrTransition.Enabled = false; _objectOut.Visible = false; _objectIn.Left = 0; _currentView = _objectIn; }
                }
                if (tmrTransition.Enabled == false && _objectOut.Visible == false) {
                    _objectIn.Enabled = true;
                    _timeLastScreenShown = DateTime.Now;
                    bool enableMaxTimeOnScreen = !IsFirstScreen(_objectIn) && Database.Settings.MaxOnScreenTime > 0;
                    tmrMaxScreenTime.Enabled = enableMaxTimeOnScreen;

                    if (_objectIn.GetType() == typeof(KahlerAutomation.EnterText))
                        ((KahlerAutomation.EnterText)_objectIn).SetFocus();
                    else if (_objectIn.GetType() == typeof(RestrictedUseProductHauled))
                        ((RestrictedUseProductHauled)_objectIn).RestrictedUseProductHauledLoad();
                    else if (_objectIn.GetType() == typeof(OrderDetails))
                        ((OrderDetails)_objectIn).OrderDetailsLoaded();
                    else if (_objectIn.GetType() == typeof(ReceivingDetails))
                        ((ReceivingDetails)_objectIn).ReceivingDetailsLoaded();
                    RemoveOutgoingScreenControls(_objectOut);
                }
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        delegate void RemoveOutgoingScreenControlsDelegate(Control objectToRemove);
        private void RemoveOutgoingScreenControls(Control objectToRemove) {
            if (this.InvokeRequired) {
                RemoveOutgoingScreenControlsDelegate d = new RemoveOutgoingScreenControlsDelegate(RemoveOutgoingScreenControls);
                this.Invoke(d, new object[] { objectToRemove });
            } else {
                // clear out the handlers, and dispose of the temporary objects
                if (objectToRemove.GetType() == typeof(KahlerAutomation.EnterText)) {
                    EnterText controlToDispose = (EnterText)objectToRemove;
                    controlToDispose.BackClicked -= enterText_BackClicked;
                    controlToDispose.OkClicked -= enterText_OkClicked;
                    controlToDispose.StartOverClicked -= StartOverClicked;
                    controlToDispose.Dispose();
                    this.Controls.Remove(controlToDispose);
                } else if (objectToRemove.GetType() == typeof(KahlerAutomation.SelectFromList)) {
                    SelectFromList controlToDispose = (SelectFromList)objectToRemove;
                    controlToDispose.BackClicked -= selectFromList_BackClicked;
                    controlToDispose.StartOverClicked -= StartOverClicked;
                    controlToDispose.RefreshClicked -= selectFromList_RefreshClicked;
                    controlToDispose.SelectedIndexChanged -= selectFromList_SelectedIndexChanged;
                    controlToDispose.Dispose();
                    this.Controls.Remove(controlToDispose);
                } else if (objectToRemove.GetType() == typeof(KahlerAutomation.EnterDate)) {
                    EnterDate controlToDispose = (EnterDate)objectToRemove;
                    controlToDispose.BackClicked -= EnterDatePromptBackClicked;
                    controlToDispose.OkClicked -= EnterDatePromptOkClicked;
                    controlToDispose.StartOverClicked -= StartOverClicked;
                    controlToDispose.Dispose();
                    this.Controls.Remove(controlToDispose);
                } else if (objectToRemove.GetType() == typeof(KahlerAutomation.YesNo)) {
                    YesNo controlToDispose = (YesNo)objectToRemove;
                    controlToDispose.NoClicked -= yesNoPrompt_NoClicked;
                    controlToDispose.YesClicked -= yesNoPrompt_YesClicked;
                    controlToDispose.BackClicked -= yesNoPrompt_BackClicked;
                    controlToDispose.StartOverClicked -= StartOverClicked;
                    controlToDispose.Dispose();
                    this.Controls.Remove(controlToDispose);
                }
                GC.Collect();
            }
        }

        private bool IsFirstScreen(Control screenDisplayed) {
            bool firstScreen = true;
            if (_firstScreenIndex == -1) {
                firstScreen = true;
            } else {
                firstScreen = (_firstScreenIndex == GetScreenIndex((string)((Control)screenDisplayed).Tag));
            }
            return firstScreen;
        }

        private Control BackFromView(Control currentView) {
            string currentTag = (string)currentView.Tag;
            string previousTag = "";
            while (true) {
                for (int i = 0; i < _viewChain.Count; i++) {
                    if (_viewChain[i] == currentTag) { // we've found the current view in the chain
                        previousTag = _viewChain[i - 1 >= 0 ? i - 1 : _viewChain.Count - 1]; // this might be the previous view
                        break;
                    }
                }
                if (_firstScreenIndex < GetScreenIndex(currentTag)) {  //Check to see if we are already at the first screen, if so, return current view (don't change screens).
                    bool shouldDisplay = false;
                    switch (previousTag) {
                        case SelectInboundOutboundTag:
                            return SelectInboundOutbound();
                        case EnterDriverNumberTag:
                            if (CurrentPanelStatus.Order != null) {
                                if (CurrentPanelStatus.IsInbound && Database.Settings.Inbound.PromptForDriverNumber)
                                    shouldDisplay = true;
                                else if (!CurrentPanelStatus.IsInbound && Database.Settings.Outbound.PromptForDriverNumber &&
                                    ((CurrentPanelStatus.StagedOrder == null && CurrentPanelStatus.InProgress == null) ||
                                    (CurrentPanelStatus.StagedOrder != null && CurrentPanelStatus.StagedOrder.DriverId.Equals(Guid.Empty)) ||
                                    (CurrentPanelStatus.StagedOrder == null && CurrentPanelStatus.InProgress != null && CurrentPanelStatus.InProgress.DriverId.Equals(Guid.Empty))))
                                    shouldDisplay = true;
                                if (shouldDisplay) return EnterDriverNumber();
                            }
                            break;
                        case EnterTransportNumberTag:
                            if (CurrentPanelStatus.IsInbound && Database.Settings.Inbound.PromptForTransportNumber)
                                shouldDisplay = true;
                            else if (!CurrentPanelStatus.IsInbound && Database.Settings.Outbound.PromptForTransportNumber) {
                                if (CurrentPanelStatus.StagedOrder != null) {
                                    foreach (KaStagedOrderTransport sot in CurrentPanelStatus.StagedOrder.Transports) {
                                        if (sot.TransportId.Equals(Guid.Empty)) shouldDisplay = true;
                                    }
                                } else if (CurrentPanelStatus.InProgress != null) {
                                    foreach (KaInProgressWeighment w in CurrentPanelStatus.InProgress.Weighments) {
                                        if (w.TransportId.Equals(Guid.Empty)) shouldDisplay = true;
                                    }
                                } else
                                    shouldDisplay = true;
                            }
                            if (shouldDisplay) return EnterTransportNumber();
                            break;
                        case EnterCarrierNumberTag:
                            if (CurrentPanelStatus.Order != null) {
                                if (CurrentPanelStatus.IsInbound && Database.Settings.Inbound.PromptForCarrierNumber)
                                    shouldDisplay = true;
                                else if (!CurrentPanelStatus.IsInbound && Database.Settings.Outbound.PromptForCarrierNumber &&
                                   ((CurrentPanelStatus.StagedOrder == null && CurrentPanelStatus.InProgress == null) ||
                                   (CurrentPanelStatus.StagedOrder != null && CurrentPanelStatus.StagedOrder.CarrierId.Equals(Guid.Empty)) ||
                                   (CurrentPanelStatus.StagedOrder == null && CurrentPanelStatus.InProgress != null && CurrentPanelStatus.InProgress.CarrierId.Equals(Guid.Empty))))
                                    shouldDisplay = true;
                                if (shouldDisplay) return EnterCarrierNumber();
                            }
                            break;
                        case EnterOrderNumberTag:
                            if ((CurrentPanelStatus.IsInbound && !Database.Settings.Inbound.AllowOrderListLookup) || (!CurrentPanelStatus.IsInbound && !Database.Settings.Outbound.AllowOrderListLookup)) return EnterOrderNumber();
                            break;
                        case SelectOrderTag:
                            if ((CurrentPanelStatus.IsInbound && Database.Settings.Inbound.AllowOrderListLookup) || (!CurrentPanelStatus.IsInbound && Database.Settings.Outbound.AllowOrderListLookup)) return SelectOrder();
                            break;
                        case SelectStagedOrderTag:
                            if (CurrentPanelStatus.StagedOrder == null && CurrentPanelStatus.InProgress == null && CurrentPanelStatus.StagedOrders != null && CurrentPanelStatus.StagedOrders.Count > 1) return SelectStagedOrder();
                            break;
                        case SelectCustomerAccountLocationTag:
                            if (CurrentPanelStatus.Order != null && Database.Settings.PromptForShipToDestinationOwners.Contains(CurrentPanelStatus.Order.OwnerId)) {
                                if (Database.Settings.AllowDriversToAddDestinations || (CurrentPanelStatus.CustomerAccountLocations != null && CurrentPanelStatus.CustomerAccountLocations.Count > 0)) return SelectCustomerAccountLocation();
                            }
                            break;
                        case ShowOrderDetailsTag:
                            return orderDetails1;
                        case ShowReceivingOrderDetailsTag:
                            return receivingDetails1;
                        case SetupStagedOrderTag:
                            break;
                        case ShowFinalConfirmationScreenTag:
                            confirm1.LoadDetails();
                            return confirm1;
                        case SaveDataTag:
                            break;
                        case ShowCompleteTag:
                            complete1.ShowCompleted();
                            return complete1;
                        case SelectPrestageCompleteProductTag:
                            Control nextScreen = SelectPrestageCompleteProduct();
                            if (nextScreen != null) return nextScreen;
                            break;
                        case VerifyCurrentTruckWeightTag:
                            return weighTruck1;
                        case RequireRestrictedUseTag:
                            return restrictedUseProductHauled1;
                        case PreviousLoadProductTag:
                            return EnterPreviousLoadProduct();
                    }
                    currentTag = previousTag; // this view isn't used, try the previous
                }
            }
        }

        private Control NextFromView(Control currentView) {
            string currentTag = (string)currentView.Tag;
            string nextTag = "";
            while (true) {
                for (int i = 0; i < _viewChain.Count; i++) {
                    if (_viewChain[i] == currentTag) { // we've found the current view in the chain
                        if (i + 1 < _viewChain.Count) {
                            nextTag = _viewChain[i + 1];
                        } else {
                            nextTag = _viewChain[0];
                            CurrentPanelStatus.ResetVariables();
                        }
                        break;
                    }
                }

                //Do some 'smart' navigation if doing Staged Orders
                int currentIndex = GetScreenIndex(currentTag);

                int targetIndex = GetScreenIndex(nextTag);
                if (targetIndex >= _firstScreenIndex) {
                    switch (nextTag) {
                        case SelectInboundOutboundTag:
                            return SelectInboundOutbound();
                        case EnterOrderNumberTag:
                            if ((CurrentPanelStatus.IsInbound && !Database.Settings.Inbound.AllowOrderListLookup) || (!CurrentPanelStatus.IsInbound && !Database.Settings.Outbound.AllowOrderListLookup)) return EnterOrderNumber();
                            break;
                        case SelectOrderTag:
                            if (CurrentPanelStatus.Order == null && CurrentPanelStatus.ReceivingPurchaseOrder == null) return SelectOrder();
                            break;
                        case EnterCarrierNumberTag:
                            if (CurrentPanelStatus.Order != null &&
                                ((CurrentPanelStatus.IsInbound && Database.Settings.Inbound.PromptForCarrierNumber) || (!CurrentPanelStatus.IsInbound && Database.Settings.Outbound.PromptForCarrierNumber && CurrentPanelStatus.Carrier == null))) return EnterCarrierNumber();
                            break;
                        case EnterDriverNumberTag:
                            if (CurrentPanelStatus.Order != null &&
                                ((CurrentPanelStatus.IsInbound && Database.Settings.Inbound.PromptForDriverNumber) || (!CurrentPanelStatus.IsInbound && Database.Settings.Outbound.PromptForDriverNumber && CurrentPanelStatus.Driver == null))) return EnterDriverNumber();
                            break;
                        case EnterTransportNumberTag:
                            if ((CurrentPanelStatus.IsInbound && Database.Settings.Inbound.PromptForTransportNumber) || (!CurrentPanelStatus.IsInbound && Database.Settings.Outbound.PromptForTransportNumber && CurrentPanelStatus.Transport == null)) return EnterTransportNumber();
                            break;
                        case SelectStagedOrderTag:
                            if (CurrentPanelStatus.StagedOrder == null && CurrentPanelStatus.InProgress == null && CurrentPanelStatus.StagedOrders != null && CurrentPanelStatus.StagedOrders.Count > 1) return SelectStagedOrder();
                            break;
                        case SelectCustomerAccountLocationTag:
                            if (CurrentPanelStatus.Order != null && Database.Settings.PromptForShipToDestinationOwners.Contains(CurrentPanelStatus.Order.OwnerId)) {
                                if (Database.Settings.AllowDriversToAddDestinations || (CurrentPanelStatus.CustomerAccountLocations != null && CurrentPanelStatus.CustomerAccountLocations.Count > 0)) return SelectCustomerAccountLocation();
                            }
                            break;
                        case ShowOrderDetailsTag:
                            return orderDetails1;
                        case ShowReceivingOrderDetailsTag:
                            return receivingDetails1;
                        case ShowFinalConfirmationScreenTag:
                            confirm1.LoadDetails();
                            return confirm1;
                        case SetupStagedOrderTag:
                            SetupStagedOrder();
                            break;
                        case SaveDataTag:
                            SaveData();
                            break;
                        case ShowCompleteTag:
                            complete1.ShowCompleted();
                            return complete1;
                        case SelectPrestageCompleteProductTag:
                            Control nextScreen = SelectPrestageCompleteProduct();
                            if (nextScreen != null) return nextScreen;
                            break;
                        case VerifyCurrentTruckWeightTag:
                            return weighTruck1;
                        case RequireRestrictedUseTag:
                            return restrictedUseProductHauled1;
                        case PreviousLoadProductTag:
                            return EnterPreviousLoadProduct();
                    }
                }
                currentTag = nextTag; // this view isn't used, try the next
            }
        }

        #region get next screens
        private EnterText EnterDriverNumber() {
            EnterText control = GetNewEnterTextObject();
            control.Tag = EnterDriverNumberTag;
            control.Title = Strings.EnterDriverName;
            if (CurrentPanelStatus.Driver != null) {
                control.Text = CurrentPanelStatus.Driver.Number;
            } else {
                control.Text = "";
            }
            control.UsePasswordCharacters = false;
            control.UseAlphaNumericKeypad = Database.Settings.UseAlaphaNumKeyboardForDrivers;
            return control;
        }

        private EnterText EnterTransportNumber() {
            EnterText control = GetNewEnterTextObject();
            control.Tag = EnterTransportNumberTag;
            control.Title = Strings.EnterTruckNumber;
            if (CurrentPanelStatus.Transport != null) {
                control.Text = CurrentPanelStatus.Transport.Number;
            } else {
                control.Text = "";
            }
            control.UsePasswordCharacters = false;
            control.UseAlphaNumericKeypad = Database.Settings.UseAlaphaNumKeyboardForTransports;
            return control;
        }

        private EnterText EnterCarrierNumber() {
            EnterText control = GetNewEnterTextObject();
            control.Tag = EnterCarrierNumberTag;
            control.Title = Strings.EnterCarrierName;
            if (CurrentPanelStatus.Carrier != null) {
                control.Text = CurrentPanelStatus.Carrier.Number;
            } else {
                control.Text = "";
            }
            control.UsePasswordCharacters = false;
            control.UseAlphaNumericKeypad = Database.Settings.UseAlaphaNumKeyboardForCarriers;
            return control;
        }

        private EnterText EnterOrderNumber() {
            EnterText control = GetNewEnterTextObject();
            control.Tag = EnterOrderNumberTag;
            control.Title = Strings.EnterOrderNumber;
            if (CurrentPanelStatus.Order != null) {
                control.Text = CurrentPanelStatus.Order.Number;
            } else if (CurrentPanelStatus.ReceivingPurchaseOrder != null) {
                control.Text = CurrentPanelStatus.ReceivingPurchaseOrder.Number;
            } else {
                control.Text = "";
            }
            control.UsePasswordCharacters = false;
            control.UseAlphaNumericKeypad = Database.Settings.UseAlaphaNumKeyboardForLoadNumbers;
            return control;
        }

        private EnterText EnterShipToCity() {
            EnterText control = GetNewEnterTextObject();
            control.Tag = EnterShipToCityTag;
            control.Title = Strings.EnterShipToCity;
            control.Text = CurrentPanelStatus.ShipToCity;
            int maxCityNameLength = (int)Math.Max(0, Tm2Database.GetMaxDatabaseFieldLength(KaCustomerAccountLocation.TABLE_NAME, KaCustomerAccountLocation.FN_CITY));
            if (maxCityNameLength > 0) control.MaxNumberOfCharsAllowed = maxCityNameLength;
            control.UsePasswordCharacters = false;
            control.UseAlphaNumericKeypad = true;
            return control;
        }

        private EnterText EnterShipToState() {
            EnterText control = GetNewEnterTextObject();
            control.Tag = EnterShipToStateTag;
            control.Title = Strings.EnterShipToState;
            control.Text = CurrentPanelStatus.ShipToState;
            control.MinNumberOfCharsRequired = 2;
            int maxStateLength = (int)Math.Max(0, Tm2Database.GetMaxDatabaseFieldLength(KaCustomerAccountLocation.TABLE_NAME, KaCustomerAccountLocation.FN_STATE));
            if (maxStateLength > 0) control.MaxNumberOfCharsAllowed = maxStateLength;
            control.UsePasswordCharacters = false;
            control.UseAlphaNumericKeypad = true;
            return control;
        }

        private EnterText EnterShipToZip() {
            EnterText control = GetNewEnterTextObject();
            control.Tag = EnterShipToZipTag;
            control.Title = Strings.EnterShipToZip;
            control.Text = CurrentPanelStatus.ShipToZip;
            control.MinNumberOfCharsRequired = 2;
            int maxZipLength = (int)Math.Max(0, Tm2Database.GetMaxDatabaseFieldLength(KaCustomerAccountLocation.TABLE_NAME, KaCustomerAccountLocation.FN_ZIP_CODE));
            if (maxZipLength > 0) control.MaxNumberOfCharsAllowed = maxZipLength;
            control.UsePasswordCharacters = false;
            control.UseAlphaNumericKeypad = true;
            return control;
        }

        private EnterText EnterPreviousLoadProduct() {
            EnterText control = GetNewEnterTextObject();
            control.Tag = PreviousLoadProductTag;
            control.Title = Strings.PreviousLoadProduct;
            control.Text = CurrentPanelStatus.PreviousLoadProduct;
            control.UsePasswordCharacters = false;
            control.UseAlphaNumericKeypad = true;
            return control;
        }
        #endregion
        #endregion

        #region selectFromList events

        private SelectFromList GetNewSelectFromListObject() {
            SelectFromList selectFromListPrompt = new SelectFromList() {
                Data = new ArrayList(),
                DisableOnClickEvent = true,
                DontCareText = "Don\'t care",
                Location = new System.Drawing.Point(0, 0),
                MaintainScrollPosition = false,
                MinimumSize = new System.Drawing.Size(784, 400),
                NewText = "New",
                PromptText = "",
                ShowBackButton = false,
                ShowDontCare = false,
                ShowNewOption = false,
                ShowStartOverButton = true,
                ShowOkVisible = Database.Settings.ShowOkButtonForSelectFromList,
                Size = new System.Drawing.Size(ClientSize.Width, ClientSize.Height),
                TagField = "",
                UseGuids = false
            };
            selectFromListPrompt.BackClicked += new KahlerAutomation.SelectFromList.BackClickedEventHandler(selectFromList_BackClicked);
            selectFromListPrompt.StartOverClicked += new KahlerAutomation.SelectFromList.StartOverClickedEventHandler(StartOverClicked);
            selectFromListPrompt.RefreshClicked += new KahlerAutomation.SelectFromList.RefreshClickedEventHandler(selectFromList_RefreshClicked);
            selectFromListPrompt.SelectedIndexChanged += new KahlerAutomation.SelectFromList.SelectedIndexChangedEventHandler(selectFromList_SelectedIndexChanged);
            selectFromListPrompt.SetAlphaNumericKeyboardSizeScalePercent(Database.Settings.AlphaNumericKeyboardScalar);

            if (Database.Settings.ShowOkButtonForSelectFromList) {
                selectFromListPrompt.ShowOkVisible = true;
            } else {
                selectFromListPrompt.ShowOkVisible = false;
            }
            selectFromListPrompt.ClearFilter();
            selectFromListPrompt.UpdateCulture();
            this.Controls.Add(selectFromListPrompt);
            return selectFromListPrompt;
        }

        private void selectFromList_BackClicked(object sender, EventArgs e) {
            try { // to transition to the previous screen
                string tag = (string)((Control)sender).Tag;
                Transition((Control)sender, BackFromView((Control)sender), false);
            } catch (InTransitionException) { /* suppress exception */
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        private void selectFromList_RefreshClicked(object sender, EventArgs e) {
            try {
                SelectFromList control = (SelectFromList)sender;
                string tag = (string)control.Tag;

                if (tag == SelectInboundOutboundTag) {
                    ArrayList directions = new ArrayList();
                    directions.Add(new Tm2StagingTerminal.SelectFromListItems.TextListItem(Strings.Entering_facility_inbound));
                    directions.Add(new Tm2StagingTerminal.SelectFromListItems.TextListItem(Strings.Exiting_facility_outbound));
                    control.Data = new ArrayList(directions);
                } else if (tag == SelectOrderTag) {
                    ArrayList data = new ArrayList();
                    List<Guid> lockedOrders = new List<Guid>();
                    Dictionary<Guid, Guid> disabledOrderAccounts = new Dictionary<Guid, Guid>();
                    List<KaOrder> validOrders = new List<KaOrder>();
                    List<KaReceivingPurchaseOrder> validReceivingOrders = new List<KaReceivingPurchaseOrder>();

                    GetOrdersThatMatchNumber(CurrentPanelStatus.RefinedOrderNumberFilter, out validOrders, out validReceivingOrders, out lockedOrders, out disabledOrderAccounts);
                    if (CurrentPanelStatus.IsInbound) {
                        foreach (KaOrder o in validOrders) {
                            data.Add(new Tm2StagingTerminal.SelectFromListItems.OrderListItem(o));
                        }
                        foreach (KaReceivingPurchaseOrder po in validReceivingOrders) {
                            data.Add(new Tm2StagingTerminal.SelectFromListItems.OrderListItem(po));
                        }
                    } else {
                        List<KaStagedOrder> validStagedOrders;
                        Dictionary<Guid, KaOrder> validOrderIds;
                        List<KaInProgress> validInProgressOrders;
                        Dictionary<Guid, KaReceivingPurchaseOrder> validPoOrderIds;
                        CheckForValidOutboundOrders(validOrders, validReceivingOrders, out validStagedOrders, out validOrderIds, out validInProgressOrders, out validPoOrderIds);
                        foreach (KaStagedOrder stagedOrder in validStagedOrders) {
                            data.Add(new Tm2StagingTerminal.SelectFromListItems.OrderListItem(stagedOrder));
                        }
                        foreach (KaInProgress ipo in validInProgressOrders) {
                            data.Add(new Tm2StagingTerminal.SelectFromListItems.OrderListItem(ipo));
                        }
                    }
                    control.Data = data;
                } else if (tag == SelectStagedOrderTag) {
                    ArrayList data = new ArrayList();
                    if (CurrentPanelStatus.StagedOrders != null && CurrentPanelStatus.StagedOrders.Count > 0) {
                        foreach (KaStagedOrder stagedOrder in CurrentPanelStatus.StagedOrders) {
                            data.Add(new SelectFromListItems.StagedOrderListItem(stagedOrder));
                        }
                    } else if (CurrentPanelStatus.Order != null) {
                        foreach (KaStagedOrder stagedOrder in KaStagedOrder.GetAll(Tm2Database.Connection, String.Format("deleted=0 AND order_id='{0}'", CurrentPanelStatus.Order.Id.ToString()), "")) {
                            data.Add(new SelectFromListItems.StagedOrderListItem(stagedOrder));
                        }
                    }
                    control.Data = data;
                } else if (tag == SelectCustomerAccountLocationTag) {
                    ArrayList data = new ArrayList();
                    Guid removeThisDestination = Guid.Empty;
                    if (CurrentPanelStatus.Order != null) {
                        //Attempt to display the current order's destination.
                        if (CurrentPanelStatus.Order.CustomerAccountLocationId != Guid.Empty) {
                            KaCustomerAccountLocation cal = new KaCustomerAccountLocation(Tm2Database.Connection, CurrentPanelStatus.Order.CustomerAccountLocationId);
                            cal.Name = "Current: " + cal.Name;
                            data.Add(new SelectFromListItems.CustomerAccountDestinationListItem(cal));
                            removeThisDestination = cal.Id;
                        } else if (!CurrentPanelStatus.Order.ShipToName.Trim().Equals("") || !CurrentPanelStatus.Order.ShipToStreet.Trim().Equals("") || !CurrentPanelStatus.Order.ShipToCity.Trim().Equals("") ||
                                !CurrentPanelStatus.Order.ShipToState.Trim().Equals("") || !CurrentPanelStatus.Order.ShipToZipCode.Trim().Equals("")) {
                            KaCustomerAccountLocation cal = new KaCustomerAccountLocation();
                            cal.Id = Guid.Empty;
                            cal.Name = "Current: " + CurrentPanelStatus.Order.ShipToName;
                            cal.Street = CurrentPanelStatus.Order.ShipToStreet;
                            cal.City = CurrentPanelStatus.Order.ShipToCity;
                            cal.State = CurrentPanelStatus.Order.ShipToState;
                            cal.ZipCode = CurrentPanelStatus.Order.ShipToZipCode;
                            cal.CrossReference = "";
                            data.Add(new SelectFromListItems.CustomerAccountDestinationListItem(cal));
                        }
                    }
                    foreach (KaCustomerAccountLocation l in CurrentPanelStatus.CustomerAccountLocations) {
                        if (l.Id == removeThisDestination) {
                            continue;
                        }
                        data.Add(new SelectFromListItems.CustomerAccountDestinationListItem(l));
                    }
                    control.Data = data;
                } else if (tag == SelectPrestageCompleteProductTag) {
                    ArrayList data = new ArrayList();
                    Dictionary<Guid, KaProduct> products = new Dictionary<Guid, KaProduct>();
                    Dictionary<Guid, List<KaProductBulkProduct>> productBulkProducts = new Dictionary<Guid, List<KaProductBulkProduct>>();
                    Dictionary<Guid, KaBulkProduct> bulkProducts = new Dictionary<Guid, KaBulkProduct>();
                    Dictionary<Guid, KaOrder> orders = new Dictionary<Guid, KaOrder>();
                    Dictionary<Guid, KaQuantity> orderItemAssignedAmounts = CurrentPanelStatus.StagedOrder.GetOrderItemAssignedAmounts(Tm2Database.Connection, null, CurrentPanelStatus.ValidatedScaleWeight.UnitId);

                    foreach (Guid orderItemId in orderItemAssignedAmounts.Keys) {
                        KaOrderItem orderItem = new KaOrderItem(Tm2Database.Connection, orderItemId);
                        KaOrder order = KaOrder.GetOrder(Tm2Database.Connection, orderItem.OrderId, ref orders, null);
                        KaProduct product = KaProduct.GetProduct(Tm2Database.Connection, orderItem.ProductId, products, null);
                        if (!productBulkProducts.ContainsKey(product.Id)) productBulkProducts.Add(product.Id, product.GetProductBulkProductsAssignedAtFacility(Tm2Database.Connection, CurrentPanelStatus.StagedOrder.LocationId, null));
                        foreach (KaProductBulkProduct pbp in productBulkProducts[product.Id]) {
                            data.Add(new SelectFromListItems.LoadedBulkProductListItem(KaBulkProduct.GetBulkProduct(Tm2Database.Connection, pbp.BulkProductId, ref bulkProducts, null), order, orderItem, product));
                        }
                    }

                    control.Data = data;
                }
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        private void selectFromList_SelectedIndexChanged(object sender, EventArgs e) {
            try {
                SelectFromList control = (SelectFromList)sender;
                string tag = (string)control.Tag;
                if (tag == SelectInboundOutboundTag) {
                    CurrentPanelStatus.IsInbound = (control.Selected == Strings.Entering_facility_inbound);
                    SetUpViewChain();// As this may have changed which options to display.
                    try { // to transition to the next screen
                        Transition(control, NextFromView(control), true);
                    } catch (InTransitionException) { /* suppress exception */ }
                } else if (tag == SelectOrderTag) {
                    SelectFromListOrderSelected(control);
                    SetUpViewChain(); // As this may have changed which options to display.
                    try { // to transition to the next screen
                        Transition(control, NextFromView(control), true);
                    } catch (InTransitionException) { /* suppress exception */ }
                } else if (tag == SelectStagedOrderTag) {
                    Guid stagedOrderId = Guid.Parse(control.Selected);
                    KaStagedOrder stagedOrder = null;
                    foreach (KaStagedOrder so in CurrentPanelStatus.StagedOrders) {
                        if (so.Id == stagedOrderId) {
                            stagedOrder = so;
                            break; // we've found the staged order so there's no need to look any further
                        }
                    }
                    if (stagedOrder == null) {
                        try {
                            stagedOrder = new KaStagedOrder(Tm2Database.Connection, stagedOrderId);
                            stagedOrder.GetChildren(Tm2Database.Connection, null, true);
                        } catch (RecordNotFoundException) { // the staged order isn't in the database
                            stagedOrder = null;
                        }
                    }
                    if (stagedOrder == null) { // Don't use staged order was selected
                        Transition(control, BackFromView(control), false);
                    } else
                        UseStagedOrder();
                } else if (tag == SelectCustomerAccountLocationTag) {
                    if (control.Selected == "-1") { // none selected
                        CurrentPanelStatus.CustomerAccountLocation = null;
                        try { // to transition to the next screen
                            Transition(control, NextFromView(control), true);
                        } catch (InTransitionException) { /* suppress exception */ }
                    } else if (control.Selected == "-2") { // new selected
                        try {
                            Transition(control, EnterShipToCity(), true);
                        } catch (InTransitionException) { /* suppress exception */ }
                    } else if (Guid.Parse(control.Selected) == Guid.Empty) {
                        //This selection is the destination typed in for the particular order
                        //It is already set to the order, so we will do nothing here except move to the next screen
                        if (CurrentPanelStatus.Order != null) {
                            KaOrder order = new KaOrder(Tm2Database.Connection, CurrentPanelStatus.Order.Id);
                            if (order.ShipToName.Trim().Length > 0 && order.ShipToStreet.Trim().Length > 0 && order.ShipToCity.Trim().Length > 0 && order.ShipToState.Trim().Length > 0 && order.ShipToZipCode.Trim().Length > 0) {
                                CurrentPanelStatus.CustomerAccountLocation = new KaCustomerAccountLocation();
                                CurrentPanelStatus.CustomerAccountLocation.Name = order.ShipToName;
                                CurrentPanelStatus.CustomerAccountLocation.Street = order.ShipToStreet;
                                CurrentPanelStatus.CustomerAccountLocation.City = order.ShipToCity;
                                CurrentPanelStatus.CustomerAccountLocation.State = order.ShipToState;
                                CurrentPanelStatus.CustomerAccountLocation.ZipCode = order.ShipToZipCode;
                            }
                        } else if (CurrentPanelStatus.StagedOrder != null) {
                            foreach (KaStagedOrderOrder soo in CurrentPanelStatus.StagedOrder.Orders) {
                                KaOrder order = new KaOrder(Tm2Database.Connection, soo.OrderId);
                                if (order.ShipToName.Trim().Length > 0 && order.ShipToStreet.Trim().Length > 0 && order.ShipToCity.Trim().Length > 0 && order.ShipToState.Trim().Length > 0 && order.ShipToZipCode.Trim().Length > 0) {
                                    CurrentPanelStatus.CustomerAccountLocation = new KaCustomerAccountLocation();
                                    CurrentPanelStatus.CustomerAccountLocation.Name = order.ShipToName;
                                    CurrentPanelStatus.CustomerAccountLocation.Street = order.ShipToStreet;
                                    CurrentPanelStatus.CustomerAccountLocation.City = order.ShipToCity;
                                    CurrentPanelStatus.CustomerAccountLocation.State = order.ShipToState;
                                    CurrentPanelStatus.CustomerAccountLocation.ZipCode = order.ShipToZipCode;
                                    break;
                                }
                            }

                        }
                        try { // to transition to the next screen
                            Transition(control, NextFromView(control), true);
                        } catch (InTransitionException) { /* suppress exception */ }
                    } else {
                        CurrentPanelStatus.CustomerAccountLocation = new KaCustomerAccountLocation(Tm2Database.Connection, Guid.Parse(control.Selected));
                        try { // to transition to the next screen
                            Transition(control, NextFromView(control), true);
                        } catch (InTransitionException) { /* suppress exception */ }
                    }
                } else if (tag == SelectPrestageCompleteProductTag) {
                    SelectFromListItems.LoadedBulkProductListItem selectedBulkProduct = (SelectFromListItems.LoadedBulkProductListItem)Tm2Database.FromXml(control.Selected, typeof(SelectFromListItems.LoadedBulkProductListItem));
                    CurrentPanelStatus.BulkProductId = selectedBulkProduct.BulkProductId;
                    CurrentPanelStatus.ProductId = selectedBulkProduct.ProductId;
                    CurrentPanelStatus.OrderItemId = selectedBulkProduct.OrderItemId;
                    try { // to transition to the next screen
                        Transition(control, NextFromView(control), true);
                    } catch (InTransitionException) { /* suppress exception */ }
                }
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
                ((Control)sender).Enabled = true;
            }
        }

        private void SelectFromListOrderSelected(SelectFromList control) {
            if (CurrentPanelStatus.IsInbound) {
                try {
                    CurrentPanelStatus.Order = new KaOrder(Tm2Database.Connection, Guid.Parse(control.Selected));
                    //Confirm that order is still valid (not deleted, completed, or locked)
                    if (CurrentPanelStatus.Order.Completed || CurrentPanelStatus.Order.Deleted || (Database.Settings.EnforceOrderLocking && CurrentPanelStatus.Order.Locked)) {
                        messageView1.Tag = control.Tag;
                        messageView1.OtherButton.Visible = false;
                        messageView1.SetColors(Color.Red, Color.White);
                        messageView1.Message = Strings.OrderNoLongerValid;
                        try {
                            CurrentPanelStatus.Order = null; //Reset this variable so it doesn't unlock it during the start over click.
                            Transition(control, messageView1, true);
                            return;
                        } catch (InTransitionException) { /* suppress exception */ }
                    }

                    //Confirm that all accounts tied to this order are not disabled
                    foreach (KaOrderCustomerAccount oca in CurrentPanelStatus.Order.OrderAccounts) {
                        KaCustomerAccount account = new KaCustomerAccount(Tm2Database.Connection, oca.CustomerAccountId);
                        if (account.Disabled) {
                            messageView1.Tag = control.Tag;
                            messageView1.OtherButton.Visible = false;
                            messageView1.SetColors(Color.Red, Color.White);
                            messageView1.Message = Strings.AccountDisabled;
                            try {
                                Transition(control, messageView1, true);
                                return;
                            } catch (InTransitionException) { /* suppress exception */ }
                        }
                    }

                    if (CurrentPanelStatus.IsInbound && Database.Settings.PromptForShipToDestinationOwners.Contains(CurrentPanelStatus.Order.OwnerId)) SetCustomerAccountLocations();
                    else CurrentPanelStatus.CustomerAccountLocations = null;
                } catch (RecordNotFoundException) {
                    CurrentPanelStatus.ReceivingPurchaseOrder = new KaReceivingPurchaseOrder(Tm2Database.Connection, Guid.Parse(control.Selected));
                    if (CurrentPanelStatus.IsInbound) {
                        //Confirm that order is still valid (not deleted, completed, or locked)
                        if (CurrentPanelStatus.ReceivingPurchaseOrder.Completed || CurrentPanelStatus.ReceivingPurchaseOrder.Deleted) {
                            messageView1.Tag = control.Tag;
                            messageView1.OtherButton.Visible = false;
                            messageView1.SetColors(Color.Red, Color.White);
                            messageView1.Message = Strings.PurchaseOrderNoLongerValid;
                            try {
                                CurrentPanelStatus.Order = null; //Reset this variable so it doesn't unlock it during the start over click.
                                Transition(control, messageView1, true);
                                return;
                            } catch (InTransitionException) { /* suppress exception */ }
                        }

                        //Confirm that all accounts tied to this order are not disabled
                        KaSupplierAccount account = new KaSupplierAccount(Tm2Database.Connection, CurrentPanelStatus.ReceivingPurchaseOrder.SupplierAccountId);
                        if (account.Disabled) {
                            messageView1.Tag = control.Tag;
                            messageView1.OtherButton.Visible = false;
                            messageView1.SetColors(Color.Red, Color.White);
                            messageView1.Message = Strings.AccountDisabled;
                            try {
                                Transition(control, messageView1, true);
                                return;
                            } catch (InTransitionException) { /* suppress exception */ }
                        }
                    }
                }
            } else { // outbound
                try {
                    CurrentPanelStatus.StagedOrder = new KaStagedOrder(Tm2Database.Connection, Guid.Parse(control.Selected));
                    CurrentPanelStatus.StagedOrder.GetChildren(Tm2Database.Connection, null, true);
                    UseStagedOrder();
                } catch (RecordNotFoundException) {
                    CurrentPanelStatus.InProgress = new KaInProgress(Tm2Database.Connection, Guid.Parse(control.Selected));
                    UseInProgressData();
                }

            }
        }

        #endregion

        #region enterText events

        private EnterText GetNewEnterTextObject() {
            EnterText enterTextPrompt = new EnterText() {
                BackColor = System.Drawing.Color.White,
                CapsLockOn = true,
                DisableOnClickEvent = true,
                Location = new System.Drawing.Point(0, 0),
                MaxLength = 32767,
                MaxNumberOfCharsAllowed = int.MaxValue,
                MinNumberOfCharsRequired = 1,
                Multiline = false,
                PromptText = "",
                ShowBackButton = true,
                ShowStartOverButton = true,
                Size = new System.Drawing.Size(ClientSize.Width, ClientSize.Height),
                TabIndex = 12,
                Title = "Title",
                UseAlphaNumericKeypad = true,
                UsePasswordCharacters = false,
                Visible = false,

            };
            enterTextPrompt.SetNumericKeyboardSizeScalePercent(Database.Settings.NumericKeyboardScalar);
            enterTextPrompt.SetAlphaNumericKeyboardSizeScalePercent(Database.Settings.AlphaNumericKeyboardScalar);
            enterTextPrompt.BackClicked += new KahlerAutomation.EnterText.BackClickedEventHandler(enterText_BackClicked);
            enterTextPrompt.OkClicked += new KahlerAutomation.EnterText.OkClickedEventHandler(enterText_OkClicked);
            enterTextPrompt.StartOverClicked += new KahlerAutomation.EnterText.StartOverClickedEventHandler(StartOverClicked);
            enterTextPrompt.UpdateCulture();
            this.Controls.Add(enterTextPrompt);
            return enterTextPrompt;
        }

        private void enterText_BackClicked(object sender, EventArgs e) {
            try {
                EnterText control = (EnterText)sender;
                string tag = (string)control.Tag;
                if (tag == EnterShipToStateTag) Transition(control, EnterShipToCity(), false);
                else if (tag == EnterShipToZipTag) Transition(control, EnterShipToState(), false);
                else if (tag == EnterConfigurationPasswordTag) Transition(control, _previousObjectView, false);
                else Transition((Control)sender, BackFromView((Control)sender), false);
            } catch (InTransitionException) { /* suppress exception */
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        private void enterText_OkClicked(object sender, EventArgs e) {
            try {
                EnterText control = (EnterText)sender;
                string tag = (string)control.Tag;
                if (tag == EnterDriverNumberTag) EnterTextOkClickedEnterDriverNumber(control);
                else if (tag == EnterTransportNumberTag) EnterTextOkClickedEnterTransportNumber(control);
                else if (tag == EnterCarrierNumberTag) EnterTextOkClickedEnterCarrierNumber(control);
                else if (tag == EnterOrderNumberTag) EnterTextOkClickedEnterOrderNumber(control);
                else if (tag == PreviousLoadProductTag) EnterTextOkPreviousLoadProductTag(control);
                else if (tag == EnterShipToCityTag) {
                    if (control.Text.Trim() == "") {
                        messageView1.CallingControl = _currentObjectView;
                        messageView1.Tag = control.Tag;
                        messageView1.OtherButton.Visible = false;
                        messageView1.SetColors(Color.Red, Color.White);
                        messageView1.Message = Strings.ShipToCityMustBeSpecified;
                        try {
                            Transition(control, messageView1, true);
                        } catch (InTransitionException) { /* suppress exception */ }
                    } else {
                        CurrentPanelStatus.ShipToCity = control.Text;
                        Transition(control, EnterShipToState(), true);
                    }
                } else if (tag == EnterShipToStateTag) {
                    if (control.Text.Trim() == "") {
                        messageView1.CallingControl = _currentObjectView;
                        messageView1.Tag = control.Tag;
                        messageView1.OtherButton.Visible = false;
                        messageView1.SetColors(Color.Red, Color.White);
                        messageView1.Message = Strings.ShipToCityMustBeSpecified;
                        try {
                            Transition(control, messageView1, true);
                        } catch (InTransitionException) { /* suppress exception */ }
                    } else {
                        CurrentPanelStatus.ShipToState = control.Text;
                        Transition(control, EnterShipToZip(), true);
                    }
                } else if (tag == EnterShipToZipTag) {
                    if (control.Text.Trim() == "") {
                        messageView1.CallingControl = _currentObjectView;
                        messageView1.Tag = control.Tag;
                        messageView1.OtherButton.Visible = false;
                        messageView1.SetColors(Color.Red, Color.White);
                        messageView1.Message = Strings.ShipToZipcodeMustBeSpecified;
                        try {
                            Transition(control, messageView1, true);
                        } catch (InTransitionException) { /* suppress exception */ }
                    } else {
                        CurrentPanelStatus.ShipToZip = control.Text;
                        KaCustomerAccount customer;
                        string shipToName;
                        if (CurrentPanelStatus.Order != null) {
                            customer = new KaCustomerAccount(Tm2Database.Connection, CurrentPanelStatus.Order.OrderAccounts[0].CustomerAccountId);
                            shipToName = (customer.Name + " - " + CurrentPanelStatus.ShipToCity);
                        } else {
                            customer = new KaCustomerAccount();
                            shipToName = CurrentPanelStatus.ShipToCity + CurrentPanelStatus.ShipToState;
                        }
                        int maxNameLength = (int)Math.Max(0, Tm2Database.GetMaxDatabaseFieldLength(KaCustomerAccountLocation.TABLE_NAME, KaCustomerAccountLocation.FN_NAME));
                        shipToName = shipToName.PadRight(maxNameLength).Substring(0, maxNameLength).Trim();

                        ArrayList customerAccounts = KaCustomerAccountLocation.GetAll(Tm2Database.Connection, $"deleted = 0 AND name = { Database.Q(shipToName) } AND city = { Database.Q(CurrentPanelStatus.ShipToCity) } AND state = { Database.Q(CurrentPanelStatus.ShipToState) } AND (customer_account_id = { Database.Q(customer.Id)} OR customer_account_id = { Database.Q(Guid.Empty)})", "");
                        if (customerAccounts.Count > 0)
                            CurrentPanelStatus.CustomerAccountLocation = (KaCustomerAccountLocation)customerAccounts[0];
                        else {
                            CurrentPanelStatus.CustomerAccountLocation = new KaCustomerAccountLocation() {
                                Name = shipToName.ToUpper(),
                                City = CurrentPanelStatus.ShipToCity.ToUpper(),
                                State = CurrentPanelStatus.ShipToState.ToUpper(),
                                ZipCode = CurrentPanelStatus.ShipToZip.ToUpper(),
                                CustomerAccountId = customer.Id
                            };
                        }
                        control.Tag = SelectCustomerAccountLocationTag;
                        Transition(control, NextFromView(control), true);
                    }
                } else if (tag == EnterConfigurationPasswordTag) {
                    EnterTextOkClickedEnterConfigurationPassword(control);
                }
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
                ((Control)sender).Enabled = true;
                Transition((Control)sender, (Control)sender, true);
            }
        }

        private void StartOverClicked(object sender, EventArgs e) {
            CurrentPanelStatus.ResetVariables();
            ShowDetermineInboundOutboundScreen(false);
        }

        private void ShowDetermineInboundOutboundScreen(bool transitionLeft) {
            Boolean showDetermineInboundOutboundScreen = false;

            bool inboundEnabled = Database.Settings.Inbound.Enabled;
            bool outboundEnabled = Database.Settings.Outbound.Enabled;
            if (inboundEnabled && !outboundEnabled)
                CurrentPanelStatus.IsInbound = true;
            else if (!inboundEnabled & outboundEnabled)
                CurrentPanelStatus.IsInbound = false;
            else // they are either both enabled or both disabled...
                showDetermineInboundOutboundScreen = true;

            Control nextObj = null;
            Control inboundOutboundScreen = SelectInboundOutbound();
            if (showDetermineInboundOutboundScreen) {
                nextObj = inboundOutboundScreen;
            } else {
                nextObj = NextFromView(inboundOutboundScreen);
                RemoveOutgoingScreenControls(inboundOutboundScreen);
                UpdateCulture(Database.ENGLISH);
            }

            if (_currentObjectView == null) {
                _currentObjectView = nextObj;
                _currentObjectView.Top = 0;
                _currentObjectView.Left = 0;
                _currentObjectView.Visible = true;
            }
            try { // to transition to the next screen
                Transition(_currentObjectView, nextObj, transitionLeft);
            } catch (InTransitionException) { /* suppress exception */ }
            if (_firstScreenIndex == -1 && nextObj != null) _firstScreenIndex = GetScreenIndex((string)nextObj.Tag);  //First screen is set at this point, remember what it is so we can come back to it.
        }

        private void EnterTextOkClickedEnterDriverNumber(EnterText control) {
            ArrayList tempDrivers = KaDriver.GetAll(Tm2Database.Connection, "number = " + Database.Q(control.Text.Trim()) + " AND deleted = 0 AND disabled = 0", "");
            ArrayList drivers = new ArrayList();
            foreach (KaDriver driver in tempDrivers) {
                drivers.Add(driver);
            }
            if (drivers.Count == 1) {
                try {
                    CurrentPanelStatus.Driver = (KaDriver)drivers[0];
                    if (CurrentPanelStatus.Driver.Password.Trim().Length == 0) {
                        CheckForStagedOrder(true);
                    } else {
                        CurrentPanelStatus.StagedOrders.Clear();
                    }
                    Transition(control, NextFromView(control), true);
                } catch (InTransitionException) { /* suppress exception */ }
            } else if (drivers.Count > 1) {
                CurrentPanelStatus.StagedOrders.Clear();
                messageView1.Tag = control.Tag;
                messageView1.OtherButton.Visible = false;
                messageView1.SetColors(Color.Red, Color.White);
                messageView1.Message = string.Format(Strings.TooManyDriversWithNumber_0, Database.Q(control.Text));
                try {
                    Transition(control, messageView1, true);
                } catch (InTransitionException) { /* suppress exception */ }
            } else if (tempDrivers.Count > 0) {
                CurrentPanelStatus.StagedOrders.Clear();
                messageView1.Tag = control.Tag;
                messageView1.OtherButton.Visible = false;
                messageView1.SetColors(Color.Red, Color.White);
                messageView1.Message = string.Format(Strings.Driver_0_NotAllowedAccess, Database.Q(control.Text));
                try {
                    Transition(control, messageView1, true);
                } catch (InTransitionException) { /* suppress exception */ }
            } else if (((CurrentPanelStatus.IsInbound && Database.Settings.Inbound.ValidateDriverNumber) || (!CurrentPanelStatus.IsInbound && Database.Settings.Outbound.ValidateDriverNumber)) || control.Text.Trim().Equals("")) {
                CurrentPanelStatus.StagedOrders.Clear();
                messageView1.Tag = control.Tag;
                messageView1.OtherButton.Visible = false;
                messageView1.SetColors(Color.Red, Color.White);
                messageView1.Message = string.Format(Strings.DriverNotFoundWithNumber_0, Database.Q(control.Text));
                try {
                    Transition(control, messageView1, true);
                } catch (InTransitionException) { /* suppress exception */ }
            } else { // create a new transport using the number entered
                CurrentPanelStatus.StagedOrders.Clear();
                CurrentPanelStatus.Driver = new KaDriver();
                CurrentPanelStatus.Driver.Name = control.Text.ToUpper();
                CurrentPanelStatus.Driver.Number = control.Text;
                CurrentPanelStatus.Driver.ValidForAllAccounts = true;
                CurrentPanelStatus.Driver.ValidForAllLocations = true;
                Transition(control, NextFromView(control), true);
            }
        }

        private void EnterTextOkClickedEnterTransportNumber(EnterText control) {
            ArrayList transports = KaTransport.GetAll(Tm2Database.Connection, "deleted=0 AND number=" + Database.Q(control.Text.Trim()), "");
            if (transports.Count > 0) {
                try {
                    KaTransport tempTransport = (KaTransport)transports[0];

                    CurrentPanelStatus.Transport = tempTransport;
                    if (!CurrentPanelStatus.IsInbound) CheckForStagedOrder(true);
                    Transition(control, NextFromView(control), true);
                } catch (InTransitionException) { /* suppress exception */ }
            } else if (((CurrentPanelStatus.IsInbound && Database.Settings.Inbound.ValidateTransportNumber) || (!CurrentPanelStatus.IsInbound && Database.Settings.Outbound.ValidateTransportNumber)) || control.Text.Trim().Equals("")) {
                CurrentPanelStatus.StagedOrders.Clear();
                messageView1.Tag = control.Tag;
                messageView1.OtherButton.Visible = false;
                messageView1.SetColors(Color.Red, Color.White);
                messageView1.Message = string.Format(Strings.TransportNotFoundWithNumber_0, Database.Q(control.Text));
                try {
                    Transition(control, messageView1, true);
                } catch (InTransitionException) { /* suppress exception */ }
            } else { // create a new transport using the number entered
                CurrentPanelStatus.StagedOrders.Clear();
                CurrentPanelStatus.Transport = new KaTransport();
                CurrentPanelStatus.Transport.Name = control.Text.ToUpper();
                CurrentPanelStatus.Transport.Number = control.Text;
                CurrentPanelStatus.Transport.UnitId = CurrentPanelStatus.ValidatedScaleWeight.UnitId;
                CurrentPanelStatus.Transport.TareWeight = 0;
                CurrentPanelStatus.Transport.MaximumWeightStandard = Database.Settings.DefaultMaxWeightForNewTransports;
                CurrentPanelStatus.Transport.Length = 0;
                Transition(control, NextFromView(control), true);
            }
        }

        private void EnterTextOkClickedEnterCarrierNumber(EnterText control) {
            ArrayList carriers = KaCarrier.GetAll(Tm2Database.Connection, "number = " + Database.Q(control.Text.Trim()) + " AND deleted = 0", "");
            if (carriers.Count > 1) {
                messageView1.Tag = control.Tag;
                messageView1.OtherButton.Visible = false;
                messageView1.SetColors(Color.Red, Color.White);
                messageView1.Message = string.Format(Strings.TooManyCarriersWithNumber_0, control.Text);
                try {
                    Transition(control, messageView1, true);
                } catch (InTransitionException) { /* suppress exception */ }
            } else if (carriers.Count == 1) {
                try {
                    CurrentPanelStatus.Carrier = (KaCarrier)carriers[0];
                    if (!CurrentPanelStatus.IsInbound) CheckForStagedOrder(true);
                    Transition(control, NextFromView(control), true);
                } catch (InTransitionException) { /* suppress exception */ }
            } else if (((CurrentPanelStatus.IsInbound && Database.Settings.Inbound.ValidateCarrierNumber) || (!CurrentPanelStatus.IsInbound && Database.Settings.Outbound.ValidateCarrierNumber)) || control.Text.Trim().Equals("")) {
                CurrentPanelStatus.StagedOrders.Clear();
                messageView1.Tag = control.Tag;
                messageView1.OtherButton.Visible = false;
                messageView1.SetColors(Color.Red, Color.White);
                messageView1.Message = string.Format(Strings.CarrierNotFoundWithNumber_0, Database.Q(control.Text));
                try {
                    Transition(control, messageView1, true);
                } catch (InTransitionException) { /* suppress exception */ }
            } else { // create a new transport using the number entered
                CurrentPanelStatus.StagedOrders.Clear();
                CurrentPanelStatus.Carrier = new KaCarrier();
                CurrentPanelStatus.Carrier.Name = control.Text.ToUpper();
                CurrentPanelStatus.Carrier.Number = control.Text;
                Transition(control, NextFromView(control), true);
            }
        }

        private void EnterTextOkClickedEnterOrderNumber(EnterText control) {
            string orderNumberEntered = control.Text.Trim();
            CurrentPanelStatus.RefinedOrderNumberFilter = orderNumberEntered;
            CurrentPanelStatus.Order = null; // remove the reference, we'll test for null later to see if an order was found
            CurrentPanelStatus.ReceivingPurchaseOrder = null;
            CurrentPanelStatus.CustomerAccountLocations = null;
            CurrentPanelStatus.StagedOrders.Clear();
            CurrentPanelStatus.InProgress = null;

            List<Guid> lockedOrders = new List<Guid>();
            Dictionary<Guid, Guid> disabledOrderAccounts = new Dictionary<Guid, Guid>();
            List<KaOrder> validOrders = new List<KaOrder>();
            List<KaOrder> orders = new List<KaOrder>();
            List<KaReceivingPurchaseOrder> validReceivingOrders = new List<KaReceivingPurchaseOrder>();
            List<KaReceivingPurchaseOrder> receivingOrders = new List<KaReceivingPurchaseOrder>();

            if (orderNumberEntered.Length > 0)
                GetOrdersThatMatchNumber(orderNumberEntered, out validOrders, out validReceivingOrders, out lockedOrders, out disabledOrderAccounts);

            if (CurrentPanelStatus.IsInbound) {
                if (validOrders.Count + validReceivingOrders.Count == 1) {
                    if (validOrders.Count == 1) {
                        CurrentPanelStatus.Order = validOrders[0];
                        try {
                            CurrentPanelStatus.CustomerAccountLocation = new KaCustomerAccountLocation(Tm2Database.Connection, CurrentPanelStatus.Order.CustomerAccountLocationId);
                        } catch (RecordNotFoundException) {

                        }
                        if (CurrentPanelStatus.Order != null && Database.Settings.PromptForShipToDestinationOwners.Contains(CurrentPanelStatus.Order.OwnerId)) SetCustomerAccountLocations();
                    } else {
                        CurrentPanelStatus.ReceivingPurchaseOrder = validReceivingOrders[0];
                        CurrentPanelStatus.InProgress = new KaInProgress() { Id = Guid.NewGuid() };
                        CurrentPanelStatus.InProgress.ReceivingPurchaseOrderId = CurrentPanelStatus.ReceivingPurchaseOrder.Id;
                        CurrentPanelStatus.InProgress.OrderType = (int)KaInProgress.OrderTypes.ReceivingPurchaseOrder;
                        CurrentPanelStatus.CustomerAccountLocations = null;
                    }
                    SetUpViewChain();
                    try { // to transition to the next view
                        Transition(control, NextFromView(control), true);
                    } catch (InTransitionException) { /* suppress exception */ }
                } else if (validOrders.Count + validReceivingOrders.Count > 1) {
                    SelectFromList sfl = SelectOrder();

                    Transition(control, sfl, true);
                    return;
                } else if (validOrders.Count + validReceivingOrders.Count == 0) { // couldn't find an order with the entered number that the driver could load, notify the driver
                    messageView1.Tag = control.Tag;
                    messageView1.OtherButton.Visible = false;
                    messageView1.SetColors(Color.Red, Color.White);

                    if (lockedOrders.Count > 0) {
                        messageView1.Message = string.Format(Strings.OrderLockedWithNumber_0, Database.Q(control.Text));
                    } else if (orders.Count > 0) {
                        messageView1.Message = string.Format(Strings.OrderUnableToLoadWithNumber_0, Database.Q(control.Text));
                    } else {
                        messageView1.Message = string.Format(Strings.OrderNotFoundWithNumber_0, Database.Q(control.Text));
                    }
                    try {
                        Transition(control, messageView1, true);
                    } catch (InTransitionException) { /* suppress exception */ }
                }
            } else {
                // Check that there is valid information assigned to these
                List<KaStagedOrder> validStagedOrders;
                Dictionary<Guid, KaOrder> validOrderIds;
                List<KaInProgress> validInProgressOrders;
                Dictionary<Guid, KaReceivingPurchaseOrder> validPoOrderIds;
                CheckForValidOutboundOrders(validOrders, validReceivingOrders, out validStagedOrders, out validOrderIds, out validInProgressOrders, out validPoOrderIds);

                if (validStagedOrders.Count + validInProgressOrders.Count == 1) {
                    if (validStagedOrders.Count == 1) { // this is for a delivery order
                        CurrentPanelStatus.StagedOrder = validStagedOrders[0];
                        foreach (KaStagedOrderOrder soo in CurrentPanelStatus.StagedOrder.Orders) {
                            if (validOrderIds.ContainsKey(soo.OrderId)) {
                                CurrentPanelStatus.Order = validOrderIds[soo.OrderId];
                            }
                        }
                        try {
                            CurrentPanelStatus.CustomerAccountLocation = new KaCustomerAccountLocation(Tm2Database.Connection, CurrentPanelStatus.Order.CustomerAccountLocationId);
                        } catch (RecordNotFoundException) {

                        }
                        if (CurrentPanelStatus.Order != null && Database.Settings.PromptForShipToDestinationOwners.Contains(CurrentPanelStatus.Order.OwnerId)) SetCustomerAccountLocations();
                        UseStagedOrder();
                        if (CurrentPanelStatus.InProgress == null) {
                            CurrentPanelStatus.InProgress = new KaInProgress() {
                                Id = Guid.NewGuid(),
                                StagedOrderId = CurrentPanelStatus.StagedOrder.Id,
                                OrderType = (int)KaInProgress.OrderTypes.StagedOrder
                            };
                        }
                    } else { // this is for a receiving order
                        CurrentPanelStatus.InProgress = validInProgressOrders[0];
                        UseInProgressData();
                        CurrentPanelStatus.ReceivingPurchaseOrder = validPoOrderIds[CurrentPanelStatus.InProgress.ReceivingPurchaseOrderId];
                        CurrentPanelStatus.CustomerAccountLocations = null;
                    }
                    SetUpViewChain();
                    try { // to transition to the next view
                        Transition(control, NextFromView(control), true);
                    } catch (InTransitionException) { /* suppress exception */ }
                } else if (validStagedOrders.Count + validInProgressOrders.Count > 1) {
                    SelectFromList sfl = SelectOrder();

                    Transition(control, sfl, true);
                    return;
                } else { // couldn't find an order with the entered number that the driver could load, notify the driver
                    messageView1.Tag = control.Tag;
                    messageView1.OtherButton.Visible = false;
                    messageView1.SetColors(Color.Red, Color.White);

                    if (lockedOrders.Count > 0) {
                        messageView1.Message = string.Format(Strings.OrderLockedWithNumber_0, Database.Q(control.Text));
                    } else if (orders.Count > 0) {
                        messageView1.Message = string.Format(Strings.OrderUnableToLoadWithNumber_0, Database.Q(control.Text));
                    } else {
                        messageView1.Message = string.Format(Strings.OrderNotFoundWithNumber_0, Database.Q(control.Text));
                    }
                    try {
                        Transition(control, messageView1, true);
                    } catch (InTransitionException) { /* suppress exception */ }
                }
            }
        }

        private void EnterTextOkClickedEnterConfigurationPassword(EnterText control) {
            string enteredPassword = control.Text.Trim();
            try {
                Configuration config = new Configuration();

                if (Database.UserId != Guid.Empty) {
                    if (Database.CurrentUser.Password == enteredPassword) {
                        _currentObjectView.Visible = false;
                        Application.DoEvents();
                        System.Threading.Thread.Sleep(0);
                        if (config.ShowDialog(this) == DialogResult.OK) {
                            RemoveOutgoingScreenControls(_currentObjectView);
                            _currentObjectView = null;
                            CurrentPanelStatus.ResetVariables();
                            SetUpViewChain();
                            ShowDetermineInboundOutboundScreen(true);
                            _firstScreenIndex = GetScreenIndex((string)_currentObjectView.Tag);  //First screen is set at this point, remember what it is so we can come back to it. 
                            StartOverClicked(null, null);
                        }
                    } else {
                        messageView1.CallingControl = _currentObjectView;
                        messageView1.Tag = control.Tag;
                        messageView1.OtherButton.Visible = false;
                        messageView1.SetColors(Color.Red, Color.White);
                        messageView1.Message = Strings.IncorrectPassword;
                        try {
                            Transition(control, messageView1, true);
                        } catch (InTransitionException) { /* suppress exception */ }
                    }
                } else {
                    //User not set yet, let them into config.  config will force a user to be saved.
                    if (config.ShowDialog(this) == DialogResult.OK) {
                        _firstScreenIndex = -1;
                        StartOverClicked(null, null);
                    }
                }
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        private void EnterTextOkClickedCustomLoadQuestion(EnterText control) {
        }

        private void EnterTextOkPreviousLoadProductTag(EnterText control) {
            CurrentPanelStatus.PreviousLoadProduct = control.Text.Trim();
            Transition(control, NextFromView(control), true);
        }
        #endregion

        #region YesNoPrompt events

        private YesNo GetNewYesNoObject() {
            YesNo yesNoPrompt = new YesNo() {
                BackColor = System.Drawing.Color.White,
                DisableOnClickEvent = true,
                Location = new System.Drawing.Point(0, 0),
                PromptText = "",
                ShowBackButton = true,
                ShowStartOverButton = true,
                Size = new System.Drawing.Size(ClientSize.Width, ClientSize.Height),
                Tag = "YesNo",
                Visible = false
            };
            yesNoPrompt.NoClicked += new KahlerAutomation.YesNo.NoClickedEventHandler(yesNoPrompt_NoClicked);
            yesNoPrompt.YesClicked += new KahlerAutomation.YesNo.YesClickedEventHandler(yesNoPrompt_YesClicked);
            yesNoPrompt.BackClicked += new KahlerAutomation.YesNo.BackClickedEventHandler(yesNoPrompt_BackClicked);
            yesNoPrompt.StartOverClicked += new KahlerAutomation.YesNo.StartOverClickedEventHandler(StartOverClicked);
            yesNoPrompt.UpdateCulture();
            this.Controls.Add(yesNoPrompt);
            return yesNoPrompt;
        }
        private void yesNoPrompt_BackClicked(object sender, EventArgs e) {
            try {
                YesNo control = (YesNo)sender;
                string tag = (string)control.Tag;
                if (tag == RequireRestrictedUseTag)
                    Transition(control, restrictedUseProductHauled1, false);
                else
                    Transition(control, BackFromView(_currentObjectView), false);
            } catch (InTransitionException) { /* suppress exception */
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        private void yesNoPrompt_YesClicked(object sender, EventArgs e) {
            string answer = Strings.Yes;
            try {
                YesNo control = (YesNo)sender;
                string tag = (string)control.Tag;
                if (tag == RequireRestrictedUseTag)
                    Transition(control, NextFromView(control), true);
            } catch (InTransitionException) { /* suppress exception */
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        private void yesNoPrompt_NoClicked(object sender, EventArgs e) {
            string answer = Strings.No;
            try {
                YesNo control = (YesNo)sender;
                string tag = (string)control.Tag;
                if (tag == RequireRestrictedUseTag)
                    Transition(control, restrictedUseProductHauled1, false);
            } catch (InTransitionException) { /* suppress exception */
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }
        #endregion

        #region EnterDatePrompt events

        private EnterDate GetNewEnterDateObject() {
            EnterDate enterDatePrompt = new EnterDate() {
                BackColor = System.Drawing.Color.White,
                DisableOnClickEvent = true,
                Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F),
                Location = new System.Drawing.Point(-1, 48),
                PromptText = "",
                SelectedDate = new System.DateTime(((long)(0))),
                SelectionType = KahlerAutomation.EnterDate.DataType.DateAndTime,
                ShowBackButton = true,
                ShowStartOverButton = true,
                Size = new System.Drawing.Size(ClientSize.Width, ClientSize.Height),
                Tag = "EnterDate",
                Visible = false,
            };

            enterDatePrompt.BackClicked += new KahlerAutomation.EnterDate.BackClickedEventHandler(EnterDatePromptBackClicked);
            enterDatePrompt.OkClicked += new KahlerAutomation.EnterDate.OkClickedEventHandler(EnterDatePromptOkClicked);
            enterDatePrompt.StartOverClicked += new KahlerAutomation.EnterDate.StartOverClickedEventHandler(StartOverClicked);
            enterDatePrompt.UpdateCulture();
            this.Controls.Add(enterDatePrompt);
            return enterDatePrompt;
        }
        private void EnterDatePromptBackClicked(object sender, EventArgs e) {
            try {
                EnterDate control = (EnterDate)sender;
                string tag = (string)control.Tag;
            } catch (InTransitionException) { /* suppress exception */
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        private void EnterDatePromptOkClicked(object sender, EventArgs e) {
            try {
                EnterDate currentEnterDate = (EnterDate)sender;
                string tag = (string)currentEnterDate.Tag;
                currentEnterDate.Enabled = true;
            } catch (InTransitionException) { // suppress exception
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }
        #endregion

        #region messageView events
        private void messageView_OkClicked(object sender, EventArgs e) {
            string tag = (string)messageView1.Tag;
            if (tag == VerifyCurrentTruckWeightTag) {
                try {
                    Transition(messageView1, weighTruck1, false);
                } catch (InTransitionException) { /* suppress exception */ }
            } else if (tag == EnterDriverNumberTag) {
                try {
                    Transition(messageView1, EnterDriverNumber(), false);
                } catch (InTransitionException) { /* suppress exception */ }
            } else if (tag == EnterTransportNumberTag) {
                try {
                    Transition(messageView1, EnterTransportNumber(), false);
                } catch (InTransitionException) { /* suppress exception */ }
            } else if (tag == EnterCarrierNumberTag) {
                try {
                    Transition(messageView1, EnterCarrierNumber(), false);
                } catch (InTransitionException) { /* suppress exception */ }
            } else if (tag == EnterOrderNumberTag) {
                try {
                    Transition(messageView1, EnterOrderNumber(), false);
                } catch (InTransitionException) { /* suppress exception */ }
            } else if (tag == EnterConfigurationPasswordTag) {
                try {
                    Transition(messageView1, GetConfigurationPrompt(), false);
                } catch (InTransitionException) { /* suppress exception */ }
            } else if (tag == SelectOrderTag) {
                try {
                    Transition(messageView1, SelectOrder(), false);
                } catch (InTransitionException) { /* suppress exception */ }
            } else
                try {
                    Transition(messageView1, _previousObjectView, false);
                } catch (InTransitionException) { /* suppress exception */ }
        }

        private void messageView_OtherClicked(object sender, EventArgs e) {

        }
        #endregion

        #region weigh trick events
        private void weighTruck1_OkClicked(object sender, EventArgs e) {
            OleDbConnection connection = Tm2Database.Connection;
            KaUnit scaleUnit = KaUnit.GetUnitForBaseUnit(connection, weighTruck1.Unit);
            KaRatio emptyDensity = new KaRatio(0, LfDatabase.DefaultMassUnitId, LfDatabase.DefaultVolumeUnitId);
            CurrentPanelStatus.ValidatedScaleWeight = new KaQuantity(weighTruck1.Weight, scaleUnit.Id);
            if (CurrentPanelStatus.IsInbound) {
                //Update truck tare_weight
                if (CurrentPanelStatus.Transport != null && CurrentPanelStatus.Order != null) {
                    try {
                        double tareWeight = KaUnit.Convert(connection, CurrentPanelStatus.ValidatedScaleWeight, emptyDensity, CurrentPanelStatus.Transport.UnitId).Numeric;
                        CurrentPanelStatus.Transport.TareWeight = tareWeight;
                        CurrentPanelStatus.Transport.TaredAt = DateTime.Now;
                        CurrentPanelStatus.Transport.TareManual = false;
                    } catch (UnitConversionException) { /* Suppress */ }
                }
            } else if (CurrentPanelStatus.InProgress != null && CurrentPanelStatus.InProgress.ReceivingPurchaseOrderId != Guid.Empty) {
                double tareWeight = double.MaxValue;
                foreach (KaInProgressWeighment weighment in CurrentPanelStatus.InProgress.Weighments) {
                    if (!weighment.Complete) {
                        try {
                            double tare = KaUnit.Convert(connection, new KaQuantity(weighment.Gross, weighment.UnitId), emptyDensity, CurrentPanelStatus.ValidatedScaleWeight.UnitId).Numeric;
                            if (tare < tareWeight) tareWeight = tare;
                        } catch (UnitConversionException) {/* Suppress */ }
                    }
                }
                if (tareWeight < CurrentPanelStatus.ValidatedScaleWeight.Numeric || tareWeight == double.MaxValue) {
                    messageView1.Tag = VerifyCurrentTruckWeightTag;
                    messageView1.OtherButton.Visible = false;
                    messageView1.SetColors(Color.Red, Color.White);
                    messageView1.Message = string.Format(Strings.CurrentWeightGreaterThanInboundWeight_0, tareWeight.ToString());
                    try {
                        Transition(weighTruck1, messageView1, true);
                    } catch (InTransitionException) { /* suppress exception */ }
                    return;
                }
            } else {
                if (CurrentPanelStatus.Transport != null && CurrentPanelStatus.Transport.MaximumWeight > 0) {
                    KaQuantity transportMaximumWeight = new KaQuantity(CurrentPanelStatus.Transport.MaximumWeight, CurrentPanelStatus.Transport.UnitId);

                    try {
                        if (CurrentPanelStatus.ValidatedScaleWeight.Numeric > KaUnit.Convert(connection, transportMaximumWeight, emptyDensity, CurrentPanelStatus.ValidatedScaleWeight.UnitId).Numeric) {
                            messageView1.Tag = VerifyCurrentTruckWeightTag;
                            messageView1.OtherButton.Visible = false;
                            messageView1.SetColors(Color.Red, Color.White);
                            messageView1.Message = string.Format(Strings.CurrentWeightOverMaximumWeight_0_Allowed, transportMaximumWeight.Numeric);
                            try {
                                Transition(weighTruck1, messageView1, true);
                            } catch (InTransitionException) { /* suppress exception */ }

                            return;
                        }
                    } catch (UnitConversionException) { /* Suppress */ }
                }
                double tareWeight = double.MaxValue;
                if (CurrentPanelStatus.InProgress != null) {
                    foreach (KaInProgressWeighment weighment in CurrentPanelStatus.InProgress.Weighments) {
                        if (!weighment.Complete) {
                            try {
                                double tare = KaUnit.Convert(connection, new KaQuantity(weighment.Tare, weighment.UnitId), emptyDensity, CurrentPanelStatus.ValidatedScaleWeight.UnitId).Numeric;
                                if (tare < tareWeight) tareWeight = tare;
                            } catch (UnitConversionException) { /* suppress exception */ }
                        }
                    }
                }
                if (tareWeight == double.MaxValue && CurrentPanelStatus.StagedOrder != null) {
                    foreach (KaStagedOrderTransport transport in CurrentPanelStatus.StagedOrder.Transports) {
                        if (CurrentPanelStatus.Transport == null || transport.TransportId == CurrentPanelStatus.Transport.Id) {
                            tareWeight = KaUnit.Convert(connection, new KaQuantity(transport.TareWeight, transport.TareUnitId), emptyDensity, CurrentPanelStatus.ValidatedScaleWeight.UnitId).Numeric;
                            break;
                        }
                    }
                }
                if (tareWeight == double.MaxValue && CurrentPanelStatus.Transport != null) {
                    tareWeight = KaUnit.Convert(connection, new KaQuantity(CurrentPanelStatus.Transport.TareWeight, CurrentPanelStatus.Transport.UnitId), emptyDensity, CurrentPanelStatus.ValidatedScaleWeight.UnitId).Numeric;
                }
                if (tareWeight < double.MaxValue && tareWeight > CurrentPanelStatus.ValidatedScaleWeight.Numeric) {
                    messageView1.Tag = VerifyCurrentTruckWeightTag;
                    messageView1.OtherButton.Visible = false;
                    messageView1.SetColors(Color.Red, Color.White);
                    messageView1.Message = string.Format(Strings.CurrentWeightLessThanTareWeight_0, tareWeight.ToString());
                    try {
                        Transition(weighTruck1, messageView1, true);
                    } catch (InTransitionException) { /* suppress exception */ }
                    return;
                }
                if (CurrentPanelStatus.StagedOrder != null && Database.IsPalletedLoad(CurrentPanelStatus.StagedOrder)) { // verify that the palleted weights fall within the acceptable range
                    Dictionary<Guid, KaProduct> products = new Dictionary<Guid, KaProduct>();
                    Dictionary<Guid, KaRatio> productDensities = new Dictionary<Guid, KaRatio>();
                    Dictionary<Guid, KaUnit> units = new Dictionary<Guid, KaUnit>();
                    double loadedAmount = CurrentPanelStatus.ValidatedScaleWeight.Numeric - tareWeight;
                    double defaultPalletWeight = Database.DefaultPalletWeight;
                    double expectedWeight = 0;
                    double palletTareWeights = 0;
                    List<Guid> palletedUnits = Database.GetPalletedUnits();
                    try {
                        Dictionary<Guid, KaQuantity> orderItemAssignedAmounts = CurrentPanelStatus.StagedOrder.GetOrderItemAssignedAmounts(connection, null, CurrentPanelStatus.ValidatedScaleWeight.UnitId);
                        foreach (Guid orderItemId in orderItemAssignedAmounts.Keys) {
                            KaOrderItem item = new KaOrderItem(connection, orderItemId);
                            KaProduct product = KaProduct.GetProduct(connection, item.ProductId, products, null);
                            if (!productDensities.ContainsKey(product.Id)) {
                                try {
                                    productDensities.Add(product.Id, product.GetDensity(CurrentPanelStatus.StagedOrder.LocationId));
                                } catch (Exception) {
                                    productDensities.Add(product.Id, Database.EmptyDensity);
                                }
                            }
                            double currentExpectedWeight = KaUnit.FastConvert(connection, orderItemAssignedAmounts[orderItemId], productDensities[product.Id], CurrentPanelStatus.ValidatedScaleWeight.UnitId, units).Numeric;
                            expectedWeight += currentExpectedWeight;
                            if (Database.IsProductPalleted(product, palletedUnits)) {
                                int numberOfPalletsLoaded = 0;
                                double palletedWeight = Database.GetProductPalletedWeight(product.DefaultUnitId, units);
                                if (palletedWeight > 0) {
                                    numberOfPalletsLoaded = (int)Math.Ceiling(Math.Max(currentExpectedWeight / palletedWeight, 0.0));
                                }
                                double palletTareWeight = KaUnit.FastConvert(connection, Database.GetProductPalletTareWeight(product.DefaultUnitId), productDensities[item.ProductId], CurrentPanelStatus.ValidatedScaleWeight.UnitId, units).Numeric;
                                if (palletTareWeight == 0) palletTareWeight = Database.DefaultPalletWeight;
                                palletTareWeights += numberOfPalletsLoaded * palletTareWeight;
                            }
                        }
                    } catch (RecordNotFoundException) {  /* Suppress */ }

                    loadedAmount = Math.Max(0.0, loadedAmount - palletTareWeights);
                    double difference = Math.Abs(loadedAmount - expectedWeight);
                    if (difference > Database.MaxAllowedPalletOverage || (expectedWeight > 0 && difference / expectedWeight > Database.PalletOverPercent * 0.01)) {
                        // The pallet quantity is not within the allowable range
                        messageView1.Tag = weighTruck1.Tag;
                        messageView1.OtherButton.Visible = false;
                        messageView1.SetColors(Color.Red, Color.White);
                        messageView1.Message = Strings.PalletedLoadAmountNotInTolerances;
                        try {
                            Transition(weighTruck1, messageView1, true);
                        } catch (InTransitionException) { /* suppress exception */ }
                        return;
                    }
                }
            }
            try {
                Transition(weighTruck1, NextFromView(weighTruck1), true);
            } catch (InTransitionException) { // suppress exception
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
                weighTruck1.Enabled = true;
            }
        }

        private void weighTruck1_BackClicked(object sender, EventArgs e) {
            try {
                Transition(weighTruck1, BackFromView(weighTruck1), false);
            } catch (InTransitionException) { // suppress exception
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
                weighTruck1.Enabled = true;
            }
        }
        #endregion

        #region GetMaxScreenTime
        internal struct LASTINPUTINFO {
            public uint cbSize;

            public uint dwTime;
        }

        [DllImport("User32.dll")]
        private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

        [DllImport("Kernel32.dll")]
        private static extern uint GetLastError();

        private void tmrMaxScreenTime_Tick(object sender, EventArgs e) {
            tmrMaxScreenTime.Enabled = false;

            if (!tmrTransition.Enabled && Database.Settings.MaxOnScreenTime > 0) {
                int maxOnScreenTime = Database.Settings.MaxOnScreenTime * 1000;
                if (_currentObjectView == null) {
                    // Don't need to do anything
                } else if (_currentObjectView == weighTruck1) {
                    if (DateTime.Now.Subtract(_timeLastScreenShown).TotalMilliseconds > maxOnScreenTime * 10) { // limit to 10 times the max on screen time
                        StartOverClicked(tmrMaxScreenTime, new EventArgs());
                        return;
                    }
                } else if (_currentObjectView == complete1) {
                    if (DateTime.Now.Subtract(_timeLastScreenShown).TotalMilliseconds > maxOnScreenTime) {
                        complete1_OkClicked(complete1, new EventArgs());
                        return;
                    }
                } else {
                    LASTINPUTINFO lastInPut = new LASTINPUTINFO();
                    lastInPut.cbSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(lastInPut);
                    GetLastInputInfo(ref lastInPut);
                    if ((uint)Environment.TickCount - lastInPut.dwTime > maxOnScreenTime && DateTime.Now.Subtract(_timeLastScreenShown).TotalMilliseconds > maxOnScreenTime) {
                        StartOverClicked(tmrMaxScreenTime, new EventArgs());
                        return;
                    }
                }
            }
            tmrMaxScreenTime.Enabled = true;
        }
        #endregion

        private void pbConfig_Click(object sender, EventArgs e) {
            try {
                if (!_currentObjectView.Tag.Equals(EnterConfigurationPasswordTag)) {
                    EnterText control = GetConfigurationPrompt();
                    Transition(_currentObjectView, control, true);
                }
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        private EnterText GetConfigurationPrompt() {
            EnterText control = GetNewEnterTextObject();
            control.Tag = EnterConfigurationPasswordTag;
            control.MinNumberOfCharsRequired = 0;
            if (Database.CurrentUser != null && Database.CurrentUser.Id != Guid.Empty) {
                control.Title = string.Format(Strings.EnterPasswordFor, Database.CurrentUser.Name);
            } else {
                control.Title = Strings.PressOkToContinueToConfiguration;
            }
            control.Text = "";
            control.UsePasswordCharacters = true;
            control.UseAlphaNumericKeypad = true;
            return control;
        }

        private void UseStagedOrder() {
            KaStagedOrder stagedOrder = CurrentPanelStatus.StagedOrder;

            if (stagedOrder.DriverId != Guid.Empty) { // the staged order specifies a driver
                try { // to load the driver from the database...
                    CurrentPanelStatus.Driver = new KaDriver(Tm2Database.Connection, stagedOrder.DriverId);
                } catch (RecordNotFoundException) { // driver specified not found in database
                    CurrentPanelStatus.Driver = null;
                }
            }

            if (stagedOrder.CarrierId != Guid.Empty) { // the staged order specifies a carrier
                try { // to load the carrier from the database...
                    CurrentPanelStatus.Carrier = new KaCarrier(Tm2Database.Connection, stagedOrder.CarrierId);
                } catch (RecordNotFoundException) { // carrier specified not found in database
                    CurrentPanelStatus.Carrier = null;
                }
            }

            List<Guid> transportIds = new List<Guid>();
            foreach (KaStagedOrderTransport soo in stagedOrder.Transports) {
                if (!soo.TransportId.Equals(Guid.Empty) && !transportIds.Contains(soo.TransportId))
                    transportIds.Add(soo.TransportId);
            }

            if (transportIds.Count == 1)
                try { // to load the transport from the database...
                    CurrentPanelStatus.Transport = new KaTransport(Tm2Database.Connection, transportIds[0]);
                } catch (RecordNotFoundException) { // carrier specified not found in database
                    CurrentPanelStatus.Transport = null;
                }

            SetCustomerAccountLocations();

            if (stagedOrder.Orders.Count > 0) {
                foreach (KaStagedOrderOrder soo in stagedOrder.Orders) {
                    KaOrder order = new KaOrder(Tm2Database.Connection, soo.OrderId);
                    if (CurrentPanelStatus.Order == null && order.Number.Contains(CurrentPanelStatus.RefinedOrderNumberFilter)) {
                        CurrentPanelStatus.Order = order;
                    }

                    try { // to load the customer account location from the database...
                        CurrentPanelStatus.CustomerAccountLocation = new KaCustomerAccountLocation(Tm2Database.Connection, soo.CustomerAccountLocationId);
                    } catch (RecordNotFoundException) { // customer account location specified not found in database
                        if (soo.IgnoreOrderShipTo == true && order.ShipToName.Trim().Length > 0 && order.ShipToStreet.Trim().Length > 0 && order.ShipToCity.Trim().Length > 0 && order.ShipToState.Trim().Length > 0 && order.ShipToZipCode.Trim().Length > 0) { // fall back to the location set at the staged order level?
                            try { // to load the customer account location from the database...
                                CurrentPanelStatus.CustomerAccountLocation = new KaCustomerAccountLocation(Tm2Database.Connection, stagedOrder.CustomerAccountLocationId);
                            } catch (RecordNotFoundException) { // customer account location specified not found in database
                                CurrentPanelStatus.CustomerAccountLocation = null;
                            }
                        } else { // use the manually entered ship to
                            CurrentPanelStatus.CustomerAccountLocation = new KaCustomerAccountLocation();
                            CurrentPanelStatus.CustomerAccountLocation.Name = order.ShipToName;
                            CurrentPanelStatus.CustomerAccountLocation.Street = order.ShipToStreet;
                            CurrentPanelStatus.CustomerAccountLocation.City = order.ShipToCity;
                            CurrentPanelStatus.CustomerAccountLocation.State = order.ShipToState;
                            CurrentPanelStatus.CustomerAccountLocation.ZipCode = order.ShipToZipCode;
                        }
                    }
                }
                if (CurrentPanelStatus.Order == null) {
                    foreach (KaStagedOrderOrder soo in stagedOrder.Orders) {
                        try {
                            KaOrder order = new KaOrder(Tm2Database.Connection, soo.OrderId);
                            CurrentPanelStatus.Order = order;
                            break;
                        } catch (RecordNotFoundException) { /* Suppress */ }
                    }
                }
            } else {
                CurrentPanelStatus.Order = new KaOrder(Tm2Database.Connection, CurrentPanelStatus.StagedOrder.OrderId);
                KaCustomerAccountLocation custAcctLocation;
                try {
                    custAcctLocation = new KaCustomerAccountLocation(Tm2Database.Connection, CurrentPanelStatus.StagedOrder.CustomerAccountLocationId);
                } catch (Exception) {
                    custAcctLocation = new KaCustomerAccountLocation();
                }


                CurrentPanelStatus.CustomerAccountLocation = custAcctLocation;
            }
            CheckForInprogressRecord(true);
        }

        private void CheckForStagedOrder(bool useStagedOrderInformation) {
            CurrentPanelStatus.StagedOrders.Clear();
            CurrentPanelStatus.StagedOrder = null;
            Dictionary<Guid, bool> customerValid = new Dictionary<Guid, bool>();
            foreach (KaStagedOrder stagedOrder in KaStagedOrder.GetAll(Tm2Database.Connection, string.Format("(deleted = 0) AND (locked = 0) AND (location_id = {0})", Database.Q(LfDatabase.LocationId)), "last_updated DESC")) {
                Boolean valid = false;

                Boolean orderFound = false;
                if (CurrentPanelStatus.Order != null && stagedOrder.OrderId == CurrentPanelStatus.Order.Id) { //this staged order is specific to this order.  This property is deprecated
                    orderFound = true;
                } else if (CurrentPanelStatus.Order != null && stagedOrder.OrderId.Equals(Guid.Empty)) { // It is using staged order orders
                    foreach (KaStagedOrderOrder soo in stagedOrder.Orders) {
                        if (soo.OrderId == CurrentPanelStatus.Order.Id) {
                            orderFound = true;
                        }
                    }
                }
                if (CurrentPanelStatus.Order == null || orderFound) {
                    if (CurrentPanelStatus.Driver == null || stagedOrder.DriverId == Guid.Empty || stagedOrder.DriverId == CurrentPanelStatus.Driver.Id) { // Driver is valid for this staged order
                        if (CurrentPanelStatus.Carrier == null || stagedOrder.CarrierId == Guid.Empty || stagedOrder.CarrierId == CurrentPanelStatus.Carrier.Id) { // Carrier is valid for this staged order
                            Boolean transportFound = true;
                            if (stagedOrder.Transports.Count > 0) {
                                if (CurrentPanelStatus.Transport != null) {
                                    Boolean found = false;
                                    foreach (KaStagedOrderTransport sot in stagedOrder.Transports) {
                                        if (sot.TransportId == CurrentPanelStatus.Transport.Id || sot.TransportId.Equals(Guid.Empty)) {
                                            found = true;
                                        }
                                    }
                                    if (!found) {
                                        transportFound = false;
                                    }
                                }
                            }
                            if (CurrentPanelStatus.Transport == null || transportFound) {
                                valid = true;
                            }
                        }
                    }

                    if (valid) {
                        //Check that each order does not have any disabled accounts, and that the accounts are valid for this facility
                        foreach (KaStagedOrderOrder soo in stagedOrder.Orders) {
                            KaOrder order = new KaOrder(Tm2Database.Connection, soo.OrderId);
                            if ((CurrentPanelStatus.IsInbound && order.Completed) || order.Deleted || (Database.Settings.EnforceOrderLocking && order.Locked)) {
                                valid = false;
                                break;
                            }
                            foreach (KaOrderCustomerAccount oca in order.OrderAccounts) {
                                if (!customerValid.ContainsKey(oca.CustomerAccountId)) {
                                    customerValid.Add(oca.CustomerAccountId, true);
                                    KaCustomerAccount account = new KaCustomerAccount(Tm2Database.Connection, oca.CustomerAccountId);
                                    if (account.Disabled) {
                                        customerValid[oca.CustomerAccountId] = false;
                                    }
                                }
                                if (!customerValid[oca.CustomerAccountId]) {
                                    valid = false;
                                    break; //Found a disabled customer account, no need to go on any further
                                }
                            }
                        }
                    }
                }


                if (valid) {
                    //If we are still valid after all of that, add it to the stagedOrders list.
                    CurrentPanelStatus.StagedOrders.Add(stagedOrder);
                }
            }

            if (useStagedOrderInformation && CurrentPanelStatus.StagedOrders.Count == 1) {
                CurrentPanelStatus.StagedOrder = CurrentPanelStatus.StagedOrders[0];
                UseStagedOrder();
            }
        }

        private List<KaInProgress> CheckForInprogressRecord(bool assignInProgressRecord) {
            CurrentPanelStatus.InProgress = null;
            List<KaInProgress> inProgressRecords;
            string whereClause = "";

            if (CurrentPanelStatus.StagedOrder != null) {
                whereClause = $"{KaInProgress.FN_STAGED_ORDER_ID} = {Database.Q(CurrentPanelStatus.StagedOrder.Id)} AND {KaInProgress.FN_ORDER_TYPE}={Database.Q(KaInProgress.OrderTypes.StagedOrder)}";
            } else if (CurrentPanelStatus.ReceivingPurchaseOrder != null) {
                whereClause = $"{KaInProgress.FN_REC_PURCH_ORDER_ID} = {Database.Q(CurrentPanelStatus.ReceivingPurchaseOrder.Id)} AND {KaInProgress.FN_ORDER_TYPE}={Database.Q(KaInProgress.OrderTypes.ReceivingPurchaseOrder)}";
            }

            if (whereClause.Length > 0) {
                if (CurrentPanelStatus.Driver != null)
                    whereClause += $" AND (driver_id = {Database.Q(Guid.Empty)} OR driver_id = {Database.Q(CurrentPanelStatus.Driver.Id)})";
                if (CurrentPanelStatus.Carrier != null)
                    whereClause += $" AND (carrier_id = {Database.Q(Guid.Empty)} OR carrier_id = {Database.Q(CurrentPanelStatus.Carrier.Id)})";
                if (CurrentPanelStatus.Transport != null)
                    whereClause += $" AND (id IN (SELECT in_progress_id FROM in_progress_weighments WHERE (transport_id = {Database.Q(CurrentPanelStatus.Transport.Id)} OR transport_id = {Database.Q(Guid.Empty)})))";
                inProgressRecords = KaInProgress.GetAll(Tm2Database.Connection, whereClause, "").Cast<KaInProgress>().ToList();
            } else
                inProgressRecords = new List<KaInProgress>();
            if (assignInProgressRecord && inProgressRecords.Count == 1) {
                CurrentPanelStatus.InProgress = (KaInProgress)inProgressRecords[0];
                UseInProgressData();
            }
            return inProgressRecords;
        }

        private void UseInProgressData() {
            if (CurrentPanelStatus.Order == null) {
                foreach (KaInProgressOrder ipo in CurrentPanelStatus.InProgress.Orders) {
                    try {
                        CurrentPanelStatus.Order = new KaOrder(Tm2Database.Connection, ipo.OrderId);
                        break;
                    } catch (RecordNotFoundException) { /* Suppress */ }
                }
            }
            if (CurrentPanelStatus.Order == null && !CurrentPanelStatus.InProgress.OrderId.Equals(Guid.Empty)) {
                try {
                    CurrentPanelStatus.Order = new KaOrder(Tm2Database.Connection, CurrentPanelStatus.InProgress.OrderId);
                } catch (RecordNotFoundException) { /* Suppress */ }
            }
            if (CurrentPanelStatus.ReceivingPurchaseOrder == null && !CurrentPanelStatus.InProgress.ReceivingPurchaseOrderId.Equals(Guid.Empty)) {
                try {
                    CurrentPanelStatus.ReceivingPurchaseOrder = new KaReceivingPurchaseOrder(Tm2Database.Connection, CurrentPanelStatus.InProgress.ReceivingPurchaseOrderId);
                } catch (RecordNotFoundException) { /* Suppress */ }
            }
            if (CurrentPanelStatus.Carrier == null && !CurrentPanelStatus.InProgress.CarrierId.Equals(Guid.Empty)) {
                try {
                    CurrentPanelStatus.Carrier = new KaCarrier(Tm2Database.Connection, CurrentPanelStatus.InProgress.CarrierId);
                } catch (RecordNotFoundException) { /* suppress */ }
            }
            if (CurrentPanelStatus.Driver == null && !CurrentPanelStatus.InProgress.DriverId.Equals(Guid.Empty)) {
                try {
                    CurrentPanelStatus.Driver = new KaDriver(Tm2Database.Connection, CurrentPanelStatus.InProgress.DriverId);
                } catch (RecordNotFoundException) { /* suppress */ }
            }
            if (CurrentPanelStatus.Transport == null) {
                for (int i = CurrentPanelStatus.InProgress.Weighments.Count - 1; i >= 0; i--) {// work from the last weighment backwards to set the last transport used
                    KaInProgressWeighment weighment = CurrentPanelStatus.InProgress.Weighments[i];
                    if (!weighment.TransportId.Equals(Guid.Empty)) {
                        try {
                            CurrentPanelStatus.Transport = new KaTransport(Tm2Database.Connection, weighment.TransportId);
                            break;
                        } catch (RecordNotFoundException) { /* suppress */ }
                    }
                }
            }
        }
        private void SetCustomerAccountLocations() {
            CurrentPanelStatus.CustomerAccountLocations = new List<KaCustomerAccountLocation>();
            List<KaOrder> orders = new List<KaOrder>();
            List<Guid> customerAccountIds = new List<Guid>();
            bool displayMaplewoodLocation = true;
            if (CurrentPanelStatus.StagedOrder != null) {
                foreach (KaStagedOrderOrder soo in CurrentPanelStatus.StagedOrder.Orders) {
                    KaOrder order = new KaOrder(Tm2Database.Connection, soo.OrderId);
                    if (order.ShipToName.Trim().Length > 0 && order.ShipToStreet.Trim().Length > 0 && order.ShipToCity.Trim().Length > 0 && order.ShipToState.Trim().Length > 0 && order.ShipToZipCode.Trim().Length > 0) {
                        KaCustomerAccountLocation cal = new KaCustomerAccountLocation();
                        cal.Name = order.ShipToName;
                        cal.Street = order.ShipToStreet;
                        cal.City = order.ShipToCity;
                        cal.State = order.ShipToState;
                        cal.ZipCode = order.ShipToZipCode;
                        if (!CurrentPanelStatus.CustomerAccountLocations.Contains(cal))  //Determine if we need to add to the list
                        {
                            CurrentPanelStatus.CustomerAccountLocations.Add(cal);
                        }
                    }
                    if (Database.Settings.OwnersIgnoreMaplewoodShipTo.Contains(order.OwnerId)) displayMaplewoodLocation = false;
                }
            } else if (CurrentPanelStatus.Order != null) {
                orders.Add(CurrentPanelStatus.Order);
                if (Database.Settings.OwnersIgnoreMaplewoodShipTo.Contains(CurrentPanelStatus.Order.OwnerId)) displayMaplewoodLocation = false;
            }
            Dictionary<Guid, KaCustomerAccountLocation> tempList = KaOrder.GetCustomerAccountLocationsForOrders(Tm2Database.Connection, null, orders);
            foreach (Guid locId in tempList.Keys) {
                KaCustomerAccountLocation cal = tempList[locId];
                if (!CurrentPanelStatus.CustomerAccountLocations.Contains(cal) && (displayMaplewoodLocation || cal.City.ToLower() != "maplewood" || cal.State.ToLower() != "mn")) //Determine if we need to add to the list
                    CurrentPanelStatus.CustomerAccountLocations.Add(cal);
            }
            string conditions = $"deleted = 0 AND {KaCustomerAccountLocation.FN_CUSTOMER_ACCOUNT_ID} IN ({  Database.Q(Guid.Empty)}";
            foreach (KaOrder order in orders) {
                foreach (KaOrderCustomerAccount oca in order.OrderAccounts) {
                    if (!customerAccountIds.Contains(oca.CustomerAccountId)) customerAccountIds.Add(oca.CustomerAccountId);
                }
            }
            foreach (Guid custId in customerAccountIds) {
                conditions += "," + Database.Q(custId);
            }
            conditions += ")";
            foreach (KaCustomerAccountLocation cal in KaCustomerAccountLocation.GetAll(Tm2Database.Connection, conditions, KaCustomerAccountLocation.FN_NAME)) {
                if (!CurrentPanelStatus.CustomerAccountLocations.Contains(cal) && (displayMaplewoodLocation || cal.City.ToLower() != "maplewood" || cal.State.ToLower() != "mn"))  //Determine if we need to add to the list
                    CurrentPanelStatus.CustomerAccountLocations.Add(cal);
            }
        }

        private SelectFromList SelectInboundOutbound() {
            SelectFromList control;
            if (_currentObjectView != null && (string)_currentObjectView.Tag == SelectInboundOutboundTag)
                control = (SelectFromList)_currentObjectView;
            else {
                control = GetNewSelectFromListObject();
                ArrayList columns = new ArrayList();
                control.Tag = SelectInboundOutboundTag;
                control.Title = Strings.InboundOrOutbound;
                control.ShowBackButton = false;
                control.ShowNewOption = false;
                control.ShowDontCare = false;
                columns.Add(new ArrayList(new String[] { "Direction", (ClientSize.Width - 50).ToString(), "TextToDisplay" }));
                control.ClearColumns();
                control.SetColumns(columns, "TextToDisplay");
                selectFromList_RefreshClicked(control, null); // force the list to load
            }
            return control;
        }

        private SelectFromList SelectOrder() {
            SelectFromList control = GetNewSelectFromListObject();
            ArrayList columns = new ArrayList();
            control.Tag = SelectOrderTag;
            control.Title = Strings.SelectOrder;
            List<KeyValuePair<string, string>> columnstoDisplay = new List<KeyValuePair<string, string>>();
            if (CurrentPanelStatus.IsInbound) {
                columnstoDisplay.Add(new KeyValuePair<string, string>("Number", Strings.OrderNumber));
                if (Database.Settings.IncludeOrderReleaseNumberInLookupForValidOrders) columnstoDisplay.Add(new KeyValuePair<string, string>("ReleaseNumber", Strings.ReleaseNumber));
                if (Database.Settings.IncludeOrderPurchaseOrderNumberInLookupForValidOrders) columnstoDisplay.Add(new KeyValuePair<string, string>("PoNumber", Strings.PurchaseOrderNumber));
                columnstoDisplay.Add(new KeyValuePair<string, string>("OrderType", Strings.OrderType));
                columnstoDisplay.Add(new KeyValuePair<string, string>("Owner", Strings.Owner));
                columnstoDisplay.Add(new KeyValuePair<string, string>("Account", Strings.Account));
                columnstoDisplay.Add(new KeyValuePair<string, string>("Product", Strings.Product));
            } else {
                columnstoDisplay.Add(new KeyValuePair<string, string>("Number", Strings.OrderNumber));
                if (Database.Settings.IncludeOrderReleaseNumberInLookupForValidOrders) columnstoDisplay.Add(new KeyValuePair<string, string>("ReleaseNumber", Strings.ReleaseNumber));
                if (Database.Settings.IncludeOrderPurchaseOrderNumberInLookupForValidOrders) columnstoDisplay.Add(new KeyValuePair<string, string>("PoNumber", Strings.PurchaseOrderNumber));
                columnstoDisplay.Add(new KeyValuePair<string, string>("Owner", Strings.Owner));
                columnstoDisplay.Add(new KeyValuePair<string, string>("EnteredAt", Strings.EnteredAt));
                columnstoDisplay.Add(new KeyValuePair<string, string>("Account", Strings.Account));
                columnstoDisplay.Add(new KeyValuePair<string, string>("Product", Strings.Product));
                columnstoDisplay.Add(new KeyValuePair<string, string>("Transport", Strings.Transport));
                columnstoDisplay.Add(new KeyValuePair<string, string>("Carrier", Strings.Carrier));
                columnstoDisplay.Add(new KeyValuePair<string, string>("Driver", Strings.Driver));
            }

            foreach (KeyValuePair<string, string> column in columnstoDisplay) {
                columns.Add(new ArrayList(new String[] { column.Value, ((ClientSize.Width - 50) / columnstoDisplay.Count).ToString(), column.Key }));
            }

            control.ShowBackButton = true;
            control.ShowNewOption = false;
            control.ShowDontCare = false;
            control.ClearColumns();
            control.SetColumns(columns, "Id");
            selectFromList_RefreshClicked(control, null); // force the list to load   
            return control;
        }

        private SelectFromList SelectStagedOrder() {
            SelectFromList control = GetNewSelectFromListObject();
            ArrayList columns = new ArrayList();
            control.Tag = SelectStagedOrderTag;
            int columnWidth = (ClientSize.Width - 50) / 4;
            control.Title = Strings.SelectStagedOrder;
            columns.Add(new ArrayList(new String[] { Strings.OrderNumber, columnWidth.ToString(), "OrderNumber" }));
            columns.Add(new ArrayList(new String[] { Strings.Account, columnWidth.ToString(), "Accounts" }));
            columns.Add(new ArrayList(new String[] { Strings.Transport, columnWidth.ToString(), "Transport" }));
            columns.Add(new ArrayList(new String[] { Strings.StagedAt, columnWidth.ToString(), "StagedAt" }));

            control.ShowBackButton = true;
            control.ShowNewOption = false;
            control.ShowDontCare = false;
            control.ClearColumns();
            control.SetColumns(columns, "Id");
            selectFromList_RefreshClicked(control, null); // force the list to load
            return control;
        }

        private SelectFromList SelectCustomerAccountLocation() {
            SelectFromList control = GetNewSelectFromListObject();
            control.Tag = SelectCustomerAccountLocationTag;
            ArrayList columns = new ArrayList();
            int columnCount = 2;
            int columnWidth = (ClientSize.Width - 50) / columnCount;
            control.Title = Strings.SelectDestination;
            columns.Add(new ArrayList(new String[] { Strings.Destination, columnWidth.ToString(), "Name" }));
            columns.Add(new ArrayList(new String[] { Strings.Address, columnWidth.ToString(), "Address" }));
            control.DontCareText = "None";
            control.NewText = "New";

            control.UseGuids = false;
            control.ShowBackButton = true;
            control.ShowNewOption = Database.Settings.AllowDriversToAddDestinations;
            control.ShowDontCare = Database.Settings.ShowDontCareInDestinationList;
            control.ClearColumns();
            control.SetColumns(columns, "Id");
            selectFromList_RefreshClicked(control, null); // force the list to load
            return control;
        }

        private Control SelectPrestageCompleteProduct() {
            SelectFromList control = GetNewSelectFromListObject();
            control.Tag = SelectPrestageCompleteProductTag;
            ArrayList columns = new ArrayList();
            int columnCount = 2;
            int columnWidth = (ClientSize.Width - 50) / columnCount;
            control.Title = Strings.SelectProductLoaded;
            columns.Add(new ArrayList(new String[] { Strings.BulkProduct, columnWidth.ToString(), "BulkProduct" }));
            columns.Add(new ArrayList(new String[] { Strings.Product, columnWidth.ToString(), "Product" }));
            columns.Add(new ArrayList(new String[] { Strings.OrderNumber, columnWidth.ToString(), "OrderNumber" }));

            control.UseGuids = false;
            control.ShowBackButton = true;
            control.ShowNewOption = false;
            control.ShowDontCare = false;
            control.ClearColumns();
            control.SetColumns(columns, "Xml");
            selectFromList_RefreshClicked(control, null); // force the list to load
            if (control.Data.Count == 1) {
                SelectFromListItems.LoadedBulkProductListItem onlyItem = (SelectFromListItems.LoadedBulkProductListItem)control.Data[0];
                CurrentPanelStatus.BulkProductId = onlyItem.BulkProductId;
                CurrentPanelStatus.ProductId = onlyItem.ProductId;
                CurrentPanelStatus.OrderItemId = onlyItem.OrderItemId;
                RemoveOutgoingScreenControls(control);
                return null;
            } else if (control.Data.Count == 0) {
                RemoveOutgoingScreenControls(control);
                messageView1.Tag = BackFromView(control).Tag; //Get the previous screen, for proper return from the MessageView screen.
                messageView1.OtherButton.Visible = false;
                messageView1.SetColors(Color.Red, Color.White);
                messageView1.Message = Strings.NoProductsAvailableForOrder;
                return messageView1;
            } else
                return control;
        }

        public object CreateTicket(OleDbConnection updateConnection, OleDbTransaction updateTransaction) {
            string applicationIdentifier = Database.ApplicationIdentifier;
            string applicationUsername = Database.ApplicationUsername;
            if (CurrentPanelStatus.StagedOrder != null) {
                foreach (KaStagedOrderCustomLoadQuestionData questionData in CurrentPanelStatus.StagedOrder.CustomLoadQuestionDatas) {
                    if (!questionData.Deleted) {
                        bool dataFound = false;
                        try {
                            KaCustomLoadQuestionData stagedData = new KahlerAutomation.KaTm2Database.KaCustomLoadQuestionData(updateConnection, questionData.CustomLoadQuestionsDataId, updateTransaction);
                            foreach (KaInProgressCustomLoadQuestionData progData in CurrentPanelStatus.InProgress.CustomLoadQuestionDatas) {
                                KaCustomLoadQuestionData inProgData = new KahlerAutomation.KaTm2Database.KaCustomLoadQuestionData(updateConnection, questionData.CustomLoadQuestionsDataId, updateTransaction);
                                if (stagedData.CustomLoadQuestionFieldsId.Equals(inProgData.CustomLoadQuestionFieldsId))
                                    dataFound = true;

                            }
                            if (!dataFound) {
                                KaInProgressCustomLoadQuestionData newInProgressCustomLoadQuestionData = new KaInProgressCustomLoadQuestionData() { CustomLoadQuestionDataId = questionData.CustomLoadQuestionsDataId, InProgressId = CurrentPanelStatus.InProgress.Id };
                                newInProgressCustomLoadQuestionData.SqlUpdateInsertIfNotFound(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                                CurrentPanelStatus.InProgress.CustomLoadQuestionDatas.Add(newInProgressCustomLoadQuestionData);
                            }
                        } catch (RecordNotFoundException) { /* Suppress */ }
                    }
                }
            }

            object ticket = null;
            try {
                int retries = Tm2Database.QueryRetries;
                do { // loop unit successful or out of retries
                    try { // to create ticket(s)...
                        ticket = CurrentPanelStatus.InProgress.CreateTicket(updateConnection, updateTransaction);
                        if (CurrentPanelStatus.StagedOrder != null) {
                            CurrentPanelStatus.StagedOrder.Deleted = true;
                            CurrentPanelStatus.StagedOrder.SqlUpdate(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                        }

                        if (!Database.Settings.EmailTickets) { // mark the tickets so that they are not e-mailed
                            if (ticket.GetType() == typeof(KaTicket)) { // one ticket
                                if (!((KaTicket)ticket).DoNotEmail) {
                                    ((KaTicket)ticket).DoNotEmail = true;
                                    ((KaTicket)ticket).SqlUpdate(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                                }
                            } else if (ticket.GetType() == typeof(List<KaTicket>)) { // multiple tickets
                                foreach (KaTicket t in (List<KaTicket>)ticket) {
                                    if (!t.DoNotEmail) {
                                        t.DoNotEmail = true;
                                        t.SqlUpdate(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                                    }
                                }
                            } else if (ticket.GetType() == typeof(KaReceivingTicket)) { // one ticket
                                if (!((KaReceivingTicket)ticket).DoNotEmail) {
                                    ((KaReceivingTicket)ticket).DoNotEmail = true;
                                    ((KaReceivingTicket)ticket).SqlUpdate(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                                }
                            } else if (ticket.GetType() == typeof(List<KaReceivingTicket>)) { // multiple tickets
                                foreach (KaReceivingTicket t in (List<KaReceivingTicket>)ticket) {
                                    if (!t.DoNotEmail) {
                                        t.DoNotEmail = true;
                                        t.SqlUpdate(updateConnection, updateTransaction, applicationIdentifier, applicationUsername); ;
                                    }
                                }
                            }
                        }


                        if (Database.Settings.MoveDriversToHistory && !CurrentPanelStatus.InProgress.DriverReferencedInOtherProgressRecords(updateConnection, updateTransaction, CurrentPanelStatus.InProgress.LocationId)) { // mark the driver as having exited the facility
                            string conditions = String.Format("deleted=0 AND in_facility = 1 AND driver_id={0}", Database.Q(CurrentPanelStatus.InProgress.DriverId));
                            foreach (KaDriverInFacility dif in KaDriverInFacility.GetAll(updateConnection, conditions, "", updateTransaction)) {
                                if (dif.LocationId == Guid.Empty || dif.LocationId == CurrentPanelStatus.InProgress.LocationId) {
                                    dif.InFacility = false;
                                    dif.ExitedAt = DateTime.Now;
                                    dif.SqlUpdate(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                                }
                            }
                        }

                        if (Database.Settings.MoveTransportsToHistory) { // mark the transport as having exited the facility
                            List<Guid> transportIds = new List<Guid>();
                            foreach (KaInProgressWeighment weighment in CurrentPanelStatus.InProgress.Weighments) {
                                if (weighment.TransportId != Guid.Empty && !transportIds.Contains(weighment.TransportId)) transportIds.Add(weighment.TransportId);
                            }
                            foreach (Guid transportId in transportIds) {
                                if (!CurrentPanelStatus.InProgress.TrasnportReferencedInOtherProgressRecords(updateConnection, updateTransaction, transportId, CurrentPanelStatus.InProgress.LocationId)) {
                                    string conditions = String.Format("deleted=0 AND in_facility = 1 AND transport_id={0}", Database.Q(transportId));
                                    foreach (KaTransportInFacility tif in KaTransportInFacility.GetAll(updateConnection, conditions, "", updateTransaction)) {
                                        if (tif.LocationId == Guid.Empty || tif.LocationId == CurrentPanelStatus.InProgress.LocationId) {
                                            tif.InFacility = false;
                                            tif.ExitedAt = DateTime.Now;
                                            tif.SqlUpdate(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                                        }
                                    }
                                }
                            }
                        }

                        break;
                    } catch (Exception ex) { // can we retry?
                        retries--;
                        if (retries <= 0 || !Tm2Database.IsDatabaseConnectivityOrTimeoutError(ex)) throw;
                    }
                } while (retries > 0);
                if (ticket != null) {
                    if (ticket.GetType() == typeof(KaTicket)) { // one ticket
                        Printing.TicketsToPrint.Enqueue((KaTicket)ticket);
                    } else if (ticket.GetType() == typeof(List<KaTicket>)) { // multiple tickets
                        foreach (KaTicket t in (List<KaTicket>)ticket) { Printing.TicketsToPrint.Enqueue(t); }
                    } else if (ticket.GetType() == typeof(KaReceivingTicket)) { // one ticket
                        Printing.ReceivingTicketsToPrint.Enqueue((KaReceivingTicket)ticket);
                    } else if (ticket.GetType() == typeof(List<KaReceivingTicket>)) { // multiple tickets
                        foreach (KaReceivingTicket t in (List<KaReceivingTicket>)ticket) { Printing.ReceivingTicketsToPrint.Enqueue(t); }
                    }

                    Printing.LastTicket = ticket; // keep track of last ticket printed for reprint button
                }
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
            return ticket;
        }

        private void SetUpViewChain() {
            _viewChain.Clear();
            Guid ownerId = Guid.Empty;
            if (CurrentPanelStatus.Order != null)
                ownerId = CurrentPanelStatus.Order.OwnerId;
            else if (CurrentPanelStatus.ReceivingPurchaseOrder != null)
                ownerId = CurrentPanelStatus.ReceivingPurchaseOrder.OwnerId;

            _viewChain.Add(SelectInboundOutboundTag);

            if ((CurrentPanelStatus.IsInbound && !Database.Settings.Inbound.AllowOrderListLookup) || (!CurrentPanelStatus.IsInbound && !Database.Settings.Outbound.AllowOrderListLookup)) {
                _viewChain.Add(EnterOrderNumberTag);
            }

            _viewChain.Add(SelectOrderTag);

            if ((CurrentPanelStatus.IsInbound && Database.Settings.Inbound.PromptForTransportNumber) || (!CurrentPanelStatus.IsInbound && Database.Settings.Outbound.PromptForTransportNumber)) {
                _viewChain.Add(EnterTransportNumberTag);
            }

            if (CurrentPanelStatus.ReceivingPurchaseOrder == null &&
                    ((CurrentPanelStatus.IsInbound && Database.Settings.Inbound.PromptForDriverNumber) || (!CurrentPanelStatus.IsInbound && Database.Settings.Outbound.PromptForDriverNumber))) {
                _viewChain.Add(EnterDriverNumberTag);
            }


            if (CurrentPanelStatus.ReceivingPurchaseOrder == null &&
                    ((CurrentPanelStatus.IsInbound && Database.Settings.Inbound.PromptForCarrierNumber) ||
                    (!CurrentPanelStatus.IsInbound && Database.Settings.Outbound.PromptForCarrierNumber))) {
                _viewChain.Add(EnterCarrierNumberTag);
            }

            if (Database.Settings.ShowOrderDetailScreen) {
                if (CurrentPanelStatus.Order != null) {
                    _viewChain.Add(ShowOrderDetailsTag);
                } else if (CurrentPanelStatus.ReceivingPurchaseOrder != null) {
                    _viewChain.Add(ShowReceivingOrderDetailsTag);
                }
            }

            if (!CurrentPanelStatus.IsInbound && CurrentPanelStatus.StagedOrders.Count > 1) {
                _viewChain.Add(SelectStagedOrderTag);
            }

            if (!CurrentPanelStatus.IsInbound && CurrentPanelStatus.StagedOrder != null && !Database.IsPalletedLoad(CurrentPanelStatus.StagedOrder)) {
                _viewChain.Add(SelectPrestageCompleteProductTag);
            }

            if (CurrentPanelStatus.IsInbound && Database.Settings.PromptForShipToDestinationOwners.Contains(ownerId)) {
                _viewChain.Add(SelectCustomerAccountLocationTag);
            }

            _viewChain.Add(VerifyCurrentTruckWeightTag);

            if (CurrentPanelStatus.IsInbound && CurrentPanelStatus.Order != null)
                _viewChain.Add(SetupStagedOrderTag);

            if (CurrentPanelStatus.ReceivingPurchaseOrder == null && CurrentPanelStatus.IsInbound && Database.RequireRestrictedUseOwners.Contains(ownerId)) {
                _viewChain.Add(RequireRestrictedUseTag);
                _viewChain.Add(PreviousLoadProductTag);
            }

            if (Database.Settings.ShowFinalConfirmationScreen) {
                _viewChain.Add(ShowFinalConfirmationScreenTag);
            }

            _viewChain.Add(SaveDataTag);
            _viewChain.Add(ShowCompleteTag);
        }

        #region scale
        private void tmrUpdateScale_Tick(object sender, EventArgs e) {
            if (tmrUpdateScale.Enabled) {
                tmrUpdateScale.Enabled = false;
                if (_scaleObject == null || ((_scaleObject.CommType == ScaleComm.ScaleCommType.Serial && !_scaleObject.CommPort.IsOpen) ||
                ((_scaleObject.CommType == ScaleComm.ScaleCommType.TCPIP || _scaleObject.CommType == ScaleComm.ScaleCommType.Ka2000) && DateTime.Now.Subtract(_scaleObject.LastValueReceivedTime).TotalSeconds > 1))) {
                    try {
                        ConnectToScale();
                    } catch (System.UnauthorizedAccessException) {
                    }
                }
                tmrUpdateScale.Enabled = true;
            }
        }
        private void ConnectToScale() {
            if (_scaleObject == null) _scaleObject = new ScaleComm();
            _scaleObject.Close();
            //Read Scale
            try {
                KaPanel scalePanel = new KaPanel(Tm2Database.Connection, Database.Settings.Scale.PanelId);
                switch (scalePanel.ConnectionType) {
                    case KaPanel.PanelConnectionType.ModbusTcp:
                    case KaPanel.PanelConnectionType.Ethernet:
                        _scaleObject.IpAddress = scalePanel.IpAddress;
                        _scaleObject.ScaleIpAddressPort = scalePanel.TcpPort;
                        _scaleObject.CommType = Database.Settings.Scale.CommType;
                        break;
                    case KaPanel.PanelConnectionType.SerialPort:
                        _scaleObject.CommType = ScaleComm.ScaleCommType.Serial;
                        _scaleObject.PortName = $"COM{ scalePanel.SerialPort}";
                        _scaleObject.BaudRate = scalePanel.BaudRate;
                        _scaleObject.DataBits = scalePanel.DataBits;
                        _scaleObject.Parity = scalePanel.Parity;
                        _scaleObject.StopBit = scalePanel.StopBits;
                        _scaleObject.StreamDataType = Database.Settings.Scale.ScaleDataStreamType;
                        break;
                    case KaPanel.PanelConnectionType.Emulate:
                    default:
                        throw new RecordNotFoundException("Scale communication type not defined");
                }

                _scaleObject.NotifyOnDataReceived(weighTruck1.NewWeightReceived);
                _scaleObject.NotifyOnDataReceived(NewWeightReceived);
                _scaleObject.Open();
            } catch (RecordNotFoundException) {
            }
        }

        public void NewWeightReceived(double weight, KaUnit.Unit unit, string mode, string status, bool weightValid) {
            if (weighTruck1.Visible && status.Trim() != "")// if scale is in motion, do not start the max screen timer...
                _timeLastScreenShown = DateTime.Now;
        }

        private void DisconnectFromScale() {
            if (_scaleObject != null) _scaleObject.Close();
        }
        #endregion

        #region BaseUnitCrossReference
        static public Guid GetBaseUnitCrossReference(KaUnit.Unit unit) {
            if (_baseUnitCrossReference == null) {
                _baseUnitCrossReference = new Dictionary<KaUnit.Unit, Guid>();
                OleDbConnection connection = Tm2Database.Connection;
                AddBaseUnitToCrossRefernce(KaUnit.Unit.CubicFeet);
                AddBaseUnitToCrossRefernce(KaUnit.Unit.FluidOunces);
                AddBaseUnitToCrossRefernce(KaUnit.Unit.Gallons);
                AddBaseUnitToCrossRefernce(KaUnit.Unit.Kilograms);
                AddBaseUnitToCrossRefernce(KaUnit.Unit.Liters);
                AddBaseUnitToCrossRefernce(KaUnit.Unit.MetricTon);
                AddBaseUnitToCrossRefernce(KaUnit.Unit.Ounces);
                AddBaseUnitToCrossRefernce(KaUnit.Unit.Pints);
                AddBaseUnitToCrossRefernce(KaUnit.Unit.Pounds);
                AddBaseUnitToCrossRefernce(KaUnit.Unit.Quarts);
                AddBaseUnitToCrossRefernce(KaUnit.Unit.Tons);
            }
            if (_baseUnitCrossReference.ContainsKey(unit))
                return _baseUnitCrossReference[unit];
            else
                return Guid.Empty;
        }

        static private void AddBaseUnitToCrossRefernce(KaUnit.Unit unit) {
            if (!_baseUnitCrossReference.ContainsKey(unit))
                _baseUnitCrossReference.Add(unit, KaUnit.GetUnitIdForBaseUnit(Tm2Database.Connection, unit));
        }
        #endregion

        private void GetOrdersThatMatchNumber(string orderNumberEntered, out List<KaOrder> validOrders, out List<KaReceivingPurchaseOrder> validReceivingOrders, out List<Guid> lockedOrders, out Dictionary<Guid, Guid> disabledOrderAccounts) {
            lockedOrders = new List<Guid>();
            disabledOrderAccounts = new Dictionary<Guid, Guid>();
            validOrders = new List<KaOrder>();
            validReceivingOrders = new List<KaReceivingPurchaseOrder>();
            List<KaOrder> orders = new List<KaOrder>();
            List<KaReceivingPurchaseOrder> receivingOrders = new List<KaReceivingPurchaseOrder>();
            if (orderNumberEntered.Length == 0)
                orders = KaOrder.GetAll(Tm2Database.Connection, "deleted = 0" + (CurrentPanelStatus.IsInbound ? " AND completed = 0" : ""), "number ASC").Cast<KaOrder>().ToList();
            else
                orders = KaOrder.GetOrdersThatMatchNumber(Tm2Database.Connection, null, orderNumberEntered, !Database.Settings.IncludeOrderNumbersThatHaveDashesInTheNumberInLookupForValidOrders, true, Database.Settings.IncludeOrderPurchaseOrderNumberInLookupForValidOrders, Database.Settings.IncludeOrderReleaseNumberInLookupForValidOrders, true);
            if (orders.Count > 0) { // at least one order matched the entered order number
                foreach (KaOrder o in orders) {
                    if (CurrentPanelStatus.IsInbound) {
                        if ((!Database.Settings.Inbound.AllowMultipleStagedOrders && OrderUsed(o.Id)) || (Database.Settings.EnforceOrderLocking && o.Locked)) {//Ensure order is not locked
                            lockedOrders.Add(o.Id);
                        } else {
                            //Ensure that all accounts are not disabled for order
                            foreach (KaOrderCustomerAccount oca in o.OrderAccounts) {
                                KaCustomerAccount account = new KaCustomerAccount(Tm2Database.Connection, oca.CustomerAccountId);
                                if (account.Disabled) {
                                    disabledOrderAccounts.Add(o.Id, account.Id);
                                    continue;
                                }
                            }

                            if (disabledOrderAccounts != null && disabledOrderAccounts.ContainsKey(o.Id)) {
                                //An account is not able to load at this facility or account is disabled on the order
                                continue;
                            } else
                                validOrders.Add(o);
                        }
                    } else { //Outbound
                        OleDbDataReader rdr = Tm2Database.ExecuteReader(Tm2Database.Connection, $"SELECT DISTINCT so.id FROM staged_orders AS so INNER JOIN staged_order_orders AS soo ON soo.staged_order_id = so.id WHERE (so.deleted = 0) AND (soo.deleted = 0) AND (so.complete = 0) AND (so.order_id = {Database.Q(o.Id)}) OR (soo.order_id = {Database.Q(o.Id)})");
                        if (rdr.Read()) validOrders.Add(o); // There is a staged order for this order.
                    }
                }
            }

            if (orderNumberEntered.Length == 0)
                receivingOrders = KaReceivingPurchaseOrder.GetAll(Tm2Database.Connection, "deleted = 0" + (CurrentPanelStatus.IsInbound ? " AND completed = 0" : ""), "number ASC").Cast<KaReceivingPurchaseOrder>().ToList();
            else
                receivingOrders = KaReceivingPurchaseOrder.GetOrdersThatMatchNumber(Tm2Database.Connection, null, orderNumberEntered, !Database.Settings.IncludeOrderNumbersThatHaveDashesInTheNumberInLookupForValidOrders);
            if (receivingOrders.Count > 0) { // at least one order matched the entered order number
                foreach (KaReceivingPurchaseOrder po in receivingOrders) {
                    //Ensure that all accounts are not disabled for order
                    if (CurrentPanelStatus.IsInbound) {
                        KaSupplierAccount account = new KaSupplierAccount(Tm2Database.Connection, po.SupplierAccountId);
                        if (account.Disabled) {
                            disabledOrderAccounts.Add(po.Id, account.Id);
                            continue;
                        }

                        if (disabledOrderAccounts != null && disabledOrderAccounts.ContainsKey(po.Id)) {
                            //An account is not able to load at this facility or account is disabled on the order
                            continue;
                        } else
                            validReceivingOrders.Add(po);
                    } else { //Outbound
                        OleDbDataReader rdr = Tm2Database.ExecuteReader(Tm2Database.Connection, $"SELECT * FROM {KaInProgress.TABLE_NAME} WHERE {KaInProgress.FN_REC_PURCH_ORDER_ID} = ({Database.Q(po.Id)})");
                        if (rdr.Read()) validReceivingOrders.Add(po); // There is a staged order for this order.
                    }
                }
            }
        }

        private bool OrderUsed(Guid orderId) {
            bool used = false;
            try {
                OleDbDataReader rdr = Tm2Database.ExecuteReader(Tm2Database.Connection, "SELECT staged_orders.id " +
                            "FROM staged_order_orders " +
                            "INNER JOIN staged_orders ON staged_orders.id = staged_order_orders.staged_order_id " +
                            "INNER JOIN orders ON orders.id = staged_order_orders.order_id " +
                            "WHERE (staged_order_orders.deleted = 0) " +
                                "AND (staged_orders.deleted = 0) " +
                                "AND (orders.locked = 0) " +
                                "AND (staged_orders.locked = 0) " +
                                "AND (staged_order_orders.order_id = " + Database.Q(orderId) + ") " +
                            "UNION SELECT in_progress.id " +
                            "FROM in_progress " +
                            "WHERE (in_progress.order_type = " + Database.Q(KaInProgress.OrderTypes.Order) + ") " +
                                "AND (in_progress.order_id = " + Database.Q(orderId) + ") " +
                            "UNION SELECT in_progress.id " +
                            "FROM in_progress " +
                            "INNER JOIN in_progress_orders ON in_progress_orders.in_progress_id = in_progress.id " +
                            "WHERE (in_progress.order_type = " + Database.Q(KaInProgress.OrderTypes.Order) + ") " +
                                "AND (in_progress_orders.order_id = " + Database.Q(orderId) + ")");
                used = rdr.Read();
                rdr.Close();
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }

            return used;
        }

        private void tmrPrintTickets_Tick(object sender, EventArgs e) {
            try {
                tmrPrintTicket.Enabled = false;
                while (Printing.TicketsToPrint.Count > 0) Printing.PrintDeliveryTicket(Printing.TicketsToPrint.Dequeue());
                while (Printing.ReceivingTicketsToPrint.Count > 0) Printing.PrintReceivingTicket(Printing.ReceivingTicketsToPrint.Dequeue());
                while (Printing.PickTicketsToPrint.Count > 0) Printing.PrintPickTicket(Printing.PickTicketsToPrint.Dequeue());
                while (Printing.ReceivingPickTicketsToPrint.Count > 0) Printing.PrintReceivingPickTicket(Printing.ReceivingPickTicketsToPrint.Dequeue());
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            } finally {
                tmrPrintTicket.Enabled = true;
            }
        }

        private void restrictedUseProductHauled1_BackClicked(object sender, EventArgs e) {
            try { // to transition to the previous screen
                Transition(restrictedUseProductHauled1, BackFromView(restrictedUseProductHauled1), false);
            } catch (InTransitionException) {
                restrictedUseProductHauled1.Enabled = true;
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
                restrictedUseProductHauled1.Enabled = true;
            }
        }

        private void restrictedUseProductHauled1_OkClicked(object sender, EventArgs e) {
            try { // to transition to the next screen
                if (!CurrentPanelStatus.TruckCleaned && !CurrentPanelStatus.TruckNotUsedRupp) {
                    YesNo yesNoForRestrictedUseProductHauled = GetNewYesNoObject();
                    yesNoForRestrictedUseProductHauled.PromptText = "WARNING!!!" + Environment.NewLine + "You have not selected either the " + Environment.NewLine + Environment.NewLine + "Truck Has Never Carried RUPP Materials option, or the " + Environment.NewLine + "Truck Has Been Cleaned option." + Environment.NewLine + Environment.NewLine + "Is this correct?";
                    yesNoForRestrictedUseProductHauled.Title = "RUPP Warning";
                    yesNoForRestrictedUseProductHauled.Tag = RequireRestrictedUseTag;
                    Transition(restrictedUseProductHauled1, yesNoForRestrictedUseProductHauled, true);
                } else if (CurrentPanelStatus.TruckCleaned && CurrentPanelStatus.TruckNotUsedRupp) {
                    YesNo yesNoForRestrictedUseProductHauled = GetNewYesNoObject();
                    yesNoForRestrictedUseProductHauled.PromptText = "WARNING!!!" + Environment.NewLine + "You have selected both the " + Environment.NewLine + Environment.NewLine + "Truck Has Never Carried RUPP Materials option and the " + Environment.NewLine + "Truck Has Been Cleaned option." + Environment.NewLine + Environment.NewLine + "Is this correct?";
                    yesNoForRestrictedUseProductHauled.Title = "RUPP Warning";
                    yesNoForRestrictedUseProductHauled.Tag = RequireRestrictedUseTag;
                    Transition(restrictedUseProductHauled1, yesNoForRestrictedUseProductHauled, true);
                } else {
                    Transition(restrictedUseProductHauled1, NextFromView(restrictedUseProductHauled1), true);
                }
            } catch (InTransitionException) {
                restrictedUseProductHauled1.Enabled = true;
            }
        }

        private void complete1_BackClicked(object sender, EventArgs e) {
            try { // to transition to the previous screen
                Transition(complete1, BackFromView(complete1), false);
            } catch (InTransitionException) {
                complete1.Enabled = true;
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
                complete1.Enabled = true;
            }
        }

        private void complete1_OkClicked(object sender, EventArgs e) {
            try { // to transition to the next screen
                Transition(complete1, NextFromView(complete1), true);
            } catch (InTransitionException) {
                complete1.Enabled = true;
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
                complete1.Enabled = true;
            }
        }

        private void confirm1_BackClicked(object sender, EventArgs e) {
            try { // to transition to the previous screen
                Transition(confirm1, BackFromView(confirm1), false);
            } catch (InTransitionException) {
                confirm1.Enabled = true;
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
                confirm1.Enabled = true;
            }
        }

        private void confirm1_OkClicked(object sender, EventArgs e) {
            try { // to transition to the next screen
                Transition(confirm1, NextFromView(confirm1), true);
            } catch (InTransitionException) {
                confirm1.Enabled = true;
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
                confirm1.Enabled = true;
            }
        }

        private void orderDetails1_BackClicked(object sender, EventArgs e) {
            try { // to transition to the previous screen
                Transition(orderDetails1, BackFromView(orderDetails1), false);
            } catch (InTransitionException) {
                orderDetails1.Enabled = true;
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
                orderDetails1.Enabled = true;
            }
        }

        private void orderDetails1_OkClicked(object sender, EventArgs e) {
            try { // to transition to the next screen
                Transition(orderDetails1, NextFromView(orderDetails1), true);
            } catch (InTransitionException) {
                orderDetails1.Enabled = true;
            }
        }

        private void receivingDetails1_BackClicked(object sender, EventArgs e) {
            try { // to transition to the previous screen
                Transition(receivingDetails1, BackFromView(receivingDetails1), false);
            } catch (InTransitionException) {
                receivingDetails1.Enabled = true;
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
                receivingDetails1.Enabled = true;
            }
        }

        private void receivingDetails1_OkClicked(object sender, EventArgs e) {
            try { // to transition to the next screen
                Transition(receivingDetails1, NextFromView(receivingDetails1), true);
            } catch (InTransitionException) {
                receivingDetails1.Enabled = true;
            }
        }

        private void SetupStagedOrder() {
            if (CurrentPanelStatus.StagedOrder == null && CurrentPanelStatus.Order != null) {
                LfLoad load = new LfLoad(LfDatabase.LocationId, LfControllers.PanelControllers);
                load.Orders.Add(new LfLoadOrder() {
                    CustomerAccountLocation = CurrentPanelStatus.CustomerAccountLocation,
                    Order = CurrentPanelStatus.Order,
                    Percentage = 100
                });

                if (CurrentPanelStatus.Transport != null && Database.Settings.AutomaticallyAddCompartmentForTransport) {
                    foreach (KaTransportCompartment compartment in CurrentPanelStatus.Transport.Compartments) {
                        if (!compartment.Deleted) {
                            load.Compartments.Add(new LfLoadCompartment() {
                                Transport = CurrentPanelStatus.Transport,
                                TransportCompartment = compartment
                            });
                        }
                    }
                }
                if (load.Compartments.Count == 0) {
                    load.Compartments.Add(new LfLoadCompartment() {
                        Transport = CurrentPanelStatus.Transport
                    });
                }
                load.DistributeLoadInCompartments(LfLoad.AdjustBy.Remaining, 0);
                CurrentPanelStatus.StagedOrder = load.MakeStagedOrder(Guid.Empty);
            }
        }

        private void CheckForValidOutboundOrders(List<KaOrder> validOrders, List<KaReceivingPurchaseOrder> validReceivingOrders, out List<KaStagedOrder> validStagedOrders, out Dictionary<Guid, KaOrder> validOrderIds, out List<KaInProgress> validInProgressOrders, out Dictionary<Guid, KaReceivingPurchaseOrder> validPoOrderIds) {
            // Check that there is valid information assigned to these
            int validCounter = 0;
            validStagedOrders = new List<KaStagedOrder>();
            validOrderIds = new Dictionary<Guid, KaOrder>();
            while (validCounter < validOrders.Count) {
                KaOrder validOrder = validOrders[validCounter];
                validOrderIds.Add(validOrder.Id, validOrder);
                CurrentPanelStatus.Order = validOrder;
                CheckForStagedOrder(false);
                foreach (KaStagedOrder stagedOrder in CurrentPanelStatus.StagedOrders) {
                    if (!validStagedOrders.Contains(stagedOrder)) validStagedOrders.Add(stagedOrder);
                }
                validCounter++;
            }
            CurrentPanelStatus.Order = null;
            validCounter = 0;
            validInProgressOrders = new List<KaInProgress>();
            validPoOrderIds = new Dictionary<Guid, KaReceivingPurchaseOrder>();
            while (validCounter < validReceivingOrders.Count) {
                KaReceivingPurchaseOrder validPo = validReceivingOrders[validCounter];
                validPoOrderIds.Add(validPo.Id, validPo);
                CurrentPanelStatus.ReceivingPurchaseOrder = validPo;
                List<KaInProgress> inProgressRecords = CheckForInprogressRecord(false);
                foreach (KaInProgress ipo in inProgressRecords) {
                    if (!validInProgressOrders.Contains(ipo)) validInProgressOrders.Add(ipo);
                }
                validCounter++;
            }
            CurrentPanelStatus.ReceivingPurchaseOrder = null;
        }

        private void SaveData() {
            OleDbConnection updateConnection = new OleDbConnection(Tm2Database.GetDbConnection());
            OleDbTransaction updateTransaction = null;
            KaStagedOrder stagedOrderInfo = CurrentPanelStatus.StagedOrder;
            KaBulkProduct bulkProduct = null;
            if (CurrentPanelStatus.BulkProductId != Guid.Empty) {
                bulkProduct = new KaBulkProduct(Tm2Database.Connection, CurrentPanelStatus.BulkProductId);
            }
            if (bulkProduct == null && CurrentPanelStatus.ReceivingPurchaseOrder != null) {
                bulkProduct = new KaBulkProduct(Tm2Database.Connection, CurrentPanelStatus.ReceivingPurchaseOrder.BulkProductId);
                CurrentPanelStatus.BulkProductId = bulkProduct.Id;
            }
            string applicationIdentifier = Database.ApplicationIdentifier;
            string applicationUsername = Database.ApplicationUsername;

            try {
                updateConnection.Open();
                updateTransaction = updateConnection.BeginTransaction();
                if (CurrentPanelStatus.Carrier != null && CurrentPanelStatus.Carrier.Id.Equals(Guid.Empty))
                    CurrentPanelStatus.Carrier.SqlUpdateInsertIfNotFound(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);

                if (CurrentPanelStatus.Driver != null) {
                    if (CurrentPanelStatus.Driver.Id.Equals(Guid.Empty))
                        CurrentPanelStatus.Driver.SqlUpdateInsertIfNotFound(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                }

                if (CurrentPanelStatus.Transport != null) {
                    bool updateRequired = CurrentPanelStatus.Transport.Id.Equals(Guid.Empty);
                    if ((CurrentPanelStatus.IsInbound && CurrentPanelStatus.Order != null) ||
                        (!CurrentPanelStatus.IsInbound && CurrentPanelStatus.ReceivingPurchaseOrder != null)) {
                        CurrentPanelStatus.Transport.TareWeight = KaUnit.Convert(updateConnection, CurrentPanelStatus.ValidatedScaleWeight, new KaRatio(0, LfDatabase.DefaultMassUnitId, LfDatabase.DefaultVolumeUnitId), CurrentPanelStatus.Transport.UnitId, updateTransaction).Numeric;
                        CurrentPanelStatus.Transport.TaredAt = DateTime.Now;
                        CurrentPanelStatus.Transport.TareManual = false;
                        updateRequired = true;
                    }
                    if (updateRequired) CurrentPanelStatus.Transport.SqlUpdateInsertIfNotFound(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                }

                if (CurrentPanelStatus.CustomerAccountLocation != null && CurrentPanelStatus.CustomerAccountLocation.Id == Guid.Empty && (CurrentPanelStatus.Order == null || Database.Settings.PromptForShipToDestinationOwners.Contains(CurrentPanelStatus.Order.OwnerId))) { // Only create ship to location if the driver has been prompted for the ship to location, and a new location was created
                    if (CurrentPanelStatus.CustomerAccountLocation.CustomerAccountId.Equals(Guid.Empty)) {
                        List<Guid> accountsAssignedToStagedOrder = new List<Guid>();
                        foreach (KaStagedOrderOrder soo in stagedOrderInfo.Orders) {
                            try {
                                if (!soo.Deleted) {
                                    foreach (KaOrderCustomerAccount oca in new KaOrder(updateConnection, soo.OrderId, updateTransaction).OrderAccounts) {
                                        if (!oca.Deleted && !accountsAssignedToStagedOrder.Contains(oca.CustomerAccountId)) accountsAssignedToStagedOrder.Add(oca.CustomerAccountId);
                                    }
                                }
                            } catch (RecordNotFoundException) { /* suppress */ }
                        }
                        if (accountsAssignedToStagedOrder.Count == 1) CurrentPanelStatus.CustomerAccountLocation.CustomerAccountId = accountsAssignedToStagedOrder[0];
                    }
                    CurrentPanelStatus.CustomerAccountLocation.SqlInsert(Tm2Database.Connection, null, Database.APP_ID, CurrentPanelStatus.Driver != null ? CurrentPanelStatus.Driver.Name : Database.ApplicationUsername);
                }

                if (CurrentPanelStatus.IsInbound) {
                    SaveInboundData(updateConnection, updateTransaction, stagedOrderInfo, bulkProduct);
                } else { //Outbound
                    if (stagedOrderInfo != null && Database.IsPalletedLoad(stagedOrderInfo))
                        SaveOutboundDataForPalletedProduct(updateConnection, updateTransaction, stagedOrderInfo);
                    else
                        SaveOutboundDataForBulkProduct(updateConnection, updateTransaction, stagedOrderInfo, bulkProduct);
                }
                updateTransaction.Commit();
            } catch (Exception) {
                if (updateTransaction != null) updateTransaction.Rollback();
                throw;
            } finally {
                if (updateTransaction != null) updateTransaction.Dispose();
                updateConnection.Close();
            }
        }

        private void SaveInboundData(OleDbConnection updateConnection, OleDbTransaction updateTransaction, KaStagedOrder stagedOrderInfo, KaBulkProduct bulkProduct) {
            string applicationIdentifier = Database.ApplicationIdentifier;
            string applicationUsername = Database.ApplicationUsername;
            Dictionary<Guid, KaOrder> orders = new Dictionary<Guid, KaOrder>();
            Guid ownerId = Guid.Empty;
            if (CurrentPanelStatus.Order != null)
                ownerId = CurrentPanelStatus.Order.OwnerId;
            else if (CurrentPanelStatus.ReceivingPurchaseOrder != null)
                ownerId = CurrentPanelStatus.ReceivingPurchaseOrder.OwnerId;
            Guid driverInFacilityId = Guid.Empty;
            if (CurrentPanelStatus.Driver != null) {
                foreach (KaDriverInFacility tid in KaDriverInFacility.GetAll(updateConnection, $"deleted = 0 AND in_facility = 1 AND driver_id = {Database.Q(CurrentPanelStatus.Driver.Id)} AND location_id = {Database.Q(LfDatabase.LocationId)}", "", updateTransaction)) {
                    tid.InFacility = false;
                    tid.SqlUpdate(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                }
                KaDriverInFacility driverInFac = new KaDriverInFacility() {
                    EnteredAt = DateTime.Now,
                    InFacility = true,
                    LocationId = LfDatabase.LocationId,
                    DriverId = CurrentPanelStatus.Driver.Id
                };
                driverInFac.SqlInsert(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);

                driverInFacilityId = driverInFac.Id;
            }

            Guid transportInFacilityId = Guid.Empty;
            if (CurrentPanelStatus.Transport != null) {
                foreach (KaTransportInFacility tif in KaTransportInFacility.GetAll(updateConnection, $"deleted = 0 AND in_facility = 1 AND transport_id = {Database.Q(CurrentPanelStatus.Transport.Id)} AND location_id = {Database.Q(LfDatabase.LocationId)}", "", updateTransaction)) {
                    tif.InFacility = false;
                    tif.SqlUpdate(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                }
                KaTransportInFacility transInFac = new KaTransportInFacility() {
                    EnteredAt = DateTime.Now,
                    InFacility = true,
                    LocationId = LfDatabase.LocationId,
                    TransportId = CurrentPanelStatus.Transport.Id
                };
                transInFac.SqlInsert(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);

                transportInFacilityId = transInFac.Id;
            }
            if (stagedOrderInfo != null) {
                if (CurrentPanelStatus.Carrier != null) stagedOrderInfo.CarrierId = CurrentPanelStatus.Carrier.Id;
                if (CurrentPanelStatus.Driver != null) stagedOrderInfo.DriverId = CurrentPanelStatus.Driver.Id;
                if (CurrentPanelStatus.Transport != null) {
                    foreach (KaStagedOrderTransport stagedTransports in stagedOrderInfo.Transports) {
                        if (stagedTransports.TransportId.Equals(Guid.Empty)) {
                            stagedTransports.TransportId = CurrentPanelStatus.Transport.Id;
                        }
                        stagedTransports.TareWeight = CurrentPanelStatus.ValidatedScaleWeight.Numeric;
                        stagedTransports.TareUnitId = CurrentPanelStatus.ValidatedScaleWeight.UnitId;
                        stagedTransports.TaredAt = DateTime.Now;
                        stagedTransports.TareManual = false;
                        break;
                    }
                } else if (stagedOrderInfo.Transports.Count > 0) {
                    foreach (KaStagedOrderTransport stagedTransports in stagedOrderInfo.Transports) {
                        if (stagedTransports.TransportId.Equals(Guid.Empty)) {
                            stagedTransports.TareWeight = CurrentPanelStatus.ValidatedScaleWeight.Numeric;
                            stagedTransports.TareUnitId = CurrentPanelStatus.ValidatedScaleWeight.UnitId;
                            stagedTransports.TaredAt = DateTime.Now;
                            stagedTransports.TareManual = false;
                            break;
                        }
                    }
                } else if (stagedOrderInfo.Transports.Count == 0) {
                    KaStagedOrderTransport newStagedTransport = new KaStagedOrderTransport() {
                        Id = Guid.NewGuid(),
                        TareWeight = CurrentPanelStatus.ValidatedScaleWeight.Numeric,
                        TareUnitId = CurrentPanelStatus.ValidatedScaleWeight.UnitId,
                        TaredAt = DateTime.Now,
                        TareManual = false
                    };
                    stagedOrderInfo.Transports.Add(newStagedTransport);
                    foreach (KaStagedOrderCompartment comp in stagedOrderInfo.Compartments) {
                        if (comp.StagedOrderTransportId.Equals(Guid.Empty))
                            comp.StagedOrderTransportId = newStagedTransport.Id;
                    }
                }
                if (CurrentPanelStatus.CustomerAccountLocation != null) {
                    stagedOrderInfo.CustomerAccountLocationId = CurrentPanelStatus.CustomerAccountLocation.Id;
                    foreach (KaStagedOrderOrder soo in stagedOrderInfo.Orders) {
                        foreach (KaOrderCustomerAccount oca in KaOrder.GetOrder(updateConnection, soo.OrderId, ref orders, updateTransaction).OrderAccounts) {
                            if (CurrentPanelStatus.CustomerAccountLocation.CustomerAccountId.Equals(Guid.Empty) || CurrentPanelStatus.CustomerAccountLocation.CustomerAccountId.Equals(oca.CustomerAccountId)) {
                                soo.CustomerAccountLocationId = CurrentPanelStatus.CustomerAccountLocation.Id;
                            }
                        }
                    }
                }
                if (Database.RequireRestrictedUseOwners.Contains(ownerId)) {
                    KaStagedOrderCustomLoadQuestionData stagedOrderInspData;
                    KaCustomLoadQuestionData inspectionData;

                    // Previous load product
                    stagedOrderInspData = GetStagedOrderInspectionData(stagedOrderInfo, Database.PREVIOUS_LOAD_INSPECTION_FIELD_ID);
                    if (stagedOrderInspData == null) stagedOrderInspData = new KaStagedOrderCustomLoadQuestionData();

                    try {
                        inspectionData = new KaCustomLoadQuestionData(updateConnection, stagedOrderInspData.CustomLoadQuestionsDataId, updateTransaction);
                    } catch (RecordNotFoundException) {
                        inspectionData = new KaCustomLoadQuestionData();
                    }
                    inspectionData.CustomLoadQuestionFieldsId = Database.PREVIOUS_LOAD_INSPECTION_FIELD_ID;
                    inspectionData.Data = CurrentPanelStatus.PreviousLoadProduct;
                    inspectionData.SqlUpdateInsertIfNotFound(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                    stagedOrderInspData.CustomLoadQuestionsDataId = inspectionData.Id;
                    stagedOrderInfo.CustomLoadQuestionDatas.Add(stagedOrderInspData);

                    // Truck cleaned
                    stagedOrderInspData = GetStagedOrderInspectionData(stagedOrderInfo, Database.TRUCK_CLEANED_INSPECTION_FIELD_ID);
                    if (stagedOrderInspData == null) stagedOrderInspData = new KaStagedOrderCustomLoadQuestionData();
                    try {
                        inspectionData = new KaCustomLoadQuestionData(updateConnection, stagedOrderInspData.CustomLoadQuestionsDataId, updateTransaction);
                    } catch (RecordNotFoundException) {
                        inspectionData = new KaCustomLoadQuestionData();
                    }
                    inspectionData.CustomLoadQuestionFieldsId = Database.TRUCK_CLEANED_INSPECTION_FIELD_ID;
                    inspectionData.Data = CurrentPanelStatus.TruckCleaned.ToString();
                    inspectionData.SqlUpdateInsertIfNotFound(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                    stagedOrderInspData.CustomLoadQuestionsDataId = inspectionData.Id;
                    stagedOrderInfo.CustomLoadQuestionDatas.Add(stagedOrderInspData);

                    // Truck not hauled
                    stagedOrderInspData = GetStagedOrderInspectionData(stagedOrderInfo, Database.TRUCK_NOT_HAULED_INSPECTION_FIELD_ID);
                    if (stagedOrderInspData == null) stagedOrderInspData = new KaStagedOrderCustomLoadQuestionData();
                    try {
                        inspectionData = new KaCustomLoadQuestionData(updateConnection, stagedOrderInspData.CustomLoadQuestionsDataId, updateTransaction);
                    } catch (RecordNotFoundException) {
                        inspectionData = new KaCustomLoadQuestionData();
                    }
                    inspectionData.CustomLoadQuestionFieldsId = Database.TRUCK_NOT_HAULED_INSPECTION_FIELD_ID;
                    inspectionData.Data = CurrentPanelStatus.TruckNotUsedRupp.ToString();
                    inspectionData.SqlUpdateInsertIfNotFound(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                    stagedOrderInspData.CustomLoadQuestionsDataId = inspectionData.Id;
                    stagedOrderInfo.CustomLoadQuestionDatas.Add(stagedOrderInspData);
                }

                if (Database.Settings.BayDirectionEnabled) {
                    GetBay();
                    if (CurrentPanelStatus.BayAssigned == null)
                        stagedOrderInfo.BayId = Guid.Empty;
                    else
                        stagedOrderInfo.BayId = CurrentPanelStatus.BayAssigned.BayId;
                }

                stagedOrderInfo.SqlUpdateInsertIfNotFound(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                //update staged order compartment
            }

            if (CurrentPanelStatus.InProgress == null && CurrentPanelStatus.ReceivingPurchaseOrder != null)
                CurrentPanelStatus.InProgress = new KaInProgress() {
                    Id = Guid.NewGuid(),
                    ReceivingPurchaseOrderId = CurrentPanelStatus.ReceivingPurchaseOrder.Id,
                    OrderType = (int)KaInProgress.OrderTypes.ReceivingPurchaseOrder
                };
            if (CurrentPanelStatus.InProgress != null) {
                if (CurrentPanelStatus.Carrier != null)
                    CurrentPanelStatus.InProgress.CarrierId = CurrentPanelStatus.Carrier.Id;
                if (CurrentPanelStatus.Driver != null) {
                    CurrentPanelStatus.InProgress.DriverId = CurrentPanelStatus.Driver.Id;
                    CurrentPanelStatus.InProgress.DriverInFacilityId = driverInFacilityId;
                }
                if (CurrentPanelStatus.CustomerAccountLocation != null) {
                    foreach (KaInProgressOrder ipo in CurrentPanelStatus.InProgress.Orders) {
                        foreach (KaOrderCustomerAccount oca in KaOrder.GetOrder(updateConnection, ipo.OrderId, ref orders, updateTransaction).OrderAccounts) {
                            if (CurrentPanelStatus.CustomerAccountLocation.CustomerAccountId.Equals(Guid.Empty) || CurrentPanelStatus.CustomerAccountLocation.CustomerAccountId.Equals(oca.CustomerAccountId)) {
                                ipo.CustomerAccountLocationId = CurrentPanelStatus.CustomerAccountLocation.Id;
                            }
                        }
                    }
                }
                KaInProgressWeighment newWeighment = GetNewInprogressWeighmentForInbound();
                if (CurrentPanelStatus.Transport != null) {
                    newWeighment.TransportId = CurrentPanelStatus.Transport.Id;
                    newWeighment.TransportInFacilityId = transportInFacilityId;
                }
                if (stagedOrderInfo != null) {
                    newWeighment.BayId = stagedOrderInfo.BayId;
                    newWeighment.StagedOrderCompartmentItemId = stagedOrderInfo.Compartments[0].Id;
                }
                CurrentPanelStatus.InProgress.Weighments.Add(newWeighment);

                if (stagedOrderInfo != null) {
                    CurrentPanelStatus.InProgress.StagedOrderId = stagedOrderInfo.Id;
                    CurrentPanelStatus.InProgress.OrderType = (int)KaInProgress.OrderTypes.StagedOrder;
                } else if (CurrentPanelStatus.ReceivingPurchaseOrder != null) {
                    CurrentPanelStatus.InProgress.ReceivingPurchaseOrderId = CurrentPanelStatus.ReceivingPurchaseOrder.Id;
                    CurrentPanelStatus.InProgress.OrderType = (int)KaInProgress.OrderTypes.ReceivingPurchaseOrder;
                    if (bulkProduct != null) {
                        newWeighment.BulkProductId = bulkProduct.Id;
                        newWeighment.AverageDensity = bulkProduct.Density;
                        newWeighment.WeightUnitId = bulkProduct.WeightUnitId;
                        newWeighment.VolumeUnitId = bulkProduct.VolumeUnitId;
                    }
                }

                CurrentPanelStatus.InProgress.SqlUpdateInsertIfNotFound(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
            }

            if (CurrentPanelStatus.Order != null) {
                Printing.PickTicketsToPrint.Enqueue(CurrentPanelStatus.StagedOrder);
            } else if (CurrentPanelStatus.ReceivingPurchaseOrder != null) {
                Printing.ReceivingPickTicketsToPrint.Enqueue(CurrentPanelStatus.InProgress);
            }
        }

        private void SaveOutboundDataForPalletedProduct(OleDbConnection updateConnection, OleDbTransaction updateTransaction, KaStagedOrder stagedOrderInfo) {
            string applicationIdentifier = Database.ApplicationIdentifier;
            string applicationUsername = Database.ApplicationUsername;
            if (CurrentPanelStatus.InProgress == null) {
                CurrentPanelStatus.InProgress = new KaInProgress() {
                    Id = Guid.NewGuid(),
                    StagedOrderId = stagedOrderInfo.Id,
                    OrderType = (int)KaInProgress.OrderTypes.StagedOrder
                };

                CurrentPanelStatus.InProgress.GetChildren(updateConnection, updateTransaction);
            }
            if (CurrentPanelStatus.Carrier != null)
                CurrentPanelStatus.InProgress.CarrierId = CurrentPanelStatus.Carrier.Id;
            if (CurrentPanelStatus.Driver != null)
                CurrentPanelStatus.InProgress.DriverId = CurrentPanelStatus.Driver.Id;
            CurrentPanelStatus.InProgress.LocationId = LfDatabase.LocationId;
            Guid driverInFacilityId = Guid.Empty;
            if (!CurrentPanelStatus.InProgress.DriverId.Equals(Guid.Empty) && Database.Settings.MoveDriversToHistory) {
                foreach (KaDriverInFacility tid in KaDriverInFacility.GetAll(updateConnection, $"deleted = 0 AND in_facility = 1 AND driver_id = {Database.Q(CurrentPanelStatus.InProgress.DriverId)} AND location_id = {Database.Q(LfDatabase.LocationId)}", "", updateTransaction)) {
                    tid.InFacility = false;
                    tid.ExitedAt = DateTime.Now;
                    tid.SqlUpdate(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                    driverInFacilityId = tid.Id;
                }
                if (driverInFacilityId.Equals(Guid.Empty)) {
                    KaDriverInFacility driverInFac = new KaDriverInFacility() {
                        ExitedAt = DateTime.Now,
                        InFacility = false,
                        LocationId = LfDatabase.LocationId,
                        DriverId = CurrentPanelStatus.InProgress.DriverId
                    };
                    driverInFac.SqlInsert(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                    driverInFacilityId = driverInFac.Id;
                }
                if (CurrentPanelStatus.InProgress.DriverInFacilityId == Guid.Empty) CurrentPanelStatus.InProgress.DriverInFacilityId = driverInFacilityId;
            }

            Guid transportInFacilityId = Guid.Empty;
            if (CurrentPanelStatus.Transport != null && !CurrentPanelStatus.Transport.Id.Equals(Guid.Empty) && Database.Settings.MoveTransportsToHistory) {
                foreach (KaTransportInFacility tif in KaTransportInFacility.GetAll(updateConnection, $"deleted = 0 AND in_facility = 1 AND transport_id = {Database.Q(CurrentPanelStatus.Transport.Id)} AND location_id = {Database.Q(LfDatabase.LocationId)}", "", updateTransaction)) {
                    tif.InFacility = false;
                    tif.SqlUpdate(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                    transportInFacilityId = tif.Id;
                    if (transportInFacilityId.Equals(Guid.Empty)) transportInFacilityId = tif.Id;
                }
                if (transportInFacilityId.Equals(Guid.Empty)) {
                    KaTransportInFacility transInFac = new KaTransportInFacility() {
                        ExitedAt = DateTime.Now,
                        InFacility = false,
                        LocationId = LfDatabase.LocationId,
                        TransportId = CurrentPanelStatus.Transport.Id
                    };
                    transInFac.SqlInsert(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);

                    transportInFacilityId = transInFac.Id;
                }
            }
            CurrentPanelStatus.InProgress.Weighments.Clear();

            Dictionary<Guid, KaProduct> products = new Dictionary<Guid, KaProduct>();
            Dictionary<Guid, KaBulkProduct> bulkProducts = new Dictionary<Guid, KaBulkProduct>();
            Dictionary<Guid, KaRatio> productDensities = new Dictionary<Guid, KaRatio>();
            Dictionary<Guid, KaRatio> bulkProductDensities = new Dictionary<Guid, KaRatio>();
            Dictionary<Guid, List<KaProductBulkProduct>> productBulkProducts = new Dictionary<Guid, List<KaProductBulkProduct>>();
            Dictionary<Guid, KaUnit> units = new Dictionary<Guid, KaUnit>();
            Dictionary<Guid, KaQuantity> orderItemAssignedAmounts = CurrentPanelStatus.StagedOrder.GetOrderItemAssignedAmounts(updateConnection, updateTransaction, CurrentPanelStatus.ValidatedScaleWeight.UnitId);
            double assignedAmount = 0;

            foreach (Guid orderItemId in orderItemAssignedAmounts.Keys) {
                KaQuantity orderItemAssignedAmount = orderItemAssignedAmounts[orderItemId];
                KaOrderItem item = new KaOrderItem(updateConnection, orderItemId, updateTransaction);
                KaProduct product = KaProduct.GetProduct(updateConnection, item.ProductId, products, updateTransaction);
                if (!productDensities.ContainsKey(product.Id)) {
                    try {
                        productDensities.Add(product.Id, product.GetDensity(CurrentPanelStatus.StagedOrder.LocationId));
                    } catch (Exception) {
                        productDensities.Add(product.Id, Database.EmptyDensity);
                    }
                }
                if (!productBulkProducts.ContainsKey(product.Id))
                    productBulkProducts.Add(product.Id, product.GetProductBulkProductsAssignedAtFacility(updateConnection, stagedOrderInfo.LocationId, updateTransaction));
                foreach (KaProductBulkProduct pbp in productBulkProducts[product.Id]) {
                    KaBulkProduct bulkProduct = KaBulkProduct.GetBulkProduct(updateConnection, pbp.BulkProductId, ref bulkProducts, updateTransaction);
                    KaInProgressWeighment newWeighment = new KaInProgressWeighment();
                    newWeighment.BulkProductId = pbp.BulkProductId;
                    newWeighment.PanelId = CurrentPanelStatus.PanelId;
                    newWeighment.OrderItemId = orderItemId;
                    newWeighment.ProductId = pbp.ProductId;
                    newWeighment.BulkProductPanelSettingId = Guid.Empty;
                    KaBulkProductPanelSettings bps;
                    try {
                        bps = KaBulkProductPanelSettings.GetSettingForProductAndPanel(updateConnection, pbp.BulkProductId, newWeighment.PanelId, updateTransaction);
                        if (bps.ProductNumber < 80 || bps.ProductNumber == 99)
                            newWeighment.BulkProductPanelSettingId = bps.Id;
                    } catch (Exception) {
                        bps = new KaBulkProductPanelSettings();
                        if (bulkProduct.IsFunction(updateConnection, updateTransaction))
                            bps.ProductNumber = 80;
                        else
                            bps.ProductNumber = 99;
                    }

                    if (bps.ProductNumber < 80 || bps.ProductNumber == 99) {
                        if (!bulkProductDensities.ContainsKey(pbp.BulkProductId)) bulkProductDensities.Add(pbp.BulkProductId, new KaRatio(bulkProduct.Density, bulkProduct.WeightUnitId, bulkProduct.VolumeUnitId));
                        KaRatio bulkProductDensity = bulkProductDensities[pbp.BulkProductId];
                        if (CurrentPanelStatus.Transport != null)
                            newWeighment.TransportId = CurrentPanelStatus.Transport.Id;

                        newWeighment.StagedOrderCompartmentItemId = CurrentPanelStatus.StagedOrderInfoCompartmentItemId;
                        newWeighment.Complete = true;
                        newWeighment.CompletedDate = DateTime.Now;

                        double bulkProductAmount = KaUnit.FastConvert(updateConnection, new KaQuantity(orderItemAssignedAmount.Numeric * pbp.Portion / 100, orderItemAssignedAmount.UnitId), bulkProductDensities[pbp.BulkProductId], CurrentPanelStatus.ValidatedScaleWeight.UnitId, units, updateTransaction).Numeric;
                        assignedAmount += bulkProductAmount;
                        newWeighment.UnitId = CurrentPanelStatus.ValidatedScaleWeight.UnitId;
                        newWeighment.Requested = bulkProductAmount;
                        newWeighment.RequestedDensity = bulkProduct.Density;
                        newWeighment.RequestedWeightUnitId = bulkProduct.WeightUnitId;
                        newWeighment.RequestedVolumeUnitId = bulkProduct.VolumeUnitId;

                        newWeighment.Delivered = bulkProductAmount;
                        newWeighment.UseDelivered = true;
                        newWeighment.AverageDensity = bulkProduct.Density;
                        newWeighment.WeightUnitId = bulkProduct.WeightUnitId;
                        newWeighment.VolumeUnitId = bulkProduct.VolumeUnitId;

                        newWeighment.StartQuantity = CurrentPanelStatus.ValidatedScaleWeight.Numeric;
                        newWeighment.StartDate = DateTime.Now;
                        newWeighment.UnitId = CurrentPanelStatus.ValidatedScaleWeight.UnitId;
                        newWeighment.Batch = 0;
                        newWeighment.Compartment = 0;
                        newWeighment.Position = CurrentPanelStatus.InProgress.Weighments.Count;
                        if (bulkProduct != null) {
                            newWeighment.AverageDensity = bulkProduct.Density;
                            newWeighment.VolumeUnitId = bulkProduct.VolumeUnitId;
                            newWeighment.WeightUnitId = bulkProduct.WeightUnitId;
                        }
                        newWeighment.UserId = Database.Settings.UserId;
                        if (stagedOrderInfo != null) {
                            newWeighment.BayId = stagedOrderInfo.BayId;
                            newWeighment.StagedOrderCompartmentItemId = stagedOrderInfo.Compartments[0].Id;
                        }
                        newWeighment.TransportInFacilityId = transportInFacilityId;
                        CurrentPanelStatus.InProgress.Weighments.Add(newWeighment);
                    }
                }
            }
            if (assignedAmount > 0) {
                foreach (KaStagedOrderTransport transport in stagedOrderInfo.Transports) {
                    if (CurrentPanelStatus.Transport == null || transport.TransportId == CurrentPanelStatus.Transport.Id) {
                        transport.TareWeight = KaUnit.FastConvert(updateConnection, new KaQuantity(CurrentPanelStatus.ValidatedScaleWeight.Numeric - assignedAmount, CurrentPanelStatus.ValidatedScaleWeight.UnitId), Database.EmptyDensity, transport.TareUnitId, units, updateTransaction).Numeric;
                        transport.SqlUpdateInsertIfNotFound(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                        break;
                    }
                }
            }
            CurrentPanelStatus.InProgress.SqlUpdateInsertIfNotFound(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
            CreateTicket(updateConnection, updateTransaction);
        }

        private void SaveOutboundDataForBulkProduct(OleDbConnection updateConnection, OleDbTransaction updateTransaction, KaStagedOrder stagedOrderInfo, KaBulkProduct bulkProduct) {
            string applicationIdentifier = Database.ApplicationIdentifier;
            string applicationUsername = Database.ApplicationUsername;
            if (CurrentPanelStatus.InProgress == null) {
                if (stagedOrderInfo != null) {
                    CurrentPanelStatus.InProgress = new KaInProgress() {
                        Id = Guid.NewGuid(),
                        StagedOrderId = stagedOrderInfo.Id,
                        OrderType = (int)KaInProgress.OrderTypes.StagedOrder
                    };
                } else if (CurrentPanelStatus.ReceivingPurchaseOrder != null) {
                    CurrentPanelStatus.InProgress = new KaInProgress() {
                        Id = Guid.NewGuid(),
                        ReceivingPurchaseOrderId = CurrentPanelStatus.ReceivingPurchaseOrder.Id,
                        OrderType = (int)KaInProgress.OrderTypes.ReceivingPurchaseOrder
                    };
                }
            }

            KaInProgressWeighment unfinishedWeighment = null;
            foreach (KaInProgressWeighment weighment in CurrentPanelStatus.InProgress.Weighments) {
                if (!weighment.Complete) {
                    unfinishedWeighment = weighment;
                    break;
                }
            }
            if (unfinishedWeighment == null) {
                unfinishedWeighment = new KaInProgressWeighment();
                CurrentPanelStatus.InProgress.Weighments.Add(unfinishedWeighment);
                if (CurrentPanelStatus.StagedOrder != null) {
                    foreach (KaStagedOrderTransport sot in stagedOrderInfo.Transports) {
                        if (sot.TransportId == Guid.Empty || CurrentPanelStatus.Transport == null || sot.TransportId.Equals(CurrentPanelStatus.Transport.Id)) {
                            if (sot.TareWeight >= unfinishedWeighment.Tare) {
                                unfinishedWeighment.Tare = sot.TareWeight;
                                unfinishedWeighment.TareDate = sot.TaredAt;
                                unfinishedWeighment.TareManual = sot.TareManual;
                            }
                        }
                    }
                    if (unfinishedWeighment.Tare == 0 && CurrentPanelStatus.Transport != null) {
                        unfinishedWeighment.Tare = CurrentPanelStatus.Transport.TareWeight;
                        unfinishedWeighment.TareDate = CurrentPanelStatus.Transport.TaredAt;
                        unfinishedWeighment.TareManual = CurrentPanelStatus.Transport.TareManual;
                    }
                }
                foreach (KaInProgressWeighment weighment in CurrentPanelStatus.InProgress.Weighments) {
                    if (weighment.TransportId == Guid.Empty || CurrentPanelStatus.Transport == null || weighment.TransportId.Equals(CurrentPanelStatus.Transport.Id)) {
                        if (CurrentPanelStatus.StagedOrder != null || CurrentPanelStatus.Order != null || (CurrentPanelStatus.InProgress != null && CurrentPanelStatus.InProgress.ReceivingPurchaseOrderId == Guid.Empty)) {
                            if (weighment.Gross >= unfinishedWeighment.Tare) {
                                unfinishedWeighment.Tare = weighment.Gross;
                                unfinishedWeighment.TareDate = weighment.GrossDate;
                                unfinishedWeighment.TareManual = weighment.GrossManual;
                            }
                        } else if (CurrentPanelStatus.ReceivingPurchaseOrder != null || (CurrentPanelStatus.InProgress != null && CurrentPanelStatus.InProgress.ReceivingPurchaseOrderId != Guid.Empty)) {
                            if (weighment.Tare <= unfinishedWeighment.Gross || unfinishedWeighment.Gross == 0) {
                                unfinishedWeighment.Gross = weighment.Tare;
                                unfinishedWeighment.GrossDate = weighment.TareDate;
                                unfinishedWeighment.GrossManual = weighment.TareManual;
                            }
                        }
                    }
                }
            }
            if (CurrentPanelStatus.Carrier != null)
                CurrentPanelStatus.InProgress.CarrierId = CurrentPanelStatus.Carrier.Id;
            if (CurrentPanelStatus.Driver != null)
                CurrentPanelStatus.InProgress.DriverId = CurrentPanelStatus.Driver.Id;
            if (CurrentPanelStatus.Transport != null)
                unfinishedWeighment.TransportId = CurrentPanelStatus.Transport.Id;
            CurrentPanelStatus.InProgress.LocationId = LfDatabase.LocationId;

            unfinishedWeighment.BulkProductId = CurrentPanelStatus.BulkProductId;
            unfinishedWeighment.PanelId = CurrentPanelStatus.PanelId;
            unfinishedWeighment.OrderItemId = CurrentPanelStatus.OrderItemId;
            unfinishedWeighment.ProductId = CurrentPanelStatus.ProductId;
            unfinishedWeighment.StagedOrderCompartmentItemId = CurrentPanelStatus.StagedOrderInfoCompartmentItemId;
            try {
                unfinishedWeighment.BulkProductPanelSettingId = KaBulkProductPanelSettings.GetSettingForProductAndPanel(updateConnection, unfinishedWeighment.BulkProductId, unfinishedWeighment.PanelId, updateTransaction).Id;

            } catch (Exception) {
                unfinishedWeighment.BulkProductPanelSettingId = Guid.Empty;
            }
            unfinishedWeighment.Complete = true;
            unfinishedWeighment.CompletedDate = DateTime.Now;

            if (CurrentPanelStatus.Order != null) {
                unfinishedWeighment.Gross = CurrentPanelStatus.ValidatedScaleWeight.Numeric;
                unfinishedWeighment.GrossDate = DateTime.Now;
                unfinishedWeighment.GrossManual = false;
            } else if (CurrentPanelStatus.ReceivingPurchaseOrder != null) {
                unfinishedWeighment.Tare = CurrentPanelStatus.ValidatedScaleWeight.Numeric;
                unfinishedWeighment.TareDate = DateTime.Now;
                unfinishedWeighment.TareManual = false;
            }
            unfinishedWeighment.Delivered = unfinishedWeighment.Gross - unfinishedWeighment.Tare;

            unfinishedWeighment.StartQuantity = CurrentPanelStatus.ValidatedScaleWeight.Numeric;
            unfinishedWeighment.StartDate = DateTime.Now;
            unfinishedWeighment.UnitId = CurrentPanelStatus.ValidatedScaleWeight.UnitId;
            unfinishedWeighment.Batch = 0;
            unfinishedWeighment.Compartment = 0;
            unfinishedWeighment.Position = CurrentPanelStatus.InProgress.Weighments.Count;
            if (bulkProduct != null) {
                unfinishedWeighment.AverageDensity = bulkProduct.Density;
                unfinishedWeighment.VolumeUnitId = bulkProduct.VolumeUnitId;
                unfinishedWeighment.WeightUnitId = bulkProduct.WeightUnitId;
            }
            unfinishedWeighment.UserId = Database.Settings.UserId;
            if (stagedOrderInfo != null) {
                unfinishedWeighment.BayId = stagedOrderInfo.BayId;
                unfinishedWeighment.StagedOrderCompartmentItemId = stagedOrderInfo.Compartments[0].Id;
            }

            CurrentPanelStatus.InProgress.SqlUpdateInsertIfNotFound(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);

            if (!CurrentPanelStatus.InProgress.DriverId.Equals(Guid.Empty) && Database.Settings.MoveDriversToHistory) {
                bool recordFound = false;
                foreach (KaDriverInFacility tid in KaDriverInFacility.GetAll(updateConnection, $"deleted = 0 AND in_facility = 1 AND driver_id = {Database.Q(CurrentPanelStatus.InProgress.DriverId)} AND location_id = {Database.Q(LfDatabase.LocationId)}", "", updateTransaction)) {
                    tid.InFacility = false;
                    tid.ExitedAt = DateTime.Now;
                    tid.SqlUpdate(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                    recordFound = true;
                    if (CurrentPanelStatus.InProgress.DriverInFacilityId.Equals(Guid.Empty)) CurrentPanelStatus.InProgress.DriverInFacilityId = tid.Id;
                }
                if (!recordFound) {
                    KaDriverInFacility driverInFac = new KaDriverInFacility() {
                        ExitedAt = DateTime.Now,
                        InFacility = false,
                        LocationId = LfDatabase.LocationId,
                        DriverId = CurrentPanelStatus.InProgress.DriverId
                    };
                    driverInFac.SqlInsert(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);

                    CurrentPanelStatus.InProgress.DriverInFacilityId = driverInFac.Id;
                }
            }

            if (!unfinishedWeighment.TransportId.Equals(Guid.Empty) && Database.Settings.MoveTransportsToHistory) {
                bool recordFound = false;
                foreach (KaTransportInFacility tif in KaTransportInFacility.GetAll(updateConnection, $"deleted = 0 AND in_facility = 1 AND transport_id = {Database.Q(CurrentPanelStatus.Transport.Id)} AND location_id = {Database.Q(LfDatabase.LocationId)}", "", updateTransaction)) {
                    tif.InFacility = false;
                    tif.ExitedAt = DateTime.Now;
                    tif.SqlUpdate(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);
                    recordFound = true;
                    if (unfinishedWeighment.TransportInFacilityId.Equals(Guid.Empty)) unfinishedWeighment.TransportInFacilityId = tif.Id;
                }
                if (!recordFound) {
                    KaTransportInFacility transInFac = new KaTransportInFacility() {
                        ExitedAt = DateTime.Now,
                        InFacility = false,
                        LocationId = LfDatabase.LocationId,
                        TransportId = CurrentPanelStatus.Transport.Id
                    };
                    transInFac.SqlInsert(updateConnection, updateTransaction, applicationIdentifier, applicationUsername);

                    unfinishedWeighment.TransportInFacilityId = transInFac.Id;
                }
            }
            CreateTicket(updateConnection, updateTransaction);
        }

        private KaInProgressWeighment GetNewInprogressWeighmentForInbound() {
            KaInProgressWeighment newWeighment = new KaInProgressWeighment();
            if (CurrentPanelStatus.Order != null) {
                newWeighment.Tare = CurrentPanelStatus.ValidatedScaleWeight.Numeric;
                newWeighment.TareDate = DateTime.Now;
                newWeighment.TareManual = false;
            } else if (CurrentPanelStatus.ReceivingPurchaseOrder != null) {
                newWeighment.Gross = CurrentPanelStatus.ValidatedScaleWeight.Numeric;
                newWeighment.GrossDate = DateTime.Now;
                newWeighment.GrossManual = false;
            }
            newWeighment.StartQuantity = CurrentPanelStatus.ValidatedScaleWeight.Numeric;
            newWeighment.StartDate = DateTime.Now;
            newWeighment.UnitId = CurrentPanelStatus.ValidatedScaleWeight.UnitId;
            newWeighment.Batch = 0;
            newWeighment.Compartment = 0;
            newWeighment.Position = CurrentPanelStatus.InProgress.Weighments.Count;
            newWeighment.VolumeUnitId = LfDatabase.DefaultVolumeUnitId;
            newWeighment.WeightUnitId = LfDatabase.DefaultMassUnitId;
            newWeighment.UserId = Database.Settings.UserId;
            if (CurrentPanelStatus.StagedOrder != null) {
                newWeighment.BayId = CurrentPanelStatus.StagedOrder.BayId;
                if (CurrentPanelStatus.StagedOrder != null && CurrentPanelStatus.StagedOrder.Compartments.Count > 0)
                    newWeighment.StagedOrderCompartmentItemId = CurrentPanelStatus.StagedOrder.Compartments[0].Id;
            }
            return newWeighment;
        }

        private KaStagedOrderCustomLoadQuestionData GetStagedOrderInspectionData(KaStagedOrder stagedorder, Guid inspectionDataId) {
            foreach (KaStagedOrderCustomLoadQuestionData stagedOrderData in stagedorder.CustomLoadQuestionDatas) {
                if (stagedOrderData.CustomLoadQuestionsDataId == inspectionDataId)
                    return stagedOrderData;
            }
            return null;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e) {
            DisconnectFromScale();
            LfControllers.Close();
        }

        #region " Bay Directions "
        private void GetBay() {
            CurrentPanelStatus.BayAssigned = null;
            if (Database.Settings.BayDirectionEnabled) {
                List<Guid> productList = new List<Guid>();
                List<Guid> ownerList = new List<Guid>();
                List<Guid> customerList = new List<Guid>();
                GetProductsForOrder(ref productList, ref ownerList, ref customerList);
                Dictionary<int, TerminalSettings.BaySetting> bayAvailableList = GetBaysValidForOrder();
                DateTime bayLastUsedDate = DateTime.MinValue.AddMilliseconds(1);
                int tempBayToUse = -1;
                foreach (int baycounter in bayAvailableList.Keys) {
                    if (tempBayToUse == -1 || Database.Settings.Bays[baycounter].LastUsedTime < bayLastUsedDate) {
                        bayLastUsedDate = Database.Settings.Bays[baycounter].LastUsedTime;
                        tempBayToUse = baycounter;
                    }
                }

                if (tempBayToUse != -1) {
                    CurrentPanelStatus.BayAssigned = Database.Settings.Bays[tempBayToUse];
                    Database.Settings.Bays[tempBayToUse].LastUsedTime = DateTime.Now;
                }
            }
        }

        private Dictionary<int, TerminalSettings.BaySetting> GetBaysValidForOrder() {
            List<Guid> productList = new List<Guid>();
            List<Guid> ownerList = new List<Guid>();
            List<Guid> customerList = new List<Guid>();
            GetProductsForOrder(ref productList, ref ownerList, ref customerList);

            Dictionary<int, TerminalSettings.BaySetting> bayAvailableList = new Dictionary<int, TerminalSettings.BaySetting>();

            int bayCounter = 0;
            while (bayCounter < Database.Settings.Bays.Count) {
                TerminalSettings.BaySetting bayInfo = Database.Settings.Bays[bayCounter];
                if (CurrentPanelStatus.Order != null || CurrentPanelStatus.ReceivingPurchaseOrder != null) {
                    // Order has been chosen, so limit the bays to that database.
                    if (bayInfo.Enabled &&
                        ((CurrentPanelStatus.Order != null && bayInfo.AvailableForLoadoutOrders) || (CurrentPanelStatus.ReceivingPurchaseOrder != null && bayInfo.AvailableForReceivingOrders)) &&
                        (CurrentPanelStatus.Order == null || bayInfo.BayAllowsProducts(productList)) &&
                        bayInfo.BayAllowsOwner(ownerList) &&
                        bayInfo.BayAllowsCustomerOrSupplier(customerList) &&
                        (CurrentPanelStatus.Carrier == null || bayInfo.BayAllowsCarrier(CurrentPanelStatus.Carrier.Id)) &&
                        (CurrentPanelStatus.Transport == null || bayInfo.BayAllowsTransport(CurrentPanelStatus.Transport.Id)) &&
                        (CurrentPanelStatus.Driver == null || bayInfo.BayAllowsDriver(CurrentPanelStatus.Driver.Id))) {
                        try {
                            // Check the bulk products
                            List<Guid> bulkProductList = GetBulkProductsForOrder(productList);
                            if (bayInfo.BayAllowsBulkIngredients(bulkProductList))
                                bayAvailableList.Add(bayCounter, Database.Settings.Bays[bayCounter]);
                        } catch (RecordNotFoundException) { }
                    }
                } else {
                    // Order has not been chosen, so all bays are available.  Just need to line up the bays with the correct database.
                    if (bayInfo.Enabled &&
                        bayInfo.BayAllowsProducts(productList) &&
                        bayInfo.BayAllowsOwner(ownerList) &&
                        bayInfo.BayAllowsCustomerOrSupplier(customerList) &&
                        (CurrentPanelStatus.Carrier == null || bayInfo.BayAllowsCarrier(CurrentPanelStatus.Carrier.Id)) &&
                        (CurrentPanelStatus.Transport == null || bayInfo.BayAllowsTransport(CurrentPanelStatus.Transport.Id)) &&
                        (CurrentPanelStatus.Driver == null || bayInfo.BayAllowsDriver(CurrentPanelStatus.Driver.Id))) {
                        try {
                            // Check the bulk products
                            List<Guid> bulkProductList = GetBulkProductsForOrder(productList);
                            if (bayInfo.BayAllowsBulkIngredients(bulkProductList))
                                bayAvailableList.Add(bayCounter, Database.Settings.Bays[bayCounter]);
                        } catch (RecordNotFoundException) { }
                    }
                }
                bayCounter += 1;
            }
            return bayAvailableList;
        }

        public void GetProductsForOrder(ref List<Guid> productList, ref List<Guid> ownerList, ref List<Guid> customerList) {
            productList.Clear();
            ownerList.Clear();
            customerList.Clear();
            if (CurrentPanelStatus.ReceivingPurchaseOrder != null) {
                KaReceivingPurchaseOrder orderInfo = CurrentPanelStatus.ReceivingPurchaseOrder;
                ownerList.Add(orderInfo.OwnerId);
                customerList.Add(orderInfo.SupplierAccountId);
            } else if (CurrentPanelStatus.Order != null) {
                KaOrder orderInfo = CurrentPanelStatus.Order;
                foreach (KaOrderItem orderEntry in orderInfo.OrderItems) {
                    if (!orderEntry.Deleted && !productList.Contains(orderEntry.ProductId)) productList.Add(orderEntry.ProductId);
                }
                if (!ownerList.Contains(orderInfo.OwnerId)) ownerList.Add(orderInfo.OwnerId);
                foreach (KaOrderCustomerAccount orderCust in orderInfo.OrderAccounts) {
                    if (!orderCust.Deleted && !customerList.Contains(orderCust.CustomerAccountId)) customerList.Add(orderCust.CustomerAccountId);
                }
            }
        }

        public List<Guid> GetBulkProductsForOrder(List<Guid> productList) {
            List<Guid> bulkIngredientList = new List<System.Guid>();
            if (CurrentPanelStatus.ReceivingPurchaseOrder != null) {
                bulkIngredientList.Add(CurrentPanelStatus.ReceivingPurchaseOrder.BulkProductId);
            } else {
                foreach (Guid productId in productList) {
                    KaProduct productInfo = new KaProduct(Tm2Database.Connection, productId);
                    int bulkProductCountForThisLocation = 0;
                    for (int bulkEntryCounter = 0; bulkEntryCounter < productInfo.ProductBulkItemCount; bulkEntryCounter++) {
                        KaProductBulkProduct bulkItem = productInfo.ProductBulkItems[bulkEntryCounter];
                        if (!bulkItem.Deleted && !bulkIngredientList.Contains(bulkItem.BulkProductId) && bulkItem.LocationId == LfDatabase.LocationId) {
                            bulkIngredientList.Add(bulkItem.BulkProductId);
                            bulkProductCountForThisLocation += 1;
                        }
                    }
                    if (bulkProductCountForThisLocation == 0)
                        throw new RecordNotFoundException($"No bulk products assigned for {productInfo.Name} for location { new KaLocation(Tm2Database.Connection, LfDatabase.LocationId).Name}");
                }
            }
            return bulkIngredientList;
        }
        #endregion
    }
}
