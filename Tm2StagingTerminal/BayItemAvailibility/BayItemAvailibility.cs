﻿using KahlerAutomation.KaTm2Database;
using System.Data.OleDb;
using System.Xml;
using System;

namespace Tm2StagingTerminal {
    [Serializable()]
    public class BayItemAvailibility {
        #region  Enumerations  
        public enum ItemAvailable {
            NotSet,
            No,
            Yes,
        }
        #endregion

        #region  Properties 
        private string _name = "Unknown item";
        [System.Xml.Serialization.XmlIgnore]
        public string Name {
            get {
                try {
                    OleDbDataReader rdr = Tm2Database.ExecuteReader(Tm2Database.Connection, $"SELECT name, deleted FROM {_tableName} WHERE id = {Database.Q(_recordId)}");
                    if (rdr.Read()) {
                        _name = (string)rdr["name"];
                        if ((bool)rdr["deleted"]) {
                            _name += " (deleted)";
                        }
                    }
                    rdr.Close();
                } catch (Exception) {
                    _name = "";
                }
                return _name;
            }
            set { _name = value; }
        }

        private string _tableName = "";
        public string TableName {
            get { return _tableName; }
            set { _tableName = value; }
        }

        private Guid _recordId = Guid.Empty;
        public Guid RecordId {
            get { return _recordId; }
            set { _recordId = value; }
        }

        private ItemAvailable _availability = ItemAvailable.NotSet;
        public ItemAvailable Availability {
            get { return _availability; }
            set { _availability = value; }
        }
        #endregion

        public override string ToString() {
            return Name;
        }
    }
}
