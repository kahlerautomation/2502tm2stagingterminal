﻿using KahlerAutomation.KaTm2Database;
using System.Windows.Forms;

namespace Tm2StagingTerminal {
    public partial class ItemAvailabiltyCheckBox : CheckBox {
        public ItemAvailabiltyCheckBox(string textDisplayed, BayItemAvailibility value) {
            InitializeComponent();
            this.Text = textDisplayed;
            _value = value;
        }

        private BayItemAvailibility _value;
        public BayItemAvailibility Value {
            get { return _value; }
        }
        public override string ToString() {
            return this.Text;
        }
    }
}
