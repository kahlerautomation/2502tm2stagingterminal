﻿using System;
using KahlerAutomation.KaTm2Database;
using System.Data.OleDb;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Tm2StagingTerminal {
    class Database {
        public const string APP_ID = "Origination Staging Terminal";
        public const string ENGLISH = "en";
        public const string SN_PALLETED_UNITS = "PalletedUnits";
        public const string SN_PALLET_TARE_WEIGHT = "PalletTareWeight/UnitId:{0}";
        public const string SN_PALLET_TARE_UNIT = "PalletTareUnit/UnitId:{0}";
        public const string SN_PRODUCT_TAG = "ProductTag/ProductId:{0}";
        public const string SN_PALLET_OVER_PERCENT = "PalletOverPercent";
        public const string SN_MAX_ALLOWED_PALLET_OVERAGE = "MaxAllowedPalletOverage";
        public const string SN_DEFAULT_PALLET_WEIGHT = "DefaultPalletWeight";
        public const string SN_REQUIRE_RESTRICTED_USE_OWNERS = "RequireRestrictedUseOwners";
        public static Guid PREVIOUS_LOAD_INSPECTION_FIELD_ID = Guid.Parse("628CD5BE-F22E-476D-A437-F4774F04CE8F");
        public static Guid TRUCK_CLEANED_INSPECTION_FIELD_ID = Guid.Parse("42E64941-20DA-48E5-8966-4D1519D2F879");
        public static Guid TRUCK_NOT_HAULED_INSPECTION_FIELD_ID = Guid.Parse("3699F6BE-89DE-4CF2-A9C9-76BBAF5428AA");
        public const string SN_BAY_ITEM_AVAILABILITY = "@{0}/{1}/BayItemAvailability/BayId:{2}/TableName:{3}";

        static private string _applicationIdentifier = "";
        static public string ApplicationIdentifier {
            get {
                if (_applicationIdentifier.Trim().Length == 0) _applicationIdentifier = Dns.GetHostName() + "/" + APP_ID;
                return _applicationIdentifier;
            }
        }

        static public string ApplicationUsername {
            get {
                if (CurrentPanelStatus.Driver != null)
                    return CurrentPanelStatus.Driver.Name;
                else if (_currentUser != null)
                    return _currentUser.Name;
                else
                    return "";
            }
        }

        static private string _computerName = System.Net.Dns.GetHostName();
        static public string ComputerName {
            get { return _computerName; }
            set { _computerName = value; }
        }

        static private KaUser _currentUser = null;
        static public KaUser CurrentUser {
            get { return _currentUser; }
            set { _currentUser = value; }
        }

        static private bool _dbOk = false;
        static public bool DbOk {
            get { return _dbOk; }
            set { _dbOk = value; }
        }

        static public Guid UserId {
            get {
                if (_currentUser == null)
                    return Guid.Empty;
                else
                    return _currentUser.Id;
            }
            set {
                try {
                    _currentUser = new KaUser(Tm2Database.Connection, value);
                } catch (RecordNotFoundException) {
                    _currentUser = new KaUser();
                }
                if (_settings != null) _settings.UserId = value;
            }
        }

        static public DateTime SQL_MIN_DATE {
            get { return DateTime.Parse("1/1/1753 00:00:00"); }
        }

        static public string Q(object value) {
            return KahlerAutomation.KaTm2Database.Database.Q(value);
        }

        static public object IsNull(object value, object defaultValue) {
            return KahlerAutomation.KaTm2Database.Database.IsNull(value, defaultValue);
        }

        public static string GetSettingName(string name) {
            return "Origination/" + name;
        }

        #region Settings
        internal static void InitializeSettings() {
            OleDbConnection connection = Tm2Database.Connection;
            try {
                _settings = (TerminalSettings)Tm2Database.FromXml(KaSetting.GetSetting(connection, string.Format(SN_SETTINGS, ComputerName, APP_ID), ""), typeof(TerminalSettings));
            } catch (Exception) {
                _settings = new TerminalSettings();
            }
            try {
                _currentUser = new KaUser(connection, _settings.UserId);
            } catch (RecordNotFoundException) {
                _currentUser = new KaUser();
            }
            try {
                _requireRestrictedUseOwners = (List<Guid>)Tm2Database.FromXml(KaSetting.GetSetting(connection, GetSettingName(SN_REQUIRE_RESTRICTED_USE_OWNERS), ""), typeof(List<Guid>));
            } catch (Exception) {
                _requireRestrictedUseOwners = new List<Guid>();
            }
        }

        public const string SN_SETTINGS = "@{0}/{1}/Settings";
        static private TerminalSettings _settings;
        static public TerminalSettings Settings {
            get { return _settings; }
            set {
                _settings = value;
            }
        }
        #endregion

        static public string DeliveryTicketUrlForOwner(Guid ownerId) {
            return KaSetting.GetSetting(Tm2Database.Connection, "TerminalManagement2/TicketAddress/OwnerId=" + ownerId.ToString(), "http://localhost/TerminalManagement2/ticket.aspx");
        }

        static public string ReceivingPoWebTicketUrlForOwner(Guid ownerId) {
            string bogusEntry = Guid.NewGuid().ToString();
            string ownerTicketAddress = KaSetting.GetSetting(Tm2Database.Connection, $"Receiving_PO_Web_Ticket_Address/OwnerId={ ownerId.ToString()}", bogusEntry, false, null);
            if (ownerTicketAddress == bogusEntry) ownerTicketAddress = KaSetting.GetSetting(Tm2Database.Connection, "Receiving_PO_Web_Ticket_Address", "http://localhost/TerminalManagement2/ReceivingTicket.aspx", false, null);
            return ownerTicketAddress;
        }

        #region Custom Load Questions
        private static List<Guid> _requireRestrictedUseOwners = new List<Guid>();
        public static List<Guid> RequireRestrictedUseOwners {
            get { return _requireRestrictedUseOwners; }
            set { _requireRestrictedUseOwners = value; }
        }

        private static double _palletOverPercent = 5;
        public static double PalletOverPercent {
            get {
                double.TryParse(KaSetting.GetSetting(Tm2Database.Connection, GetSettingName(SN_PALLET_OVER_PERCENT), "5"), out _palletOverPercent);
                return _palletOverPercent;
            }
        }

        private static double _maxAllowedPalletOverage = 200;
        public static double MaxAllowedPalletOverage {
            get {
                double.TryParse(KaSetting.GetSetting(Tm2Database.Connection, GetSettingName(SN_MAX_ALLOWED_PALLET_OVERAGE), "200"), out _maxAllowedPalletOverage);
                return _maxAllowedPalletOverage;
            }
        }

        private static double _defaultPalletWeight = 60;
        public static double DefaultPalletWeight {
            get {
                double.TryParse(KaSetting.GetSetting(Tm2Database.Connection, GetSettingName(SN_DEFAULT_PALLET_WEIGHT), "60"), out _defaultPalletWeight);
                return _defaultPalletWeight;
            }
        }
        #endregion

        public static void CreateEventLogEntry(string message) {
            KaEventLog entry = new KaEventLog() {
                ApplicationIdentifier = APP_ID,
                ApplicationVersion = Tm2Database.FormatVersion(System.Reflection.Assembly.GetEntryAssembly().GetName().Version, "X"),
                Category = KaEventLog.Categories.Warning,
                Computer = Dns.GetHostName(),
                Description = message
            };
            entry.SqlInsert(Tm2Database.Connection, null, Database.ApplicationIdentifier, Database.ApplicationUsername);
        }

        public static bool IsPalletedLoad(KaStagedOrder stagedOrder) {
            foreach (KaStagedOrderOrder soo in stagedOrder.Orders) {
                if (IsPalletedLoad(new KaOrder(Tm2Database.Connection, soo.OrderId)))
                    return true;
            }
            return false;
        }

        public static bool IsPalletedLoad(KaOrder order) {
            List<Guid> palletedUnits = GetPalletedUnits();
            Dictionary<Guid, KaProduct> products = new Dictionary<Guid, KaProduct>();

            foreach (KaOrderItem item in order.OrderItems) {
                try {
                    if (IsProductPalleted(item.ProductId, products, palletedUnits))
                        return true;
                } catch (RecordNotFoundException) {/* suppress */ }
            }
            return false;
        }

        public static bool IsProductPalleted(Guid productId, Dictionary<Guid, KaProduct> products, List<Guid> palletedUnits) {
            return IsProductPalleted(KaProduct.GetProduct(Tm2Database.Connection, productId, products, null), palletedUnits);
        }

        public static bool IsProductPalleted(KaProduct product, List<Guid> palletedUnits) {
            return palletedUnits.Contains(product.DefaultUnitId);
        }

        public static List<Guid> GetPalletedUnits() {
            List<Guid> palletedUnits;
            try {
                palletedUnits = (List<Guid>)Tm2Database.FromXml(KaSetting.GetSetting(Tm2Database.Connection, GetSettingName(SN_PALLETED_UNITS), ""), typeof(List<Guid>));
            } catch (Exception) {
                palletedUnits = new List<Guid>();
            }
            return palletedUnits;
        }

        public static double GetProductPalletedWeight(Guid unitId, Dictionary<Guid, KaUnit> units) {
            return KaUnit.GetUnit(unitId, units).Factor;
        }

        public static KaQuantity GetProductPalletTareWeight(Guid unitId) {
            OleDbConnection connection = Tm2Database.Connection;
            double tareWeight = 0;
            Guid tareUnit = KahlerAutomation.KaTm2LoadFramework.LfDatabase.DefaultMassUnitId;
            double.TryParse(KaSetting.GetSetting(connection, GetSettingName(String.Format(SN_PALLET_TARE_WEIGHT, unitId.ToString())), "0"), out tareWeight);
            Guid.TryParse(KaSetting.GetSetting(connection, GetSettingName(String.Format(SN_PALLET_TARE_UNIT, unitId.ToString())), tareUnit.ToString()), out tareUnit);

            return new KaQuantity(tareWeight, tareUnit);
        }

        private static KaRatio _emptyDensity = null;
        public static KaRatio EmptyDensity {
            get {
                if (_emptyDensity == null) {
                    _emptyDensity = new KaRatio(0,KahlerAutomation.KaTm2LoadFramework.LfDatabase.DefaultMassUnitId, KahlerAutomation.KaTm2LoadFramework.LfDatabase.DefaultVolumeUnitId);
                }
                return _emptyDensity;
            }
        }
    }
}