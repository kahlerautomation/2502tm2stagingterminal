﻿using System;
using KahlerAutomation.KaTm2Database;

namespace Tm2StagingTerminal {
    public class WeightValidEventArgs : EventArgs {
        public double Weight { get; set; }
        public KaUnit.Unit Unit { get; set; }
        public WeightValidEventArgs() {
            Weight = 0;
            Unit = KaUnit.Unit.Pounds;
        }
        public WeightValidEventArgs(double weight, KaUnit.Unit unit) {
            Weight = weight;
            Unit = unit;
        }
    }
}
