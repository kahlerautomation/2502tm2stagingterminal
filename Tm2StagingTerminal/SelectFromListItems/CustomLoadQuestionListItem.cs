﻿namespace Tm2StagingTerminal.SelectFromListItems {
    class CustomLoadQuestionListItem {
        public CustomLoadQuestionListItem(string id, string p) {
            _id = id;
            _parameter = p;
        }

        private string _id;
        public string Id {
            get { return _id; }
        }

        private string _parameter;
        public string Parameter {
            get { return _parameter; }
        }
    }
}
