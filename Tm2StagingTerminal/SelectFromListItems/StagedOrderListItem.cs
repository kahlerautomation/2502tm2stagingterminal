﻿using System;
using KahlerAutomation.KaTm2Database;

namespace Tm2StagingTerminal.SelectFromListItems {
    class StagedOrderListItem {
        private KaStagedOrder _stagedOrder;

        public StagedOrderListItem(KaStagedOrder stagedOrder) {
            _stagedOrder = stagedOrder;
            try { // to load the order record(s) associated with this staged order...
                _orderNumber = "";
                _accounts = "";

                foreach (KaStagedOrderOrder soo in _stagedOrder.Orders) {
                    KaOrder order = new KaOrder(Tm2Database.Connection, soo.OrderId);
                    if (!_orderNumber.Contains(order.Number)) {
                        _orderNumber += (_orderNumber.Length > 0 ? ", " : "") + order.Number;
                    }

                    foreach (KaOrderCustomerAccount oca in order.OrderAccounts) {
                        try {
                            KaCustomerAccount account = new KaCustomerAccount(Tm2Database.Connection, oca.CustomerAccountId);
                            _accounts += (_accounts.Length > 0 ? ", " : "") + account.Name;
                            if (_stagedOrder.Orders.Count > 1 || order.OrderAccounts.Count > 1) {
                                _accounts += " (";
                                if (_stagedOrder.Orders.Count > 1) _accounts += $"Order {order.Number}";
                                if (_stagedOrder.Orders.Count > 1 && order.OrderAccounts.Count > 1) _accounts += ": ";
                                if (order.OrderAccounts.Count > 1) _accounts += String.Format("{0:0}%", oca.Percentage);
                                _accounts += ")";
                            }
                        } catch (RecordNotFoundException) { /* suppress exception */ }
                    }
                }
            } catch (RecordNotFoundException) { // leave the order number and accounts blank
                _orderNumber = "";
                _accounts = "";
            }
            _transport = "";
            foreach (KaStagedOrderTransport sot in _stagedOrder.Transports) {
                try { // to load the transport record...
                    KaTransport transport = new KaTransport(Tm2Database.Connection, sot.TransportId);
                    if (!_transport.Contains(transport.Name) && !_transport.Contains(transport.Number)) {
                        _transport += (_transport.Length > 0 ? ", " : "") + transport.Name + (transport.Number.Length > 0 ? " (" + transport.Number + ")" : "");
                    }
                } catch (RecordNotFoundException) { /* suppress exception */ }
            }

            _stagedAt = String.Format("{0:g}", _stagedOrder.AddedAt);
        }

        public Guid Id {
            get { return _stagedOrder.Id; }
        }

        private string _orderNumber = "";
        public string OrderNumber {
            get { return _orderNumber; }
        }

        private string _accounts = "";
        public string Accounts {
            get { return _accounts; }
        }

        private string _transport = "";
        public string Transport {
            get { return _transport; }
        }

        private string _stagedAt = "";
        public string StagedAt {
            get { return _stagedAt; }
        }
    }
}
