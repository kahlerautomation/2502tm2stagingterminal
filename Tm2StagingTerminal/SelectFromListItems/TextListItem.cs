﻿namespace Tm2StagingTerminal.SelectFromListItems {
    class TextListItem {
        public TextListItem(string textToDisplay) {
            _textToDisplay = textToDisplay;
        }
        private string _textToDisplay;

        public string TextToDisplay {
            get { return _textToDisplay; }
            set { _textToDisplay = value; }
        }

    }
}
