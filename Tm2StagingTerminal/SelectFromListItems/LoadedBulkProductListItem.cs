﻿using System;
using KahlerAutomation.KaTm2Database;


namespace Tm2StagingTerminal.SelectFromListItems {
    [Serializable()]
    public class LoadedBulkProductListItem {
        public LoadedBulkProductListItem() { }

        public LoadedBulkProductListItem(KaBulkProduct bulkProduct, KaOrder order, KaOrderItem orderItem, KaProduct product) {
            _bulkProduct = bulkProduct;
            _order = order;
            _orderItem = orderItem;
            _product = product;
        }

        private KaBulkProduct _bulkProduct;
        public string BulkProduct {
            get { return _bulkProduct.Name; }
        }
        public Guid BulkProductId {
            get { return _bulkProduct.Id; }
        }

        private KaOrder _order;
        public string OrderNumber {
            get { return _order.Number; }
        }

        private KaOrderItem _orderItem;
        public Guid OrderItemId {
            get { return _orderItem.Id; }
        }

        private KaProduct _product;
        public string Product {
            get { return _product.Name; }
        }
        public Guid ProductId {
            get { return _product.Id; }
        }

        public string Xml {
            get { return Tm2Database.ToXml(this, typeof(SelectFromListItems.LoadedBulkProductListItem)); }
        }
    }
}
