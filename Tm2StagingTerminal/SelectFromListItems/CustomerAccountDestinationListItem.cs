﻿using System;
using KahlerAutomation.KaTm2Database;

namespace Tm2StagingTerminal.SelectFromListItems {
    class CustomerAccountDestinationListItem {
        private KaCustomerAccountLocation _location;

        public CustomerAccountDestinationListItem(KaCustomerAccountLocation location) {
            _location = location;
        }

        public Guid Id {
            get { return _location.Id; }
        }

        public string Name {
            get { return _location.Name; }
        }

        public string Address {
            get { return _location.Street + ", " + _location.City + ", " + _location.State + " " + _location.ZipCode; }
        }

        public string CrossReference {
            get { return _location.CrossReference; }
        }
    }
}
