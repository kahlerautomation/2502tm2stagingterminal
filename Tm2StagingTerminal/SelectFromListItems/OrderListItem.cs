﻿using System;
using KahlerAutomation.KaTm2Database;
using KahlerAutomation.KaTm2LoadFramework;
using System.Collections.Generic;
using System.Data.OleDb;

namespace Tm2StagingTerminal.SelectFromListItems {
    class OrderListItem {
        private KaOrder _order = null;
        private KaReceivingPurchaseOrder _receivingPurchaseOrder = null;
        private KaStagedOrder _stagedOrder = null;
        private KaInProgress _inprogressOrder = null;
        public OrderListItem(KaOrder order) {
            OleDbConnection connection = Tm2Database.Connection;
            _order = order;
            _id = order.Id;
            _number = order.Number;
            _releaseNumber = order.ReleaseNumber;
            _account = ""; // initialize the account list string
            foreach (KaOrderCustomerAccount oca in order.OrderAccounts) {
                try { // to get the customer account name and percentage
                    KaCustomerAccount account = new KaCustomerAccount(connection, oca.CustomerAccountId);
                    _account += (_account.Length > 0 ? ", " : "") + account.Name + (order.OrderAccounts.Count > 1 ? String.Format(" ({0:0}%)", oca.Percentage) : "");
                } catch (RecordNotFoundException) { /* suppress exception */ }
            }

            foreach (KaOrderItem oi in order.OrderItems) {
                KaProduct p = new KaProduct(connection, oi.ProductId);
                KaUnit u = new KaUnit(connection, oi.UnitId);
                _product += (_product.Length > 0 ? ", " : "") + p.Name + " (" + (oi.Request - oi.Delivered) + " " + u.Abbreviation + ")";
            }

            _poNumber = order.PurchaseOrder;

            _owner = new KaOwner(connection, order.OwnerId).Name;

            try {
                KaBranch b = new KaBranch(connection, order.BranchId);
                _branch = b.Name;
            } catch (RecordNotFoundException) { /* suppress exception */ }
        }

        public OrderListItem(KaReceivingPurchaseOrder receivingOrder) {
            _receivingPurchaseOrder = receivingOrder;
            AssignReceivingPurchaseOrderValues(receivingOrder);
            _id = receivingOrder.Id;
        }

        public OrderListItem(KaInProgress ipo) {
            OleDbConnection connection = Tm2Database.Connection;
            _inprogressOrder = ipo;
            _id = ipo.Id;
            _enteredAt = DateTime.MaxValue;
            AssignReceivingPurchaseOrderValues(new KaReceivingPurchaseOrder(connection, ipo.ReceivingPurchaseOrderId));
            Dictionary<Guid, KaUnit> units = new Dictionary<Guid, KaUnit>();
            foreach (KaInProgressWeighment weighment in ipo.Weighments) {
                if (weighment.TransportId != Guid.Empty) {
                    KaTransport transport = new KaTransport(connection, weighment.TransportId);
                    if (!_transport.Contains(transport.Name)) {
                        _transport += (_transport.Length > 0 ? ", " : "") + transport.Name;
                        KaUnit tareUnit = KaUnit.GetUnit(connection, weighment.UnitId, units);
                        if (ipo.ReceivingPurchaseOrderId != Guid.Empty && weighment.Gross > 0) {
                            _transport += string.Format(" ({0:" + tareUnit.UnitPrecision + "} {1})", weighment.Gross, tareUnit.Abbreviation);
                        } else if (weighment.Tare > 0) {
                            _transport += string.Format(" ({0:" + tareUnit.UnitPrecision + "} {1})", weighment.Tare, tareUnit.Abbreviation);
                        }
                    }
                    try {
                        KaTransportInFacility tif = new KaTransportInFacility(connection, weighment.TransportInFacilityId, null);
                        if (tif.EnteredAt > new DateTime(1900, 1, 1) && tif.EnteredAt < _enteredAt)
                            _enteredAt = tif.EnteredAt;
                    } catch (RecordNotFoundException) { /* suppress */ }
                }
            }
            try { // to get the customer account name and percentage
                KaCarrier carrier = new KaCarrier(connection, ipo.CarrierId);
                _carrier = carrier.Name;
            } catch (RecordNotFoundException) { /* suppress exception */ }
            try { // to get the customer account name and percentage
                KaDriver driver = new KaDriver(connection, ipo.DriverId);
                _driver = driver.Name;
            } catch (RecordNotFoundException) { /* suppress exception */ }
            if (_enteredAt == DateTime.MaxValue) {
                try {
                    KaDriverInFacility tid = new KaDriverInFacility(connection, ipo.DriverInFacilityId, null);
                    if (tid.EnteredAt > new DateTime(1900, 1, 1))
                        _enteredAt = tid.EnteredAt;
                } catch (RecordNotFoundException) { /* suppress */ }
            }
            if (_enteredAt == DateTime.MaxValue) {
                _enteredAt = ipo.Created;
            }
        }

        private void AssignReceivingPurchaseOrderValues(KaReceivingPurchaseOrder receivingOrder) {
            OleDbConnection connection = Tm2Database.Connection;
            _number = receivingOrder.Number;
            try { // to get the customer account name and percentage
                KaSupplierAccount account = new KaSupplierAccount(connection, receivingOrder.SupplierAccountId);
                _account = account.Name;
            } catch (RecordNotFoundException) { /* suppress exception */ }

            KaBulkProduct p = new KaBulkProduct(connection, receivingOrder.BulkProductId);
            KaUnit u = new KaUnit(connection, receivingOrder.UnitId);
            _product = p.Name + " (" + string.Format("{0:" + u.UnitPrecision + "}", Math.Max(0, receivingOrder.Purchased - receivingOrder.Delivered)) + " " + u.Abbreviation + ")";

            _owner = new KaOwner(connection, receivingOrder.OwnerId).Name;
        }

        public OrderListItem(KaStagedOrder so) {
            OleDbConnection connection = Tm2Database.Connection;
            _stagedOrder = so;
            _id = so.Id;
            _number = "";
            _account = "";
            _releaseNumber = "";
            _product = "";
            _poNumber = "";
            _owner = "";
            _branch = "";
            _transport = "";
            _carrier = "";
            _driver = "";
            try { // to load the order record(s) associated with this staged order...
                foreach (KaStagedOrderOrder soo in _stagedOrder.Orders) {
                    KaOrder order = new KaOrder(connection, soo.OrderId);
                    if (!_number.Contains(order.Number)) {
                        _number += (_number.Length > 0 ? ", " : "") + order.Number;
                    }
                    if (!_releaseNumber.Contains(order.ReleaseNumber))
                        _releaseNumber += (_releaseNumber.Length > 0 ? ", " : "") + order.ReleaseNumber;

                    foreach (KaOrderCustomerAccount oca in order.OrderAccounts) {
                        try {
                            KaCustomerAccount account = new KaCustomerAccount(connection, oca.CustomerAccountId);
                            _account += (_account.Length > 0 ? ", " : "") + account.Name;
                            if (_stagedOrder.Orders.Count > 1 || order.OrderAccounts.Count > 1) {
                                _account += " (";
                                if (_stagedOrder.Orders.Count > 1) _account += $"Order {order.Number}";
                                if (_stagedOrder.Orders.Count > 1 && order.OrderAccounts.Count > 1) _account += ": ";
                                if (order.OrderAccounts.Count > 1) _account += String.Format("{0:0}%", oca.Percentage);
                                _account += ")";
                            }
                        } catch (RecordNotFoundException) { /* suppress exception */ }
                    }

                    if (!_poNumber.Contains(order.PurchaseOrder))
                        _poNumber += (_poNumber.Length > 0 ? ", " : "") + order.PurchaseOrder;

                    string owner = new KaOwner(connection, order.OwnerId).Name;
                    if (!_owner.Contains(owner))
                        _owner += (_owner.Length > 0 ? ", " : "") + owner;

                    try {
                        KaBranch b = new KaBranch(connection, order.BranchId);
                        if (!_branch.Contains(b.Name))
                            _branch = b.Name;
                    } catch (RecordNotFoundException) { /* suppress exception */ }

                    Dictionary<Guid, KaUnit> units = new Dictionary<Guid, KaUnit>();
                    foreach (KaStagedOrderTransport stagedTransport in so.Transports) {
                        if (stagedTransport.TransportId != Guid.Empty) {
                            KaTransport transport = new KaTransport(connection, stagedTransport.TransportId);
                            if (!_transport.Contains(transport.Name)) {
                                _transport += (_transport.Length > 0 ? ", " : "") + transport.Name;
                                KaUnit tareUnit = KaUnit.GetUnit(connection, stagedTransport.TareUnitId, units);
                                if (stagedTransport.TareWeight > 0) {
                                    _transport += string.Format(" ({0:" + tareUnit.UnitPrecision + "} {1})", stagedTransport.TareWeight, tareUnit.Abbreviation);
                                }
                            }
                        }
                    }
                    try { // to get the customer account name and percentage
                        KaCarrier carrier = new KaCarrier(connection, so.CarrierId);
                        _carrier = carrier.Name;
                    } catch (RecordNotFoundException) { /* suppress exception */ }
                    try { // to get the customer account name and percentage
                        KaDriver driver = new KaDriver(connection, so.DriverId);
                        _driver = driver.Name;
                    } catch (RecordNotFoundException) { /* suppress exception */ }
                }
                Dictionary<Guid, KaQuantity> orderItemAssignedAmounts = so.GetOrderItemAssignedAmounts(connection, null, LfDatabase.DefaultMassUnitId);
                foreach (Guid orderItemId in orderItemAssignedAmounts.Keys) {
                    KaOrderItem oi = new KaOrderItem(connection, orderItemId);
                    KaProduct p = new KaProduct(connection, oi.ProductId);
                    KaUnit u = new KaUnit(connection, orderItemAssignedAmounts[orderItemId].UnitId);
                    _product += (_product.Length > 0 ? ", " : "") + p.Name + string.Format(" ({0:" + u.UnitPrecision + "} {1})", orderItemAssignedAmounts[orderItemId].Numeric, u.Abbreviation);
                }
                _enteredAt = so.AddedAt;
                if (_enteredAt == DateTime.MaxValue) {
                    _enteredAt = so.Created;
                }
            } catch (RecordNotFoundException) { // leave the order number and accounts blank
                _number = "";
                _account = "";
                _releaseNumber = "";
                _product = "";
                _poNumber = "";
                _owner = "";
                _branch = "";
                _transport = "";
                _carrier = "";
                _driver = "";
            }
        }

        public Guid _id = Guid.Empty;
        public Guid Id {
            get { return _id; }
        }

        public string _number = "";
        public string Number {
            get { return _number; }
        }

        private string _account = "";
        public string Account {
            get { return _account; }
        }

        public string _releaseNumber = "";
        public string ReleaseNumber {
            get { return _releaseNumber; }
        }

        private string _product = "";
        public string Product {
            get { return _product; }
        }

        private string _poNumber = "";
        public string PoNumber {
            get { return _poNumber; }
        }

        private string _owner = "";
        public string Owner {
            get { return _owner; }
        }

        private string _branch = "";
        public string Branch {
            get { return _branch; }
        }

        private string _transport = "";
        public string Transport {
            get { return _transport; }
        }

        private string _carrier = "";
        public string Carrier {
            get { return _carrier; }
        }

        private string _driver = "";
        public string Driver {
            get { return _driver; }
        }

        public string OrderType {
            get {
                if (_receivingPurchaseOrder != null)
                    return Strings.ReceivingPurchaseOrder;
                else
                    return Strings.LoadoutOrder;
            }
        }

        private DateTime _enteredAt = DateTime.MinValue;
        public string EnteredAt {
            get {
                if (_enteredAt == DateTime.MinValue || _enteredAt == DateTime.MaxValue)
                    return "";
                else
                    return string.Format("{0:g}", _enteredAt);
            }
        }

    }
}
