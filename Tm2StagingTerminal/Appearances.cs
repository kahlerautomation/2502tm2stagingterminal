﻿using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;

namespace Tm2StagingTerminal
{
    class Appearances
    {
        static public void ScaleFonts(Form form, ArrayList affectedControls)
        {
            Graphics g = form.CreateGraphics(); // force the font size to fit the original form layout
            foreach (Control control in form.Controls)
                if (affectedControls.Count == 0 || affectedControls.IndexOf(control.GetType().Name) != -1)
                    control.Font = new Font(control.Font.Name, 96 * control.Font.Size / g.DpiX);
        }

        static public void ScaleFonts(Control control, ArrayList affectedControls)
        {
            Graphics g = control.CreateGraphics(); // force the font size to fit the original form layout
            foreach (Control c in control.Controls)
                if (affectedControls.Count == 0 || affectedControls.IndexOf(c.GetType().Name) != -1)
                    c.Font = new Font(c.Font.Name, 96 * c.Font.Size / g.DpiX);
        }

        static public void MaximizeFontSizeForControl(Control control, float maxSize, float minSize) {
            Graphics g = control.CreateGraphics();
            Font font;
            bool tooBig = true;
            do {
                font = new Font(control.Font.FontFamily.Name, 96 * maxSize / g.DpiX);
                string[] parts = control.Text.Split(' ');
                float height = g.MeasureString(control.Text, font).Height;
                int lines; // how many lines will fit on this control?
                if (height > 0 && parts.Length > 1) lines = (int)Math.Floor(control.Height / height); else lines = 1;
                int i = 0; // index though all the words in the text
                for (int line = 0; line < lines; line++) {
                    string lineText = ""; // start a new line
                    while (i < parts.Length && g.MeasureString(lineText + (lineText.Length > 0 ? " " : "") + parts[i], font).Width < control.Width)
                        lineText += (lineText.Length > 0 ? " " : "") + parts[i++]; // if the word fits, add it
                    if (i == parts.Length) break; // no more words
                }
                tooBig = i < parts.Length;
                maxSize -= 1; // try a font size smaller
            } while (tooBig && maxSize >= minSize);
            control.Font = font;
        }
    }
}
