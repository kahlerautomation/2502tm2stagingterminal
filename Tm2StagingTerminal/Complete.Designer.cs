﻿namespace Tm2StagingTerminal {
    partial class Complete : StagingTerminalBaseControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblPleaseDriveAhead = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblPleaseDriveAhead
            // 
            this.lblPleaseDriveAhead.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPleaseDriveAhead.Location = new System.Drawing.Point(0, 100);
            this.lblPleaseDriveAhead.Name = "lblPleaseDriveAhead";
            this.lblPleaseDriveAhead.Size = new System.Drawing.Size(1008, 557);
            this.lblPleaseDriveAhead.TabIndex = 9;
            this.lblPleaseDriveAhead.Text = "Please Drive Ahead";
            this.lblPleaseDriveAhead.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Complete
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.lblPleaseDriveAhead);
            this.Name = "Complete";
            this.Title = "Finished";
            this.Resize += new System.EventHandler(this.Complete_Resize);
            this.Controls.SetChildIndex(this.lblPleaseDriveAhead, 0);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label lblPleaseDriveAhead;
    }
}
