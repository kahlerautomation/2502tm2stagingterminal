﻿using System;
using System.Windows.Forms;

namespace Tm2StagingTerminal {
    public partial class StProgressBar : Form {
        public StProgressBar() {
            InitializeComponent();
        }

        public string ProgressBarLabel { get { return lblPleaseWait.Text; } set { SetPropertyValue(lblPleaseWait, "Text", value); } }
        public ProgressBar ProgressBar { get { return pbPleaseWait; } set { pbPleaseWait = value; } }
        public ProgressBarStyle ProgressStyle { get { return pbPleaseWait.Style; } set { SetPropertyValue(pbPleaseWait, "Style", value); } }
        public int ProgressMarqueeAnimationSpeed { get { return pbPleaseWait.MarqueeAnimationSpeed; } set { SetPropertyValue(pbPleaseWait, "MarqueeAnimationSpeed", value); } }
        public int ProgressValue { get { return pbPleaseWait.Value; } set { SetPropertyValue(pbPleaseWait, "Value", value); } }

        #region   Thread-Safe Delegate Subs  
        public delegate void SetPropertyValueCallback(System.Windows.Forms.Control obj, string propertyName, object value);
        public void SetPropertyValue(System.Windows.Forms.Control obj, string propertyName, object value) {
            if (this.InvokeRequired) {
                SetPropertyValueCallback d = new SetPropertyValueCallback(SetPropertyValue);
                this.BeginInvoke(d, new object[] { obj, propertyName, value });
            } else {
                obj.GetType().GetProperty(propertyName).SetValue(obj, value, null);
                try {
                    obj.Refresh();
                } catch (Exception) {
                }
            }
        }
        #endregion
    }
}
