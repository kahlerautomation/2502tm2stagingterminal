﻿namespace Tm2StagingTerminal {
    partial class Confirm : StagingTerminalBaseControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblDisclaimer = new System.Windows.Forms.Label();
            this.dgvConfirmInfo = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConfirmInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDisclaimer
            // 
            this.lblDisclaimer.BackColor = System.Drawing.Color.LightBlue;
            this.lblDisclaimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisclaimer.ForeColor = System.Drawing.Color.Black;
            this.lblDisclaimer.Location = new System.Drawing.Point(0, 102);
            this.lblDisclaimer.Name = "lblDisclaimer";
            this.lblDisclaimer.Size = new System.Drawing.Size(1024, 74);
            this.lblDisclaimer.TabIndex = 36;
            this.lblDisclaimer.Text = "By clicking OK, you acknowledge the\r\nreceipt of the products and quantities liste" +
    "d below.";
            this.lblDisclaimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvConfirmInfo
            // 
            this.dgvConfirmInfo.AllowUserToAddRows = false;
            this.dgvConfirmInfo.AllowUserToDeleteRows = false;
            this.dgvConfirmInfo.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.dgvConfirmInfo.BackgroundColor = System.Drawing.Color.White;
            this.dgvConfirmInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvConfirmInfo.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvConfirmInfo.ColumnHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvConfirmInfo.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvConfirmInfo.Location = new System.Drawing.Point(0, 177);
            this.dgvConfirmInfo.MultiSelect = false;
            this.dgvConfirmInfo.Name = "dgvConfirmInfo";
            this.dgvConfirmInfo.ReadOnly = true;
            this.dgvConfirmInfo.RowHeadersVisible = false;
            this.dgvConfirmInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvConfirmInfo.Size = new System.Drawing.Size(1017, 520);
            this.dgvConfirmInfo.TabIndex = 35;
            this.dgvConfirmInfo.TabStop = false;
            // 
            // Confirm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblDisclaimer);
            this.Controls.Add(this.dgvConfirmInfo);
            this.Name = "Confirm";
            this.Size = new System.Drawing.Size(1024, 700);
            this.Title = "Confirm";
            this.Resize += new System.EventHandler(this.Confirm_Resize);
            this.Controls.SetChildIndex(this.dgvConfirmInfo, 0);
            this.Controls.SetChildIndex(this.lblDisclaimer, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dgvConfirmInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label lblDisclaimer;
        internal System.Windows.Forms.DataGridView dgvConfirmInfo;
    }
}
