﻿using System;
using System.Threading;
using System.Management;
using System.Windows.Forms;
using System.Drawing.Printing;
using KahlerAutomation.KaTm2Database;
using System.Collections.Generic;

namespace Tm2StagingTerminal {
    class Printing {
        static private bool _fetchingWebPage = false;

        private static Queue<KaTicket> _ticketsToPrint = new Queue<KaTicket>();
        public static Queue<KaTicket> TicketsToPrint {
            get { return _ticketsToPrint; }
        }
        private static Queue<KaStagedOrder> _pickTicketsToPrint = new Queue<KaStagedOrder>();
        public static Queue<KaStagedOrder> PickTicketsToPrint {
            get { return _pickTicketsToPrint; }
        }
        private static Queue<KaStagedOrder> _truckInspectionCertificateToPrint = new Queue<KaStagedOrder>();
        public static Queue<KaStagedOrder> TruckInspectionCertificateToPrint {
            get { return _truckInspectionCertificateToPrint; }
        }
        private static Queue<KaInProgress> _receivingPickTicketsToPrint = new Queue<KaInProgress>();
        public static Queue<KaInProgress> ReceivingPickTicketsToPrint {
            get { return _receivingPickTicketsToPrint; }
        }
        private static Queue<KaReceivingTicket> _receivingTicketsToPrint = new Queue<KaReceivingTicket>();
        public static Queue<KaReceivingTicket> ReceivingTicketsToPrint {
            get { return _receivingTicketsToPrint; }
        }

        static private void WebBrowserDocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e) {
            _fetchingWebPage = false;
        }

        static public WebBrowser GetWebBrowser(string url) {
            WebBrowser obj = new WebBrowser();
            obj.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(WebBrowserDocumentCompleted);
            obj.Navigate(url);
            _fetchingWebPage = true;
            while (_fetchingWebPage) {
                Thread.Sleep(10);
                Application.DoEvents();
            }
            obj.DocumentCompleted -= new WebBrowserDocumentCompletedEventHandler(WebBrowserDocumentCompleted);
            return obj;
        }

        static public string GetDefaultPrinterName() {
            ManagementObjectCollection collection = new ManagementObjectSearcher("SELECT * FROM Win32_Printer").Get();
            foreach (ManagementObject obj in collection) if ((bool)obj["Default"]) return ((string)obj["Name"]).Trim();
            collection.Dispose();
            return "";
        }

        static public void SetDefaultPrinter(string printer) {
            printer = printer.Trim();
            ManagementObjectCollection collection = new ManagementObjectSearcher("SELECT * FROM Win32_Printer").Get();
            foreach (ManagementObject obj in collection) {
                if (((string)obj["Name"]).Trim() == printer) { // is this the printer that we want to use?
                    obj.InvokeMethod("SetDefaultPrinter", null); // if so, set it as the default printer
                    break;
                }
            }
            collection.Dispose();
            DateTime d = DateTime.Now; // wait for the default printer to change to printer
            while (GetDefaultPrinterName() != printer && DateTime.Now.Subtract(d).TotalSeconds < 30) Application.DoEvents(); // but only for 30 seconds
        }

        static public void PrintDeliveryTicket(KaTicket ticket) {
            string url = Database.DeliveryTicketUrlForOwner(ticket.OwnerId);
            if (url.Trim().Length > 0) { // only print/send a ticket if a ticket has been specified
                url += (url.Contains("ticket_id=") ? "" : (url.Contains("?") ? "&" : "?") + "ticket_id=") + ticket.Id.ToString();
                PrintUrltoPrinters(url, Database.Settings.OutboundTicketPrinters, $"Error printing ticket {ticket.Number}.");
            }
        }

        static public void PrintReceivingTicket(KaReceivingTicket ticket) {
            string url = Database.ReceivingPoWebTicketUrlForOwner(ticket.OwnerId);
            if (url.Trim().Length > 0) { // only print/send a ticket if a ticket has been specified
                url += (url.Contains("ticket_id=") ? "" : (url.Contains("?") ? "&" : "?") + "ticket_id=") + ticket.Id.ToString();
                PrintUrltoPrinters(url, Database.Settings.ReceivingProductTicketPrinters, $"Error printing receiving ticket {ticket.Number}.");
            }
        }

        static public void PrintPickTicket(KaStagedOrder stagedOrder) {
            if (stagedOrder != null) {
                bool ownerRequiresRestrictedUse = false;
                KaOrder order = null;
                foreach (KaStagedOrderOrder soo in stagedOrder.Orders) {
                    order = new KaOrder(Tm2Database.Connection, soo.OrderId);
                    if (Database.RequireRestrictedUseOwners.Contains(order.OwnerId)) ownerRequiresRestrictedUse = true;
                }
                if (Database.IsPalletedLoad(stagedOrder)) {
                    PrintPickTicket(stagedOrder, Database.Settings.BaggedProductPickTicketAddress, Database.Settings.InboundBaggedProductPrinters, $"Error printing palleted pick ticket for order {(order == null ? "Unknown" : order.Number)}.");
                } else {
                    PrintPickTicket(stagedOrder, Database.Settings.WebPickTicketAddress, Database.Settings.InboundBulkProductPrinters, $"Error printing pick ticket for order { (order == null ? "Unknown" : order.Number)}.");
                }
                if (ownerRequiresRestrictedUse) {
                    PrintPickTicket(stagedOrder, Database.Settings.TruckInspectionTicketAddress, Database.Settings.InboundTruckInspectionPrinters, $"Error printing truck inspection certificate for order { (order == null ? "Unknown" : order.Number)}.");
                }
            }
        }

        static public void PrintPickTicket(KaStagedOrder stagedOrder, string url, List<PrinterConfiguration> printerList, string errorCaption) {
            if (url.Trim().Length > 0) { // only print/send a ticket if a ticket has been specified
                url += (url.Contains("?") ? "&" : "?") + $"staged_order_id={ stagedOrder.Id.ToString()}";
                if (stagedOrder.Transports.Count > 0) {
                    KaStagedOrderTransport transport = stagedOrder.Transports[0];
                    url += $"&truck_id={transport.TransportId.ToString()}";
                    url += $"&scale_uom={transport.TareUnitId.ToString()}";
                    url += $"&tare_wt={transport.TareWeight.ToString()}";
                }
                PrintUrltoPrinters(url, printerList, errorCaption);
            }
        }

        static public void PrintReceivingPickTicket(KaInProgress inProgress) {
            string url = Database.Settings.WebPoPickTicketAddress;
            if (url.Trim().Length > 0) { // only print/send a ticket if a ticket has been specified
                if (inProgress.Weighments.Count > 0) {
                    KaInProgressWeighment weighment = inProgress.Weighments[inProgress.Weighments.Count - 1];
                    url += (url.Contains("?") ? "&" : "?") + $"weighment_id={ weighment.Id.ToString()}";
                    url += $"&transport_id={ weighment.TransportId.ToString()}";
                    url += $"&gross_wt={weighment.Gross}";
                } else {
                    url += (url.Contains("?") ? "&" : "?") + $"po_id={ inProgress.ReceivingPurchaseOrderId.ToString()}";
                }
                url += $"&carrier_id={ inProgress.CarrierId.ToString()}";
                url += $"&driver_id={ inProgress.DriverId.ToString()}";
                PrintUrltoPrinters(url, Database.Settings.ReceivingProductPickTicketPrinters, $"Error printing receiving pick ticket for order {new KaReceivingPurchaseOrder(Tm2Database.Connection, inProgress.ReceivingPurchaseOrderId).Number}.");
            }
        }

        static private void PrintUrltoPrinters(string url, List<PrinterConfiguration> printerList, string errorCaption) {
            string defaultPrinter = Printing.GetDefaultPrinterName(); // keep track of the original default printer so that we can set it back when we're done
            string printerName = "";
            foreach (PrinterConfiguration printer in printerList) {
                WebBrowser wb = null;
                try {
                    int copies = printer.Copies;
                    if (copies > 0) {
                        printerName = printer.PrinterName;
                        if (Printing.IsPrinter(printerName)) { // make sure the printer is still installed
                            Printing.SetDefaultPrinter(printerName); // set the default printer
                            wb = Printing.GetWebBrowser(url); // get the ticket
                            for (int i = 0; i < copies; i++) {
                                wb.Print();
                            }
                            wb.Dispose();
                            wb = null;
                        }
                        if (printerName.Length > 2 && printerName.Substring(0, 2) == "\\")
                            System.Threading.Thread.Sleep(4000); // Give the system 4 seconds to finish printing the document, prior to moving on
                        else
                            System.Threading.Thread.Sleep(1000); // Give the system 1 seconds to finish printing the document, prior to moving on
                    }
                } catch (Exception ex) {
                    Message.ShowMessage(errorCaption, $"Could not print ticket{(printerName.Length > 0 ? " to " + printerName : "")} ({ ex.Message })");
                    new KaEventLog() {
                        ApplicationIdentifier = Database.APP_ID,
                        ApplicationVersion = Tm2Database.FormatVersion(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version, "X"),
                        Category = KaEventLog.Categories.Failure,
                        Computer = System.Net.Dns.GetHostName(),
                        Description = errorCaption + Environment.NewLine + $"Could not print ticket{(printerName.Length > 0 ? " to " + printerName : "")}. {KaCommonObjects.Alerts.FormatException(ex)}"
                    }.SqlInsert(Tm2Database.Connection, null, Database.ApplicationIdentifier, Database.ApplicationUsername);
                } finally {
                    if (wb != null) {
                        wb.Dispose();
                        wb = null;
                    }
                    GC.Collect();
                }
            }
            Printing.SetDefaultPrinter(defaultPrinter); // set the default printer back to the original
        }

        static private object _lastTicket = null;
        static public object LastTicket {
            get { return _lastTicket; }
            set {
                _lastTicket = value;
                _lastTicketSetAt = DateTime.Now;
            }
        }

        static private DateTime _lastTicketSetAt = DateTime.MinValue;
        static public DateTime LastTicketSetAt {
            get { return _lastTicketSetAt; }
        }

        static public bool IsPrinter(string printer) {
            printer = printer.Trim();
            ManagementObjectCollection collection = new ManagementObjectSearcher("SELECT * FROM Win32_Printer").Get();
            foreach (ManagementObject obj in collection) {
                if (((string)obj["Name"]).Trim() == printer) { // is this the printer that we want to use?
                    return true;
                }
            }
            collection.Dispose();
            return false;
        }
    }
}