﻿using System;
using System.Collections;
using System.Linq;
using System.Windows.Forms;
using KahlerAutomation.KaTm2Database;
using KahlerAutomation.KaTm2LoadFramework;
using System.Data.OleDb;
using System.Reflection;
using System.Threading;

namespace Tm2StagingTerminal {
    public partial class Splash : Form {
        private bool _done = false;
        private int _step = 0;

        public Splash() {
            InitializeComponent();
            Appearances.ScaleFonts(this, new ArrayList());
        }

        private void Initialize() {
            Tm2Database d = new Tm2Database("");
            try {
                d.CheckDatabase(Tm2Database.Connection);
                Version v = Assembly.GetExecutingAssembly().GetName().Version;
                Tm2Database.SetApplicationVersion(Tm2Database.Connection, null, System.Net.Dns.GetHostName(), Database.APP_ID, v.Major, v.Minor, v.Build, v.Revision);
                Database.DbOk = true;
            } catch (OleDbException) {
                Message.ShowMessage("Tm2StagingTerminal", "Could not connect to the database. Check that this computer has connectivity to the database server and that the database server is running.");
                _done = true;
                return;
            } catch (DatabaseMaintenanceException ex) {
                Message.ShowMessage("Tm2StagingTerminal", "Staging Terminal is not allowed to use the database. " + ex.Message);
                _done = true;
                return;
            } catch (AlterTrackedTableException ex) {
                Message.ShowMessage("Tm2StagingTerminal", "This version of Staging Terminal requires a newer version of database but cannot update the database since it is provisioned for synchronization (" + ex.Message + "). Please contact support for assistance updating the database version.");
                _done = true;
                return;
            }

            _step = 1;
            LfDatabase.ApplicationIdentifier = Database.APP_ID;
            LfDatabase.InitializeSettings();
            Database.InitializeSettings();
            OleDbConnection connection = Tm2Database.Connection;
            if (LfDatabase.DefaultMassUnitId.Equals(Guid.Empty)) LfDatabase.DefaultMassUnitId = KaUnit.GetSystemDefaultMassUnitOfMeasure(connection, null);
            if (LfDatabase.DefaultVolumeUnitId.Equals(Guid.Empty)) LfDatabase.DefaultVolumeUnitId = KaUnit.GetSystemDefaultVolumeUnitOfMeasure(connection, null);
            try {
                LfDatabase.LocationId = new KaPanel(connection, Database.Settings.Scale.PanelId).LocationId;
            } catch (RecordNotFoundException) {
            }


            if (LfDatabase.LocationId == Guid.Empty) { // location hasn't been specified
                Message.ShowMessage("Tm2StagingTerminal", "Panel for the scale has not been selected. Please select a panel under the \"General\" tab in configuration.");
                Configuration f = new Configuration();
                if (f.ShowDialog() != DialogResult.OK) Application.Exit(); ;
            }
            _step = 2;
            LfControllers.Emulate = true; // since we will not be dispensing anything, we don't need to actually connect to the panels set up.
            string message = LfControllers.Initialize(false);
            if (message.Length > 0) Message.ShowMessage("Staging Terminal", message);
            _step = 3;
            KaCustomLoadQuestionFields inspectionField;

            try {
                inspectionField = new KaCustomLoadQuestionFields(Tm2Database.Connection, Database.PREVIOUS_LOAD_INSPECTION_FIELD_ID);
                if (inspectionField.Deleted) {
                    inspectionField.Deleted = false;
                    inspectionField.SqlUpdateInsertIfNotFound(Tm2Database.Connection, null, Database.ApplicationIdentifier, Database.ApplicationUsername);
                }
            } catch (RecordNotFoundException) {
                inspectionField = new KaCustomLoadQuestionFields() {
                    Id = Database.PREVIOUS_LOAD_INSPECTION_FIELD_ID,
                    BayId = Guid.Empty,
                    Deleted = false,
                    Name = "Previous product hauled",
                    OwnerId = Guid.Empty,
                    InputType = KaCustomLoadQuestionFields.InputTypes.TextField,
                    PostLoad = false,
                    PromptText = "Previous product hauled"
                };
                inspectionField.SqlUpdateInsertIfNotFound(Tm2Database.Connection, null, Database.ApplicationIdentifier, Database.ApplicationUsername);
            }

            try {
                inspectionField = new KaCustomLoadQuestionFields(Tm2Database.Connection, Database.TRUCK_NOT_HAULED_INSPECTION_FIELD_ID);
                if (inspectionField.Deleted) {
                    inspectionField.Deleted = false;
                    inspectionField.SqlUpdateInsertIfNotFound(Tm2Database.Connection, null, Database.ApplicationIdentifier, Database.ApplicationUsername);
                }
            } catch (RecordNotFoundException) {
                inspectionField = new KaCustomLoadQuestionFields() {
                    Id = Database.TRUCK_NOT_HAULED_INSPECTION_FIELD_ID,
                    BayId = Guid.Empty,
                    Deleted = false,
                    Name = "Truck not hauled restricted proteins",
                    OwnerId = Guid.Empty,
                    InputType = KaCustomLoadQuestionFields.InputTypes.YesNo,
                    PostLoad = false,
                    PromptText = "Truck not hauled restricted proteins"
                };
                inspectionField.SqlUpdateInsertIfNotFound(Tm2Database.Connection, null, Database.ApplicationIdentifier, Database.ApplicationUsername);
            }
            try {
                inspectionField = new KaCustomLoadQuestionFields(Tm2Database.Connection, Database.TRUCK_CLEANED_INSPECTION_FIELD_ID);
                if (inspectionField.Deleted) {
                    inspectionField.Deleted = false;
                    inspectionField.SqlUpdateInsertIfNotFound(Tm2Database.Connection, null, Database.ApplicationIdentifier, Database.ApplicationUsername);
                }
            } catch (RecordNotFoundException) {
                inspectionField = new KaCustomLoadQuestionFields() {
                    Id = Database.TRUCK_CLEANED_INSPECTION_FIELD_ID,
                    BayId = Guid.Empty,
                    Deleted = false,
                    Name = "Truck cleaned",
                    OwnerId = Guid.Empty,
                    InputType = KaCustomLoadQuestionFields.InputTypes.YesNo,
                    PostLoad = false,
                    PromptText = "Truck cleaned"
                };
                inspectionField.SqlUpdateInsertIfNotFound(Tm2Database.Connection, null, Database.ApplicationIdentifier, Database.ApplicationUsername);
            }
            _step = 4;
            _done = true;
        }

        private void Splash_Load(object sender, EventArgs e) {
            try {
                Thread t = new Thread(new ThreadStart(Initialize));
                t.Start();
                tmrWaitForBackground.Interval = 100;
                tmrWaitForBackground.Enabled = true;
                Version v = Assembly.GetExecutingAssembly().GetName().Version;
                Text = "Staging Terminal " + String.Format("Version {0:0}.{1:0}.{2:0}", v.Major, v.Minor, v.Build);
                if (v.Revision > 0) Text += String.Format(" X{0:0}", v.Revision);
                lblBuild.Text = Text;
                lblCopyright.Text = AssemblyCopyright;
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }

        public string AssemblyCopyright {
            get {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length != 0) return ((AssemblyCopyrightAttribute)attributes[0]).Copyright; else return "";
            }
        }


        private void tmrWaitForBackground_Tick(object sender, EventArgs e) {
            try {
                if (_done) this.Close();
                switch (_step) {
                    case 0: lblStatus.Text = "Connecting to database and checking table structure"; break;
                    case 1: lblStatus.Text = "Initializing settings"; break;
                    case 2:
                        if (LfControllers.CurrentPanel.Length > 0)
                            lblStatus.Text = String.Format("Retrieving information from panel ({0})...", LfControllers.CurrentPanel);
                        else
                            lblStatus.Text = "Retrieving information from panels...";
                        break;
                    case 3: lblStatus.Text = "Initializing custom load questions"; break;
                    default: lblStatus.Text = ""; break;
                }
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
            }
        }
    }
}
