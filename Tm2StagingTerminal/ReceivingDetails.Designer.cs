﻿namespace Tm2StagingTerminal {
    partial class ReceivingDetails {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblTotalLabel = new System.Windows.Forms.Label();
            this.lblAccountNumber = new System.Windows.Forms.Label();
            this.lstProducts = new System.Windows.Forms.ListView();
            this.chProduct = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chRequest = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblSupplier = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(508, 596);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(504, 48);
            this.lblTotal.TabIndex = 29;
            // 
            // lblTotalLabel
            // 
            this.lblTotalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalLabel.Location = new System.Drawing.Point(3, 596);
            this.lblTotalLabel.Name = "lblTotalLabel";
            this.lblTotalLabel.Size = new System.Drawing.Size(496, 48);
            this.lblTotalLabel.TabIndex = 28;
            this.lblTotalLabel.Text = "Total:";
            // 
            // lblAccountNumber
            // 
            this.lblAccountNumber.BackColor = System.Drawing.Color.White;
            this.lblAccountNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccountNumber.Location = new System.Drawing.Point(0, 154);
            this.lblAccountNumber.Name = "lblAccountNumber";
            this.lblAccountNumber.Size = new System.Drawing.Size(1024, 54);
            this.lblAccountNumber.TabIndex = 27;
            this.lblAccountNumber.Text = "Account Number:";
            this.lblAccountNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lstProducts
            // 
            this.lstProducts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chProduct,
            this.chRequest});
            this.lstProducts.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstProducts.FullRowSelect = true;
            this.lstProducts.Location = new System.Drawing.Point(3, 228);
            this.lstProducts.Name = "lstProducts";
            this.lstProducts.Size = new System.Drawing.Size(1008, 360);
            this.lstProducts.TabIndex = 26;
            this.lstProducts.UseCompatibleStateImageBehavior = false;
            this.lstProducts.View = System.Windows.Forms.View.Details;
            // 
            // chProduct
            // 
            this.chProduct.Text = "Product";
            this.chProduct.Width = 509;
            // 
            // chRequest
            // 
            this.chRequest.Text = "Request";
            this.chRequest.Width = 479;
            // 
            // lblSupplier
            // 
            this.lblSupplier.BackColor = System.Drawing.Color.White;
            this.lblSupplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSupplier.Location = new System.Drawing.Point(0, 100);
            this.lblSupplier.Name = "lblSupplier";
            this.lblSupplier.Size = new System.Drawing.Size(1024, 54);
            this.lblSupplier.TabIndex = 25;
            this.lblSupplier.Text = "Supplier:";
            this.lblSupplier.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ReceivingDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.lblTotalLabel);
            this.Controls.Add(this.lblAccountNumber);
            this.Controls.Add(this.lstProducts);
            this.Controls.Add(this.lblSupplier);
            this.Name = "ReceivingDetails";
            this.Resize += new System.EventHandler(this.ReceivingDetails_Resize);
            this.Controls.SetChildIndex(this.lblSupplier, 0);
            this.Controls.SetChildIndex(this.lstProducts, 0);
            this.Controls.SetChildIndex(this.lblAccountNumber, 0);
            this.Controls.SetChildIndex(this.lblTotalLabel, 0);
            this.Controls.SetChildIndex(this.lblTotal, 0);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label lblTotal;
        internal System.Windows.Forms.Label lblTotalLabel;
        internal System.Windows.Forms.Label lblAccountNumber;
        internal System.Windows.Forms.ListView lstProducts;
        internal System.Windows.Forms.ColumnHeader chProduct;
        internal System.Windows.Forms.ColumnHeader chRequest;
        internal System.Windows.Forms.Label lblSupplier;
    }
}
