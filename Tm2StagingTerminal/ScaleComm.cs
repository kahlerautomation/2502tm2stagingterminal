﻿using System.IO.Ports;
using System.Net;
using System.Net.Sockets;
using KahlerAutomation.KaTm2Database;
using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;

namespace Tm2StagingTerminal {
    public class ScaleComm {
        public enum ScaleCommType {
            Serial,
            TCPIP,
            WebService,
            Ka2000
        }
        public enum ScaleDataStreamType {
            RLWS_Condec = 0,
            MettlerToledo = 1
        }

        private SerialPort _comm = new SerialPort();
        private KaController.Controller _ka2000Controller = new KaController.Controller();
        private Socket _tcpSocket = null;

        private System.Threading.Timer _validTimer;
        private System.Threading.Timer _checkCommsTimer;
        private System.Threading.Timer _readSerialCommTimer;
        private System.Threading.Timer _readMettlerToledoTcpTimer;
        private System.Threading.Timer _readKahlerWebserviceTimer;
        private double _weight = 0;
        private KaUnit.Unit _unit = KaUnit.Unit.Pounds;
        private string _mode = "";
        private string _status = "";
        private bool _commsClosing = false;
        private ArrayList _buffer = new ArrayList();
        private object _obj = new object();

        #region  Properties 
        public int BaudRate {
            get {
                return _comm.BaudRate;
            }
            set {
                _comm.BaudRate = value;
            }
        }

        public int DataBits {
            get {
                return _comm.DataBits;
            }
            set {
                _comm.DataBits = value;
            }
        }

        public Parity Parity {
            get {
                return _comm.Parity;
            }
            set {
                _comm.Parity = value;
            }
        }

        public string PortName {
            get {
                return _comm.PortName;
            }
            set {
                _comm.PortName = value;
            }
        }

        public StopBits StopBit {
            get {
                return _comm.StopBits;
            }
            set {
                _comm.StopBits = value;
            }
        }

        private string _ipAddress = "0.0.0.0";
        public string IpAddress {
            get {
                return _ipAddress;
            }
            set {
                _ipAddress = value;
            }
        }

        private int _scaleIpAddressPort = 2000;
        public int ScaleIpAddressPort {
            get {
                return _scaleIpAddressPort;
            }
            set {
                _scaleIpAddressPort = value;
            }
        }

        private int _scaleDecimalPlaces = 0;
        public int ScaleDecimalPlaces {
            get {
                return _scaleDecimalPlaces;
            }
            set {
                _scaleDecimalPlaces = value;
            }
        }

        private string _lastValueReceived = "";
        public string LastValueReceived {
            get {
                return _lastValueReceived;
            }
        }

        private System.DateTime _lastValueReceivedTime = DateTime.MinValue;
        public DateTime LastValueReceivedTime {
            get {
                return _lastValueReceivedTime;
            }
        }

        private bool _validReading = false;
        public bool ValidReading {
            get {
                return _validReading && DateTime.Now.Subtract(_lastValueReceivedTime).TotalMilliseconds <= _scaleInvalidReadingtime;
            }
        }

        private int _scaleInvalidReadingtime = 750;
        public int ScaleInvalidReadingtime {
            get {
                return _scaleInvalidReadingtime;
            }
            set {
                _scaleInvalidReadingtime = value;
            }
        }

        public System.IO.Ports.SerialPort CommPort {
            get {
                return _comm;
            }
        }

        public KaModbus.ModbusNetwork Ka2000ControllerNetwork {
            get {
                return _ka2000Controller.Network;
            }
        }

        public Socket EthernetConnection {
            get {
                return _tcpSocket;
            }
        }

        private ScaleCommType _commType;
        public ScaleCommType CommType {
            get {
                return _commType;
            }
            set {
                _commType = value;
            }
        }

        private ScaleDataStreamType _streamDataType = ScaleDataStreamType.RLWS_Condec;
        public ScaleDataStreamType StreamDataType {
            get {
                return _streamDataType;
            }
            set {
                _streamDataType = value;
            }
        }

        private bool _dataStreamed = true;
        public bool DataStreamed {
            get {
                return _dataStreamed;
            }
            set {
                _dataStreamed = value;
            }
        }

        private bool _commsConnecting = false;
        public bool CommsConnecting {
            get {
                return _commsConnecting;
            }
        }
        #endregion

        public ScaleComm() {
            System.Threading.TimerCallback validTimerCallback = new TimerCallback(ReadingValidation);
            _validTimer = new System.Threading.Timer(validTimerCallback, new System.Threading.AutoResetEvent(false), Timeout.Infinite, Timeout.Infinite);

            System.Threading.TimerCallback checkCommsTimerCallback = new TimerCallback(CheckComms);
            _checkCommsTimer = new System.Threading.Timer(checkCommsTimerCallback, new System.Threading.AutoResetEvent(false), System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);

            System.Threading.TimerCallback readMettlerToledoSerialTimerCallback = new TimerCallback(ReadMettlerToledoScaleSerial);
            _readSerialCommTimer = new System.Threading.Timer(readMettlerToledoSerialTimerCallback, new System.Threading.AutoResetEvent(false), System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);

            System.Threading.TimerCallback readMettlerToledoTcpTimerCallback = new TimerCallback(ReadMettlerToledoScaleTcp);
            _readMettlerToledoTcpTimer = new System.Threading.Timer(readMettlerToledoTcpTimerCallback, new System.Threading.AutoResetEvent(false), System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);

            System.Threading.TimerCallback readKahlerWebserviceTimerCallback = new TimerCallback(ReadKahlerWebserviceScale);
            _readKahlerWebserviceTimer = new System.Threading.Timer(readKahlerWebserviceTimerCallback, new System.Threading.AutoResetEvent(false), System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
        }

        public void Open() {
            Close();
            _commsConnecting = true;
            _commsClosing = false;
            if (_commType == ScaleCommType.Serial) {
                _comm.Open();
                _comm.DataReceived += DataReceived;
                if (_streamDataType == ScaleDataStreamType.MettlerToledo) {
                    _readSerialCommTimer.Change(10, System.Threading.Timeout.Infinite);
                }
            } else if (_commType == ScaleCommType.TCPIP && _streamDataType == ScaleDataStreamType.MettlerToledo) {
                System.Net.IPAddress remoteAddress = new System.Net.IPAddress(new byte[4] { 127, 0, 0, 1 });
                IPAddress.TryParse(_ipAddress, out remoteAddress);
                IPEndPoint endPoint = new IPEndPoint(remoteAddress, _scaleIpAddressPort);
                _tcpSocket = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                _tcpSocket.ReceiveTimeout = 1000;
                _tcpSocket.Connect(endPoint);
                _readMettlerToledoTcpTimer.Change(10, System.Threading.Timeout.Infinite);
            } else if (_commType == ScaleCommType.WebService) {
                ReadKahlerWebserviceScale(null);
            } else if (_commType == ScaleCommType.Ka2000) {
                KaModbus.ModbusNetwork network = new KaModbus.ModbusNetwork();
                network.VerifyConnectedForSend = false;
                network.ConnectionType = KaModbus.ModbusNetwork.ModbusConnectionType.Tcp;
                network.TcpAddress = _ipAddress;
                network.TcpPort = _scaleIpAddressPort;
                try {
                    network.Open();
                } catch (Exception) {
                }
                _ka2000Controller = new KaController.Controller();
                _ka2000Controller.SystemName = "Scale";
                _ka2000Controller.Network = network;
                _ka2000Controller.ExtraProductDataEntryCount = 16;
                _ka2000Controller.NotifyOnSystemRead(SystemReadHandler);
                _ka2000Controller.ReadSystem();
            }
            _commsConnecting = false;
            _validTimer.Change(500, System.Threading.Timeout.Infinite);
            _checkCommsTimer.Change(500, System.Threading.Timeout.Infinite);
        }

        private void ReadingValidation(object stateInfo) {
            _validTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);

            if ((_commType == ScaleCommType.Serial && _comm.IsOpen) ||
                (_commType == ScaleCommType.Ka2000 && _ka2000Controller.Network != null) ||
                (_commType == ScaleCommType.TCPIP && _streamDataType == ScaleDataStreamType.MettlerToledo && _tcpSocket != null) ||
                (_commType == ScaleCommType.WebService)) {
                if (DateTime.Now.Subtract(_lastValueReceivedTime).TotalMilliseconds > _scaleInvalidReadingtime) {
                    _weightReceived.Invoke(_weight, _unit, _mode, _status, ValidReading);

                    if (_commType == ScaleCommType.Serial && _streamDataType == ScaleDataStreamType.MettlerToledo) {
                        Thread scaleThread = new Thread(ReadMettlerToledoScaleSerial);
                        scaleThread.Start();
                    }
                }
                _validTimer.Change(500, System.Threading.Timeout.Infinite);
            }
        }

        private void CheckComms(object stateInfo) {
            _checkCommsTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
            if (DateTime.Now.Subtract(_lastValueReceivedTime).TotalMilliseconds > _scaleInvalidReadingtime * 4 ||
                (_commType == ScaleCommType.Serial && _streamDataType == ScaleDataStreamType.RLWS_Condec && (_comm == null || !_comm.IsOpen)) ||
                (_commType == ScaleCommType.Serial && _streamDataType == ScaleDataStreamType.MettlerToledo && (_comm == null || !_comm.IsOpen)) ||
                (_commType == ScaleCommType.Ka2000 && _ka2000Controller.Network == null) ||
                (_commType == ScaleCommType.TCPIP && _streamDataType == ScaleDataStreamType.MettlerToledo && (_tcpSocket == null || !_tcpSocket.Connected)) ||
                (_commType == ScaleCommType.WebService)) {
                try {
                    Open();
                } catch (Exception ex) {
                    StUnhandledException.ShowException(ex);
                }
            }
            _checkCommsTimer.Change(500, System.Threading.Timeout.Infinite);
        }

        public void Close() {
            _checkCommsTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
            _commsClosing = true;
            if (_comm != null && _comm.IsOpen) {
                _comm.DataReceived -= DataReceived;
                _comm.DiscardInBuffer();
                _comm.Close();
            }
            if (_ka2000Controller != null) {
                try {
                    _ka2000Controller.Network.UnsubscribeAllReceived();
                    _ka2000Controller.Close();
                    _ka2000Controller.Network.Close();
                } catch (Exception  ) {

                }
            }
            _readSerialCommTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
            _readMettlerToledoTcpTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
            _readKahlerWebserviceTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);

            if (_tcpSocket != null) {
                try {
                    _tcpSocket.Shutdown(SocketShutdown.Both);
                    _tcpSocket.Close();
                } catch (Exception) {

                }
                _tcpSocket = null;
            }
        }

        public void Dispose() {
            Close();
            if (_comm != null) _comm.Dispose();
            if (_tcpSocket != null) _tcpSocket.Dispose();
        }

        private string Buildstring(ArrayList buffer, int start, int length) {
            string value = "";
            int i = start;
            while (i < (start + length)) {
                value += Convert.ToChar(buffer[i]);
                i += 1;
            }
            return value;
        }


        private void DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e) {
            if (_streamDataType == ScaleDataStreamType.RLWS_Condec) {
                HandleRLWSCommDataStream();
            } else {
                HandleMettlerToledoCommDataStream();
            }
        }

        private void HandleRLWSCommDataStream() {
            try {
                // <stx><pol><wwwwwww><unit><g/n><s><term>
                while (_comm.BytesToRead > 0) {
                    _buffer.Add(_comm.ReadByte());
                }

                int i = _buffer.Count - 1;
                int endIndex = -1;
                int startIndex = -1;
                while (i >= 0) {
                    if (endIndex == -1 && ((byte)_buffer[i] == 10 || (byte)_buffer[i] == 13)) {
                        endIndex = i;
                    } else if (endIndex != -1 && (byte)_buffer[i] == 2) {
                        startIndex = i;
                        break;
                    }
                    i -= 1;
                }
                if (endIndex != -1 && startIndex != -1) {
                    _validReading = false;

                    _lastValueReceived = Buildstring(_buffer, startIndex, endIndex - startIndex + 1);

                    _lastValueReceivedTime = DateTime.Now;

                    HandleRLWSDataStream();
                }
                if (i > 0) {
                    _buffer.RemoveRange(0, i + (endIndex - startIndex) + 1);
                    if (_validReading) {
                        UpdateWeightClients();
                    }
                }
            } catch (Exception) {

            }
        }

        private void HandleMettlerToledoCommDataStream() {
            try {
                //  <stx><StA><StB><StC><wwwwww><tttttt><cr><chk>
                while (_comm.BytesToRead > 0) {
                    _buffer.Add(_comm.ReadByte());
                }

                int i = _buffer.Count - 1;
                int endIndex = -1;
                int startIndex = -1;
                while (i >= 0) {
                    if (endIndex == -1 && ((byte)_buffer[i] == 10 || (byte)_buffer[i] == 13)) {
                        endIndex = i;
                    } else if (endIndex != -1 && (byte)_buffer[i] == 2) {
                        startIndex = i;
                        break;
                    }
                    i -= 1;
                }
                if (endIndex != -1 && startIndex != -1) {
                    _validReading = false;

                    _lastValueReceived = Buildstring(_buffer, startIndex, endIndex - startIndex + 1);

                    _lastValueReceivedTime = DateTime.Now;

                    HandleMettlerToledoDataStream();
                }
                if (i > 0) {
                    _buffer.RemoveRange(0, i + 11);
                    if (_validReading) {
                        UpdateWeightClients();
                        _readSerialCommTimer.Change(100, System.Threading.Timeout.Infinite);
                    }
                }
            } catch (Exception) {
                // Finally
                //     _buffer.Clear()
            }
        }

        public delegate void WeightReceived(double weight, KaUnit.Unit unit, string mode, string status, bool weightValid);
        private WeightReceived _weightReceived;

        public void NotifyOnDataReceived(WeightReceived value) {
            try {
                if (_weightReceived == null)
                    _weightReceived = value;
                else
                    Delegate.Combine(_weightReceived, value);
            } catch (Exception) {

            }
        }

        public static KaUnit.Unit TranslateUnitIdToKa2000Unit(string id) {
            switch (id) {
                case "L": return KaUnit.Unit.Pounds;
                case "G": return KaUnit.Unit.Gallons;
                case "T": return KaUnit.Unit.Tons;
                case "O": return KaUnit.Unit.Ounces;
                case "K": return KaUnit.Unit.Kilograms;
                case "P": return KaUnit.Unit.Pints;
                case "Q": return KaUnit.Unit.Quarts;
                case "F": return KaUnit.Unit.FluidOunces;
                case "I": return KaUnit.Unit.Liters;
                default: throw new Exception("Unit of measure not defined for " + id);
            }
        }

        private void UpdateWeightClients() {
            if (_weightReceived != null) {
                _validTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
                _weightReceived.Invoke(_weight, _unit, _mode, _status, ValidReading);
                _validTimer.Change(500, System.Threading.Timeout.Infinite);
            }
        }

        #region  KA-2000 Scale 
        private DateTime _lastKa2000WeightRead = DateTime.Now;
        private void SystemReadHandler(bool success) {
            try {
                if (success) {
                    _lastValueReceivedTime = DateTime.Now;
                    _weight = _ka2000Controller.ScaleReading;
                    _status = _ka2000Controller.ScaleMotion ? "M" : "";
                    _unit = (KaUnit.Unit)_ka2000Controller.UnitOfMeasureId;
                    _mode = _ka2000Controller.ScaleError == 0 ? "G" : "?";
                    if (_ka2000Controller.ScaleError == 0) {
                        _validReading = true;
                        _lastValueReceived = (_weight < 0.0 ? "-" : " ") + Math.Abs(_weight).ToString().PadLeft(7) + _unit + _mode + _status;
                    } else {
                        _validReading = false;
                        _lastValueReceived = KaController.Controller.GetScaleErrorText(_ka2000Controller.ScaleError);
                    }
                    if (DateTime.Now.Subtract(_lastKa2000WeightRead).TotalMilliseconds > 250) {
                        UpdateWeightClients();
                        _lastKa2000WeightRead = DateTime.Now;
                    }
                }
            } catch (Exception ex) {
                _validReading = false;
                StUnhandledException.ShowException(ex);
            }
        }
        #endregion

        #region " RLWS Scale "
        private void HandleRLWSDataStream() {
            try {
                string reading = _lastValueReceived.Replace(Convert.ToChar(2).ToString(), "").Replace(Convert.ToChar(13).ToString(), "").Replace(Convert.ToChar(10).ToString(), "");
                string weightRead = reading.Substring(0, reading.Length - 3);
                if (weightRead.IndexOf(".") == -1) {
                    _scaleDecimalPlaces = 0;
                } else {
                    _scaleDecimalPlaces = weightRead.Length - weightRead.IndexOf(".") - 1;
                }

                _weight = Double.Parse(weightRead.Replace(" ", ""));
                _unit = TranslateUnitIdToKa2000Unit(reading.Substring(reading.Length - 3, 1));
                _mode = reading.Substring(reading.Length - 2, 1);
                _status = reading.Substring(reading.Length - 1, 1);
                _validReading = true;
            } catch (Exception) {
                _validReading = false;
            }
        }
        #endregion

        #region   Mettler Toledo Scale 
        private void ReadMettlerToledoScaleSerial(object stateInfo) {
            _readSerialCommTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
            if (!_dataStreamed && _comm != null && _comm.IsOpen) {
                string sendWeightCommand = "ePrint" + Environment.NewLine;
                _comm.Write(sendWeightCommand);
            }
        }

        private void ReadMettlerToledoScaleTcp(object stateInfo) {
            _readMettlerToledoTcpTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
            bool dataValid = true;
            string dataRead = "";
            byte[] databuffer = new byte[1024];
            NetworkStream dataStream = null;
            if (_tcpSocket != null && _tcpSocket.Connected && !_commsClosing) {
                try {
                    dataStream = new NetworkStream(_tcpSocket);
                    if (dataValid && !_dataStreamed) {
                        string sendWeightCommand = "ePrint" + Environment.NewLine;
                        _tcpSocket.Send(System.Text.Encoding.ASCII.GetBytes(sendWeightCommand));
                        dataValid = false;
                        System.Threading.Thread.Sleep(100);
                    }
                    if (dataStream.DataAvailable) {
                        int numOfBytesRead = dataStream.Read(databuffer, 0, databuffer.Length);
                        dataRead += System.Text.Encoding.ASCII.GetString(databuffer);
                        int startCharPos = dataRead.IndexOf(Convert.ToChar(2));

                        while (startCharPos >= 0) {
                            int endCharPos = dataRead.IndexOf(Convert.ToChar(13), startCharPos);
                            if (endCharPos >= 0) {
                                _lastValueReceived = dataRead.Substring(startCharPos, endCharPos - startCharPos + 1);
                                _lastValueReceivedTime = DateTime.Now;
                                dataRead = "";
                                HandleMettlerToledoDataStream();
                                dataValid = true;
                                break;
                            } else {
                                if (startCharPos < dataRead.Length) {
                                    startCharPos = dataRead.IndexOf(Convert.ToChar(2), startCharPos + 1);
                                }
                            }
                        }
                    }
                    _readMettlerToledoTcpTimer.Change(100, System.Threading.Timeout.Infinite);
                } catch (System.IO.IOException) {
                    Close();
                } catch (ObjectDisposedException) {
                    Close();
                }
            }
        }

        private void HandleMettlerToledoDataStream() {
            try {
                //  <stx><StA><StB><StC><wwwwww><tttttt><cr><chk>
                string reading = _lastValueReceived.Replace(Convert.ToChar(2).ToString(), "").Replace(Convert.ToChar(13).ToString(), "").Replace(Convert.ToChar(10).ToString(), "");
                string weightRead = reading.Substring(3, 6).Trim();

                int statusA = System.Text.Encoding.ASCII.GetBytes(reading.Substring(0, 1))[0];
                int statusB = System.Text.Encoding.ASCII.GetBytes(reading.Substring(1, 1))[0];
                int statusC = System.Text.Encoding.ASCII.GetBytes(reading.Substring(2, 1))[0];
                int scaleDecimalCode = (statusA & 7);
                if (scaleDecimalCode >= 2) {
                    _scaleDecimalPlaces = scaleDecimalCode - 2;
                } else {
                    _scaleDecimalPlaces = scaleDecimalCode - 1;
                }

                _weight = Double.Parse(weightRead.Replace(" ", "")) * Math.Pow(10, (_scaleDecimalPlaces * -1));

                if ((statusB & 2) != 0) {
                    _weight = _weight * -1;
                }

                int currentUnit = statusC & 7;
                switch (currentUnit) {
                    case 0://  lb or kg
                        if ((statusB & 16) == 0) {
                            _unit = _unit = KaUnit.Unit.Pounds;
                        } else {
                            _unit = _unit = KaUnit.Unit.Kilograms;
                        }
                        break;
                    case 2: //  metric tons 
                        _unit = KaUnit.Unit.MetricTon;
                        break;
                    case 3://  ounces
                        _unit = KaUnit.Unit.Ounces;
                        break;
                    case 6: //  tons
                        _unit = KaUnit.Unit.Tons;
                        break;
                    case 1: //  grams 
                    case 4: //  troy ounces 
                    case 5://  penny weight 
                    case 7:  //  custom units 
                    default:
                        _unit = KaUnit.Unit.Pulses;
                        break;
                }
                if ((statusB & 1) == 0) {
                    _mode = "G";
                } else {
                    _mode = "T";
                }
                if ((statusB & 8) == 0) {
                    _status = " ";
                } else {
                    _status = "M";
                }
                _validReading = ((statusB & 4) == 0) && _unit != KaUnit.Unit.Pulses;
                if (_unit == KaUnit.Unit.Pulses) _unit = KaUnit.Unit.Pounds; // Change back to 
            } catch (Exception) {
                _validReading = false;
            }
            UpdateWeightClients();
        }
        #endregion

        #region " Kahler Web services "
        private void ReadKahlerWebserviceScale(object stateInfo) {
            _readKahlerWebserviceTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
            _validReading = false;
            try {
                StreamlineKa2000WebService.Service streamlineKa2000Service = new StreamlineKa2000WebService.Service();
                streamlineKa2000Service.Url = "http://" + _ipAddress + ":" + _scaleIpAddressPort.ToString() + "/Service.asmx";
                StreamlineKa2000WebService.ScaleReading scaleResult = streamlineKa2000Service.GetScaleReading();

                if (scaleResult.Status == StreamlineKa2000WebService.ScaleStatus.Ok) {
                    _lastValueReceived = scaleResult.ScaleData;
                    _lastValueReceivedTime = scaleResult.Timestamp;
                    HandleRLWSDataStream();
                } else {
                    _lastValueReceived = "";
                    _lastValueReceivedTime = DateTime.Now;
                    switch (scaleResult.Status) {
                        case StreamlineKa2000WebService.ScaleStatus.ConnectionClosed:
                            _lastValueReceived = "Connection closed";
                            break;
                        case StreamlineKa2000WebService.ScaleStatus.ConnectionReset:
                            _lastValueReceived = "Connection reset";
                            break;
                        case StreamlineKa2000WebService.ScaleStatus.DataOverrun:
                            _lastValueReceived = "Data overrun";
                            break;
                        case StreamlineKa2000WebService.ScaleStatus.DataTimeout:
                            _lastValueReceived = "Data timeout";
                            break;
                        case StreamlineKa2000WebService.ScaleStatus.NoConnection:
                            _lastValueReceived = "No connection";
                            break;
                        case StreamlineKa2000WebService.ScaleStatus.OpenTimeout:
                            _lastValueReceived = "Open timeout";
                            break;
                        default:
                            _lastValueReceived = "Unknown error: " + scaleResult.Status.ToString();
                            break;
                    }
                }
                UpdateWeightClients();

                _readKahlerWebserviceTimer.Change(200, System.Threading.Timeout.Infinite);
            } catch (System.IO.IOException) {
                Close();
            } catch (ObjectDisposedException) {
                Close();
            }
        }
        #endregion
    }
}