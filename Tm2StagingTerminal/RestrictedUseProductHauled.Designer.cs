﻿namespace Tm2StagingTerminal {
    partial class RestrictedUseProductHauled : StagingTerminalBaseControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RestrictedUseProductHauled));
            this.lblRuppListDetail = new System.Windows.Forms.Label();
            this.lblOtherExemptDetail = new System.Windows.Forms.Label();
            this.lblOtherExemptHeader = new System.Windows.Forms.Label();
            this.lblRuppExemptDetail = new System.Windows.Forms.Label();
            this.lblRuppExemptHeader = new System.Windows.Forms.Label();
            this.lblRuppListHeader = new System.Windows.Forms.Label();
            this.lblNotHauledRupp = new System.Windows.Forms.Label();
            this.chkNotHauledRupp = new System.Windows.Forms.Label();
            this.lblCleanedCertification = new System.Windows.Forms.Label();
            this.chkCleanedCertification = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlCleanedCertification = new System.Windows.Forms.Panel();
            this.pnlNotHauledRupp = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1.SuspendLayout();
            this.pnlCleanedCertification.SuspendLayout();
            this.pnlNotHauledRupp.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblRuppListDetail
            // 
            this.lblRuppListDetail.AutoSize = true;
            this.lblRuppListDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRuppListDetail.Location = new System.Drawing.Point(3, 261);
            this.lblRuppListDetail.MaximumSize = new System.Drawing.Size(800, 0);
            this.lblRuppListDetail.Name = "lblRuppListDetail";
            this.lblRuppListDetail.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.lblRuppListDetail.Size = new System.Drawing.Size(799, 100);
            this.lblRuppListDetail.TabIndex = 20;
            this.lblRuppListDetail.Text = resources.GetString("lblRuppListDetail.Text");
            // 
            // lblOtherExemptDetail
            // 
            this.lblOtherExemptDetail.AutoSize = true;
            this.lblOtherExemptDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOtherExemptDetail.Location = new System.Drawing.Point(3, 449);
            this.lblOtherExemptDetail.MaximumSize = new System.Drawing.Size(800, 0);
            this.lblOtherExemptDetail.Name = "lblOtherExemptDetail";
            this.lblOtherExemptDetail.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.lblOtherExemptDetail.Size = new System.Drawing.Size(727, 40);
            this.lblOtherExemptDetail.TabIndex = 24;
            this.lblOtherExemptDetail.Text = "Poultry, Marine (Fish), Vegetable Protein Products, Grease, Fat, Amino Acids, Tal" +
    "low Oil, Dicalcium Phosphate ";
            // 
            // lblOtherExemptHeader
            // 
            this.lblOtherExemptHeader.AutoSize = true;
            this.lblOtherExemptHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOtherExemptHeader.Location = new System.Drawing.Point(3, 425);
            this.lblOtherExemptHeader.Name = "lblOtherExemptHeader";
            this.lblOtherExemptHeader.Size = new System.Drawing.Size(182, 24);
            this.lblOtherExemptHeader.TabIndex = 23;
            this.lblOtherExemptHeader.Text = "Other exemptions:";
            // 
            // lblRuppExemptDetail
            // 
            this.lblRuppExemptDetail.AutoSize = true;
            this.lblRuppExemptDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRuppExemptDetail.Location = new System.Drawing.Point(3, 385);
            this.lblRuppExemptDetail.MaximumSize = new System.Drawing.Size(800, 0);
            this.lblRuppExemptDetail.Name = "lblRuppExemptDetail";
            this.lblRuppExemptDetail.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.lblRuppExemptDetail.Size = new System.Drawing.Size(795, 40);
            this.lblRuppExemptDetail.TabIndex = 22;
            this.lblRuppExemptDetail.Text = "Blood and Blood Products, Pure Porcine (Pork) Protein, Inspected Meat Products, s" +
    "uch as plate waste, Pure Equine (Horse), Protein, Milk Products (Milk and Milk P" +
    "roteins), Gelatin";
            // 
            // lblRuppExemptHeader
            // 
            this.lblRuppExemptHeader.AutoSize = true;
            this.lblRuppExemptHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRuppExemptHeader.Location = new System.Drawing.Point(3, 361);
            this.lblRuppExemptHeader.Name = "lblRuppExemptHeader";
            this.lblRuppExemptHeader.Size = new System.Drawing.Size(534, 24);
            this.lblRuppExemptHeader.TabIndex = 21;
            this.lblRuppExemptHeader.Text = "The following proteins derived from animals are exempt:";
            // 
            // lblRuppListHeader
            // 
            this.lblRuppListHeader.AutoSize = true;
            this.lblRuppListHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRuppListHeader.Location = new System.Drawing.Point(3, 198);
            this.lblRuppListHeader.Name = "lblRuppListHeader";
            this.lblRuppListHeader.Padding = new System.Windows.Forms.Padding(0, 15, 0, 0);
            this.lblRuppListHeader.Size = new System.Drawing.Size(846, 63);
            this.lblRuppListHeader.TabIndex = 19;
            this.lblRuppListHeader.Text = "Listing of Restricted Use Protein Products taken from AAFCO Official Publication " +
    "and FDA Guidance Document:";
            // 
            // lblNotHauledRupp
            // 
            this.lblNotHauledRupp.AutoSize = true;
            this.lblNotHauledRupp.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold);
            this.lblNotHauledRupp.Location = new System.Drawing.Point(64, 0);
            this.lblNotHauledRupp.MaximumSize = new System.Drawing.Size(900, 0);
            this.lblNotHauledRupp.Name = "lblNotHauledRupp";
            this.lblNotHauledRupp.Size = new System.Drawing.Size(808, 62);
            this.lblNotHauledRupp.TabIndex = 18;
            this.lblNotHauledRupp.Text = "I certify that this truck has not hauled Restricted Use Protein Products. ";
            // 
            // chkNotHauledRupp
            // 
            this.chkNotHauledRupp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.chkNotHauledRupp.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkNotHauledRupp.Location = new System.Drawing.Point(3, 0);
            this.chkNotHauledRupp.Name = "chkNotHauledRupp";
            this.chkNotHauledRupp.Size = new System.Drawing.Size(40, 40);
            this.chkNotHauledRupp.TabIndex = 17;
            this.chkNotHauledRupp.Text = "X";
            this.chkNotHauledRupp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkNotHauledRupp.Click += new System.EventHandler(this.chkNotHauledRupp_Click);
            // 
            // lblCleanedCertification
            // 
            this.lblCleanedCertification.AutoSize = true;
            this.lblCleanedCertification.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCleanedCertification.Location = new System.Drawing.Point(64, 0);
            this.lblCleanedCertification.MaximumSize = new System.Drawing.Size(900, 0);
            this.lblCleanedCertification.Name = "lblCleanedCertification";
            this.lblCleanedCertification.Size = new System.Drawing.Size(821, 124);
            this.lblCleanedCertification.TabIndex = 16;
            this.lblCleanedCertification.Text = resources.GetString("lblCleanedCertification.Text");
            // 
            // chkCleanedCertification
            // 
            this.chkCleanedCertification.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.chkCleanedCertification.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCleanedCertification.Location = new System.Drawing.Point(3, 0);
            this.chkCleanedCertification.Name = "chkCleanedCertification";
            this.chkCleanedCertification.Size = new System.Drawing.Size(40, 40);
            this.chkCleanedCertification.TabIndex = 15;
            this.chkCleanedCertification.Text = "X";
            this.chkCleanedCertification.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkCleanedCertification.Click += new System.EventHandler(this.chkCleanedCertification_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.pnlCleanedCertification);
            this.flowLayoutPanel1.Controls.Add(this.pnlNotHauledRupp);
            this.flowLayoutPanel1.Controls.Add(this.lblRuppListHeader);
            this.flowLayoutPanel1.Controls.Add(this.lblRuppListDetail);
            this.flowLayoutPanel1.Controls.Add(this.lblRuppExemptHeader);
            this.flowLayoutPanel1.Controls.Add(this.lblRuppExemptDetail);
            this.flowLayoutPanel1.Controls.Add(this.lblOtherExemptHeader);
            this.flowLayoutPanel1.Controls.Add(this.lblOtherExemptDetail);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 106);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(895, 591);
            this.flowLayoutPanel1.TabIndex = 25;
            // 
            // pnlCleanedCertification
            // 
            this.pnlCleanedCertification.AutoSize = true;
            this.pnlCleanedCertification.Controls.Add(this.chkCleanedCertification);
            this.pnlCleanedCertification.Controls.Add(this.lblCleanedCertification);
            this.pnlCleanedCertification.Location = new System.Drawing.Point(3, 3);
            this.pnlCleanedCertification.Name = "pnlCleanedCertification";
            this.pnlCleanedCertification.Size = new System.Drawing.Size(888, 124);
            this.pnlCleanedCertification.TabIndex = 26;
            // 
            // pnlNotHauledRupp
            // 
            this.pnlNotHauledRupp.AutoSize = true;
            this.pnlNotHauledRupp.Controls.Add(this.chkNotHauledRupp);
            this.pnlNotHauledRupp.Controls.Add(this.lblNotHauledRupp);
            this.pnlNotHauledRupp.Location = new System.Drawing.Point(3, 133);
            this.pnlNotHauledRupp.Name = "pnlNotHauledRupp";
            this.pnlNotHauledRupp.Size = new System.Drawing.Size(875, 62);
            this.pnlNotHauledRupp.TabIndex = 27;
            // 
            // RestrictedUseProductHauled
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "RestrictedUseProductHauled";
            this.Title = "Restricted Use Protein Products";
            this.Resize += new System.EventHandler(this.RestrictedUseProductHauled_Resize);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.pnlCleanedCertification.ResumeLayout(false);
            this.pnlCleanedCertification.PerformLayout();
            this.pnlNotHauledRupp.ResumeLayout(false);
            this.pnlNotHauledRupp.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label lblRuppListDetail;
        internal System.Windows.Forms.Label lblOtherExemptDetail;
        internal System.Windows.Forms.Label lblOtherExemptHeader;
        internal System.Windows.Forms.Label lblRuppExemptDetail;
        internal System.Windows.Forms.Label lblRuppExemptHeader;
        internal System.Windows.Forms.Label lblRuppListHeader;
        internal System.Windows.Forms.Label lblNotHauledRupp;
        internal System.Windows.Forms.Label chkNotHauledRupp;
        internal System.Windows.Forms.Label lblCleanedCertification;
        internal System.Windows.Forms.Label chkCleanedCertification;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel pnlCleanedCertification;
        private System.Windows.Forms.Panel pnlNotHauledRupp;
    }
}
