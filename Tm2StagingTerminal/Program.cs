﻿using System;
using System.Windows.Forms;
using System.Threading;

namespace Tm2StagingTerminal {
    static class Program {
        static Mutex _mutex = new Mutex(true, "Tm2StagingTerminal");

        // used to ensure that only one instance of Tm2StagingTerminal is running
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            if (_mutex.WaitOne(TimeSpan.Zero, true)) { // we're the first instance
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
                Application.Run(new Splash());
                if (Database.DbOk) {
                    Application.Run(new MainForm());
                }
            } else {
                //Do nothing here.  We are not the first instance running, exit program.
            }
        }

        static private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e) {
            StUnhandledException.ShowException((Exception)e.ExceptionObject);
        }
    }
}
