﻿using System;
using KahlerAutomation.KaTm2Database;

namespace Tm2StagingTerminal {
    public partial class Complete : StagingTerminalBaseControl {
        public Complete() {
            InitializeComponent();
            btnBack.Visible = false;
            btnStartOver.Visible = false;
        }

        public void ShowCompleted() {
            int ticketCopies = 0;
            if (CurrentPanelStatus.IsInbound) {
                if (CurrentPanelStatus.Order != null) {
                    bool ownerRequiresRestrictedUse = Database.RequireRestrictedUseOwners.Contains(CurrentPanelStatus.Order.OwnerId);
                    if (Database.IsPalletedLoad(CurrentPanelStatus.Order)) {
                        foreach (PrinterConfiguration printSetting in Database.Settings.InboundBaggedProductPrinters) {
                            ticketCopies += printSetting.Copies;
                        }
                    } else {
                        foreach (PrinterConfiguration printSetting in Database.Settings.InboundBulkProductPrinters) {
                            ticketCopies += printSetting.Copies;
                        }
                    }
                    if (ownerRequiresRestrictedUse) {
                        foreach (PrinterConfiguration printSetting in Database.Settings.InboundTruckInspectionPrinters) {
                            ticketCopies += printSetting.Copies;
                        }
                    }
                } else if (CurrentPanelStatus.ReceivingPurchaseOrder != null) {
                    foreach (PrinterConfiguration printSetting in Database.Settings.ReceivingProductPickTicketPrinters) {
                        ticketCopies += printSetting.Copies;
                    }
                }
            } else if (!CurrentPanelStatus.IsInbound) {
                if (CurrentPanelStatus.StagedOrder != null) {
                    foreach (PrinterConfiguration printSetting in Database.Settings.OutboundTicketPrinters) {
                        ticketCopies += printSetting.Copies;
                    }
                } else {
                    foreach (PrinterConfiguration printSetting in Database.Settings.ReceivingProductTicketPrinters) {
                        ticketCopies += printSetting.Copies;
                    }
                }
            }
            string pleaseDriveAheadText = "";
            KaBay bay = new KaBay();
            try {
                if (CurrentPanelStatus.IsInbound && CurrentPanelStatus.StagedOrder != null && CurrentPanelStatus.StagedOrder.BayId != Guid.Empty)
                    bay = new KaBay(Tm2Database.Connection, CurrentPanelStatus.StagedOrder.BayId);
            } catch (RecordNotFoundException) { /* suppress */ }

            if (CurrentPanelStatus.IsInbound) {
                if (CurrentPanelStatus.Order != null) {
                    if (bay.Name.Length == 0) {
                        if (ticketCopies == 0)
                            pleaseDriveAheadText = Database.Settings.OrderCompleteInboundOrderBayNotAssignedNoTickets;
                        else
                            pleaseDriveAheadText = Database.Settings.OrderCompleteInboundOrderBayNotAssignedHasTickets;
                    } else {
                        if (ticketCopies == 0)
                            pleaseDriveAheadText = Database.Settings.OrderCompleteInboundOrderBayAssignedNoTickets;
                        else
                            pleaseDriveAheadText = Database.Settings.OrderCompleteInboundOrderBayAssignedHasTickets;
                    }
                } else if (CurrentPanelStatus.ReceivingPurchaseOrder != null) {
                    if (bay.Name.Length == 0) {
                        if (ticketCopies == 0)
                            pleaseDriveAheadText = Database.Settings.OrderCompleteInboundReceivingBayNotAssignedNoTickets;
                        else
                            pleaseDriveAheadText = Database.Settings.OrderCompleteInboundReceivingBayNotAssignedHasTickets;
                    } else {
                        if (ticketCopies == 0)
                            pleaseDriveAheadText = Database.Settings.OrderCompleteInboundReceivingBayAssignedNoTickets;
                        else
                            pleaseDriveAheadText = Database.Settings.OrderCompleteInboundReceivingBayAssignedHasTickets;
                    }
                }
            } else {
                if (ticketCopies == 0)
                    pleaseDriveAheadText = Database.Settings.OrderCompleteOutboundNoTickets;
                else
                    pleaseDriveAheadText = Database.Settings.OrderCompleteOutboundHasTickets;
            }
            pleaseDriveAheadText = pleaseDriveAheadText.Trim();
            if (pleaseDriveAheadText.Length == 0) pleaseDriveAheadText = "Thank you.<br>Please drive ahead.";
            lblPleaseDriveAhead.Text = pleaseDriveAheadText.Replace("<br>", Environment.NewLine).Replace("<bay>", bay.Name);
        }

        private void Complete_Resize(object sender, EventArgs e) {
            base.StagingTerminalBaseControl_Resize(sender, e);
            lblPleaseDriveAhead.Left = this.Margin.Left;
            lblPleaseDriveAhead.Width = ClientSize.Width - Margin.Horizontal;
            lblPleaseDriveAhead.Top = buttonBar.Bottom + Margin.Vertical;
            lblPleaseDriveAhead.Height = ClientSize.Height - lblPleaseDriveAhead.Top - Margin.Bottom;
        }

        protected override void btnOk_Click(object sender, EventArgs e) {
            base.btnOk_Click(sender, e);
        }
    }
}
