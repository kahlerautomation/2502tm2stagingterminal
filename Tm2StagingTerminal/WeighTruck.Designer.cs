﻿namespace Tm2StagingTerminal {
    partial class WeighTruck : StagingTerminalBaseControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblCurrentScaleReading = new System.Windows.Forms.Label();
            this.lblCurrentScaleReadingValue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblCurrentScaleReading
            // 
            this.lblCurrentScaleReading.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F);
            this.lblCurrentScaleReading.Location = new System.Drawing.Point(190, 157);
            this.lblCurrentScaleReading.Name = "lblCurrentScaleReading";
            this.lblCurrentScaleReading.Size = new System.Drawing.Size(666, 73);
            this.lblCurrentScaleReading.TabIndex = 4;
            this.lblCurrentScaleReading.Text = "Current scale reading:";
            // 
            // lblCurrentScaleReadingValue
            // 
            this.lblCurrentScaleReadingValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F);
            this.lblCurrentScaleReadingValue.Location = new System.Drawing.Point(190, 230);
            this.lblCurrentScaleReadingValue.Name = "lblCurrentScaleReadingValue";
            this.lblCurrentScaleReadingValue.Size = new System.Drawing.Size(645, 73);
            this.lblCurrentScaleReadingValue.TabIndex = 5;
            this.lblCurrentScaleReadingValue.Text = "0 lb M";
            this.lblCurrentScaleReadingValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // WeighTruck
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.lblCurrentScaleReading);
            this.Controls.Add(this.lblCurrentScaleReadingValue);
            this.Name = "WeighTruck";
            this.Size = new System.Drawing.Size(1025, 460);
            this.Load += new System.EventHandler(this.WeighTruck_Load);
            this.Resize += new System.EventHandler(this.WeighTruck_Resize);
            this.Controls.SetChildIndex(this.lblCurrentScaleReadingValue, 0);
            this.Controls.SetChildIndex(this.lblCurrentScaleReading, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblCurrentScaleReading;
        private System.Windows.Forms.Label lblCurrentScaleReadingValue;
    }
}
