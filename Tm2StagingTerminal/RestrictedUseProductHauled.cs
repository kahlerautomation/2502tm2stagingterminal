﻿using System;
using System.Drawing;

namespace Tm2StagingTerminal {
    public partial class RestrictedUseProductHauled : StagingTerminalBaseControl {
        public RestrictedUseProductHauled() {
            InitializeComponent();
        }

        public void RestrictedUseProductHauledLoad() {
            chkCleanedCertification.Text = CurrentPanelStatus.TruckCleaned ? "X" : "";
            chkNotHauledRupp.Text = CurrentPanelStatus.TruckNotUsedRupp ? "X" : "";
        }

        private void chkCleanedCertification_Click(object sender, EventArgs e) {
            CurrentPanelStatus.TruckCleaned = !CurrentPanelStatus.TruckCleaned;
            chkCleanedCertification.Text = CurrentPanelStatus.TruckCleaned ? "X" : "";
        }

        private void chkNotHauledRupp_Click(object sender, EventArgs e) {
            CurrentPanelStatus.TruckNotUsedRupp = !CurrentPanelStatus.TruckNotUsedRupp;
            chkNotHauledRupp.Text = CurrentPanelStatus.TruckNotUsedRupp ? "X" : "";
        }

        private void RestrictedUseProductHauled_Resize(object sender, EventArgs e) {
            flowLayoutPanel1.Width = ClientSize.Width;
            flowLayoutPanel1.Height = ClientSize.Height - flowLayoutPanel1.Top;
            pnlCleanedCertification.MinimumSize = new Size(flowLayoutPanel1.ClientSize.Width / 2, 20);
            pnlNotHauledRupp.MinimumSize = new Size(flowLayoutPanel1.ClientSize.Width / 2, 20);
            lblCleanedCertification.MaximumSize = new Size(pnlCleanedCertification.Width - lblCleanedCertification.Left + pnlCleanedCertification.Margin.Horizontal, 0);
            lblNotHauledRupp.MaximumSize = new Size(pnlNotHauledRupp.Width - lblNotHauledRupp.Left + pnlNotHauledRupp.Margin.Horizontal, 0);
            lblOtherExemptDetail.MaximumSize = new Size(this.ClientSize.Width - flowLayoutPanel1.Margin.Horizontal - lblOtherExemptDetail.Padding.Left, 0);
            lblOtherExemptHeader.MaximumSize = new Size(this.ClientSize.Width - flowLayoutPanel1.Margin.Horizontal - lblOtherExemptHeader.Padding.Left, 0);
            lblRuppExemptDetail.MaximumSize = new Size(this.ClientSize.Width - flowLayoutPanel1.Margin.Horizontal - lblRuppExemptDetail.Padding.Left, 0);
            lblRuppExemptHeader.MaximumSize = new Size(this.ClientSize.Width - flowLayoutPanel1.Margin.Horizontal - lblRuppExemptHeader.Padding.Left, 0);
            lblRuppListDetail.MaximumSize = new Size(this.ClientSize.Width - flowLayoutPanel1.Margin.Horizontal - lblRuppListDetail.Padding.Left, 0);
            lblRuppListHeader.MaximumSize = new Size(this.ClientSize.Width - flowLayoutPanel1.Margin.Horizontal - lblRuppListHeader.Padding.Left, 0);
        }
    }
}
