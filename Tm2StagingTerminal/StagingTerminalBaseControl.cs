﻿using System;
using System.Windows.Forms;

namespace Tm2StagingTerminal {
    public partial class StagingTerminalBaseControl : UserControl {
        public StagingTerminalBaseControl() {
            InitializeComponent();
        }
        public string Title {
            get {
                return titleBar.Title;
            }
            set {
                titleBar.Title = value;
            }
        }

        protected virtual void StagingTerminalBaseControl_Resize(object sender, EventArgs e) {
            titleBar.Width = ClientSize.Width;
            buttonBar.Width = ClientSize.Width;
            btnStartOver.Left = 3;
            btnBack.Left = btnStartOver.Right + 10;
            btnOk.Left = Math.Max(ClientSize.Width - btnOk.Width - 3, 0);
        }

        public event EventHandler BackClicked;
        protected virtual void btnBack_Click(object sender, EventArgs e) {
            try {
                if (BackClicked != null) {
                    this.Enabled = false;
                    BackClicked(this, e);
                }
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
                this.Enabled = true;
            }
        }

        public event EventHandler OkClicked;
        protected virtual void btnOk_Click(object sender, EventArgs e) {
            try {
                if (OkClicked != null) {
                    this.Enabled = false;
                    OkClicked(this, e);
                }
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
                this.Enabled = true;
            }
        }

        public event EventHandler StartOverClicked;
        protected virtual void btnStartOver_Click(object sender, EventArgs e) {
            try {
                if (StartOverClicked != null) {
                    this.Enabled = false;
                    StartOverClicked(this, e);
                }
            } catch (Exception ex) {
                StUnhandledException.ShowException(ex);
                this.Enabled = true;
            }
        }

        #region Thread-Safe Delegate Subs 
        public delegate void SetPropertyValueCallback(Control obj, string propertyName, object value);
        public void SetPropertyValue(Control obj, string propertyName, object value) {
            if (obj.InvokeRequired) {
                SetPropertyValueCallback d = new SetPropertyValueCallback(SetPropertyValue);
                obj.BeginInvoke(d, new object[] { obj, propertyName, value });
            } else {
                obj.GetType().GetProperty(propertyName).SetValue(obj, value, null);
                try {
                    obj.Refresh();
                } catch (Exception) {
                }
            }
        }
        #endregion
    }
}
