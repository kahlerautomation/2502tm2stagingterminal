﻿using System;
using KahlerAutomation.KaTm2Database;
using System.Data.OleDb;
using System.Net;
using System.Reflection;
using KahlerAutomation;

namespace Tm2StagingTerminal {
    class StUnhandledException {
        static public void ShowException(Exception exception) {
            try {
                KaEventLog entry = new KaEventLog() {
                    ApplicationIdentifier = Database.APP_ID,
                    ApplicationVersion = Tm2Database.FormatVersion(Assembly.GetExecutingAssembly().GetName().Version, "X"),
                    Category = KaEventLog.Categories.Failure,
                    Computer = Dns.GetHostName(),
                    Description = GetDescription(exception)
                };
                entry.SqlInsert(Tm2Database.Connection, null, Database.ApplicationIdentifier, Database.ApplicationUsername);
            } catch (Exception) { /* suppress exception */ }
            UnhandledException.ShowException(exception);
        }

        static private string GetDescription(Exception exception) {
            string description = exception.ToString();
            if (exception.GetType() == typeof(OleDbException)) { // if this is an OleDbException add addition information
                foreach (OleDbError e in ((OleDbException)exception).Errors) {
                    description += (description.Length > 0 ? "; " : "") + "OleDbException NativeError: " + e.NativeError.ToString();
                }
            }
            return description;
        }
    }
}
