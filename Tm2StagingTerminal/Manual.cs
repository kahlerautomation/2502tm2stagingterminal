﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Tm2StagingTerminal {
    public partial class Manual : Form {
        public Manual(string bookmark) {
            InitializeComponent();
            Manual_Resize(null, null);
            string manualUrl = Environment.CurrentDirectory + "\\manual\\manual.htm#" + bookmark;
            webBrowser.Navigate(manualUrl);
            TopMost = true;
        }

        private void Manual_Resize(object sender, EventArgs e) {
            webBrowser.Width = ClientSize.Width;
            webBrowser.Height = ClientSize.Height - closeButton.Height;
            closeButton.Top = webBrowser.Height;
            closeButton.Width = ClientSize.Width;
        }

        private void closeButton_Click(object sender, EventArgs e) {
            Close();
        }
    }
}